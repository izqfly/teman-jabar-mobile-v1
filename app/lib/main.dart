import 'dart:io';

import 'package:app/modules/app_module.dart';
import 'package:app/screens/notification/notification.dart';
import 'package:app/screens/untrusted_version_app/untrusted_version_app.dart';
import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:repository/http/middlemans/input_task_middleman_service.dart';
import 'package:repository/response/json_response.dart';
// import 'package:trust_fall/trust_fall.dart';
// import 'package:connectivity_widget/connectivity_widget.dart';
// import 'package:repository/constants/env.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:common/helper/flutter_local_notification.dart';
import 'package:repository/sqflite/sqf_lite.dart';
import 'package:repository/sqflite/model_task.dart';
import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:public_bloc/input_task/bloc.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:repository/models/task/task.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void backgroundFetchHeadlessTask(HeadlessTask task) async {
  String taskId = task.taskId;
  bool isTimeout = task.timeout;
  if (isTimeout) {
    // This task has exceeded its allowed running-time.
    // You must stop what you're doing and immediately .finish(taskId)
    print("[BackgroundFetch] Headless task timed-out: $taskId");
    BackgroundFetch.finish(taskId);
    return;
  }
  print('[BackgroundFetch] Headless event received.');
  showNotification("Data di draft berhasil di upload");
  // int result = await _getData();
  // if (result == 0) {
  //   showNotification("Data di draft berhasil di upload");
  // }

  BackgroundFetch.finish(taskId);
}

showNotification(String desc) async {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  var android = new AndroidNotificationDetails(
    'id',
    'channel ',
    'description',
    priority: Priority.high,
    importance: Importance.max,
    autoCancel: false,
    enableVibration: true,
    playSound: true,
  );
  var iOS = new IOSNotificationDetails();
  var platform = new NotificationDetails(android: android, iOS: iOS);
  await flutterLocalNotificationsPlugin.show(
      0, 'Fetching Data', desc, platform);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();


  //_checkConnectivity();
  bool isTrustued = true;
  //!await TrustFall.isJailBroken && await TrustFall.isRealDevice;

  if (isTrustued) {
    _initializeNotification();
      runApp(ModularApp(
        module: AppModule(),
      ));
  } else {
    runApp(UntrustedVersionApp());
  }

  BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTask);
}

Future<int> _getData() async {
  // final _storage = FlutterLocalStorage();
  // print("TOKEN" + _storage.token.toString());
  print("TASK STARTED");

  SqfLite dbHelper = new SqfLite();
  Database db = await dbHelper.initDb();
  List<Map<String, dynamic>> mapList =
      await db.query('draft', orderBy: 'id DESC');
  int count = mapList.length;
  List<ModelTask> taskList = List<ModelTask>();
  for (int i = 0; i < count; i++) {
    taskList.add(ModelTask.fromMap(mapList[i]));
    print("BERHASIL DI UPLOAD " + mapList[i].toString());
  }
  int result = 1;
  taskList.map((data) async {
    result = await _saveDraft(data);
    if (result == 0) {
      print("BERHASIL Di UPLOAD");
    }
  }).toList();
  return result;
}

Future<int> _saveDraft(data) async {
  final _reposervice = InputTaskMiddlemanService();
  List<Map<String, File>> files = <Map<String, File>>[
    {
      "fotoAwal": File(data?.foto_awal ?? ""),
      "fotoSedang": File(data?.foto_sedang ?? ""),
      "fotoAkhir": File(data?.foto_akhir ?? ""),
      "video": File(data?.video ?? ""),
    }
  ];

  var task = Task((t) {
    t.tanggal = data.tanggal;
    t.idSup = data.id_sup;
    t.namaPaket = data.nama_paket;
    t.idRuasJalan = data.id_ruas;
    //t.idJenisPekerjaan = data.id_pekerjaan;
    t.lokasi = data.lokasi;
    t.lat = data.lat;
    t.long = data.long;
    t.panjang = data.panjang;
    t.pekerja = data.jumlah_pekerja;
    t.alat = data.alat;
  });

  final response = await _save(files, task);
  if (response != null && response.isSuccess()) {
    return 0;
  } else {
    return 1;
  }
}

Future<JsonSqlite> _save(dynamic file, Task task) async {
  print("MASUK SINI");
  final _flutterRestClient = FlutterRestClient();
  try {
    final response = await _flutterRestClient.multipartBackground(
      file,
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjQuODEuMTIyLjEzMVwvdGVtYW5qYWJhclwvcHVibGljXC9hcGlcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNjE1MTkwNDIzLCJleHAiOjE2MTUyNzY4MjMsIm5iZiI6MTYxNTE5MDQyMywianRpIjoiS3VmV0JjdzhBS1JVQ0dQQyIsInN1YiI6NDE0LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.PT2TWIVsm36caqdP5oDNL3R1C4Xe5f_YMh4HNxv6u74",
      "pekerjaan",
      "POST",
      body: task,
    );
    JsonSqlite jsonResponse = JsonSqlite(response);

    return jsonResponse;
  } catch (_) {
    rethrow;
  }
}

// void _checkConnectivity() {
//   ConnectivityUtils.initialize(
//     serverToPing: Env.serverToPing,
//     callback: (response) {
//       response.contains("hello world");
//       return true;
//     },
//   );
// }

_initializeNotification() async {
  var android = AndroidInitializationSettings('notification_icon');
  var settings = InitializationSettings(android: android);
  await FlutterLocalNotificationsPlugin().initialize(settings,
      onSelectNotification: (String payload) async {
    if (payload != null) {
      print("PAYLOAD = " +payload);
      FlutterLocalNotification().selectNotificationSubject.sink.add(payload);
    }
  });
}
