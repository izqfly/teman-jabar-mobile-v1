import 'package:app/modules/auth_module.dart';
import 'package:app/modules/constants/route_name.dart';
import 'package:app/screens/app.dart';
import 'package:app/screens/splash/splah_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public_bloc/input_task/bloc.dart';

class AppModule extends MainModule {
  @override
  Widget get bootstrap => AppWidget();

  @override
  List<Bind> get binds => [
    Bind<InputTaskBloc>((i) => InputTaskService()),
  ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          RouteName.Splash,
          child: (context, args) => SplashScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.Auth,
          module: AuthModule(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
