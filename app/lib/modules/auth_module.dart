import 'package:app/modules/constants/route_name.dart';
import 'package:internal/modules/internal_module.dart';
import 'package:public/modules/constants/route_name.dart' as public;
import 'package:internal/modules/constants/route_name.dart' as internal;
import 'package:mandor/modules/constants/route_name.dart' as mandor;
import 'package:app/modules/login_module.dart';
import 'package:app/modules/register_module.dart';
import 'package:app/screens/auth/auth_screen.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:auth_bloc/bloc.dart';
import 'package:public/modules/public_module.dart';
import 'package:mandor/modules/mandor_module.dart';

class AuthModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind<AuthBloc>((i) => AuthService()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          "/",
          child: (context, args) => AuthScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.Login,
          module: LoginModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.Register,
          module: RegisterModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          public.RouteName.LandingPage,
          module: PublicModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          internal.RouteName.LandingPage,
          module: InternalModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          mandor.RouteName.LandingPage,
          module: MandorModule(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;

  static Inject get to => Inject<AuthModule>.of();
}
