class RouteName {
  static const Splash = "/splash";

  // AUTH
  static const Auth = "/auth";

  // LOGIN
  static const Login = "/login";
  static const ResetPassword = "/resetPassword";

  //  REGISTRATION
  static const Register = "/register";
  static const VerificationOtp = "/verificationOtp";
  static const RegisterSucces = "/registerSucces";
}
