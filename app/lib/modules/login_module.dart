import 'package:app/modules/constants/route_name.dart';
import 'package:app/screens/landing_page/login/login_screen.dart';
import 'package:app/screens/landing_page/login/reset_password/reset_password_screen.dart';
import 'package:flutter_modular/flutter_modular.dart';

class LoginModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          "/",
          child: (context, args) => LoginScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.ResetPassword,
          child: (context, args) => ResetPasswordScreen(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
