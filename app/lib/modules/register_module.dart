import 'package:app/modules/constants/route_name.dart';
import 'package:app/screens/landing_page/register/otp/otp_screen.dart';
import 'package:app/screens/landing_page/register/register_screen.dart';
import 'package:app/screens/landing_page/register/succes_register/success_register_screen.dart';
import 'package:flutter_modular/flutter_modular.dart';

class RegisterModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          "/",
          child: (context, args) => RegisterScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.VerificationOtp,
          child: (context, args) => OTPScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.RegisterSucces,
          child: (context, args) => SuccessRegisterScreen(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
