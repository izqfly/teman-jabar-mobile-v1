import 'package:app/modules/constants/route_name.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/color_util.dart';
import 'package:common/constants/theme_color.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/services.dart';
import 'package:common/helper/fcm.dart';

class AppWidget extends StatefulWidget {
  @override
  _AppWidgetState createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _initSystemChrome();
    return MaterialApp(
      title: "Teman Jabar",
      debugShowCheckedModeBanner: false,
      theme: _theme(context),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('id', 'ID'),
        // const Locale('en', 'US'),
      ],
      navigatorKey: Modular.navigatorKey,
      onGenerateRoute: Modular.generateRoute,
      onUnknownRoute: (settings) => MaterialPageRoute(
        builder: (_) => Scaffold(
          body: Center(child: Text('No route defined for ${settings.name}')),
        ),
      ),
      initialRoute: RouteName.Splash,
    );
  }

  void _initSystemChrome() {
    // SystemChrome.setPreferredOrientations(
    //   [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
    // );
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
  }

  ThemeData _theme(BuildContext context) {
    return ThemeData(
      appBarTheme: AppBarTheme(
        brightness: Brightness.dark,
      ),
      primaryColor: ColorUtil().parseHexColor(ThemeColor.bgTemplateColor),
      cursorColor: ColorUtil().parseHexColor(ThemeColor.bgTemplateColor),
      colorScheme: Theme.of(context).colorScheme.copyWith(
            brightness: Brightness.light,
            primary: ColorUtil().parseHexColor(ThemeColor.bgTemplateColor),
          ),
      buttonTheme: ButtonThemeData(
        textTheme: ButtonTextTheme.primary,
        colorScheme: Theme.of(context).colorScheme.copyWith(
              primary: ColorUtil().parseHexColor(ThemeColor.bgTemplateColor),
            ),
      ),
      textTheme: Theme.of(context).textTheme.apply(
          fontFamily: 'Open Sans',
          bodyColor: Colors.black,
          displayColor: Colors.white),
    );
  }
}
