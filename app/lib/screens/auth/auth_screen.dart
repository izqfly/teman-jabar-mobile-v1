import 'package:app/screens/landing_page/intro_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:repository/helpers/json_web_token.dart';
import 'package:common/widgets/alerts/alert_message.dart';

class AuthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthScreenState();
  }
}

class _AuthScreenState extends State<AuthScreen> {
  final _storage = FlutterLocalStorage();
  Widget authWidget = Material(
    child: Center(
      child: CircularProgressIndicator(),
    ),
  );

  void initState() {
    super.initState();
    _init();
  }

  void _init() async {
    if (await Permission.storage.request().isGranted) {
    print("ACCESS_STORAGE_GRANTED");
  }
    if (await _storage.getUserStorage().ready) {
      if (await _validateToken() &&
          _storage.user != null &&
          _storage.user.role != null) {
        Modular.to.pushReplacementNamed(_storage.user.getNiceRole());
      } else {
        if (mounted) {
          setState(() {
            authWidget = IntroScreen();
          });
        }
      }
    }
  }

  Future<bool> _validateToken() async {
    try {
      await JsonWebToken().validateToken();
      return true;
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return authWidget;
  }
}
