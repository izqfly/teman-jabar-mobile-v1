import 'package:app/modules/constants/route_name.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/color_util.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:flutter_modular/flutter_modular.dart';

class IntroScreen extends StatelessWidget {
  static const _length = 3;
  final _pageIndexNotifier = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: FractionalOffset.bottomCenter,
        children: <Widget>[
          PageView.builder(
            onPageChanged: (index) => _pageIndexNotifier.value = index,
            itemCount: _length,
            itemBuilder: (context, index) {
              return _Background._(index: index);
            },
          ),
          _Register(),
          _PageViewIndicator._(
            pageIndexNotifier: _pageIndexNotifier,
            length: _length,
          )
        ],
      ),
    );
  }
}

class _Register extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topRight,
      child: FlatButton(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + 20),
        onPressed: () =>
            Modular.to.pushNamed("${RouteName.Auth}${RouteName.Register}"),
        child: Text(
          "Daftar",
          style: TextStyle(
            color: Colors.white,
            fontSize: _screen.fontSize(18),
            fontWeight: FontWeight.w500,
            letterSpacing: 0.5,
          ),
        ),
      ),
    );
  }
}

class _PageViewIndicator extends StatelessWidget {
  _PageViewIndicator._({this.pageIndexNotifier, this.length});
  final ValueNotifier pageIndexNotifier;
  final length;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SizedBox(width: 5),
              PageViewIndicator(
                indicatorPadding: const EdgeInsets.all(0),
                alignment: MainAxisAlignment.start,
                pageIndexNotifier: pageIndexNotifier,
                length: length,
                normalBuilder: (animationController, index) => _Indicator._(
                  color: Colors.grey[100],
                ),
                highlightedBuilder: (animationController, index) => ScaleTransition(
                  scale: CurvedAnimation(
                    parent: animationController,
                    curve: Curves.ease,
                  ),
                  child: _IndicatorS._(
                    color: Colors.grey[100],
                  ),
                ),
              ),
            ],
          ),
          RaisedButton(
            shape: StadiumBorder(),
            color: Colors.white,
            onPressed: () =>
                Modular.to.pushNamed("${RouteName.Auth}${RouteName.Login}"),
            child: Row(
              children: [
                Text(
                  "MASUK",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: _screen.fontSize(16),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.3),
                ),
                SizedBox(width: 3),
                Icon(
                  Icons.arrow_forward,
                  size: 18,
                  color: Theme.of(context).primaryColor,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _Indicator extends StatelessWidget {
  _Indicator._({this.color});
  final Color color;
  @override
  Widget build(BuildContext context) {
    final _screen = FlutterScreenUtil();
    return Container(
      width: _screen.width(5),
      height: _screen.height(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: color,
      ),
    );
  }
}

class _IndicatorS extends StatelessWidget {
  _IndicatorS._({this.color});
  final Color color;
  @override
  Widget build(BuildContext context) {
    final _screen = FlutterScreenUtil();
    return Container(
      width: _screen.width(30),
      height: _screen.height(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: color,
      ),
    );
  }
}

class _Background extends StatelessWidget {
  _Background._({this.index});
  final int index;
  @override
  Widget build(BuildContext context) {
    final _screen = FlutterScreenUtil();
    final _color = ColorUtil();
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: _screen.widthDefault(context),
          height: _screen.heightDefault(context),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                _color.parseHexColor(_color1),
                _color.parseHexColor(_color2),
              ],
            ),
          ),
        ),
        _BackgroundLogo._(index: index),
      ],
    );
  }

  String get _color1 =>
      index == 0 ? "#14379A" : index == 1 ? "#1A73E8" : "#FD9E16";

  String get _color2 =>
      index == 0 ? "#12ABEF" : index == 1 ? "#12ABEF" : "#FFBE18";
}

class _BackgroundLogo extends StatelessWidget {
  _BackgroundLogo._({this.index});
  final int index;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).padding.top,
        right: 30,
        left: 30,
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "${BasePathAsset.icons}/common/${getImage(index)}.png",
              width: _screen.width(280),
              height: _screen.height(280),
              fit: BoxFit.contain,
            ),
            SizedBox(height: 25),
            Text(
              _getTitle(index),
              style: TextStyle(
                  fontSize: _screen.fontSize(24),
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            Text(
              _getDescription(index),
              style: TextStyle(
                fontSize: _screen.fontSize(13),
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  String getImage(int index) {
    String image;

    if (index == 0) {
      image = "temanjabar-logo";
    } else if (index == 1) {
      image = "1";
    } else {
      image = "2";
    }

    return image;
  }

  String _getTitle(int index) {
    String title;

    if (index == 0) {
      title = "TEMAN JABAR";
    } else if (index == 1) {
      title = "Rute Jalan Terbaik";
    } else {
      title = "Pengaduan Jalan Rusak";
    }

    return title;
  }

  String _getDescription(int index) {
    String desc;

    if (index == 0) {
      desc = "Monitoring Infrastruktur Jalan";
    } else if (index == 1) {
      desc = "Mencarikan rute terbaik dan aman\nmenurut data dbmpr jawa barat";
    } else {
      desc =
          "Ayo bangun infrastruktur bersama.\nLaporkan kerusakan jalan yang ada disekitar anda";
    }

    return desc;
  }
}
