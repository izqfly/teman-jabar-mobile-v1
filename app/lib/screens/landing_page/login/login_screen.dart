import 'package:auth_bloc/bloc.dart';
import 'package:app/modules/constants/route_name.dart';
import 'package:app/screens/landing_page/template/template.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/costum_argon_button.dart';

part 'package:app/screens/landing_page/login/widgets/form_login.dart';

class LoginScreen extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  final _bloc = Modular.get<AuthBloc>();
  @override
  Widget build(BuildContext context) {
    return CostumScaffold(
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.only(top: 50, left: 10, right: 25),
          child: Column(
            children: [
              Text(
                "Akun",
                style: TextStyle(
                  fontSize: _screen.fontSize(28),
                  color: Colors.black,
                  letterSpacing: 0.5,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 30),
              _FormLogin(),
              SizedBox(height: 30),
              FlatButton(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onPressed: () =>
                    Modular.link.pushNamed(RouteName.ResetPassword),
                child: Text(
                  "Lupa Kata Sandi?",
                  style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: _screen.fontSize(14),
                  ),
                ),
              ),
              SizedBox(height: 20),
              RichText(
                text: TextSpan(
                    text: 'Belum punya akun?',
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: _screen.fontSize(14),
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: ' Daftar Sekarang!',
                          style: TextStyle(
                              color: Colors.blueAccent,
                              fontSize: _screen.fontSize(14)),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Modular.to.pushNamed(
                                  "${RouteName.Auth}${RouteName.Register}");
                            })
                    ]),
              ),
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: CostumArgonButton(
                  title: "MASUK",
                  stream: _bloc.isValidLogin,
                  onTap: (startLoading, stopLoading) async {
                    await _onHandleLogin(
                      context: context,
                      startLoading: startLoading,
                      stopLoading: stopLoading,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onHandleLogin({
    BuildContext context,
    Function startLoading,
    Function stopLoading,
  }) async {
    try {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      startLoading();
      final res = await _bloc.login();
      stopLoading();
      Modular.to.pushNamedAndRemoveUntil(
        res.getNiceRole(),
        (Route<dynamic> route) => false,
      );
    } on PlatformException catch (e) {
      stopLoading();
      AlertMessage.showToast(
        context: context,
        message: e.message,
        backgroundColor: Colors.red[900],
        textColor: Colors.white,
      );
    }
  }
}
