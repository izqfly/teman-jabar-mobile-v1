import 'package:auth_bloc/bloc.dart';
import 'package:app/screens/landing_page/template/template.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/costum_argon_button.dart';

part 'package:app/screens/landing_page/login/reset_password/widgets/form_reset.dart';

class ResetPasswordScreen extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  final _bloc = Modular.get<AuthBloc>();
  @override
  Widget build(BuildContext context) {
    return CostumScaffold(
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.only(top: 50, left: 10, right: 25),
          child: Column(
            children: [
              Text(
                "Masukkan Email Kamu",
                style: TextStyle(
                  fontSize: _screen.fontSize(28),
                  color: Colors.black,
                  letterSpacing: 0.5,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 30),
              _FormReset(),
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: CostumArgonButton(
                  title: "RESET PASSWORD",
                  stream: _bloc.isValidResetPass,
                  onTap: (startLoading, stopLoading) {
                    _onHandSubmit(
                      context: context,
                      startLoading: startLoading,
                      stopLoading: stopLoading,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onHandSubmit({
    BuildContext context,
    Function startLoading,
    Function stopLoading,
  }) async {
    try {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      startLoading();
      await Modular.get<AuthBloc>().resetPassword();
      stopLoading();
      AlertMessage.showFlushbar(
        context: context,
        title: Text(
          "Informasi",
          style: TextStyle(
              color: Colors.white,
              fontSize: _screen.fontSize(16),
              letterSpacing: 0.5),
        ),
        message: Text(
          "Tautan pengaturan ulang kata sandi berhasil dikirim ke email Anda.",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.black.withOpacity(0.8),
      );
    } on PlatformException catch (e) {
      stopLoading();
      AlertMessage.showToast(
        context: context,
        message: e.message,
        backgroundColor: Colors.red[900],
        textColor: Colors.white,
      );
    }
  }
}
