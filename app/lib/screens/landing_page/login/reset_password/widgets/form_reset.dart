part of 'package:app/screens/landing_page/login/reset_password/reset_password_screen.dart';

class _FormReset extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FormResetState();
  }
}

class _FormResetState extends State<_FormReset> {
  final _bloc = Modular.get<AuthBloc>();

  void initState() {
    super.initState();
    _bloc.initResetPassword();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CostumTextFormField(
          hintText: "Alamat Email",
          icon: Icons.email,
          onChanged: _bloc.onChangedEmail,
          stream: _bloc.email,
          textInputType: TextInputType.emailAddress,
        ),
        Divider(
          thickness: 0.5,
          color: Colors.black45,
          indent: 49,
          height: 0,
        ),
      ],
    );
  }
}
