part of 'package:app/screens/landing_page/login/login_screen.dart';

class _FormLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FormLoginrState();
  }
}

class _FormLoginrState extends State<_FormLogin> {
  final _valuNotifier = ValueNotifier<bool>(true);
  final _bloc = Modular.get<AuthBloc>();

  void initState() {
    super.initState();
    _bloc.initLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CostumTextFormField(
          hintText: "Email/NIP",
          icon: Icons.email,
          onChanged: _bloc.onChangedEmail,
          textInputType: TextInputType.emailAddress,
          stream: _bloc.email,
        ),
        Divider(thickness: 0.5, color: Colors.black45, indent: 49, height: 0),
        SizedBox(height: 10),
        ValueListenableBuilder(
          valueListenable: _valuNotifier,
          builder: (context, value, child) => CostumTextFormField(
            hintText: "Kata Sandi",
            icon: Icons.lock,
            onChanged: _bloc.onChangedPassword,
            stream: _bloc.password,
            maxLines: 1,
            visibility: value,
            suffixIcon: GestureDetector(
              onTap: () =>
                  _valuNotifier.value = _valuNotifier.value ? false : true,
              child: Icon(
                _valuNotifier.value ? Icons.visibility : Icons.visibility_off,
              ),
            ),
          ),
        ),
        Divider(thickness: 0.5, color: Colors.black45, indent: 49, height: 0),
      ],
    );
  }
}
