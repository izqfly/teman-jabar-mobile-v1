import 'package:auth_bloc/bloc.dart';
import 'package:app/modules/constants/route_name.dart';
import 'package:common/widgets/costum_argon_button.dart';
import 'package:app/screens/landing_page/template/costum_scaffold.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:common/widgets/loading_dialog.dart';

part 'package:app/screens/landing_page/register/otp/widgets/costum_pin_code.dart';

class OTPScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _OTPScreenState();
  }
}

class _OTPScreenState extends State<OTPScreen> {
  final _screen = FlutterScreenUtil();
  final _bloc = Modular.get<AuthBloc>();

  void initState() {
    super.initState();
    _bloc.onChangedOTP(null);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: CostumScaffold(
        automaticallyImplyLeading: false,
        body: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              children: [
                Text(
                  "Masukan Kode Otentifikasi",
                  style: TextStyle(
                    fontSize: _screen.fontSize(28),
                    color: Colors.black54,
                    letterSpacing: 0.5,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 50),
                _CostumePinCodeText(),
                SizedBox(height: 20),
                StreamBuilder(
                  stream: _bloc.email,
                  builder: (_, snapshot) {
                    return RichText(
                      text: TextSpan(
                          text:
                              'Untuk alasan keamanan. Silahkan memasukkan kode OTP yang sudah dikirmkan ke email ',
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: _screen.fontSize(14)),
                          children: <TextSpan>[
                            TextSpan(
                              text: '${snapshot.data ?? ""}',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: _screen.fontSize(14)),
                            )
                          ]),
                    );
                  },
                ),
                SizedBox(height: 50),
                RichText(
                  text: TextSpan(
                      text: 'Tidak mendapatkan kode OTP?',
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: _screen.fontSize(14)),
                      children: <TextSpan>[
                        TextSpan(
                            text: ' Kirim Ulang',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: _screen.fontSize(14)),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => _resendOTP(context))
                      ]),
                ),
                SizedBox(height: 30),
                CostumArgonButton(
                  title: "KONRIMASI",
                  stream: _bloc.isVerifOTP,
                  onTap: (startLoading, stopLoading) {
                    _onHandleVerifOTP(
                      context: context,
                      startLoading: startLoading,
                      stopLoading: stopLoading,
                    );
                  },
                )
              ],
            ),
          ),
        ),
      ),
      onWillPop: () => _showInfo(context),
    );
  }

  Future<bool> _showInfo(BuildContext context) {
    AlertMessage.showToast(
      context: context,
      message: "Silahkan melakukan konfirmasi kode OTP terlebih dahulu.",
      backgroundColor: Colors.black,
      textColor: Colors.white,
    );
    return null;
  }

  void _resendOTP(BuildContext context) async {
    try {
      showDialog(
        context: context,
        child: LoadingDialog(),
      );
      await _bloc.sendOTPCode();
      Navigator.pop(context);
      AlertMessage.showFlushbar(
        context: context,
        title: Text(
          "Informasi",
          style: TextStyle(
              color: Colors.white,
              fontSize: _screen.fontSize(16),
              letterSpacing: 0.5),
        ),
        message: Text(
          "Kode OTP Berhasil dikirimkan ulang.",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.black.withOpacity(0.8),
      );
    } on PlatformException catch (e) {
      Navigator.pop(context);
      AlertMessage.showToast(
        context: context,
        message: e.message,
        backgroundColor: Colors.red[900],
        textColor: Colors.white,
      );
    }
  }

  Future<void> _onHandleVerifOTP({
    BuildContext context,
    Function startLoading,
    Function stopLoading,
  }) async {
    try {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      startLoading();
      await _bloc.verifOTP();
      stopLoading();
      Modular.link.pushNamed(RouteName.RegisterSucces);
    } on PlatformException catch (e) {
      stopLoading();
      AlertMessage.showToast(
        context: context,
        message: e.message,
        backgroundColor: Colors.red[900],
        textColor: Colors.white,
      );
    }
  }
}
