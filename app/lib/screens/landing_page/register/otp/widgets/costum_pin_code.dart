part of 'package:app/screens/landing_page/register/otp/otp_screen.dart';

class _CostumePinCodeText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      length: 6,
      appContext: context,
      autoFocus: true,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.underline,
        fieldHeight: 50,
        inactiveColor: Colors.red,
        activeColor: Theme.of(context).primaryColor,
        selectedColor: Colors.red,
        fieldWidth: 40,
        activeFillColor: Colors.white,
      ),
      animationDuration: Duration(milliseconds: 300),
      backgroundColor: Colors.transparent,
      // onCompleted: (v) {
      //   print("Completed");
      // },

      onChanged: Modular.get<AuthBloc>().onChangedOTP,
      beforeTextPaste: (text) {
        // print("Allowing to paste $text");
        return true;
      },
    );
  }
}
