import 'package:auth_bloc/bloc.dart';
import 'package:app/modules/constants/route_name.dart';
import 'package:app/screens/landing_page/template/template.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/widgets/costum_argon_button.dart';

part 'package:app/screens/landing_page/register/widgets/form_register.dart';

class RegisterScreen extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  final _bloc = Modular.get<AuthBloc>();
  @override
  Widget build(BuildContext context) {
    return CostumScaffold(
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.only(top: 50, left: 10, right: 25),
          child: Column(
            children: [
              Text(
                "Selamat Datang,",
                style: TextStyle(
                  fontSize: _screen.fontSize(24),
                  color: Colors.black,
                  letterSpacing: 0.5,
                ),
                textAlign: TextAlign.center,
              ),
              Text(
                "Daftar Sekarang!",
                style: TextStyle(
                  fontSize: _screen.fontSize(28),
                  color: Colors.black54,
                  letterSpacing: 0.5,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 50),
              _FormRegister(),
              SizedBox(height: 30),
              RichText(
                text: TextSpan(
                    text: 'Sudah punya akun?',
                    style: TextStyle(
                        color: Colors.black54, fontSize: _screen.fontSize(14)),
                    children: <TextSpan>[
                      TextSpan(
                          text: ' Masuk sini',
                          style: TextStyle(
                              color: Colors.blueAccent,
                              fontSize: _screen.fontSize(14)),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Modular.to.pushNamed(
                                "${RouteName.Auth}${RouteName.Login}",
                              );
                            })
                    ]),
              ),
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: CostumArgonButton(
                  title: "LANJUT",
                  stream: _bloc.isValidRegister,
                  onTap: (startLoading, stopLoading) {
                    _onHandleRegister(
                      context: context,
                      startLoading: startLoading,
                      stopLoading: stopLoading,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onHandleRegister({
    BuildContext context,
    Function startLoading,
    Function stopLoading,
  }) async {
    try {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      startLoading();
      await Modular.get<AuthBloc>().register();
      stopLoading();
      Modular.link.pushNamed(RouteName.VerificationOtp);
    } on PlatformException catch (e) {
      stopLoading();
      AlertMessage.showToast(
        context: context,
        message: e.message,
        backgroundColor: Colors.red[900],
        textColor: Colors.white,
      );
    }
  }
}
