import 'package:app/modules/constants/route_name.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/utils/color_util.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SuccessRegisterScreen extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  final _color = ColorUtil();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        child: Container(
          alignment: Alignment.center,
          width: _screen.widthDefault(context),
          height: _screen.heightDefault(context),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                _color.parseHexColor("#5ED0FB"),
                _color.parseHexColor("#14379A"),
              ],
            ),
          ),
          child: Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top,
              right: 20,
              left: 20,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "${BasePathAsset.icons}/common/congratulations.png",
                  width: _screen.width(280),
                  height: _screen.height(280),
                  fit: BoxFit.fill,
                ),
                Text(
                  "Pendaftaran Berhasil!",
                  style: TextStyle(
                      fontSize: _screen.fontSize(30),
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                ButtonTheme(
                  minWidth: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    color: Colors.white,
                    shape: StadiumBorder(),
                    onPressed: () {
                      Modular.to.pushNamedAndRemoveUntil(
                        RouteName.Auth,
                        (Route<dynamic> route) => false,
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(child: Container()),
                        Text(
                          "Kembali ke halaman awal",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: _screen.fontSize(18),
                              fontWeight: FontWeight.w500,
                              letterSpacing: 0.3),
                        ),
                        Expanded(child: Container()),
                        Icon(
                          Icons.arrow_forward,
                          size: 18,
                          color: Theme.of(context).primaryColor,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        onWillPop: () async => Future.value(false),
      ),
    );
  }
}
