part of 'package:app/screens/landing_page/register/register_screen.dart';

class _FormRegister extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FormRegisterState();
  }
}

class _FormRegisterState extends State<_FormRegister> {
  final _passNotifier = ValueNotifier<bool>(true);
  // final _confirmPassNotifier = ValueNotifier<bool>(true);
  final _bloc = Modular.get<AuthBloc>();

  void initState() {
    super.initState();
    _bloc.initRegister();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CostumTextFormField(
          hintText: "Nama",
          icon: Icons.account_circle,
          onChanged: _bloc.onChangedFullname,
          stream: _bloc.fullname,
        ),
        Divider(thickness: 0.5, color: Colors.black45, indent: 49, height: 0),
        SizedBox(height: 10),
        CostumTextFormField(
          hintText: "Email",
          icon: Icons.email,
          textInputType: TextInputType.emailAddress,
          onChanged: _bloc.onChangedEmail,
          stream: _bloc.email,
        ),
        Divider(thickness: 0.5, color: Colors.black45, indent: 49, height: 0),
        SizedBox(height: 10),
        // CostumTextFormField(
        //   hintText: "No. Handphone",
        //   icon: Icons.phone_android,
        //   onChanged: _bloc.onChangedPhoneNumber,
        //   textInputType: TextInputType.number,
        //   isNumber: true,
        //   stream: _bloc.phoneNumber,
        // ),
        // Divider(thickness: 0.5, color: Colors.black45, indent: 49, height: 0),
        // SizedBox(height: 10),
        ValueListenableBuilder(
          valueListenable: _passNotifier,
          builder: (context, value, child) => CostumTextFormField(
            hintText: "Kata Sandi",
            icon: Icons.lock,
            onChanged: _bloc.onChangedPassword,
            stream: _bloc.password,
            maxLines: 1,
            visibility: value,
            suffixIcon: GestureDetector(
              onTap: () =>
                  _passNotifier.value = _passNotifier.value ? false : true,
              child: Icon(
                _passNotifier.value ? Icons.visibility : Icons.visibility_off,
              ),
            ),
          ),
        ),
        Divider(thickness: 0.5, color: Colors.black45, indent: 49, height: 0),
        // SizedBox(height: 10),
        // ValueListenableBuilder(
        //   valueListenable: _confirmPassNotifier,
        //   builder: (context, value, child) => CostumTextFormField(
        //     hintText: "Konfirmasi Kata Sandi",
        //     icon: Icons.lock,
        //     onChanged: _bloc.onChangedConfirmPassword,
        //     stream: _bloc.confirmPassword,
        //     maxLines: 1,
        //     visibility: value,
        //     suffixIcon: GestureDetector(
        //       onTap: () => _confirmPassNotifier.value =
        //           _confirmPassNotifier.value ? false : true,
        //       child: Icon(
        //         _confirmPassNotifier.value
        //             ? Icons.visibility
        //             : Icons.visibility_off,
        //       ),
        //     ),
        //   ),
        // ),
        Divider(thickness: 0.5, color: Colors.black45, indent: 49, height: 0),
      ],
    );
  }
}
