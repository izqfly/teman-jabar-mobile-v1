import 'package:flutter/material.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:common/utils/flutter_screen_util.dart';

class CostumScaffold extends StatelessWidget {
  CostumScaffold({@required this.body, this.automaticallyImplyLeading = true});
  final Widget body;
  final bool automaticallyImplyLeading;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        automaticallyImplyLeading: automaticallyImplyLeading,
        elevation: 1,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              "${BasePathAsset.icons}/common/temanjabar-logo.png",
              height: AppBar().preferredSize.height / 1.5,
            ),
            SizedBox(width: 10),
            Text(
              "Teman Jabar",
              style: TextStyle(
                fontSize: FlutterScreenUtil().fontSize(18),
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            )
          ],
        ),
      ),
      body: body,
    );
  }
}
