import 'package:flutter/material.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/services.dart';

class CostumTextFormField extends StatelessWidget {
  CostumTextFormField({
    @required this.hintText,
    @required this.icon,
    @required this.onChanged,
    @required this.stream,
    this.isNumber = false,
    this.suffixIcon,
    this.visibility = false,
    this.maxLines,
    this.textInputType,
  });
  final Function onChanged;

  final String hintText;
  final IconData icon;
  final Widget suffixIcon;
  final bool visibility;
  final int maxLines;
  final Stream stream;
  final bool isNumber;
  final TextInputType textInputType;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: stream,
      builder: (_, snapshot) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              autocorrect: false,
              obscureText: visibility,
              onChanged: onChanged,
              enableSuggestions: false,
              keyboardType: textInputType ?? TextInputType.text,
              textInputAction: TextInputAction.done,
              maxLines: maxLines ?? null,
              inputFormatters: isNumber
                  ? <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ]
                  : <TextInputFormatter>[],
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hintText ?? "",
                hintStyle: TextStyle(
                  fontSize: FlutterScreenUtil().fontSize(14),
                  color: Colors.black45,
                ),
                prefixIcon: Icon(icon),
                suffixIcon: suffixIcon,
              ),
            ),
            snapshot.hasError
                ? Container(
                    padding: const EdgeInsets.only(left: 49, bottom: 3),
                    child: Text(
                      snapshot.error,
                      style: TextStyle(
                        fontSize: FlutterScreenUtil().fontSize(13),
                        color: Colors.red,
                      ),
                    ),
                  )
                : Container(),
            Divider(
              thickness: 0.5,
              color: Colors.black45,
              indent: 49,
              height: 0,
            )
          ],
        );
      },
    );
  }
}
