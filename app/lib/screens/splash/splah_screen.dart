import 'dart:async';

import 'package:after_init/after_init.dart';
import 'package:app/modules/constants/route_name.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:common/utils/color_util.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen>
    with AfterInitMixin<SplashScreen> {
  final _screen = FlutterScreenUtil();
  final _color = ColorUtil();

  @override
  void didInitState() {
    _screen.initScreen(context);
    _navigationPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: _screen.widthDefault(context),
        height: _screen.heightDefault(context),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            stops: [
              10,
              10,
            ],
            colors: [
              _color.parseHexColor("#3355b4"),
              _color.parseHexColor("#14379a"),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "${BasePathAsset.icons}/common/temanjabar-logo.png",
              width: _screen.width(80),
              height: _screen.height(80),
              fit: BoxFit.fill,
            ),
            SizedBox(height: 25),
            Text(
              "Teman Jabar",
              style: TextStyle(
                fontSize: _screen.fontSize(35),
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              "DBMPR Jawa Barat",
              style: TextStyle(
                fontSize: _screen.fontSize(22),
                color: Colors.white,
                letterSpacing: 5,
              ),
            ),
            SizedBox(height: 20),
            SpinKitWave(
              color: _color.parseHexColor("#ffffff").withOpacity(0.7),
              size: 30,
            )
          ],
        ),
      ),
    );
  }

  _navigationPage() {
    Timer(
      Duration(seconds: 3),
      () {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        Modular.to.pushReplacementNamed(RouteName.Auth);
      },
    );
  }
}
