import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class UntrustedVersionApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: _ScreenBuilder(),
    );
  }
}

class _ScreenBuilder extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ScreenState();
  }
}

class _ScreenState extends State<_ScreenBuilder>
    with AfterLayoutMixin<_ScreenBuilder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      child: AlertDialog(
        title: Text("Alert"),
        content: Text("Perangkat anda tidak kompatibel dengan versi ini."),
        actions: [
          FlatButton(
            child: Text("OK", style: TextStyle(color: Colors.black)),
            onPressed: () => SystemNavigator.pop(),
          ),
        ],
      ),
    );
  }
}
