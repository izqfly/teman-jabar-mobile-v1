import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/models/user/user.dart';

abstract class AuthBloc extends Disposable {
  Function(String) get onChangedFullname;
  Function(String) get onChangedPhoneNumber;
  Function(String) get onChangedPassword;
  Function(String) get onChangedConfirmPassword;
  Function(String) get onChangedEmail;
  Function(String) get onChangedOTP;
  Function(String) get onChangedPasswordNew;

  Stream<bool> get isValidLogin;
  Stream<bool> get isValidChangePassword;
  Stream<bool> get isValidRegister;
  Stream<bool> get isValidResetPass;
  Stream<bool> get isVerifOTP;

  Stream<String> get password;
  Stream<String> get email;
  Stream<String> get confirmPassword;
  Stream<String> get phoneNumber;
  Stream<String> get fullname;

  Future<User> login();
  Future<void> logout();

  Future<int> register();
  Future<void> sendOTPCode();
  Future<void> verifOTP();
  Future<void> changePassword();

  Future<String> resetPassword();

  void initRegister();
  void initLogin();
  void initResetPassword();
}
