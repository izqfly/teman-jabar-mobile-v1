import 'dart:async';
import 'package:auth_bloc/validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:repository/http/middlemans/auth_middleman_service.dart';
import 'package:repository/models/user/user.dart';
import 'package:repository/constants/regex_pattern.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'auth_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AuthService with Validator implements AuthBloc {
  final _fullname = BehaviorSubject<String>();
  final _phoneNumber = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();
  final _passwordNew = BehaviorSubject<String>();
  final _confirmPassword = BehaviorSubject<String>();
  final _email = BehaviorSubject<String>();
  final _otp = BehaviorSubject<String>();

  final _service = AuthMiddlemanService();
  final _storage = FlutterLocalStorage();

  @override
  void dispose() {
    _fullname.close();
    _phoneNumber.close();
    _password.close();
    _email.close();
    _otp.close();
    _confirmPassword.close();
    _passwordNew.close();
  }

  @override
  Stream<bool> get isValidLogin =>
      CombineLatestStream.combine2(_email, _password, (e, p) {
        String email = e;
        String pass = p;
        if (email.isEmpty || pass.isEmpty) {
          return false;
        } else if (pass.length < 5) {
          return false;
        }
        // else if (!RegExp(RegexPattern.email).hasMatch(email)) {
        //   return false;
        // }
        else {
          return true;
        }
      });

  @override
  Stream<bool> get isValidRegister => CombineLatestStream.combine3(
        _fullname,
        _password,
        _email,
        // _confirmPassword,
        // _phoneNumber,
        (u, p, e /*, c, pn*/) {
          String name = u;
          String pass = p;
          String email = e;
          // String confirmPass = c;
          // String phoneNumber = pn;

          if (name.isEmpty || pass.isEmpty || email.isEmpty
              // phoneNumber.isEmpty ||
              // confirmPass.isEmpty
              ) {
            return false;
          } else if (pass.length < 5) {
            return false;
          } else if (!RegExp(RegexPattern.email).hasMatch(email)) {
            return false;
            // } else if (confirmPass != pass) {
            //   return true;
            // } else if (phoneNumber.length < 10) {
            //   return true;
          } else {
            return true;
          }
        },
      );

  @override
  Stream<String> get password =>
      _password.stream.transform(emptyAndMin6Charcater);

  @override
  Stream<String> get email => _email.stream.transform(validEmail);

  @override
  Stream<String> get phoneNumber =>
      _phoneNumber.stream.transform(validPhoneNumber);

  @override
  Stream<String> get fullname => _fullname.stream.transform(notEmpty);

  @override
  Stream<bool> get isVerifOTP =>
      CombineLatestStream.combine2(_email, _otp, (e, o) {
        if (e != "" && o != "" && (o as String).length == 6) {
          return true;
        }
        return false;
      });

  @override
  Stream<bool> get isValidResetPass =>
      CombineLatestStream.combine2(_email, _email, (e, e1) {
        if (e != "" &&
            e1 != "" &&
            RegExp(RegexPattern.email).hasMatch(e1 ?? "")) {
          return true;
        }
        return false;
      });

  @override
  Stream<String> get confirmPassword => _confirmPassword.stream.transform(
          StreamTransformer<String, String>.fromHandlers(
              handleData: (String value, EventSink<String> sink) {
        if (value != null) {
          if (value != _password.value) {
            sink.addError('Kata sandi tidak sama');
          } else {
            sink.add(value);
          }
        }
      }));

  @override
  Stream<bool> get isValidChangePassword =>
      CombineLatestStream.combine3(_password, _passwordNew, _confirmPassword,
          (a, b, c) {
        if (a != "" && b != "" && c != "") {
          return true;
        }
        return false;
      });

  @override
  Function(String p1) get onChangedPassword => _password.sink.add;

  @override
  Function(String p1) get onChangedEmail => _email.sink.add;

  @override
  Function(String p1) get onChangedOTP => _otp.sink.add;

  @override
  Function(String p1) get onChangedFullname => _fullname.sink.add;

  @override
  Function(String p1) get onChangedPhoneNumber => _phoneNumber.sink.add;

  @override
  Function(String p1) get onChangedConfirmPassword => _confirmPassword.sink.add;

  @override
  Future<User> login() async {
    var user = User((u) {
      u.email = _email.value;
      u.password = _password.value;
    });

    final res = await _service.login(user);

    await _storage.saveAppStorage(res);

    final fcmToken = await FirebaseMessaging().getToken();

    if ((_storage?.user?.role == "internal" && fcmToken != null) ||
        (_storage?.user?.role == "masyarakat" && fcmToken != null)) {
      var fcm = User((u) async {
        u.userId = res["user"]["id"];
        u.fcmToken = fcmToken;
      });
      
      await _service.storeTokenFcm(fcm);

      //print(_storage?.user?.role);
    }

    return _storage.user;
  }

  @override
  Future<void> logout() async {
    await _storage.clearAppStorage();
  }

  @override
  Future<int> register() async {
    var user = User((u) {
      u.fullname = _fullname.value;
      u.password = _password.value;
      u.email = _email.value;
      // u.phoneNumber = _phoneNumber.value;
    });

    final id = await _service.register(user);

    return id;
  }

  @override
  Future<void> verifOTP() async {
    var user = User((u) {
      u.otp = _otp.value;
      u.email = _email.value;
    });
    await _service.verifOTP(user);
  }

  @override
  Future<String> resetPassword() async {
    var user = User((u) {
      u.email = _email.value;
    });

    await _service.resetPassword(user);

    return _email.value;
  }

  @override
  Future<void> sendOTPCode() async {
    var user = User((u) {
      u.email = _email.value;
    });
    await _service.sendOTP(user);
  }

  @override
  void initLogin() {
    _email.add(null);
    _password.add(null);
  }

  @override
  void initRegister() {
    _fullname.add(null);
    _email.add(null);
    _phoneNumber.add(null);
    _password.add(null);
  }

  @override
  void initResetPassword() {
    _email.add(null);
  }

  @override
  Future<void> changePassword() async {
    if (_passwordNew.value == _confirmPassword.value) {
      var u = User((u) {
        u.passwordOld = _password.value;
        u.passwordNew = _passwordNew.value;
      });
      await _service.changePassword(u);
    } else {
      throw PlatformException(
        code: "wrong_password",
        message: "Konfirmasi kata sandi tidak sesuai",
      );
    }
  }

  @override
  Function(String p1) get onChangedPasswordNew => _passwordNew.sink.add;
}
