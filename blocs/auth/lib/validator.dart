import 'dart:async';
import 'package:repository/constants/regex_pattern.dart';

class Validator {
  final notEmpty = StreamTransformer<String, String>.fromHandlers(
      handleData: (String value, EventSink<String> sink) {
    if (value != null) {
      if (value.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else {
        sink.add(value);
      }
    }
  });

  final validEmail = StreamTransformer<String, String>.fromHandlers(
      handleData: (String value, EventSink<String> sink) {
    if (value != null) {
      if (value.isEmpty) {
        sink.addError('Tidak boleh kosong');
      }
      // else if (!RegExp(RegexPattern.email).hasMatch(value)) {
      //   sink.addError('Email tidak valid');
      // }
      else {
        sink.add(value);
      }
    }
  });

  final validPhoneNumber = StreamTransformer<String, String>.fromHandlers(
      handleData: (String value, EventSink<String> sink) {
    if (value != null) {
      if (value.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else if (value.length < 10) {
        sink.addError('Minimal 10 katakter');
      } else {
        sink.add(value);
      }
    }
  });

  final emptyAndMin6Charcater = StreamTransformer<String, String>.fromHandlers(
      handleData: (String value, EventSink<String> sink) {
    if (value != null) {
      if (value.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else if (value.length < 6) {
        sink.addError('Minimal 6 karakter');
      } else {
        sink.add(value);
      }
    }
  });
}
