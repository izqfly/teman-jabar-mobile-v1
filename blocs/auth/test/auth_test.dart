import 'package:auth_bloc/auth_bloc.dart';
import 'package:auth_bloc/auth_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:repository/models/user/user.dart';

class AuthMock extends Mock implements AuthBloc {}

void main() async {
  AuthMock authMock;

  setUp(() {
    authMock = AuthMock();
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test("login", () async {
    var user = User((u) {
      u.email = "admin";
      u.password = 'admin';
      u.role = "masyarakat";
    });

    when(authMock.login()).thenAnswer((_) => Future.value(user));
    final res = await authMock.login();
    expectLater(res.role, "masyarakat");
    verify(authMock.login()).called(1);
  });

  test("register", () async {
    final id = 1;
    when(authMock.register()).thenAnswer((_) => Future.value(id));
    final res = await authMock.register();
    expectLater(res, id);
    verify(authMock.register()).called(1);
  });

  test("reset password", () async {
    String email = "admin@gmail.com";
    when(authMock.resetPassword()).thenAnswer((_) => Future.value(email));
    final res = await authMock.resetPassword();
    expectLater(res, email);
    verify(authMock.resetPassword()).called(1);
  });

  test("send OTP", () {
    final authBloc = AuthService();

    authBloc.onChangedPhoneNumber("08112633671");
    authBloc.sendOTPCode();
  });
}
