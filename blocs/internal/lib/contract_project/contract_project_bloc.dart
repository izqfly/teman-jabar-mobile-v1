import 'package:repository/models/contract_project/contract_project.dart';

abstract class ContractProjectBloc {
  Future<ContractProject> getSummary();
  Future<List<ContractProject>> fetchList(int skip, String query);
  Future<List<ContractProject>> geDetail();
}
