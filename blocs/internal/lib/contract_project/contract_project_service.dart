import 'package:internal_bloc/contract_project/contract_project_bloc.dart';
import 'package:repository/http/middlemans/contract_project_middleman_service.dart';
import 'package:repository/models/contract_project/contract_project.dart';

class ContractProjectService implements ContractProjectBloc {
  final _service = ContractProjectMiddlemanService();

  @override
  Future<ContractProject> getSummary() async {
    return await _service.getSummary();
  }

  @override
  Future<List<ContractProject>> fetchList(int skip, String query) async {
    return await _service.fetchList(skip, query);
  }

  @override
  Future<List<ContractProject>> geDetail() async {
    return await _service.geDetail();
  }
}
