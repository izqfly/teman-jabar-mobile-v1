import 'package:repository/models/damage_road/damage_road.dart';

abstract class DamageReportBloc {
  Future<List<DamageRoad>> fetchList();
  Future<DamageRoad> findByID(int id);
}
