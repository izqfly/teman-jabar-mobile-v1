import 'package:internal_bloc/damage_report/bloc.dart';
import 'package:repository/http/middlemans/damage_report_middleman_service.dart';
import 'package:repository/models/damage_road/damage_road.dart';

class DamageReportService extends DamageReportBloc {
  final _service = DamageReportMiddlemanService();
  @override
  Future<List<DamageRoad>> fetchList() async {
    return await _service.fetchList();
  }

  @override
  Future<DamageRoad> findByID(int id) async {
    return await _service.findByID(id);
  }
}
