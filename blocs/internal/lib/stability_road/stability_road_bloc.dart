import 'package:repository/models/stability_road/stability_road.dart';

abstract class StabilityRoadBloc {
  Future<List<StabilityRoad>> fetchList();
  Future<StabilityRoad> findByID(int id);
  Future<StabilityRoad> getRecapitulation();
}
