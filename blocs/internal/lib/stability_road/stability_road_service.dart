import 'package:internal_bloc/stability_road/stability_road_bloc.dart';
import 'package:repository/http/middlemans/stability_road_middleman_service.dart';
import 'package:repository/models/stability_road/stability_road.dart';

class StabilityRoadService implements StabilityRoadBloc {
  final _service = StabilityRoadMiddlemanService();
  @override
  Future<List<StabilityRoad>> fetchList() async {
    return await _service.fetchList();
  }

  @override
  Future<StabilityRoad> findByID(int id) async {
    return await _service.findByID(id);
  }

  @override
  Future<StabilityRoad> getRecapitulation() async {
    return await _service.getRecapitulation();
  }
}
