import 'dart:io';

import 'package:flutter_modular/flutter_modular.dart';
// import 'package:repository/models/report/report.dart';
// import 'package:repository/response/dropdown_response.dart';

abstract class InputTaskBloc extends Disposable {
 
  Function(double) get onChangedLatitude;
  Function(double) get onChangedLongitude;

  void init();

}
