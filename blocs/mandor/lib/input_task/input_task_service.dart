import 'package:repository/models/notification/notification.dart' as model;
import 'package:repository/http/middlemans/reporting_street_middleman_service.dart';
import 'package:repository/models/report/report.dart';
import 'package:repository/response/dropdown_response.dart';
import 'package:rxdart/rxdart.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'bloc.dart';
import 'package:repository/common/flutter_local_storage.dart';

class InputTaskService
    implements InputTaskBloc {
  final _latitude = BehaviorSubject<double>();
  final _longitude = BehaviorSubject<double>();

  @override
  void dispose() {
    _latitude.close();
    _longitude.close();
  }


  @override
  void init() {
    _latitude.add(null);
    _longitude.add(null);
  }


  @override
  Function(double p1) get onChangedLatitude => _latitude.sink.add;

  @override
  Function(double p1) get onChangedLongitude => _longitude.sink.add;
  
}
