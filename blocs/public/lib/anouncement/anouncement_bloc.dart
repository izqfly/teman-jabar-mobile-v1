import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/models/anouncement/anouncement.dart';

abstract class AnouncementBloc extends Disposable {

  Future<List<Anouncement>> getList();
  
}
