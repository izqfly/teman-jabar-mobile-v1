import 'package:public_bloc/anouncement/anouncement_bloc.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:repository/models/anouncement/anouncement.dart';
import 'package:repository/http/middlemans/anouncement_middleman_sevice.dart';

class AnouncementService implements AnouncementBloc {
  final _reposervice = AnouncementMiddlemanService();
  final _storage = FlutterLocalStorage();

  @override
  void dispose() {
    // TODO: implement dispose
  }

  @override
  Future<List<Anouncement>> getList() async {
    String role = "";

    if (_storage.user.role == "internal" || _storage.user.role == "mandor") {
      role = "internal";
    } else {
      role = "masyarakat";
    }

    return await _reposervice.getList(role);
  }
}
