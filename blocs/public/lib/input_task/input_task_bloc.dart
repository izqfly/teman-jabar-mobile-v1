import 'dart:io';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/models/task/task.dart';
import 'package:repository/response/mandor_response.dart';
// import 'package:repository/models/report/report.dart';
// import 'package:repository/response/dropdown_response.dart';

abstract class InputTaskBloc extends Disposable {
  Function(MandorResponse) get onChangedRuasJalan;
  Function(DropdownResponse) get onChangedSup;
  Function(JenisPekerjaan) get onChangedPekerjaanType;
  Function(PaketPekerjaan) get onChangedPaketPekerjaan;

  Function(double) get onChangedLatitude;
  Function(double) get onChangedLongitude;
  Function(String) get onChangeLocation;
  Function(String) get onChangedDate;
  Function(File) get onChangedFotoPegawai;
  Function(File) get onChangedFotoAwal;
  Function(File) get onChangedFotoSedang;
  Function(File) get onChangedFotoAkhir;
  Function(File) get onChangedVideo;
  Function (String) get onChangedPanjang;
  Function (String) get onChangedPekerja;
  Function (String) get onChangedAlat;

  Stream<bool> get isValidSubmit;
  Stream<bool> get isValidEdit;

  void init();

  Future<List<MandorResponse>> getRuasJalan();
  Future<List<DropdownResponse>> getSUP();
  Future<List<JenisPekerjaan>> getPekerjaanType();
  Future<List<PaketPekerjaan>> getpaketPekerjaan();

  Future<List<Task>> getTaskList();

  Future<int> save();
  Future<int> edit(String id_pek, int idSup);
  Future<int> saveDraft(data);
  Future<int> delete(String id);
}
