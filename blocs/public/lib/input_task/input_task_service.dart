import 'package:repository/http/middlemans/input_task_middleman_service.dart';
import 'package:repository/models/task/task.dart';
import 'package:repository/response/mandor_response.dart';
import 'package:repository/sqflite/crud_sqflite.dart';
import 'package:repository/sqflite/model_task.dart';
import 'package:rxdart/rxdart.dart';
import 'package:intl/intl.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'dart:io';
import 'bloc.dart';

class InputTaskService implements InputTaskBloc {
  final _storage = FlutterLocalStorage();
  final _latitude = BehaviorSubject<double>();
  final _longitude = BehaviorSubject<double>();
  final _reposervice = InputTaskMiddlemanService();
  final _ruasjalan = BehaviorSubject<MandorResponse>();
  final _sup = BehaviorSubject<DropdownResponse>();
  final _jenispekerjaan = BehaviorSubject<JenisPekerjaan>();
  final _paketpekerjaan = BehaviorSubject<PaketPekerjaan>();
  final _location = BehaviorSubject<String>();
  final _date = BehaviorSubject<String>();
  final _fotopegawai = BehaviorSubject<File>();
  final _fotoawal = BehaviorSubject<File>();
  final _fotosedang = BehaviorSubject<File>();
  final _fotoakhir = BehaviorSubject<File>();
  final _video = BehaviorSubject<File>();
  final _panjang = BehaviorSubject<String>();
  final _pekerja = BehaviorSubject<String>();
  final _alat = BehaviorSubject<String>();

  @override
  void dispose() {
    _latitude.close();
    _longitude.close();
    _ruasjalan.close();
    _sup.close();
    _jenispekerjaan.close();
    _paketpekerjaan.close();
    _location.close();
    _date.close();
    _fotopegawai.close();
    _fotoawal.close();
    _fotosedang.close();
    _fotoakhir.close();
    _video.close();
    _panjang.close();
    _pekerja.close();
    _alat.close();
  }

  @override
  void init() {
    _latitude.add(null);
    _longitude.add(null);
    _ruasjalan.add(null);
    _sup.add(null);
    _jenispekerjaan.add(null);
    _paketpekerjaan.add(null);
    _location.add(null);
    _date.add(null);
    _fotopegawai.add(null);
    _fotoawal.add(null);
    _fotosedang.add(null);
    _fotoakhir.add(null);
    _video.add(null);
    _panjang.add(null);
    _pekerja.add(null);
    _alat.add(null);
  }

  @override
  Function(double p1) get onChangedLatitude => _latitude.sink.add;

  @override
  Function(double p1) get onChangedLongitude => _longitude.sink.add;

  @override
  Future<List<MandorResponse>> getRuasJalan() async {
    return await _reposervice.getRuasJalan();
  }

  @override
  Function(MandorResponse p1) get onChangedRuasJalan => _ruasjalan.sink.add;

  @override
  Future<List<DropdownResponse>> getSUP() async {
    return await _reposervice.getSUP();
  }

  @override
  Function(DropdownResponse p1) get onChangedSup => _sup.sink.add;

  @override
  Future<List<JenisPekerjaan>> getPekerjaanType() async {
    return await _reposervice.getPekerjaanType();
  }

  @override
  Function(JenisPekerjaan p1) get onChangedPekerjaanType =>
      _jenispekerjaan.sink.add;

  @override
  Future<List<PaketPekerjaan>> getpaketPekerjaan() async {
    return await _reposervice.getPackage();
  }

  @override
  Function(PaketPekerjaan p1) get onChangedPaketPekerjaan =>
      _paketpekerjaan.sink.add;

  @override
  Function(String p1) get onChangeLocation => _location.sink.add;

  @override
  Function(String p1) get onChangedDate => _date.sink.add;

  @override
  Function(File p1) get onChangedFotoAwal => _fotoawal.sink.add;

  @override
  Function(File p1) get onChangedFotoAkhir => _fotoakhir.sink.add;

  @override
  Function(File p1) get onChangedFotoSedang => _fotosedang.sink.add;

  @override
  Function(File p1) get onChangedVideo => _video.sink.add;

  @override
  Function(String p1) get onChangedAlat => _alat.sink.add;

  @override
  Function(String p1) get onChangedPanjang => _panjang.sink.add;

  @override
  Function(String p1) get onChangedPekerja => _pekerja.sink.add;

  @override
  Future<int> save() async {
    CrudSqfLite sqfLite = CrudSqfLite();

    List<Map<String, File>> files = <Map<String, File>>[
      {
        "fotoPegawai": File(_fotopegawai?.value?.path ?? ""),
        "fotoAwal": File(_fotoawal?.value?.path ?? ""),
        "fotoSedang": File(_fotosedang?.value?.path ?? ""),
        "fotoAkhir": File(_fotoakhir?.value?.path ?? ""),
        "video": File(_video?.value?.path ?? ""),
      }
    ];

    print("FILENYA : " + files.toString());
    //print("IDSUP :" + _date.toString());

    var task = Task((t) {
      t.tanggal = _date.value;
      t.idSup = _storage?.user?.supid;
      t.namaPaket = _paketpekerjaan.value.name;
      t.idRuasJalan = _ruasjalan.value.id;
      //t.idJenisPekerjaan = _jenispekerjaan.value.id;
      t.lokasi = _location.value;
      t.lat = _latitude.value;
      t.long = _longitude.value;
      t.panjang = _panjang.value;
      t.pekerja = int.parse(_pekerja.value);
      //t.alat = _alat.value;
    });
    var result = await _reposervice.save(files, task);

    if (result != 0) {
      ModelTask model = ModelTask(
          _date.value,
          null,
          "Pemeliharaan",
          _paketpekerjaan.value.name,
          _storage?.user?.supid,
          _storage?.user?.sup,
          _ruasjalan.value.id,
          _ruasjalan.value.name,
          _location.value,
          _latitude.value,
          _longitude.value,
          _panjang.value,
          int.parse(_pekerja.value),
          //_alat.value,
          _fotopegawai?.value?.path ?? "",
          _fotoawal?.value?.path ?? "",
          _fotosedang?.value?.path ?? "",
          _fotoakhir?.value?.path ?? "",
          _video?.value?.path ?? "");

      var insert = await sqfLite.insert(model);

      print("DRAAAAAFTTTTTT");

      return insert;
    }
    return result;
  }

  @override
  Stream<bool> get isValidSubmit => CombineLatestStream.combine5(
          _date,
          _paketpekerjaan,
          _ruasjalan,
          _panjang,
          _pekerja, (a, b, c, d, e) {
        if (a != "" &&
            b != "" &&
            c != null &&
            d != "" &&
            e != "") {
          print("VAAAAALIIIIDDDD");
          return true;
        }
        return false;
      });

  @override
  Future<List<Task>> getTaskList() async {
    return await _reposervice.getTaskList();
  }

  @override
  Future<int> edit(String id_pek, int idSup) async {
    List<Map<String, File>> files = <Map<String, File>>[];

    if (_fotoawal != null) {
      files = <Map<String, File>>[
        {
          "fotoAwal": File(_fotoawal?.value?.path ?? ""),
        }
      ];
    }

    if (_fotosedang != null) {
      files = <Map<String, File>>[
        {
          "fotoSedang": File(_fotosedang?.value?.path ?? ""),
        }
      ];
    }

    if (_fotoakhir != null) {
      files = <Map<String, File>>[
        {
          "fotoAkhir": File(_fotoakhir?.value?.path ?? ""),
        }
      ];
    }

    if (_video != null) {
      files = <Map<String, File>>[
        {
          "video": File(_video?.value?.path ?? ""),
        }
      ];
    }

    print("FILENYA : " + files.toString());
    //print("IDSUP :" + _date.toString());

    var task = Task((t) {
      t.idSup = idSup;
      if (_panjang != "" || _panjang != null) {
        t.panjang = _panjang?.value;
      }

      if (_pekerja != "" || _pekerja != null) {
        t.pekerja = int.parse(_pekerja?.value);
      }

      if (_alat != "" || _alat != null) {
        t.alat = _alat?.value;
      }
    });
    return await _reposervice.edit(files, task, id_pek);
  }

  @override
  Stream<bool> get isValidEdit => CombineLatestStream.combine7(
          _panjang, _pekerja, _alat, _fotoawal, _fotosedang, _fotoakhir, _video,
          (a, b, c, d, e, f, g) {
        if ((!a.isEmpty || b != "" || c != "") ||
            (d != null || e != null || f != null || g != null)) {
          print("VAAAAALIIIIDDDD");
          return true;
        }
        return false;
      });

  @override
  Future<int> saveDraft(data) async {
    List<Map<String, File>> files = <Map<String, File>>[
      {
        "fotoPegawai": File(data?.foto_pegawai ?? ""),
        "fotoAwal": File(data?.foto_awal ?? ""),
        "fotoSedang": File(data?.foto_sedang ?? ""),
        "fotoAkhir": File(data?.foto_akhir ?? ""),
        "video": File(data?.video ?? ""),
      }
    ];

    print("FILENYA : " + files.toString());
    //print("IDSUP :" + _date.toString());getTa

    var task = Task((t) {
      t.tanggal = data.tanggal;
      t.idSup = data.id_sup;
      t.namaPaket = data.nama_paket;
      t.idRuasJalan = data.id_ruas;
      //t.idJenisPekerjaan = data.id_pekerjaan;
      t.lokasi = data.lokasi;
      t.lat = data.lat;
      t.long = data.long;
      t.panjang = data.panjang;
      t.pekerja = data.jumlah_pekerja;
      //t.alat = data.alat;
    });

    var result = await _reposervice.save(files, task);

    return result;
  }

  @override
  Future<int> delete(String id) async {
    var result = await _reposervice.delete(id);
    return result;
  }

  @override
  Function(File p1) get onChangedFotoPegawai => _fotopegawai.sink.add;
}
