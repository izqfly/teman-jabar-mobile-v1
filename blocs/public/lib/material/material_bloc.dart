import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/models/material/material.dart';
import 'package:repository/response/material_response.dart';

abstract class MaterialBloc extends Disposable {
  Function(String) get onChangeId;
  Function(String) get onChangJenis;
  Function(String) get onChangeUptd;
  Function(String) get onChangeAlat;
  Function(BahanMaterial) get onChangeBahan1;
  Function(BahanMaterial) get onChangeBahan2;
  Function(BahanMaterial) get onChangeBahan3;
  Function(BahanMaterial) get onChangeBahan4;
  Function(BahanMaterial) get onChangeBahan5;
  Function(BahanMaterial) get onChangeBahan6;
  Function(BahanMaterial) get onChangeBahan7;
  Function(BahanMaterial) get onChangeBahan8;
  Function(BahanMaterial) get onChangeBahan9;
  Function(BahanMaterial) get onChangeBahan10;
  Function(BahanMaterial) get onChangeBahan11;
  Function(BahanMaterial) get onChangeBahan12;
  Function(BahanMaterial) get onChangeBahan13;
  Function(BahanMaterial) get onChangeBahan14;
  Function(BahanMaterial) get onChangeBahan15;
  Function(String) get onChangeJumlah1;
  Function(String) get onChangeJumlah2;
  Function(String) get onChangeJumlah3;
  Function(String) get onChangeJumlah4;
  Function(String) get onChangeJumlah5;
  Function(String) get onChangeJumlah6;
  Function(String) get onChangeJumlah7;
  Function(String) get onChangeJumlah8;
  Function(String) get onChangeJumlah9;
  Function(String) get onChangeJumlah10;
  Function(String) get onChangeJumlah11;
  Function(String) get onChangeJumlah12;
  Function(String) get onChangeJumlah13;
  Function(String) get onChangeJumlah14;
  Function(String) get onChangeJumlah15;
  Function(SatuanMaterial) get onChangeSatuan1;
  Function(SatuanMaterial) get onChangeSatuan2;
  Function(SatuanMaterial) get onChangeSatuan3;
  Function(SatuanMaterial) get onChangeSatuan4;
  Function(SatuanMaterial) get onChangeSatuan5;
  Function(SatuanMaterial) get onChangeSatuan6;
  Function(SatuanMaterial) get onChangeSatuan7;
  Function(SatuanMaterial) get onChangeSatuan8;
  Function(SatuanMaterial) get onChangeSatuan9;
  Function(SatuanMaterial) get onChangeSatuan10;
  Function(SatuanMaterial) get onChangeSatuan11;
  Function(SatuanMaterial) get onChangeSatuan12;
  Function(SatuanMaterial) get onChangeSatuan13;
  Function(SatuanMaterial) get onChangeSatuan14;
  Function(SatuanMaterial) get onChangeSatuan15;

  Future<List<BahanMaterial>> getBahanMaterial();
  Future<List<SatuanMaterial>> getSatuan();

  void init();

  Future<Material> getData(String id);

  Future<int> save(int uptd);
  Future<int> update(int uptd);
}
