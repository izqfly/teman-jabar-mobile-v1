import 'package:repository/response/material_response.dart';
import 'package:repository/http/middlemans/material_middleman_service.dart';
import 'package:repository/models/material/material.dart';
import 'package:rxdart/rxdart.dart';
import 'bloc.dart';

class MaterialService implements MaterialBloc {
  final _reposervice = MaterialMiddlemanService();
  final _idpekerjaan = BehaviorSubject<String>();
  final _uptdid = BehaviorSubject<String>();
  final _jenispekerjaan = BehaviorSubject<String>();
  final _peralatan = BehaviorSubject<String>();
  final _bahan1 = BehaviorSubject<BahanMaterial>();
  final _bahan2 = BehaviorSubject<BahanMaterial>();
  final _bahan3 = BehaviorSubject<BahanMaterial>();
  final _bahan4 = BehaviorSubject<BahanMaterial>();
  final _bahan5 = BehaviorSubject<BahanMaterial>();
  final _bahan6 = BehaviorSubject<BahanMaterial>();
  final _bahan7 = BehaviorSubject<BahanMaterial>();
  final _bahan8 = BehaviorSubject<BahanMaterial>();
  final _bahan9 = BehaviorSubject<BahanMaterial>();
  final _bahan10 = BehaviorSubject<BahanMaterial>();
  final _bahan11 = BehaviorSubject<BahanMaterial>();
  final _bahan12 = BehaviorSubject<BahanMaterial>();
  final _bahan13 = BehaviorSubject<BahanMaterial>();
  final _bahan14 = BehaviorSubject<BahanMaterial>();
  final _bahan15 = BehaviorSubject<BahanMaterial>();
  final _jumlah1 = BehaviorSubject<String>();
  final _jumlah2 = BehaviorSubject<String>();
  final _jumlah3 = BehaviorSubject<String>();
  final _jumlah4 = BehaviorSubject<String>();
  final _jumlah5 = BehaviorSubject<String>();
  final _jumlah6 = BehaviorSubject<String>();
  final _jumlah7 = BehaviorSubject<String>();
  final _jumlah8 = BehaviorSubject<String>();
  final _jumlah9 = BehaviorSubject<String>();
  final _jumlah10 = BehaviorSubject<String>();
  final _jumlah11 = BehaviorSubject<String>();
  final _jumlah12 = BehaviorSubject<String>();
  final _jumlah13 = BehaviorSubject<String>();
  final _jumlah14 = BehaviorSubject<String>();
  final _jumlah15 = BehaviorSubject<String>();
  final _satuan1 = BehaviorSubject<SatuanMaterial>();
  final _satuan2 = BehaviorSubject<SatuanMaterial>();
  final _satuan3 = BehaviorSubject<SatuanMaterial>();
  final _satuan4 = BehaviorSubject<SatuanMaterial>();
  final _satuan5 = BehaviorSubject<SatuanMaterial>();
  final _satuan6 = BehaviorSubject<SatuanMaterial>();
  final _satuan7 = BehaviorSubject<SatuanMaterial>();
  final _satuan8 = BehaviorSubject<SatuanMaterial>();
  final _satuan9 = BehaviorSubject<SatuanMaterial>();
  final _satuan10 = BehaviorSubject<SatuanMaterial>();
  final _satuan11 = BehaviorSubject<SatuanMaterial>();
  final _satuan12 = BehaviorSubject<SatuanMaterial>();
  final _satuan13 = BehaviorSubject<SatuanMaterial>();
  final _satuan14 = BehaviorSubject<SatuanMaterial>();
  final _satuan15 = BehaviorSubject<SatuanMaterial>();

  @override
  void dispose() {
    _idpekerjaan.close();
    _uptdid.close();
    _jenispekerjaan.close();
    _peralatan.close();
    _bahan1.close();
    _bahan2.close();
    _bahan3.close();
    _bahan4.close();
    _bahan5.close();
    _bahan6.close();
    _bahan7.close();
    _bahan8.close();
    _bahan9.close();
    _bahan10.close();
    _bahan11.close();
    _bahan12.close();
    _bahan13.close();
    _bahan14.close();
    _bahan15.close();
    _jumlah1.close();
    _jumlah2.close();
    _jumlah3.close();
    _jumlah4.close();
    _jumlah5.close();
    _jumlah6.close();
    _jumlah7.close();
    _jumlah8.close();
    _jumlah9.close();
    _jumlah10.close();
    _jumlah11.close();
    _jumlah12.close();
    _jumlah13.close();
    _jumlah14.close();
    _jumlah15.close();
    _satuan1.close();
    _satuan2.close();
    _satuan3.close();
    _satuan4.close();
    _satuan5.close();
    _satuan6.close();
    _satuan7.close();
    _satuan8.close();
    _satuan9.close();
    _satuan10.close();
    _satuan11.close();
    _satuan12.close();
    _satuan13.close();
    _satuan14.close();
    _satuan15.close();
  }

  @override
  void init() {
    _idpekerjaan.add(null);
    _uptdid.add(null);
    _jenispekerjaan.add(null);
    _peralatan.add(null);
    _bahan1.add(null);
    _bahan2.add(null);
    _bahan3.add(null);
    _bahan4.add(null);
    _bahan5.add(null);
    _bahan6.add(null);
    _bahan7.add(null);
    _bahan8.add(null);
    _bahan9.add(null);
    _bahan10.add(null);
    _bahan11.add(null);
    _bahan12.add(null);
    _bahan13.add(null);
    _bahan14.add(null);
    _bahan15.add(null);
    _jumlah1.add(null);
    _jumlah2.add(null);
    _jumlah3.add(null);
    _jumlah4.add(null);
    _jumlah5.add(null);
    _jumlah6.add(null);
    _jumlah7.add(null);
    _jumlah8.add(null);
    _jumlah9.add(null);
    _jumlah10.add(null);
    _jumlah11.add(null);
    _jumlah12.add(null);
    _jumlah13.add(null);
    _jumlah14.add(null);
    _jumlah15.add(null);
    _satuan1.add(null);
    _satuan2.add(null);
    _satuan3.add(null);
    _satuan4.add(null);
    _satuan5.add(null);
    _satuan6.add(null);
    _satuan7.add(null);
    _satuan8.add(null);
    _satuan9.add(null);
    _satuan10.add(null);
    _satuan11.add(null);
    _satuan12.add(null);
    _satuan13.add(null);
    _satuan14.add(null);
    _satuan15.add(null);
  }

  @override
  Future<List<BahanMaterial>> getBahanMaterial() async {
    return await _reposervice.getBahanMaterial();
  }

  @override
  Future<List<SatuanMaterial>> getSatuan() async {
    return await _reposervice.getSatuan();
  }

  @override
  Future<Material> getData(String id) async {
    //String jsonString =
    var result = await _reposervice.getData(id);
    return result;
  }

  @override
  Function(String p1) get onChangJenis => _jenispekerjaan.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan1 => _bahan1.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan10 => _bahan10.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan11 => _bahan11.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan12 => _bahan12.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan13 => _bahan13.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan14 => _bahan14.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan15 => _bahan15.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan2 => _bahan2.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan3 => _bahan3.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan4 => _bahan4.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan5 => _bahan5.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan6 => _bahan6.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan7 => _bahan7.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan8 => _bahan8.sink.add;

  @override
  Function(BahanMaterial p1) get onChangeBahan9 => _bahan9.sink.add;

  @override
  Function(String p1) get onChangeId => _idpekerjaan.sink.add;

  @override
  Function(String p1) get onChangeJumlah1 => _jumlah1.sink.add;

  @override
  Function(String p1) get onChangeJumlah10 => _jumlah10.sink.add;

  @override
  Function(String p1) get onChangeJumlah11 => _jumlah11.sink.add;

  @override
  Function(String p1) get onChangeJumlah12 => _jumlah12.sink.add;

  @override
  Function(String p1) get onChangeJumlah13 => _jumlah13.sink.add;

  @override
  Function(String p1) get onChangeJumlah14 => _jumlah14.sink.add;

  @override
  Function(String p1) get onChangeJumlah15 => _jumlah15.sink.add;

  @override
  Function(String p1) get onChangeJumlah2 => _jumlah2.sink.add;

  @override
  Function(String p1) get onChangeJumlah3 => _jumlah3.sink.add;

  @override
  Function(String p1) get onChangeJumlah4 => _jumlah4.sink.add;

  @override
  Function(String p1) get onChangeJumlah5 => _jumlah5.sink.add;

  @override
  Function(String p1) get onChangeJumlah6 => _jumlah6.sink.add;

  @override
  Function(String p1) get onChangeJumlah7 => _jumlah7.sink.add;

  @override
  Function(String p1) get onChangeJumlah8 => _jumlah8.sink.add;

  @override
  Function(String p1) get onChangeJumlah9 => _jumlah9.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan1 => _satuan1.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan10 => _satuan10.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan11 => _satuan11.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan12 => _satuan12.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan13 => _satuan13.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan15 => _satuan15.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan2 => _satuan2.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan3 => _satuan3.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan4 => _satuan4.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan5 => _satuan5.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan6 => _satuan6.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan7 => _satuan7.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan8 => _satuan8.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan9 => _satuan9.sink.add;

  @override
  Function(String p1) get onChangeUptd => _uptdid.sink.add;

  @override
  Function(SatuanMaterial p1) get onChangeSatuan14 => _satuan14.sink.add;

  @override
  Future<int> save(int uptd) async {
    var material = Material((m) {
      m.id = _idpekerjaan.value;
      m.jenis = _jenispekerjaan.value;
      m.uptd = uptd;
      m.alat = _peralatan?.value ?? "";
      m.bahan1 = _bahan1?.value?.name ?? "";
      m.bahan2 = _bahan2?.value?.name ?? "";
      m.bahan3 = _bahan3?.value?.name ?? "";
      m.bahan4 = _bahan4?.value?.name ?? "";
      m.bahan5 = _bahan5?.value?.name ?? "";
      m.bahan6 = _bahan6?.value?.name ?? "";
      m.bahan7 = _bahan7?.value?.name ?? "";
      m.bahan8 = _bahan8?.value?.name ?? "";
      m.bahan9 = _bahan9?.value?.name ?? "";
      m.bahan10 = _bahan10?.value?.name ?? "";
      m.bahan11 = _bahan11?.value?.name ?? "";
      m.bahan12 = _bahan12?.value?.name ?? "";
      m.bahan13 = _bahan13?.value?.name ?? "";
      m.bahan14 = _bahan14?.value?.name ?? "";
      m.bahan15 = _bahan15?.value?.name ?? "";
      m.jumlah1 = _jumlah1?.value ?? "";
      m.jumlah2 = _jumlah2?.value ?? "";
      m.jumlah3 = _jumlah3?.value ?? "";
      m.jumlah4 = _jumlah4?.value ?? "";
      m.jumlah5 = _jumlah5?.value ?? "";
      m.jumlah6 = _jumlah6?.value ?? "";
      m.jumlah7 = _jumlah7?.value ?? "";
      m.jumlah8 = _jumlah8?.value ?? "";
      m.jumlah9 = _jumlah9?.value ?? "";
      m.jumlah10 = _jumlah10?.value ?? "";
      m.jumlah11 = _jumlah11?.value ?? "";
      m.jumlah12 = _jumlah12?.value ?? "";
      m.jumlah13 = _jumlah13?.value ?? "";
      m.jumlah14 = _jumlah14?.value ?? "";
      m.jumlah15 = _jumlah15?.value ?? "";
      m.satuan1 = _satuan1?.value?.name ?? "";
      m.satuan2 = _satuan2?.value?.name ?? "";
      m.satuan3 = _satuan3?.value?.name ?? "";
      m.satuan4 = _satuan4?.value?.name ?? "";
      m.satuan5 = _satuan5?.value?.name ?? "";
      m.satuan6 = _satuan6?.value?.name ?? "";
      m.satuan7 = _satuan7?.value?.name ?? "";
      m.satuan8 = _satuan8?.value?.name ?? "";
      m.satuan9 = _satuan9?.value?.name ?? "";
      m.satuan10 = _satuan10?.value?.name ?? "";
      m.satuan11 = _satuan11?.value?.name ?? "";
      m.satuan12 = _satuan12?.value?.name ?? "";
      m.satuan13 = _satuan13?.value?.name ?? "";
      m.satuan14 = _satuan14?.value?.name ?? "";
      m.satuan15 = _satuan15?.value?.name ?? "";
    });

    print("BODY "+material.toString());

    var result = await _reposervice.save(material);

    return result;
  }

  @override
  Future<int> update(int uptd) async {
    var material = Material((m) {
      m.id = _idpekerjaan.value;
      m.jenis = _jenispekerjaan.value;
      m.uptd = uptd;
      if (_peralatan?.value != null){
        m.alat = _peralatan?.value;
      }
      if (_bahan1?.value?.name != null){
        m.bahan1 = _bahan1?.value?.name;
      }
      if (_bahan2?.value?.name != null){
        m.bahan2 = _bahan2?.value?.name;
      }
      if (_bahan3?.value?.name != null){
        m.bahan3 = _bahan3?.value?.name;
      }
      if (_bahan4?.value?.name != null){
        m.bahan4 = _bahan4?.value?.name;
      }
      if (_bahan5?.value?.name != null){
        m.bahan5 = _bahan5?.value?.name;
      }
      if (_bahan6?.value?.name != null){
        m.bahan6 = _bahan6?.value?.name;
      }
      if (_bahan7?.value?.name != null){
        m.bahan7 = _bahan7?.value?.name;
      }
      if (_bahan8?.value?.name != null){
        m.bahan8 = _bahan8?.value?.name;
      }
      if (_bahan9?.value?.name != null){
        m.bahan9 = _bahan9?.value?.name;
      }
      if (_bahan10?.value?.name != null){
        m.bahan10 = _bahan10?.value?.name;
      }
      if (_bahan11?.value?.name != null){
        m.bahan11 = _bahan11?.value?.name;
      }
      if (_bahan12?.value?.name != null){
        m.bahan12 = _bahan12?.value?.name;
      }
      if (_bahan13?.value?.name != null){
        m.bahan13 = _bahan13?.value?.name;
      }
      if (_bahan14?.value?.name != null){
        m.bahan14 = _bahan14?.value?.name;
      }
      if (_bahan15?.value?.name != null){
        m.bahan15 = _bahan15?.value?.name;
      }
      if (_jumlah1?.value != ''){
        m.jumlah1 = _jumlah1?.value;
      }
      if (_jumlah2?.value != ''){
        m.jumlah2 = _jumlah2?.value;
      }
      if (_jumlah3?.value != ''){
        m.jumlah3 = _jumlah3?.value;
      }
      if (_jumlah4?.value != ''){
        m.jumlah4 = _jumlah4?.value;
      }
      if (_jumlah5?.value != ''){
        m.jumlah5 = _jumlah5?.value;
      }
      if (_jumlah6?.value != ''){
        m.jumlah6 = _jumlah6?.value;
      }
      if (_jumlah7?.value != ''){
        m.jumlah7 = _jumlah7?.value;
      }
      if (_jumlah8?.value != ''){
        m.jumlah8 = _jumlah8?.value;
      }
      if (_jumlah9?.value != ''){
        m.jumlah9 = _jumlah9?.value;
      }
      if (_jumlah10?.value != ''){
        m.jumlah10 = _jumlah10?.value;
      }
      if (_jumlah11?.value != ''){
        m.jumlah11 = _jumlah11?.value;
      }
      if (_jumlah12?.value != ''){
        m.jumlah12 = _jumlah12?.value;
      }
      if (_jumlah13?.value != ''){
        m.jumlah13 = _jumlah13?.value;
      }
      if (_jumlah14?.value != ''){
        m.jumlah14 = _jumlah14?.value;
      }
      if (_jumlah15?.value != ''){
        m.jumlah15 = _jumlah15?.value;
      }
      if (_satuan1?.value?.name != null){
        m.satuan1 = _satuan1?.value?.name;
      }
      if (_satuan2?.value?.name != null){
        m.satuan2 = _satuan2?.value?.name;
      }
      if (_satuan3?.value?.name != null){
        m.satuan3 = _satuan3?.value?.name;
      }
      if (_satuan4?.value?.name != null){
        m.satuan4 = _satuan4?.value?.name;
      }
      if (_satuan5?.value?.name != null){
        m.satuan5 = _satuan5?.value?.name;
      }
      if (_satuan6?.value?.name != null){
        m.satuan6 = _satuan6?.value?.name;
      }
      if (_satuan7?.value?.name != null){
        m.satuan7 = _satuan7?.value?.name;
      }
      if (_satuan8?.value?.name != null){
        m.satuan8 = _satuan8?.value?.name;
      }
      if (_satuan9?.value?.name != null){
        m.satuan9 = _satuan9?.value?.name;
      }
      if (_satuan10?.value?.name != null){
        m.satuan10 = _satuan10?.value?.name;
      }
      if (_satuan11?.value?.name != null){
        m.satuan11 = _satuan11?.value?.name;
      }
      if (_satuan12?.value?.name != null){
        m.satuan12 = _satuan12?.value?.name;
      }
      if (_satuan13?.value?.name != null){
        m.satuan13 = _satuan13?.value?.name;
      }
      if (_satuan14?.value?.name != null){
        m.satuan14 = _satuan14?.value?.name;
      }
      if (_satuan15?.value?.name != null){
        m.satuan15 = _satuan15?.value?.name;
      }
    });

    print("BODY "+material.toString());

    var result = await _reposervice.edit(material, _idpekerjaan.value);

    return result;
  }

  @override
  // TODO: implement onChangeAlat
  Function(String p1) get onChangeAlat => _peralatan.sink.add;
}
