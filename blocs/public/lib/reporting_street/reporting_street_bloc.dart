import 'dart:io';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/models/notification/notification.dart';
import 'package:repository/models/report/report.dart';
import 'package:repository/response/dropdown_response.dart';

abstract class ReportingStreetBloc extends Disposable {
  Function(String) get onChangedName;
  Function(String) get onChangedIdentityCard;
  Function(String) get onChangeAddress;
  Function(String) get onChangePhoneNumber;
  Function(String) get onChangedEmail;
  Function(String) get onChangedDescription;
  Function(double) get onChangedLatitude;
  Function(double) get onChangedLongitude;
  Function(DateTime) get onChangedDate;
  Function(String) get onChangedProgress;
  Function(String) get onChangedPercentage;

  Function(DropdownResponse) get onChangedUptdID;
  Function(DropdownResponse) get onChangedReportType;
  Function(DropdownResponse) get onChangedReportLocation;
  Function(DropdownResponse) get onChangedEmployee;

  Function(File) get onChangedStreetPicture;
  Function(File) get onChangedDocumentation;

  Stream<String> get identityCard;
  Stream<String> get email;

  Stream<bool> get isValidSubmit;
  Stream<bool> get isValidUpdateProgressReport;

  void init();
  void initUpdateReportOnProgress();

  Future<int> save();
  Future<int> updateOnProgressReport(int id);
  Future<int> approve(int id);

  Future<List<DropdownResponse>> getReportType();
  Future<List<DropdownResponse>> getUPTD();
  Future<List<DropdownResponse>> getReportLocation();
  Future<List<DropdownResponse>> getEmployee();

  Future<List<Notification>> getNotificationReport();
  Future<List<Report>> getAll(int skip);
  Future<List<Report>> findByStatus(int skip, String status);
  Future<List<Report>> getByStatusOnProgress(int id);
}
