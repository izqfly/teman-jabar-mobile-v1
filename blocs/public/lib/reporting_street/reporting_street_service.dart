import 'package:repository/models/notification/notification.dart' as model;
import 'package:repository/http/middlemans/reporting_street_middleman_service.dart';
import 'package:repository/models/report/report.dart';
import 'package:repository/response/dropdown_response.dart';
import 'package:rxdart/rxdart.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'bloc.dart';
import 'package:repository/common/flutter_local_storage.dart';

class ReportingStreetService
    with ReportingStreetValidator
    implements ReportingStreetBloc {
  final _name = BehaviorSubject<String>();
  final _email = BehaviorSubject<String>();
  final _identityCard = BehaviorSubject<String>();
  final _phoneNumber = BehaviorSubject<String>();
  final _description = BehaviorSubject<String>();
  final _address = BehaviorSubject<String>();
  final _date = BehaviorSubject<DateTime>();
  final _progress = BehaviorSubject<String>();
  final _percentage = BehaviorSubject<String>();
  final _uptd = BehaviorSubject<DropdownResponse>();
  final _latitude = BehaviorSubject<double>();
  final _longitude = BehaviorSubject<double>();
  final _streetPicture = BehaviorSubject<File>();
  final _documentation = BehaviorSubject<File>();
  final _reportType = BehaviorSubject<DropdownResponse>();
  final _reportLocation = BehaviorSubject<DropdownResponse>();
  final _employeeID = BehaviorSubject<DropdownResponse>();
  final _storage = FlutterLocalStorage();
  final _repoService = ReportingStreetMiddlemanService();

  @override
  void dispose() {
    _name.close();
    _email.close();
    _identityCard.close();
    _phoneNumber.close();
    _description.close();
    _streetPicture.close();
    _reportType.close();
    _reportLocation.close();
    _address.close();
    _latitude.close();
    _longitude.close();
    _uptd.close();
    _date.close();
    _progress.close();
    _percentage.close();
    _documentation.close();
    _employeeID.close();
  }

  @override
  void initUpdateReportOnProgress() {
    _date.add(null);
    _progress.add(null);
    _percentage.add(null);
    _documentation.add(null);
    _employeeID.add(null);
  }

  @override
  void init() {}

  @override
  Stream<String> get email => _email.stream.transform(validEmail);

  @override
  Stream<String> get identityCard =>
      _identityCard.stream.transform(validIdentityCard);

  @override
  Function(String p1) get onChangeAddress => _address.sink.add;

  @override
  Function(String p1) get onChangePhoneNumber => _phoneNumber.sink.add;

  @override
  Function(String p1) get onChangedDescription => _description.sink.add;

  @override
  Function(String p1) get onChangedEmail => _email.sink.add;

  @override
  Function(String p1) get onChangedIdentityCard => _identityCard.sink.add;

  @override
  Function(File p1) get onChangedStreetPicture => _streetPicture.sink.add;

  @override
  Function(DropdownResponse p1) get onChangedReportType => _reportType.sink.add;

  @override
  Function(DropdownResponse p1) get onChangedReportLocation =>
      _reportLocation.sink.add;

  @override
  Function(double p1) get onChangedLatitude => _latitude.sink.add;

  @override
  Function(double p1) get onChangedLongitude => _longitude.sink.add;

  @override
  Function(String p1) get onChangedName => _name.sink.add;

  @override
  Function(DropdownResponse p1) get onChangedUptdID => _uptd.sink.add;

  @override
  Function(DateTime p1) get onChangedDate => _date.sink.add;

  @override
  Function(File p1) get onChangedDocumentation => _documentation.sink.add;

  @override
  Function(DropdownResponse p1) get onChangedEmployee => _employeeID.sink.add;

  @override
  Function(String p1) get onChangedPercentage => _percentage.sink.add;

  @override
  Function(String p1) get onChangedProgress => _progress.sink.add;

  @override
  Stream<bool> get isValidUpdateProgressReport => CombineLatestStream.combine5(
          _date, _documentation, _employeeID, _percentage, _progress,
          (a, b, c, d, e) {
        print(a);
        print(b);
        print(c);
        print(d);
        if (a != null && b != null && c != null && d != "" && e != "") {
          return true;
        }
        return false;
      });

  @override
  Stream<bool> get isValidSubmit => CombineLatestStream.combine8(
          _name,
          _identityCard,
          _phoneNumber,
          _reportType,
          _description,
          _streetPicture,
          _reportLocation,
          _uptd, (a, b, c, d, e, f, g, h) {
        if (a != "" &&
            b != "" &&
            c != "" &&
            d != null &&
            e != "" &&
            f != null &&
            g != null &&
            h != null) {
          return true;
        }
        return false;
      });

  @override
  Future<List<DropdownResponse>> getReportType() async {
    return await _repoService.getReportType();
  }

  @override
  Future<List<DropdownResponse>> getReportLocation() async {
    return await _repoService.getReportLocation();
  }

  @override
  Future<List<Report>> getAll(int skip) async {
    return await _repoService.getAll(skip);
  }

  @override
  Future<List<model.Notification>> getNotificationReport() async {
    return await _repoService.getNotificationReport();
  }

  @override
  Future<List<DropdownResponse>> getUPTD() async {
    return await _repoService.getUPTD();
  }

  @override
  Future<List<Report>> findByStatus(int skip, String status) async {
    return _repoService.findByStatus(skip, status);
  }

  @override
  Future<List<Report>> getByStatusOnProgress(int id) async {
    return await _repoService.getByStatusOnProgress(id);
  }

  @override
  Future<List<DropdownResponse>> getEmployee() async {
    return await _repoService.getEmployee();
  }

  @override
  Future<int> save() async {
    List<Map<String, File>> files = <Map<String, File>>[
      {"gambar": File(_streetPicture?.value?.path ?? "")}
    ];
    //print("DATA  =====  "+_reportType.value.id.toString());
    var report = Report((r) {
      r.name = _name?.value;
      r.identityCard = _identityCard?.value;
      r.email = _storage?.user?.email;
      r.phoneNumber = _phoneNumber?.value;
      r.address = _address?.value;
      r.reportType = _reportType?.value?.id.toString();
      r.location = _reportLocation?.value?.name;
      r.latitude = _latitude?.value?.toString();
      r.longitude = _longitude.value.toString();
      r.description = _description?.value;
      r.uptdId = _uptd?.value?.id;
    });

    //print("REPORT ==== "+report.toString());

    //return 1;

    return _repoService.save(files, report);
  }

  @override
  Future<int> approve(int id) async {
    var report = Report((r) {
      r.id = id;
    });
    return await _repoService.approve(report);
  }

  @override
  Future<int> updateOnProgressReport(int id) async {
    List<Map<String, File>> files = <Map<String, File>>[
      {"dokumentasi": File(_documentation.value.path)}
    ];

    var report = Report((r) {
      r.employeeId = _employeeID.value.id;
      r.date = DateFormat('yyyy-MM-dd').format(_date.value);
      r.progress = _progress.value;
      r.percentage = double.parse(_percentage.value);
      r.reportId = id;
    });

    return await _repoService.updateOnProgressReport(files, report);
  }
}
