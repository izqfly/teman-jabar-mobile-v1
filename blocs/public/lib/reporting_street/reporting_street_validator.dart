import 'dart:async';
import 'package:repository/constants/regex_pattern.dart';

class ReportingStreetValidator {
  final validEmail = StreamTransformer<String, String>.fromHandlers(
      handleData: (String value, EventSink<String> sink) {
    if (value != null) {
      if (value.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else if (!RegExp(RegexPattern.email).hasMatch(value)) {
        sink.addError('Email tidak valid');
      } else {
        sink.add(value);
      }
    }
  });

  final validIdentityCard = StreamTransformer<String, String>.fromHandlers(
      handleData: (String value, EventSink<String> sink) {
    if (value != null) {
      if (value.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else if (value.length != 16) {
        sink.addError('Nomor KTP tidak valid');
      } else {
        sink.add(value);
      }
    }
  });
}
