import 'package:flutter_modular/flutter_modular.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_place/google_place.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:repository/models/route/route.dart';
import 'package:repository/models/route_coordinate/route_coordinates.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

abstract class RouteStreetBloc extends Disposable {
  Stream<String> get startingPoint;
  Stream<String> get destination;
  Stream<Set<Marker>> get markers;
  Stream<dynamic> get directions;

  void init();
  Future<List<PointLatLng>> getRouteBetweenCoordinates(
      {RouteCoordinates routeCoordinates});
  Future<dynamic> getRouteDirections({RouteCoordinates routeCoordinates});
  Future<List<AutocompletePrediction>> getAddressPrediction(String address);
  Future<Position> getCurrentLocation();
  Future<RouteCoordinates> getDetailPlace(String placeId, bool isStartingPoint);
  Future<Route> findRouteRepair(Position position);
}
