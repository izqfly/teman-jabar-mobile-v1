import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:public_bloc/route/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_place/google_place.dart';
import 'package:repository/http/middlemans/route_street_middleman_service.dart';
import 'package:repository/models/route/route.dart';
import 'package:repository/models/route_coordinate/route_coordinates.dart';
import 'package:rxdart/rxdart.dart';
import 'package:repository/google_api/google_api_service.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

class RouteStreetService implements RouteStreetBloc {
  final _startingPointAddress = BehaviorSubject<String>();
  final _destinationAddress = BehaviorSubject<String>();
  final _directions = BehaviorSubject<dynamic>();
  final _markers = BehaviorSubject<Set<Marker>>();

  final _googleApiService = GoogleApiService();
  final _service = RouteStreetMiddlemanService();

  LatLng _destinatonLoc;
  LatLng _originLoc;

  @override
  void init() {
    _startingPointAddress.add("");
    _destinationAddress.add("");
    _markers.add({});
    _directions.add(null);
  }

  @override
  void dispose() {
    _startingPointAddress.close();
    _destinationAddress.close();
    _markers.close();
  }

  @override
  Stream<String> get destination => _destinationAddress.stream;

  @override
  Stream<String> get startingPoint => _startingPointAddress.stream;

  @override
  Stream<Set<Marker>> get markers => _markers.stream;

  @override
  Stream get directions => _directions.stream;

  @override
  Future<RouteCoordinates> getDetailPlace(
      String placeId, bool isStartingPoint) async {
    RouteCoordinates routes = new RouteCoordinates();

    final result = await _googleApiService.getDetailPlace(placeId);

    if (isStartingPoint) {
      _originLoc = LatLng(
        result.geometry?.location?.lat,
        result.geometry?.location?.lng,
      );
      _startingPointAddress.add(result.formattedAddress);
    } else {
      _destinatonLoc = LatLng(
        result.geometry?.location?.lat,
        result.geometry?.location?.lng,
      );

      _destinationAddress.add(result.formattedAddress);

      routes = RouteCoordinates(
        originLat: _originLoc.latitude,
        originLong: _originLoc.longitude,
        destLat: _destinatonLoc.latitude,
        destLong: _destinatonLoc.longitude,
      );
    }

    return routes;
  }

  @override
  Future<List<AutocompletePrediction>> getAddressPrediction(
    String address,
  ) async {
    return _googleApiService.getAddressPrediction(address);
  }

  Future<String> _findAddressFromCoordinate() async {
    try {
      final coordinates = new Coordinates(
        _originLoc.latitude,
        _originLoc.longitude,
      );

      final result =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);

      return result?.first?.addressLine;
    } catch (e) {
      throw PlatformException(
        code: "getCurrentPosition",
        message: "Gagal mengambil detail alamat lokasi",
      );
    }
  }

  @override
  Future<List<PointLatLng>> getRouteBetweenCoordinates(
      {RouteCoordinates routeCoordinates}) async {
    final response = await _googleApiService.getRouteBetweenCoordinates(
      routeCoordinates ??
          RouteCoordinates(
            originLat: _originLoc.latitude,
            originLong: _originLoc.longitude,
            destLat: _destinatonLoc.latitude,
            destLong: _destinatonLoc.longitude,
          ),
    );

    return response;
  }

  @override
  Future<Position> getCurrentLocation() async {
    try {
      final position = await Geolocator.getCurrentPosition(
        timeLimit: Duration(seconds: 10),
      );
      _originLoc = LatLng(position.latitude, position.longitude);
      _startingPointAddress.add(await _findAddressFromCoordinate());
      return position;
    } catch (e) {
      throw PlatformException(
        code: "getCurrentPosition",
        message: "Gagal mengambil data lokasi terkini",
      );
    }
  }

  @override
  Future<void> getRouteDirections({RouteCoordinates routeCoordinates}) async {
    final response = await _googleApiService.getRouteDirections(
      routeCoordinates ??
          RouteCoordinates(
            originLat: _originLoc.latitude,
            originLong: _originLoc.longitude,
            destLat: _destinatonLoc.latitude,
            destLong: _destinatonLoc.longitude,
          ),
    );

    _directions.add(response);
    return response;
  }

  @override
  Future<Route> findRouteRepair(Position position) async {
    var route = Route((r) {
      r.lat = position.latitude.toString();
      r.long = position.longitude.toString();
    });
    return await _service.findRoadRepair(route);
  }
}
