import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:common/helper/flutter_local_notification.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:public/screens/announcement/notification_screen.dart';
import 'package:public/modules/constants/route_name.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:repository/http/middlemans/auth_middleman_service.dart';
import 'package:repository/models/user/user.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_core/firebase_core.dart';

class Fcm {
  final _firebaseMessaging = FirebaseMessaging();
  final _localNotif = FlutterLocalNotification();

  static final Fcm _singleton = Fcm._internal();

  factory Fcm() => _singleton;

  Fcm._internal();

  Future<String> getToken() async {
    return await FirebaseMessaging().getToken();
  }

  void initializeNotification(BuildContext context) async {
    await Firebase.initializeApp();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        //while in use
        _localNotif.showNotification(message);
      },
      onResume: (Map<String, dynamic> message) async {
        //in background
        _navigateToPage(context, message['data']);
      },
      onLaunch: (Map<String, dynamic> message) async {
        //while not launch
        _navigateToPage(context, message['data']);
      },
    );

    _onRefreshToken(context);
    _foregroundSelectNotifiaction(context);
  }

  void _foregroundSelectNotifiaction(BuildContext context) async {
    _localNotif.selectNotificationSubject.stream.listen((payload) {
      print("PAYLOAD ==== " + payload);
      if (payload != null) {
        _onSelectNotification(context, json.decode(payload));
      }
    });
  }

  void _onRefreshToken(BuildContext context) {
    _firebaseMessaging.onTokenRefresh.listen((onData) async {
      try {
        var fcm = User((u) async {
          u.userId = FlutterLocalStorage().user.userId;
          u.fcmToken = await FirebaseMessaging().getToken();
        });
        await AuthMiddlemanService().storeTokenFcm(fcm);
      } catch (e) {
        print(e);
      }
    });
  }

  Future _onSelectNotification(BuildContext context, dynamic data) async {
    //Modular.to.pushNamed("${RouteName.Announcement}", arguments: data);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NotificationScreen()),
    );
    // if (data["redirectTo"].toString().contains("done")) {
    // } else {}
  }

  Future _navigateToPage(BuildContext context, dynamic data) async {
    //Modular.to.pushNamed("${RouteName.Announcement}", arguments: data);
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => NotificationScreen()),
      );
  }
}

Future<dynamic> _myBackgroundMessageHandler(
  Map<String, dynamic> message,
) async {
  FlutterLocalNotification().showNotification(message);
  return Future<void>.value();
}


