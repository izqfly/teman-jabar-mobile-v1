import 'dart:convert';
import 'dart:math';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';

class FlutterLocalNotification {
  static final FlutterLocalNotification _singleton =
      FlutterLocalNotification._internal();
  factory FlutterLocalNotification() => _singleton;

  FlutterLocalNotification._internal();

  final BehaviorSubject<String> selectNotificationSubject =
      BehaviorSubject<String>();

  NotificationDetails _notificationDetails() {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'com.digitak.asdp', 'airsdp', '',
        playSound: true,
        enableVibration: true,
        importance: Importance.max,
        priority: Priority.high,
        icon: "notification_icon",
        visibility: NotificationVisibility.public,
        ticker: "airsdp");

    var platformChannelSpecifics = new NotificationDetails(
      android: androidPlatformChannelSpecifics,
    );

    return platformChannelSpecifics;
  }

  void showNotification(Map<String, dynamic> message) {
    FlutterLocalNotificationsPlugin().show(
      new Random().nextInt(100),
      message["notification"]["title"],
      message["notification"]["body"],
      _notificationDetails(),
      payload: json.encode(message["data"]),
    );
  }

  void clear() {
    selectNotificationSubject.add(null);
  }

  void dispose() {
    selectNotificationSubject.close();
  }
}
