import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FlutterScreenUtil {
  static final FlutterScreenUtil _singleton = FlutterScreenUtil._internal();

  factory FlutterScreenUtil() => _singleton;

  FlutterScreenUtil._internal();

  void initScreen(BuildContext context) {
    
    ScreenUtil.init(
      context,
      designSize: Size(
        screenSize(context).width,
        screenSize(context).height,
      ),
    );
    // ScreenUtil.init(
    //   designSize: Size(
    //     screenSize(context).width,
    //     screenSize(context).height,
    //   ),
    //   allowFontScaling: false,
    // );
  }

  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  double fontSize(double size) {
    return ScreenUtil().setSp(
      size,
      allowFontScalingSelf: true,
    );
  }

  double height(double height) {
    return ScreenUtil().setHeight(height);
  }

  double width(double width) {
    return ScreenUtil().setWidth(width);
  }

  double widthDefault(BuildContext context) {
    return ScreenUtil().setWidth(screenSize(context).width);
  }

  double heightDefault(BuildContext context) {
    return ScreenUtil().setHeight(screenSize(context).height);
  }
}
