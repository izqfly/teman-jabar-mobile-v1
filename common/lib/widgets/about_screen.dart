import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tentang Kami"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Text(
          "Aplikasi mobile Teman Jabar versi 1.0",
          style: TextStyle(
            fontSize: FlutterScreenUtil().fontSize(
              18,
            ),
          ),
        ),
      ),
    );
  }
}
