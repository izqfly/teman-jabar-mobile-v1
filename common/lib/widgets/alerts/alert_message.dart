import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class AlertMessage {
  static void showToast({
    @required BuildContext context,
    @required String message,
    Color textColor,
    Color backgroundColor,
    int duration = 2,
  }) async {
    Toast.show(
      message,
      context,
      textColor: textColor ?? Colors.white,
      backgroundColor: backgroundColor ?? Colors.red[900],
      duration: duration,
      gravity: Toast.BOTTOM,
    );
  }

  static void showFlushbar({
    @required BuildContext context,
    @required Widget title,
    @required Widget message,
    Widget icon,
    Color color,
    int duration = 3,
  }) {
    Flushbar(
      icon: icon,
      titleText: title,
      messageText: message,
      duration: Duration(seconds: duration),
      backgroundColor: color ?? Colors.red[900],
    )..show(context);
  }
}
