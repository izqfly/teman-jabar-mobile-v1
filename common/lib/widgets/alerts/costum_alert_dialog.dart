import 'package:common/constants/base_path_asset.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';

class CostumAlertDialog extends StatefulWidget {
  const CostumAlertDialog({
    Key key,
    @required this.title,
    @required this.message,
    this.assetIcon,
  }) : super(key: key);

  final String title;
  final String message;
  final String assetIcon;

  @override
  _AlertDialogState createState() {
    return _AlertDialogState();
  }
}

class _AlertDialogState extends State<CostumAlertDialog>
    with TickerProviderStateMixin {
  AnimationController ac;
  Animation animation;
  double width;
  double dwidth;
  double dheight;
  double height;
  int animationAxis = 0;
  final screen = FlutterScreenUtil();

  int package = 0;

  @override
  void initState() {
    int animationType = -2;

    if (animationType.abs() == 2) animationAxis = 1;
    ac = AnimationController(duration: Duration(milliseconds: 400), vsync: this);
    animation = Tween(begin: 1.0, end: 0.0)
        .animate(CurvedAnimation(parent: ac, curve: Curves.easeIn));
    animation.addListener(() {
      setState(() {});
    });
    ac.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height * 0.6;
    dwidth = 0.59 * height;
    dheight = 0.7 * height;

    var image;

    image = Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
            ),
            color: Theme.of(context).primaryColor,
          ),
        ),
        Image.asset(
          "${BasePathAsset.icons}/error.png",
          fit: BoxFit.fill,
        )
      ],
    );

    return GestureDetector(
      onTap: () => Navigator.of(context).pop(),
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Center(
          child: Container(
            width: dwidth,
            height: dheight,
            transform: Matrix4.translationValues(
                animationAxis == 0 ? animation.value * width : 0,
                animationAxis == 1 ? animation.value * width : 0,
                0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
            ),
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    height: 0.4 * dheight,
                    child: image,
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: screen.fontSize(18),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 13,
                      left: 7,
                      right: 7,
                      bottom: 13,
                    ),
                    child: Container(
                      alignment: Alignment.center,
                      height: 0.30 * dheight,
                      child: SingleChildScrollView(
                        child: Text(
                          widget.message,
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: screen.fontSize(15),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(right: 25),
                    alignment: Alignment.bottomRight,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Text(
                        "OK",
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: screen.fontSize(15),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
