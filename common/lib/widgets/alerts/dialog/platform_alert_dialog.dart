import 'dart:io';

import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

part 'package:common/widgets/alerts/dialog/platform_widget.dart';

class PlatformAlertDialog extends PlatformWidget {
  PlatformAlertDialog({
    @required this.title,
    @required this.content,
    this.cancelActionText,
    @required this.defaultActionText,
  })  : assert(title != null),
        assert(content != null),
        assert(defaultActionText != null);

  final String title;
  final String content;
  final String defaultActionText;
  final String cancelActionText;

  final screen = FlutterScreenUtil();

  Future<bool> show(BuildContext context) async {
    return Platform.isIOS
        ? await showCupertinoDialog<bool>(
            context: context,
            builder: (BuildContext context) => this,
          )
        : await showDialog<bool>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => this,
          );
  }

  @override
  Widget _buildCupertinoWidget(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(
        title,
        style: _titleDialog(),
      ),
      content: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          content,
          style: _subtitleDialog(),
        ),
      ),
      actions: _buildActions(context),
    );
  }

  @override
  Widget _buildMaterialWidget(BuildContext context) {
    return AlertDialog(
      title: Text(
        title,
        style: _titleDialog(),
      ),
      content: Text(
        content,
        style: _subtitleDialog(),
      ),
      actions: _buildActions(context),
    );
  }

  List<Widget> _buildActions(BuildContext context) {
    final List<Widget> actions = <Widget>[];
    if (cancelActionText != null) {
      actions.add(
        PlatformAlertDialogAction(
          child: Text(
            cancelActionText,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: screen.fontSize(15),
            ),
            key: Key('alertCancel'),
          ),
          onPressed: () => Navigator.of(context).pop(false),
        ),
      );
    }
    actions.add(
      PlatformAlertDialogAction(
        child: Text(
          defaultActionText,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: Theme.of(context).primaryColor,
            fontSize: screen.fontSize(15),
          ),
          key: Key('alertDefault'),
        ),
        onPressed: () => Navigator.of(context).pop(true),
      ),
    );
    return actions;
  }

  TextStyle _titleDialog() => TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: screen.fontSize(20),
        letterSpacing: 0.18,
      );

  TextStyle _subtitleDialog() => TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: screen.fontSize(16),
    letterSpacing: -0.04,
  );
}

class PlatformAlertDialogAction extends PlatformWidget {
  PlatformAlertDialogAction({this.child, this.onPressed});
  final Widget child;
  final VoidCallback onPressed;

  @override
  Widget _buildCupertinoWidget(BuildContext context) {
    return CupertinoDialogAction(
      child: child,
      onPressed: onPressed,
    );
  }

  @override
  Widget _buildMaterialWidget(BuildContext context) {
    return FlatButton(
      child: child,
      onPressed: onPressed,
    );
  }
}
