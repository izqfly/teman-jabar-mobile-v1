part of 'package:common/widgets/alerts/dialog/platform_alert_dialog.dart';

abstract class PlatformWidget extends StatelessWidget {
  Widget _buildCupertinoWidget(BuildContext context);
  Widget _buildMaterialWidget(BuildContext context);

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return _buildCupertinoWidget(context);
    }
    return _buildMaterialWidget(context);
  }
}
