import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget {
  BottomBar(
      {@required this.indexNotifier,
      @required this.items,
      @required this.currentIndex});

  final ValueNotifier indexNotifier;
  final List<BottomNavyBarItem> items;
  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    return BottomNavyBar(
      selectedIndex: currentIndex,
      showElevation: true,
      itemCornerRadius: 8,
      curve: Curves.easeInBack,
      onItemSelected: (index) => indexNotifier.value = index,
      items: items,
    );
  }
}
