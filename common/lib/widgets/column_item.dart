import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';

class ColumnItem extends StatelessWidget {
  ColumnItem({this.title, this.value, this.child, this.textColor});
  final String title;
  final String value;
  final Widget child;
  final Color textColor;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: _screen.fontSize(14),
              fontWeight: FontWeight.bold,
              color: Colors.black45),
        ),
        SizedBox(width: 50),
        child ??
            Text(
              value,
              style: TextStyle(
                fontSize: _screen.fontSize(14),
                color: textColor ?? Colors.black,
              ),
              textAlign: TextAlign.left,
            )
      ],
    );
  }
}
