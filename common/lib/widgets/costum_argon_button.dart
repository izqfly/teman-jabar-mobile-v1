import 'package:flutter/material.dart';
import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:common/utils/flutter_screen_util.dart';

class CostumArgonButton extends StatelessWidget {
  CostumArgonButton({
    this.onTap,
    this.stream,
    this.title,
    this.width,
    this.height,
    this.child,
  });
  final Stream stream;
  final String title;
  final double width;
  final double height;
  final Widget child;
  final Function(Function startLoading, Function stopLoading) onTap;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: stream,
      builder: (_, snapshot) {
        return ArgonButton(
          height: height ?? 50,
          roundLoadingShape: true,
          width: width ?? MediaQuery.of(context).size.width,
          onTap: (startLoading, stopLoading, btnState) {
            if (snapshot.hasData && snapshot.data) {
              onTap(startLoading, stopLoading);
            }
          },
          child: child ??
              Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(child: Container()),
                    Text(
                      title ?? "",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: FlutterScreenUtil().fontSize(18),
                          letterSpacing: 1,
                          fontWeight: FontWeight.w700),
                    ),
                    Expanded(child: Container()),
                    Icon(Icons.arrow_forward, color: Colors.white)
                  ],
                ),
              ),
          loader: Container(
            padding: EdgeInsets.all(10),
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
          borderRadius: 24,
          color: snapshot.hasData && snapshot.data
              ? Theme.of(context).primaryColor
              : Theme.of(context).primaryColor.withOpacity(0.5),
        );
      },
    );
  }
}
