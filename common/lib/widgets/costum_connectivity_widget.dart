import 'package:common/utils/flutter_screen_util.dart';
import 'package:connectivity_widget/connectivity_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CostumConnectivityWidget extends StatelessWidget {
  CostumConnectivityWidget({
    Key key,
    @required this.child,
    this.onlineCallback,
    this.offlineCallback,
  }) : super(key: key);

  final Widget child;
  final Function onlineCallback;
  final Function offlineCallback;

  @override
  Widget build(BuildContext context) {
    return ConnectivityWidget(
      onlineCallback: onlineCallback,
      offlineBanner: Container(
        padding: EdgeInsets.all(FlutterScreenUtil().width(8)),
        width: double.infinity,
        color: Colors.red[900],
        child: Text(
          "Tidak ada koneksi",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
      ),
      builder: (_, __) => child,
    );
  }
}
