import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';

class DetailCard extends StatelessWidget {
  DetailCard({
    this.description,
    this.title,
    this.isHideTopDivider = true,
    this.onTap,
  });

  final String title;
  final String description;
  final bool isHideTopDivider;
  final Function onTap;

  final _screen = FlutterScreenUtil();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        isHideTopDivider
            ? Container()
            : Divider(thickness: 0.5, color: Colors.black45),
        FlatButton(
          onPressed: onTap,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 3),
                    Text(
                      description,
                      style: TextStyle(
                        fontSize: _screen.fontSize(14),
                        color: Colors.black45,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(width: 20),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.black45,
                size: 16,
              ),
            ],
          ),
        ),
        Divider(thickness: 0.5, color: Colors.black45),
      ],
    );
  }
}
