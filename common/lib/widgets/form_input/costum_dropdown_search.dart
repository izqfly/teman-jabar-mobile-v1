import 'package:common/utils/flutter_screen_util.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

class CostumDropdownSearch<T> extends StatelessWidget {
  CostumDropdownSearch(
      {@required this.compateFn,
      @required this.dropdownBuilder,
      @required this.hintText,
      @required this.itemAsString,
      @required this.items,
      @required this.onChanged,
      @required this.popupItemBuilder,
      @required this.selectedItem,
      @required this.title});

  final Function(T) onChanged;
  final Function(T, T) compateFn;
  final Function(T) itemAsString;
  final Function(BuildContext, T, String) dropdownBuilder;
  final Function(BuildContext, T, bool) popupItemBuilder;
  final String hintText;
  final String title;
  final List<T> items;
  final T selectedItem;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: FlutterScreenUtil().fontSize(15),
            color: Colors.black54,
          ),
        ),
        SizedBox(height: 3),
        DropdownSearch<T>(
          dropdownSearchDecoration: InputDecoration(
            hintText: hintText ?? "",
            hintStyle: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(14),
              color: Colors.black45,
            ),
          ),
          compareFn: compateFn,
          showSelectedItem: true,
          mode: Mode.DIALOG,
          items: items,
          showSearchBox: true,
          itemAsString: itemAsString,
          searchBoxDecoration: InputDecoration(
            isDense: false,
            hintText: "Cari $title",
            hintStyle: TextStyle(fontStyle: FontStyle.italic),
            prefixIcon: Icon(Icons.search),
          ),
          popupShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          onChanged: onChanged,
          dropDownButton: Icon(
            Icons.arrow_drop_down,
            color: Colors.black54,
          ),
          dropdownBuilder: dropdownBuilder,
          selectedItem: selectedItem,
          // emptyBuilder: (context) => Center(
          //   child: Text("Tidak ada data $title"),
          // ),
          popupItemBuilder: popupItemBuilder,
          // loadingBuilder: (context) => Center(
          //   child: CircularProgressIndicator(
          //     valueColor: new AlwaysStoppedAnimation<Color>(
          //         Theme.of(context).primaryColor),
          //   ),
          // ),
        ),
        SizedBox(height: 10),
      ],
    );
  }
}
