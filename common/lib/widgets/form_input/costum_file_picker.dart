import 'dart:io';

import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class CostumFilePicker extends StatelessWidget {
  CostumFilePicker({@required this.title, @required this.onChanged});

  final String title;
  final Function(File) onChanged;
  final _pathNotifier = ValueNotifier<String>("");

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: FlutterScreenUtil().fontSize(15),
            color: Colors.black54,
          ),
        ),
        SizedBox(height: 3),
        ValueListenableBuilder(
          valueListenable: _pathNotifier,
          builder: (context, value, child) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _ImageViewer._(path: value),
              SizedBox(height: 5),
              ButtonTheme(
                minWidth: double.infinity,
                child: RaisedButton(
                  onPressed: () async => await _openFileExplorer(context),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(8.0),
                  ),
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    value.isNotEmpty ? "Ubah Gambar" : "Pilih Gambar",
                    style: TextStyle(
                      fontSize: FlutterScreenUtil().fontSize(14),
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Divider(thickness: 1, color: Colors.black45)
      ],
    );
  }

  Future<void> _openFileExplorer(BuildContext context) async {
    try {
      final _picker = ImagePicker();
      final file = await _picker.getImage(
        source: ImageSource.camera,
        maxHeight: 800,
        maxWidth: 600,
        imageQuality: 50,
      );

      bool isValid = true;

      if (isValid) {
        _pathNotifier.value = file.path;
        onChanged(File(file.path));
      }

      print(File(file.path));

    } on PlatformException catch (e) {
      String message = "Terjadi kesalahan saat memproses file";

      if (e.code == "already_active") {
        message = "Mohon tunggu sebentar, aplikasi sedang memproses file";
      }
      _showFlushBar(context, message);
    }
  }

  void _showFlushBar(BuildContext context, String message) {
    AlertMessage.showFlushbar(
      context: context,
      title: Text(
        "Pesan Kesalahan",
        style: TextStyle(
          color: Colors.white,
          fontSize: FlutterScreenUtil().fontSize(15),
        ),
      ),
      message: Text(
        message,
        style: TextStyle(
          color: Colors.white,
          fontSize: FlutterScreenUtil().fontSize(14),
        ),
      ),
    );
  }
}

class _ImageViewer extends StatelessWidget {
  _ImageViewer._({@required this.path});
  final String path;
  @override
  Widget build(BuildContext context) {
    return path.isEmpty
        ? Container()
        : Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(8.0),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.file(
                File(path),
                height: 200,
                fit: BoxFit.fitHeight,
              ),
            ),
          );
  }
}
