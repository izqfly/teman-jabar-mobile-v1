import 'dart:io';

import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class CostumCameraPicker extends StatelessWidget {
  CostumCameraPicker({@required this.title, @required this.onChanged});

  final String title;
  final Function(File) onChanged;
  final _pathNotifier = ValueNotifier<String>("");

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: FlutterScreenUtil().fontSize(15),
            color: Colors.black54,
          ),
        ),
        SizedBox(height: 3),
        ValueListenableBuilder(
          valueListenable: _pathNotifier,
          builder: (context, value, child) => Stack(
            children: [
              value.isEmpty
                  ? Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 150,
                        width: 150,
                        child: RaisedButton(
                          onPressed: () async => _showPicker(context),
                          child: Icon(
                            Icons.add_circle,
                            color: Colors.grey,
                          ),
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadiusDirectional.circular(8),
                          ),
                        ),
                      ),
                    )
                  : Align(
                      child: _ImageViewer._(
                        path: value,
                        onClose: () => onClose(),
                      ),
                      alignment: Alignment.center,
                    ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library_rounded),
                      title: new Text('Pilih File'),
                      onTap: () async {
                        Navigator.of(context).pop();
                        await _openFileExplorer(context, false);
                      }),
                  new ListTile(
                    leading: new Icon(Icons.camera_outlined),
                    title: new Text('Kamera'),
                    onTap: () async {
                      Navigator.of(context).pop();
                      await _openFileExplorer(context, true);
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

  Future<void> _openFileExplorer(BuildContext context, bool camera) async {
    try {
      final _picker = ImagePicker();
      final file = await _picker.getImage(
        source: camera ? ImageSource.camera : ImageSource.gallery,
        maxHeight: 800,
        maxWidth: 600,
        imageQuality: 50,
      );

      bool isValid = true;

      if (isValid) {
        _pathNotifier.value = file.path;
        print("NAMA FILENYA : "+file.path);
        onChanged(File(file.path));
      }

      print(File(file.path));
    } on PlatformException catch (e) {
      String message = "Terjadi kesalahan saat memproses file";

      if (e.code == "already_active") {
        message = "Mohon tunggu sebentar, aplikasi sedang memproses file";
      }
      _showFlushBar(context, message);
    }
  }

  void onClose() {
    _pathNotifier.value = "";
  }

  void _showFlushBar(BuildContext context, String message) {
    AlertMessage.showFlushbar(
      context: context,
      title: Text(
        "Pesan Kesalahan",
        style: TextStyle(
          color: Colors.white,
          fontSize: FlutterScreenUtil().fontSize(15),
        ),
      ),
      message: Text(
        message,
        style: TextStyle(
          color: Colors.white,
          fontSize: FlutterScreenUtil().fontSize(14),
        ),
      ),
    );
  }
}

class _ImageViewer extends StatelessWidget {
  _ImageViewer._({@required this.path, @required this.onClose});

  final String path;
  final Function onClose;

  @override
  Widget build(BuildContext context) {
    return path.isEmpty
        ? Container()
        : Container(
            height: 150,
            width: 150,
            child: Stack(
              children: [
                Card(
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(8.0),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.file(
                      File(path),
                      height: 150,
                      width: 150,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      height: 24,
                      width: 24,
                      child: RaisedButton(
                        padding: EdgeInsets.all(0),
                        color: Colors.black45,
                        onPressed: onClose,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadiusDirectional.circular(12.0),
                        ),
                        child: Icon(
                          Icons.close_rounded,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
