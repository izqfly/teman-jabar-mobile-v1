import 'package:common/utils/flutter_screen_util.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CostumDateFormFiled extends StatelessWidget {
  CostumDateFormFiled({
    @required this.onChanged,
    @required this.title,
    this.hintText,
    this.pattern = "dd MMMM yyyy",
  });

  final String title;
  final String hintText;
  final String pattern;
  final Function(DateTime) onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: FlutterScreenUtil().fontSize(15),
            color: Colors.black54,
          ),
        ),
        SizedBox(height: 3),
        DateTimeField(
          autocorrect: false,
          autofocus: false,
          resetIcon: Icon(Icons.close, size: 16),
          format: DateFormat(pattern),
          style: TextStyle(fontSize: FlutterScreenUtil().fontSize(14)),
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(14),
              color: Colors.black45,
            ),
            suffixIcon: Icon(Icons.calendar_today),
          ),
          onChanged: onChanged,
          onShowPicker: (context, currentValue) async {
            final date = await showDatePicker(
              locale: Locale('id', 'ID'),
              helpText: "PILIH TANGGAL",
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(DateTime.now().year + 5),
            );
            if (date != null) {
              return date;
            } else {
              return currentValue;
            }
          },
        ),
        SizedBox(height: 10),
      ],
    );
  }
}
