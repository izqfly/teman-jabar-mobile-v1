import 'package:flutter/material.dart';

class Loader extends StatefulWidget {
  Loader({
    Key key,
    this.radius = 10,
    this.color1,
    this.color2,
    this.color3,
  }) : super(key: key);

  final double radius;
  final Color color1;
  final Color color2;
  final Color color3;

  @override
  _ColorLoader4State createState() => _ColorLoader4State();
}

class _ColorLoader4State extends State<Loader>
    with SingleTickerProviderStateMixin {
  Animation<double> _animation_1;
  Animation<double> _animation_2;
  Animation<double> _animation_3;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: Duration(milliseconds: 1000),
      vsync: this,
    );

    _animation_1 = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(0.0, 0.80, curve: Curves.ease),
      ),
    );

    _animation_2 = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(0.1, 0.9, curve: Curves.ease),
      ),
    );

    _animation_3 = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(0.2, 1.0, curve: Curves.ease),
      ),
    );

    // important, please don't remove!
    _controller.addListener(() {
      setState(() {});
    });

    _controller.repeat();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Transform.translate(
            offset: Offset(
              0.0,
              -30 *
                  (_animation_1.value <= 0.50
                      ? _animation_1.value
                      : 1.0 - _animation_1.value),
            ),
            child: new Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Dot(
                radius: widget.radius,
                color: widget.color1 ?? Theme.of(context).primaryColor,
                icon: Icon(Icons.blur_on),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(
              0.0,
              -30 *
                  (_animation_2.value <= 0.50
                      ? _animation_2.value
                      : 1.0 - _animation_2.value),
            ),
            child: new Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Dot(
                radius: widget.radius,
                color: widget.color2 ?? Theme.of(context).accentColor,
                icon: Icon(Icons.blur_on),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(
              0.0,
              -30 *
                  (_animation_3.value <= 0.50
                      ? _animation_3.value
                      : 1.0 - _animation_3.value),
            ),
            child: new Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Dot(
                radius: widget.radius,
                color: widget.color3 ?? Theme.of(context).primaryColor,
                icon: Icon(Icons.blur_on),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class Dot extends StatelessWidget {
  final double radius;
  final Color color;
  final Icon icon;

  Dot({this.radius, this.color, this.icon});

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Transform.rotate(
        angle: 0.0,
        child: Container(
          width: radius,
          height: radius,
          decoration: BoxDecoration(color: color, shape: BoxShape.circle),
        ),
      ),
    );
  }
}
