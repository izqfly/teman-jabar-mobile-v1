import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screen = FlutterScreenUtil();
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: new BorderRadius.circular(15.0),
          ),
          height: screen.height(100),
          width: screen.width(120),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  "Mohon tunggu...",
                  style: TextStyle(
                    fontSize: screen.fontSize(14),
                    fontWeight: FontWeight.w400,
                  ),
                  textAlign: TextAlign.center,
                ),
                Loader(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
