import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';

class RowItem extends StatelessWidget {
  RowItem({this.title, this.value, this.color, this.child});
  final String title;
  final String value;
  final Color color;
  final Widget child;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: _screen.fontSize(14),
              fontWeight: FontWeight.bold,
              color: Colors.black45),
        ),
        SizedBox(width: 50),
        child ??
            Flexible(
                child: Text(
              value,
              style: TextStyle(
                fontSize: _screen.fontSize(14),
                color: color ?? Colors.black,
              ),
              textAlign: TextAlign.right,
            ))
      ],
    );
  }
}
