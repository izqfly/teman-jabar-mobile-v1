import 'package:common/constants/base_path_asset.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:public/screens/settings/change-password.dart';
import 'package:public/screens/settings/change-detail.dart';

class UserAccount extends StatelessWidget {
  UserAccount({
    this.isSetting = false,
    this.onTap,
    this.stream,
    this.confirmPassword,
    this.passwordNew,
    this.passwordOld,
  });

  final _screen = FlutterScreenUtil();
  final _storage = FlutterLocalStorage();
  final bool isSetting;
  final Stream stream;
  final Function passwordOld;
  final Function passwordNew;
  final Function confirmPassword;
  final Function(Function startLoading, Function stopLoading) onTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 140,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: [
              Positioned(
                child: Container(
                  width: double.infinity,
                  height: 48,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Positioned(
                left: 20,
                top: 20,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Card(
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadiusDirectional.circular(8.0),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.asset(
                          "${BasePathAsset.icons}/common/temanjabar-logo.png",
                          height: 100,
                          width: 100,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _storage?.user?.role ?? "-",
                          style: TextStyle(color: Colors.black54),
                        ),
                        SizedBox(height: 10),
                        Text(
                          _storage?.user?.fullname ?? "-",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: _screen.fontSize(15),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          _storage?.user?.city ?? "-",
                          style: TextStyle(
                            color: Colors.black45,
                            fontSize: _screen.fontSize(13),
                          ),
                        ),
                        SizedBox(height: 5),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Divider(thickness: 0.5, color: Colors.black45),
        _Detail._(
          isSetting: isSetting,
          onTap: onTap,
          stream: stream,
          passwordNew: passwordNew,
          passwordOld: passwordOld,
          confirmPassword: confirmPassword,
        ),
      ],
    );
  }
}

class _Detail extends StatelessWidget {
  _Detail._({
    this.isSetting = false,
    this.onTap,
    this.stream,
    this.passwordNew,
    this.confirmPassword,
    this.passwordOld,
  });

  final bool isSetting;
  final Stream stream;
  final Function passwordOld;
  final Function passwordNew;
  final Function confirmPassword;
  final Function(Function startLoading, Function stopLoading) onTap;
  final _storage = FlutterLocalStorage();
  final _screen = FlutterScreenUtil();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
                  child: _ColItem._(
                      title: "Status", value: _storage?.user?.role ?? "-")),
              SizedBox(height: 15),
              Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
                  child: _ColItem._(
                      title: "Email", value: _storage?.user?.email ?? "-")),
              SizedBox(height: 15),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
                child: _ColItem._(
                    title: "No. Handphone",
                    value: _storage?.user?.phoneNumber ?? "-"),
              ),
              SizedBox(height: 15),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
                child: _ColItem._(
                    title: "Alamat", value: _storage?.user?.address ?? "-"),
              ),
              SizedBox(height: 20),
              Divider(
                height: 2,
                color: Colors.black38,
                thickness: 0.5,
              ),
              _ButtonSettings._(
                title: "Edit Detail",
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ChangeDetail()));
                },
              ),
              _ButtonSettings._(
                title: "Ganti Kata Sandi",
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ChangePassword()));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ButtonSettings extends StatelessWidget {
  _ButtonSettings._({this.title, this.onTap});

  final String title;
  final Function() onTap;
  final _screen = FlutterScreenUtil();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  this.title,
                  style: TextStyle(
                    fontSize: _screen.fontSize(16),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ],
            ),
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            width: MediaQuery.of(context).size.width,
          ),
          onTap: onTap,
        ),
        Divider(
          height: 2,
          color: Colors.black38,
          thickness: 0.5,
        ),
      ],
    );
  }
}

class _ColItem extends StatelessWidget {
  _ColItem._({this.title, this.value});

  final String title;
  final String value;
  final _screen = FlutterScreenUtil();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: _screen.fontSize(16),
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          value,
          style: TextStyle(
            fontSize: _screen.fontSize(14),
            color: Colors.black45,
          ),
        )
      ],
    );
  }
}
