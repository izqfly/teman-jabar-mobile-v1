class RouteName {
  static const BaseModule = "/auth";
  static const LandingPage = "/internal-landing-page";

  static const WorkProgress = "/work-progress";
  static const DamageReport = "/damage-report";
  static const StabilityRoad = "/stability-road";
  static const FinancialRealization = "/financial-realization";
  static const VerifiedStreet = "/verified-stret";
  static const DamageStreet = "/damage-street";
  static const About = "/about";
  static const Settings = "/settings";

  // Monitoring
  static const MonitoringModule = "/monitoring";
  static const ListMonitoringBudget = "/list-monitoring-budget";
  static const ContractProject = "/contract-project";
  static const DetailContractProject = "/detail-contract-project";

  // Public Report
  static const PublicReportModule = "/public-report-module";
  static const PublicReport = "/public-report";
  static const DetailPublicReport = "/detail-public-report";
  static const DetailProgressReport = "/detail-progress-report";
  static const FormProgressReport = "/form-progress-report";
}
