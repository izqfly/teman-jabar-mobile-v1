import 'package:flutter_modular/flutter_modular.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:internal/modules/monitoring_module.dart';
import 'package:internal/modules/public_report_module.dart';
import 'package:internal/screens/damage_report/damage_report_screen.dart';
import 'package:internal/screens/financial_relaization/financial_realization_screen.dart';
import 'package:internal/screens/financial_relaization/monitoring/list_monitoring_screen.dart';
import 'package:internal/screens/settings/settings_screen.dart';
import 'package:internal/screens/stability_road/stability_road_screen.dart';
import 'package:common/widgets/about_screen.dart';
import 'package:internal/screens/webview/webview_screen.dart';
import 'package:internal_bloc/contract_project/bloc.dart';
import 'package:internal_bloc/damage_report/bloc.dart';
import 'package:internal_bloc/stability_road/bloc.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:auth_bloc/bloc.dart';

class InternalModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind<AuthBloc>((i) => AuthService()),
        Bind<ReportingStreetBloc>((i) => ReportingStreetService()),
        Bind<ContractProjectBloc>((i) => ContractProjectService()),
        Bind<DamageReportBloc>((i) => DamageReportService()),
        Bind<StabilityRoadBloc>((i) => StabilityRoadService()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          "/",
          child: (_, args) => WebViewScreen(), //InternalLandingPage(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.DamageReport,
          child: (_, __) => DamageReportScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.StabilityRoad,
          child: (_, __) => StabilityRoadScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.FinancialRealization,
          child: (_, __) => FinancialRealizationScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.PublicReportModule,
          module: PublicReportModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.ListMonitoringBudget,
          child: (_, args) => ListMonitoringBudget(title: args.data),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.MonitoringModule,
          module: MonitoringModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.About,
          child: (_, args) => AboutScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.Settings,
          child: (_, args) => SettingsScreen(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
