import 'package:flutter_modular/flutter_modular.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:internal/screens/landing_page/menu/monitoring/contract_project/contract_project_screen.dart';
import 'package:internal/screens/landing_page/menu/monitoring/contract_project/detail_contract_project_screen.dart';

class MonitoringModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          RouteName.ContractProject,
          child: (_, args) => ContractProjectScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.DetailContractProject,
          child: (_, args) => DetailContractProjectScreen(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
