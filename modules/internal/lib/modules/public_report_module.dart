import 'package:flutter_modular/flutter_modular.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:internal/screens/public_report/detail/detail_public_report_screen.dart';
import 'package:internal/screens/public_report/detail/progress/detail_progress_screen.dart';
import 'package:internal/screens/public_report/detail/progress/form_update_progress.dart';
import 'package:internal/screens/public_report/public_report_screen.dart';
import 'package:public_bloc/reporting_street/bloc.dart';

class PublicReportModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind<ReportingStreetBloc>((i) => ReportingStreetService()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          RouteName.PublicReport,
          child: (_, __) => PublicReportScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.DetailPublicReport,
          child: (_, args) => DetailPublicReportScreen(title: args.data),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.DetailProgressReport,
          child: (_, args) => DetailProgressReportScreen(id: args.data),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.FormProgressReport,
          child: (_, args) => FormUpdateProgress(id: args.data),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
