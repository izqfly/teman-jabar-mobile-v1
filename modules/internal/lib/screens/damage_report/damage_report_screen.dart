import 'package:common/utils/color_util.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:internal_bloc/damage_report/bloc.dart';
import 'package:location_permissions/location_permissions.dart' as lp;
import 'package:repository/models/damage_road/damage_road.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:shimmer/shimmer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:common/widgets/row_item.dart';
import 'package:common/widgets/column_item.dart';

part 'package:internal/screens/damage_report/widgets/dialog_marker.dart';

class DamageReportScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DamageReportState();
  }
}

class _DamageReportState extends State<DamageReportScreen> {
  Widget _body;

  void initState() {
    super.initState();
    _body = Center(
        child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(
        ColorUtil().parseHexColor("#3355b4"),
      ),
    ));
    _checkLocationPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Kondisi Jalan"),
      ),
      body: _body,
    );
  }

  void _checkLocationPermission() async {
    final statusFuture = await lp.LocationPermissions().checkPermissionStatus();

    if (statusFuture != lp.PermissionStatus.granted) {
      lp.PermissionStatus ps =
          await lp.LocationPermissions().requestPermissions(
        permissionLevel: lp.LocationPermissionLevel.locationWhenInUse,
      );
      if (ps != lp.PermissionStatus.granted) {
        return;
      }
      _setMap(ps);
    } else {
      _setMap(statusFuture);
    }
  }

  void _setMap(lp.PermissionStatus permissionStatus) {
    Future.delayed(const Duration(seconds: 2), () async {
      if (mounted) {
        final markers = await _setMarker();
        setState(() {
          _body = permissionStatus == lp.PermissionStatus.granted
              ? GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(
                      -7.090911,
                      107.668887,
                    ),
                    zoom: 8,
                  ),
                  markers: markers,
                )
              : Container();
        });
      }
    });
  }

  Future<Set<Marker>> _setMarker() async {
    final result = await _onHandlerFetchList();
    Set<Marker> markers = {};
    for (int i = 0; i < result.length; i++) {
      markers.add(Marker(
        markerId: MarkerId(i.toString()),
        position: LatLng(result[i].latitude, result[i].longitude),
        icon: await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5),
          '${BasePathAsset.icons}/markers/road_repair.png',
        ),
        infoWindow: InfoWindow(title: result[i].description),
        onTap: () => showDialog(
          context: context,
          barrierDismissible: true,
          child: _DialogMarker._(id: result[i].id),
        ),
      ));
    }

    return markers;
  }

  Future<List<DamageRoad>> _onHandlerFetchList() async {
    try {
      return await Modular.get<DamageReportBloc>().fetchList();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: null, message: e.message);
    }
    return <DamageRoad>[];
  }
}
