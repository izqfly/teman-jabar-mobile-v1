part of 'package:internal/screens/damage_report/damage_report_screen.dart';

class _DialogMarker extends StatelessWidget {
  _DialogMarker._({this.id});
  final int id;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Container(
        height: _screen.height(360),
        width: _screen.width(100),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              FutureBuilder<DamageRoad>(
                future: _onHandlerFindByID(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    final data = snapshot.data;
                    return Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RowItem(
                              title: "No. Pengaduan",
                              value: data?.reportNumber ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            ColumnItem(
                              title: "Pelapor",
                              value:
                                  "${data?.reporterName ?? ""}\n${data?.reporterIdentity ?? ""}\n${data?.reporterPhoneNumber ?? ""}\n${data?.reporterEmail ?? ""}",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "UPTD",
                              value: data?.uptd ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Kategori Laporan",
                              value: data?.reportCategory ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Lokasi",
                              value: "${data?.latitude}, ${data?.longitude}",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            ColumnItem(
                              title: "Foto Kondisi",
                              child: _PhotoCard._(
                                photoUrl: data?.photoConditionUrl,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  return Container();
                },
              ),
              SizedBox(height: 10),
              ButtonTheme(
                minWidth: double.infinity,
                height: 40.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: () => Navigator.pop(context, false),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(24.0)),
                  child: Text(
                    "TUTUP",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                      letterSpacing: 0.3,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<DamageRoad> _onHandlerFindByID() async {
    try {
      return await Modular.get<DamageReportBloc>().findByID(id);
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: null, message: e.message);
    }
    return new DamageRoad();
  }
}

class _PhotoCard extends StatelessWidget {
  _PhotoCard._({this.photoUrl});
  final String photoUrl;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadiusDirectional.circular(8.0),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: CachedNetworkImage(
          imageUrl: photoUrl,
          errorWidget: (_, __, ___) => Icon(
            Icons.broken_image,
            color: Colors.black54,
          ),
          fit: BoxFit.fitHeight,
          filterQuality: FilterQuality.high,
          height: 200,
          width: double.infinity,
          placeholder: (__, _) => Shimmer.fromColors(
            child: Container(
              height: 200,
              width: double.infinity,
              color: Colors.white,
            ),
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
          ),
        ),
      ),
    );
  }
}
