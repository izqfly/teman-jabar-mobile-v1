part of 'package:internal/screens/financial_relaization/financial_realization_screen.dart';

class _DbmprBudgetChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  _DbmprBudgetChart(this.seriesList, {this.animate});

  factory _DbmprBudgetChart.withSampleData() {
    return new _DbmprBudgetChart(
      _createSampleData(),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            "Anggaran Belanja DBMPR Provinsi Jawa Barat ${DateTime.now().year}",
            style: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(17),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: 250,
          child: new charts.PieChart(
            seriesList,
            animate: animate,
            defaultRenderer: new charts.ArcRendererConfig(
              arcWidth: 70,
              arcRendererDecorators: [new charts.ArcLabelDecorator()],
            ),
          ),
        )
      ],
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<_PieData, String>> _createSampleData() {
    final data = [
      new _PieData('Pencairan', 30),
      new _PieData('Belum Cair', 60),
    ];

    return [
      new charts.Series<_PieData, String>(
        id: "DBMPR Budget",
        domainFn: (_PieData ca, _) => ca.activity,
        measureFn: (_PieData ca, _) => ca.percentage,
        data: data,
        labelAccessorFn: (datum, index) =>
            index == 0 ? "Pencairan" : "Belum Cair",
        colorFn: (datum, index) => index == 0
            ? charts.MaterialPalette.green.shadeDefault
            : charts.MaterialPalette.red.shadeDefault,
      )
    ];
  }
}

class _PieData {
  final String activity;
  final double percentage;

  _PieData(this.activity, this.percentage);
}
