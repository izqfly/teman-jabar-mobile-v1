part of 'package:internal/screens/financial_relaization/financial_realization_screen.dart';

class _HighestBudgetChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  _HighestBudgetChart(this.seriesList, {this.animate});

  factory _HighestBudgetChart.withSampleData() {
    return new _HighestBudgetChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            "Info Penyerapan anggaran tertinggi",
            style: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(17),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: 250,
          child: new charts.BarChart(
            seriesList,
            animate: animate,
            barRendererDecorator: new charts.BarLabelDecorator<String>(
              labelAnchor: charts.BarLabelAnchor.middle,
              labelPosition: charts.BarLabelPosition.inside,
            ),
            domainAxis: new charts.OrdinalAxisSpec(
              viewport: new charts.OrdinalViewport('organization', 4),
            ),
            rtlSpec:
                charts.RTLSpec(axisDirection: charts.AxisDirection.reversed),
            defaultInteractions: false,
            behaviors: [
              new charts.SlidingViewport(),
              new charts.PanAndZoomBehavior(),
            ],
          ),
        )
      ],
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<_ChartAxis, String>> _createSampleData() {
    final data = [
      new _ChartAxis('UPTD-I', 4),
      new _ChartAxis('UPTD-II', 2),
      new _ChartAxis('UPTD-III', 5),
      new _ChartAxis('UPTD-IV', 4),
      new _ChartAxis('UPTD-V', 3),
      new _ChartAxis('UPTD-VI', 5),
      new _ChartAxis('SEKRE', 9),
      new _ChartAxis('J & K', 5),
      new _ChartAxis('JT', 2),
      new _ChartAxis('PPJ', 4),
      new _ChartAxis('PR', 11),
    ];

    return [
      new charts.Series<_ChartAxis, String>(
        id: "Highest Budget",
        domainFn: (_ChartAxis ca, _) => ca.organization,
        measureFn: (_ChartAxis ca, _) => ca.percentage,
        data: data,
        labelAccessorFn: (datum, index) => datum.organization,
      )
    ];
  }
}

class _ChartAxis {
  final String organization;
  final int percentage;

  _ChartAxis(this.organization, this.percentage);
}
