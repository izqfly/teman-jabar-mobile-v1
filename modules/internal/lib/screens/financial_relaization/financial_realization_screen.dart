import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_carousel_slider/carousel_slider_indicators.dart';
import 'package:flutter_carousel_slider/carousel_slider_transforms.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:sprintf/sprintf.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:common/widgets/detail_card.dart';

part 'package:internal/screens/financial_relaization/app_bar/carousel.dart';
part 'package:internal/screens/financial_relaization/app_bar/title.dart';

part 'package:internal/screens/financial_relaization/summary.dart';
part 'package:internal/screens/financial_relaization/charts/highest_budget_chart.dart';
part 'package:internal/screens/financial_relaization/charts/dbmpr_budget_chart.dart';

class FinancialRealizationScreen extends StatelessWidget {
  final _screen = FlutterScreenUtil();

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: _screen.widthDefault(context),
            height: 230,
            child: Stack(
              children: [
                _Carousel(),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                      left: 15, top: MediaQuery.of(context).padding.top + 15),
                  child: ClipOval(
                    child: Material(
                      color: Colors.white,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        child: SizedBox(
                            width: 30,
                            height: 30,
                            child: Icon(Icons.arrow_back)),
                        onTap: () => Modular.navigator.pop(),
                      ),
                    ),
                  ),
                ),
                _Title(),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _Summary(),
                    SizedBox(height: 25),
                    _HighestBudgetChart.withSampleData(),
                    SizedBox(height: 25),
                    DetailCard(
                      title: _getTitle(0),
                      description:
                          "Lihat data monitoring realisasi anggaran dibawah 1 Miliar.",
                      isHideTopDivider: false,
                      onTap: () => _navigateToDetail(_getTitle(0)),
                    ),
                    DetailCard(
                      title: _getTitle(1),
                      description:
                          "Lihat data monitoring realisasi anggaran 1 Miliar sampai dengan 20 Miliar.",
                      onTap: () => _navigateToDetail(_getTitle(1)),
                    ),
                    DetailCard(
                      title: _getTitle(2),
                      description:
                          "Lihat data monitoring realisasi anggaran diatas 20 Miliar.",
                      onTap: () => _navigateToDetail(_getTitle(2)),
                    ),
                    SizedBox(height: 25),
                    _DbmprBudgetChart.withSampleData(),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  String _getTitle(int index) {
    switch (index) {
      case 0:
        return "ANGGARAN < 1 M";
      case 1:
        return "ANGGARAN 1 M - 20 M";
      default:
        return "ANGGARAN > 20 M";
    }
  }

  void _navigateToDetail(dynamic data) {
    Modular.link.pushNamed(
      RouteName.ListMonitoringBudget,
      arguments: data,
    );
  }
}
