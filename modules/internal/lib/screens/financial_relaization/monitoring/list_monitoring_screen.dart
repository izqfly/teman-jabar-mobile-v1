import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';

class ListMonitoringBudget extends StatelessWidget {
  ListMonitoringBudget({Key key, this.title}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView.builder(
        itemCount: 5,
        itemBuilder: (context, index) => Card(
          margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                      color: Colors.grey[300],
                    ),
                  ),
                  Text(
                    "UPTD-1",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Colors.black54,
                    ),
                  )
                ],
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _RowlItem._(
                      title: "Anggaran",
                      value: "Rp. 316.145.101,0",
                    ),
                    SizedBox(height: 10),
                    _RowlItem._(
                      title: "Realisasi",
                      value: "Rp. 216.145.101,0",
                    ),
                    SizedBox(height: 10),
                    _RowlItem._(
                      title: "Persentasi",
                      value: "83.96 %",
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _RowlItem extends StatelessWidget {
  _RowlItem._({this.title, this.value});
  final String title;
  final String value;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: _screen.fontSize(14),
              fontWeight: FontWeight.bold,
              color: Colors.black45),
        ),
        Text(
          value,
          style: TextStyle(
            fontSize: _screen.fontSize(14),
            color: Colors.black,
          ),
          textAlign: TextAlign.right,
        )
      ],
    );
  }
}
