part of 'package:internal/screens/financial_relaization/financial_realization_screen.dart';

class _Summary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.only(top: 20, left: 10),
          child: Text(
            "Anggaran & Realisasi Keuangan",
            style: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(17),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(height: 5),
        Container(
          height: 160,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: (_, index) => Container(
              child: Container(
                width: 200,
                child: Card(
                  elevation: 3,
                  margin: const EdgeInsets.all(10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: _ItemCard._(index: index),
                ),
              ),
            ),
            itemCount: 3,
          ),
        )
      ],
    );
  }
}

class _ItemCard extends StatelessWidget {
  _ItemCard._({this.index});
  final int index;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            _getTitle(),
            style: TextStyle(
              fontSize: _screen.fontSize(15),
            ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          index == 2
              ? Container()
              : Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("(RP)"),
                    _getIcon(),
                  ],
                ),
          Text(index == 2 ? "100%" : "384.837.332.333"),
          Divider(thickness: 0.5, color: Colors.black45),
          Text(
            "Update : 08:00 WIB",
            style: TextStyle(
              fontSize: _screen.fontSize(12),
            ),
          ),
          Text(
            "22/10/2020",
            style: TextStyle(
              fontSize: _screen.fontSize(12),
            ),
          )
        ],
      ),
    );
  }

  String _getTitle() {
    switch (index) {
      case 0:
        return "Pagu Anggaran";
      case 1:
        return "Capaian Anggaran";
      default:
        return "Prosentase";
    }
  }

  Icon _getIcon() {
    switch (index) {
      case 0:
        return Icon(Icons.attach_money, color: Colors.orange[400], size: 18);
      case 1:
        return Icon(Icons.attach_money, color: Colors.blue[400], size: 18);
      default:
        return Icon(Icons.attach_money, color: Colors.green[400], size: 18);
    }
  }
}
