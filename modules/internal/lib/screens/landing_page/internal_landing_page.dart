import 'package:auth_bloc/bloc.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/bottom_bar.dart';
import 'package:common/widgets/user_account.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_carousel_slider/carousel_slider_indicators.dart';
import 'package:flutter_carousel_slider/carousel_slider_transforms.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:internal_bloc/contract_project/bloc.dart';
import 'package:sprintf/sprintf.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:shimmer/shimmer.dart';
import 'package:repository/models/notification/notification.dart' as modelNotif;
import 'package:repository/models/contract_project/contract_project.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:common/widgets/detail_card.dart';

part 'package:internal/screens/landing_page/menu/monitoring/monitoring_screen.dart';
part 'package:internal/screens/landing_page/menu/monitoring/app_bar/carousel.dart';
part 'package:internal/screens/landing_page/menu/monitoring/app_bar/costum_app_bar.dart';
part 'package:internal/screens/landing_page/menu/monitoring/app_bar/title.dart';
part 'package:internal/screens/landing_page/menu/monitoring/summary.dart';

part 'package:internal/screens/landing_page/menu/notification/notification_screen.dart';

part 'package:internal/screens/landing_page/widgets/app_drawer.dart';
part 'package:internal/screens/landing_page/widgets/body.dart';
part 'package:internal/screens/landing_page/widgets/bottom_bar.dart';

class InternalLandingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _InternalLandingPageState();
  }
}

class _InternalLandingPageState extends State<InternalLandingPage> {
  final _indexNotifier = ValueNotifier<int>(0);
  final _landingPageKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: _indexNotifier,
      builder: (context, value, child) => Scaffold(
        key: _landingPageKey,
        drawer: _AppDrawer(),
        appBar: value == 0
            ? null
            : AppBar(
                elevation: 0,
                title: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      "${BasePathAsset.icons}/common/temanjabar-logo.png",
                      height: AppBar().preferredSize.height / 1.5,
                    ),
                    SizedBox(width: 10),
                    Text(
                      "Teman Jabar",
                      style: TextStyle(
                          fontSize: FlutterScreenUtil().fontSize(18),
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    )
                  ],
                ),
              ),
        body: _Body._(index: value, landingPageKey: _landingPageKey),
        bottomNavigationBar: _BottomBar._(
          currentIndex: value,
          indexNotifier: _indexNotifier,
        ),
      ),
    );
  }
}
