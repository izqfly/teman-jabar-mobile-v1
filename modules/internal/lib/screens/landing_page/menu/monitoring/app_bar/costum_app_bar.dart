part of 'package:internal/screens/landing_page/internal_landing_page.dart';

class _CostumAppBar extends StatelessWidget {
  _CostumAppBar._({this.landingPageKey});
  final GlobalKey<ScaffoldState> landingPageKey;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      color: Theme.of(context).primaryColor.withOpacity(0.5),
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.symmetric(
        horizontal: 0,
        vertical: MediaQuery.of(context).padding.top,
      ),
      child: Row(
        children: [
          IconButton(
            padding: const EdgeInsets.all(0),
            icon: Icon(
              Icons.menu,
              color: Colors.white,
            ),
            onPressed: () => landingPageKey.currentState.openDrawer(),
          ),
          Image.asset(
            "${BasePathAsset.icons}/common/temanjabar-logo.png",
            height: AppBar().preferredSize.height / 1.5,
          ),
          SizedBox(width: 10),
          Text(
            "Teman Jabar",
            style: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(18),
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
