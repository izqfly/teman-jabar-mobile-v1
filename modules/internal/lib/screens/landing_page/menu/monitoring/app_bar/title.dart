part of 'package:internal/screens/landing_page/internal_landing_page.dart';

class _Title extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomLeft,
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "DBMPR JAWA BARAT",
            style: TextStyle(
              fontSize: _screen.fontSize(18),
              color: Colors.white,
              letterSpacing: 1,
            ),
          ),
          SizedBox(height: 5),
          Text(
            "Dinas Bina Marga dan Penataan Ruang",
            style: TextStyle(
              fontSize: _screen.fontSize(12),
              color: Colors.grey[300],
            ),
          ),
        ],
      ),
    );
  }
}
