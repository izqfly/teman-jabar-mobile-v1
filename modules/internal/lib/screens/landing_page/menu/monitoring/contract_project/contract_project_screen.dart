import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/services.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:internal_bloc/contract_project/contract_project_bloc.dart';
import 'package:repository/models/contract_project/contract_project.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:common/widgets/row_item.dart';
import 'package:common/widgets/column_item.dart';

class ContractProjectScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ContractProjectState();
  }
}

class _ContractProjectState extends State<ContractProjectScreen> {
  SearchBar _searchBar;
  final _searchNotifier = ValueNotifier<String>("");

  _ContractProjectState() {
    _searchBar = SearchBar(
      inBar: false,
      buildDefaultAppBar: buildAppBar,
      setState: setState,
      onSubmitted: (value) => _searchNotifier.value = value,
      clearOnSubmit: false,
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Title(color: Colors.white, child: Text("Proyek kontrak")),
      actions: [
        _searchBar.getSearchAction(context),
      ],
      elevation: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _searchBar.build(context),
      body: ValueListenableBuilder(
        valueListenable: _searchNotifier,
        builder: (__, value, _) => PagewiseListView<ContractProject>(
          key: UniqueKey(),
          pageSize: 10,
          padding: const EdgeInsets.only(top: 10),
          addAutomaticKeepAlives: false,
          pageFuture: (index) async => await _getData(context, index, value),
          itemBuilder: (context, entry, index) => Card(
            margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(8.0),
            ),
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ColumnItem(
                    title: "Nama Paket",
                    value: entry?.packageName ?? "",
                  ),
                  SizedBox(height: 10),
                  RowItem(
                    title: "Tanggal Mulai",
                    value: entry?.startDate ?? "",
                  ),
                  SizedBox(height: 10),
                  RowItem(
                    title: "Tanggal Akhir",
                    value: entry?.endDate ?? "",
                  ),
                  SizedBox(height: 10),
                  RowItem(
                    title: "Rencana",
                    value: "${entry.getPlanning()}",
                  ),
                  SizedBox(height: 10),
                  RowItem(
                    title: "Realisasi",
                    value: "${entry.getRealization()}",
                  ),
                  SizedBox(height: 10),
                  RowItem(
                    title: "Deviasi",
                    value: "${entry.getDeviation()}",
                  )
                ],
              ),
            ),
          ),
          noItemsFoundBuilder: (context) {
            return Center(child: Text("Tidak ada data yang ditemukan"));
          },
          loadingBuilder: (context) {
            return Center(
                child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
            ));
          },
        ),
      ),
    );
  }

  Future<List<ContractProject>> _getData(
    BuildContext context,
    int index,
    String query,
  ) async {
    try {
      print(query);
      int skip = index * 10;
      return await Modular.get<ContractProjectBloc>().fetchList(skip, query);
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <ContractProject>[];
  }
}
