import 'package:common/widgets/row_item.dart';
import 'package:common/widgets/column_item.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/services.dart';
import 'package:internal_bloc/contract_project/contract_project_bloc.dart';
import 'package:repository/models/contract_project/contract_project.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class DetailContractProjectScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Proyek kontrak"),
        brightness: Brightness.light,
      ),
      body: FutureBuilder<List<ContractProject>>(
        future: _getData(context),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.length > 0) {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  final data = snapshot.data[index];
                  return Card(
                    margin: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 10,
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8.0),
                                  topRight: Radius.circular(8.0),
                                ),
                                color: Colors.grey[300],
                              ),
                            ),
                            Text(
                              "${data.date ?? ""}",
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Colors.black54,
                              ),
                            )
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 10),
                              ColumnItem(
                                title: "Jenis Pekerjaan",
                                value: "${data?.typeWork ?? ""}",
                              ),
                              SizedBox(height: 10),
                              RowItem(
                                title: "Ruas Jalan",
                                value: "${data?.roads ?? ""}",
                              ),
                              SizedBox(height: 10),
                              RowItem(
                                title: "Status",
                                value: "${data?.status ?? ""}",
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              );
            } else {
              return Center(child: Text("Tidak ada data yang ditemukan"));
            }
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Future<List<ContractProject>> _getData(BuildContext context) async {
    try {
      return await Modular.get<ContractProjectBloc>().geDetail();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <ContractProject>[];
  }
}
