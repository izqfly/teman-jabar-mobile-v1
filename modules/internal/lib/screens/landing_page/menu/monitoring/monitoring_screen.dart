part of 'package:internal/screens/landing_page/internal_landing_page.dart';

class _MonitoringScreen extends StatelessWidget {
  _MonitoringScreen._({this.landingPageKey});
  final GlobalKey<ScaffoldState> landingPageKey;
  final _screen = FlutterScreenUtil();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: _screen.widthDefault(context),
          height: 230,
          child: Stack(
            children: [
              _Carousel(),
              _CostumAppBar._(landingPageKey: landingPageKey),
              _Title(),
            ],
          ),
        ),
        _Summary(),
        Container(
          padding: const EdgeInsets.only(top: 15, left: 10),
          child: Text(
            "Daftar Penyelesaian Pekerjaan Kontraktor",
            style: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(17),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        DetailCard(
          title: "Proyek Kontrak",
          description: "Lihat data proyek kontrak",
          isHideTopDivider: false,
          onTap: () => Modular.to.pushNamed(
            "${RouteName.BaseModule}${RouteName.LandingPage}${RouteName.MonitoringModule}${RouteName.ContractProject}",
          ),
        ),
        DetailCard(
          title: "Detail Proyek Kontrak",
          description: "Lihat data detail proyek kontak.",
          onTap: () => Modular.to.pushNamed(
            "${RouteName.BaseModule}${RouteName.LandingPage}${RouteName.MonitoringModule}${RouteName.DetailContractProject}",
          ),
        ),
      ],
    );
  }
}
