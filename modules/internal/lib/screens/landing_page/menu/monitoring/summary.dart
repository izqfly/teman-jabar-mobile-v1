part of 'package:internal/screens/landing_page/internal_landing_page.dart';

class _Summary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.only(top: 20, left: 10),
          child: Text(
            "Proyek Kontrak",
            style: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(17),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(height: 5),
        FutureBuilder(
          future: _getSummary(context),
          builder: (_, snapshot) {
            if (snapshot.hasData) {
              return Container(
                height: 110,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (_, index) => Container(
                    child: Container(
                      width: 180,
                      child: Card(
                        elevation: 3,
                        margin: const EdgeInsets.all(10),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: _ItemCard._(index: index, item: snapshot.data),
                      ),
                    ),
                  ),
                  itemCount: 3,
                ),
              );
            }
            return _Loading();
          },
        )
      ],
    );
  }

  Future<ContractProject> _getSummary(BuildContext context) async {
    try {
      return await Modular.get<ContractProjectBloc>().getSummary();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return new ContractProject();
  }
}

class _ItemCard extends StatelessWidget {
  _ItemCard._({this.index, this.item});
  final int index;
  final ContractProject item;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            _getTitle(),
            style: TextStyle(
              fontSize: _screen.fontSize(15),
            ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          _getIcon(),
          Text(
            _getValue(),
            style: TextStyle(
              color: _getColor(),
              fontSize: _screen.fontSize(14),
            ),
          )
        ],
      ),
    );
  }

  String _getTitle() {
    switch (index) {
      case 0:
        return "On Progress";
      case 1:
        return "Critical Contract";
      default:
        return "Finish";
    }
  }

  String _getValue() {
    switch (index) {
      case 0:
        return item.onProgress?.toString();
      case 1:
        return item.criticalContract?.toString();
      default:
        return item.finish?.toString();
    }
  }

  Icon _getIcon() {
    switch (index) {
      case 0:
        return Icon(Icons.trending_up, color: Colors.green[400], size: 18);
      case 1:
        return Icon(Icons.hourglass_full, color: Colors.orange[400], size: 18);
      default:
        return Icon(Icons.file_download, color: Colors.blue[400], size: 18);
    }
  }

  Color _getColor() {
    switch (index) {
      case 0:
        return Colors.green[400];
      case 1:
        return Colors.orange[400];
      default:
        return Colors.blue[400];
    }
  }
}

class _Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: Container(
        height: 110,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (_, index) => Container(
            child: Container(
              width: 180,
              child: Card(
                elevation: 3,
                margin: const EdgeInsets.all(10),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ),
          ),
          itemCount: 4,
        ),
      ),
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
    );
  }
}
