part of 'package:internal/screens/landing_page/internal_landing_page.dart';

class _AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        addAutomaticKeepAlives: false,
        children: <Widget>[
          _DrawerHeader(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List.generate(
              7,
              (index) => _DrawerItem._(
                index: index,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _DrawerHeader extends StatelessWidget {
  final _storage = FlutterLocalStorage();
  @override
  Widget build(BuildContext context) {
    return UserAccountsDrawerHeader(
      accountName: Text(_storage?.user?.fullname ?? ""),
      accountEmail: Text(_storage?.user?.email ?? "-"),
      currentAccountPicture: CircleAvatar(
        backgroundColor: Colors.white,
        child: Text(
          _storage?.user?.getFistCharacterFullname(),
          style: TextStyle(fontSize: 40.0),
        ),
      ),
    );
  }
}

class _DrawerItem extends StatelessWidget {
  _DrawerItem._({this.index});
  final int index;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          selected: index == 0,
          title: Text(_getTitle()),
          leading: Icon(_getIconData()),
          onTap: () {
            if (index == 6) {
              _showAlertDialog(context);
            } else {
              if (_getRouteName() != "") {
                Modular.to.pushNamed(
                  "${RouteName.BaseModule}${RouteName.LandingPage}${_getRouteName()}",
                );
              } else {
                Modular.navigator.pop();
              }
            }
          },
        ),
        index == 3 || index == 5
            ? Divider(color: Colors.black45, thickness: 0.5)
            : Container(),
      ],
    );
  }

  String _getRouteName() {
    switch (index) {
      case 1:
        return RouteName.StabilityRoad;
      case 2:
        return RouteName.PublicReportModule + RouteName.PublicReport;
      case 3:
        return RouteName.FinancialRealization;
      case 4:
        return RouteName.About;
      case 5:
        return RouteName.Settings;
      default:
        return "";
    }
  }

  String _getTitle() {
    switch (index) {
      case 0:
        return "Proyek Kontrak";
      case 1:
        return "Kemantapan Jalan";
      case 2:
        return "Laporan Kerusakan";
      case 3:
        return "Anggaran & Realisasi Keuangan";
      case 4:
        return "Tentang Kami";
      case 5:
        return "Pengaturan";
      default:
        return "Keluar";
    }
  }

  IconData _getIconData() {
    switch (index) {
      case 0:
        return Icons.dashboard;
      case 1:
        return Icons.beenhere;
      case 2:
        return Icons.report;
      case 3:
        return Icons.attach_money;
      case 4:
        return Icons.info;
      case 5:
        return Icons.settings_applications;
      default:
        return Icons.exit_to_app;
    }
  }

  void _showAlertDialog(BuildContext context) async {
    AlertDialog alert = AlertDialog(
      title: Text("Konfirmasi"),
      content: Text("Apakah anda ingin keluar dari Teman Jabar?"),
      actions: [
        FlatButton(
          child: Text(
            "Batal",
            style: TextStyle(color: Colors.black),
          ),
          onPressed: () => Modular.navigator.pop(),
        ),
        FlatButton(
          child: Text(
            "Keluar",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () async {
            await Modular.get<AuthBloc>().logout();
            Modular.navigator.pop();
            Modular.to.pushNamedAndRemoveUntil(
              "${RouteName.BaseModule}",
              (Route<dynamic> route) => false,
            );
          },
        ),
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
