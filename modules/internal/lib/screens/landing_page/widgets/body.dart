part of 'package:internal/screens/landing_page/internal_landing_page.dart';

class _Body extends StatelessWidget {
  _Body._({Key key, this.index, this.landingPageKey}) : super(key: key);
  final int index;
  final GlobalKey<ScaffoldState> landingPageKey;
  @override
  Widget build(BuildContext context) {
    switch (index) {
      case 1:
        return _Notificationcreen();
      case 2:
        return UserAccount();
      default:
        return _MonitoringScreen._(landingPageKey: landingPageKey);
    }
  }
}
