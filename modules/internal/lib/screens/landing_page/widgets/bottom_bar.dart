part of 'package:internal/screens/landing_page/internal_landing_page.dart';

class _BottomBar extends StatelessWidget {
  _BottomBar._({@required this.currentIndex, @required this.indexNotifier});
  final ValueNotifier indexNotifier;
  final int currentIndex;
  @override
  Widget build(BuildContext context) {
    return BottomBar(
      indexNotifier: indexNotifier,
      items: List.generate(3, (index) => _item(context, index)),
      currentIndex: currentIndex,
    );
  }

  BottomNavyBarItem _item(BuildContext context, int index) {
    return BottomNavyBarItem(
      icon: Icon(_getIcon(index)),
      title: Text(
        _getTitle(index),
        style: TextStyle(
          fontSize: FlutterScreenUtil().fontSize(13),
        ),
      ),
      inactiveColor: Colors.black45,
      activeColor: Theme.of(context).primaryColor,
      textAlign: TextAlign.center,
    );
  }

  String _getTitle(int index) {
    switch (index) {
      case 0:
        return "Monitoring";
      case 1:
        return "Notifikasi";
      default:
        return "Akun";
    }
  }

  IconData _getIcon(int index) {
    switch (index) {
      case 0:
        return Icons.dashboard;
      case 1:
        return Icons.notifications;
      default:
        return Icons.person;
    }
  }
}
