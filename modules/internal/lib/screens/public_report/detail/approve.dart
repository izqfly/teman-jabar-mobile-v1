part of 'package:internal/screens/public_report/detail/detail_public_report_screen.dart';

class _Approve extends StatelessWidget {
  final _bloc = Modular.get<ReportingStreetBloc>();
  @override
  Widget build(BuildContext context) {
    return PagewiseListView<Report>(
      pageSize: 10,
      addAutomaticKeepAlives: false,
      pageFuture: (index) async => await _getData(context, index),
      itemBuilder: (context, entry, index) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            title: Text("Nomor Pengaduan"),
            subtitle: Text(
              entry.reportNumber ?? "-",
              style: TextStyle(color: Colors.black45),
            ),
            trailing: IconButton(
              onPressed: () async => _onHandlerApprove(context, entry.id),
              icon: Icon(
                Icons.check,
                color: Colors.green[900],
              ),
            ),
          ),
          Divider(
            thickness: 0.5,
            color: Colors.black45,
          )
        ],
      ),
      noItemsFoundBuilder: (context) {
        return Center(child: Text("Tidak ada data yang ditemukan"));
      },
      loadingBuilder: (context) {
        return Center(
            child: CircularProgressIndicator(
          valueColor:
              AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
        ));
      },
    );
  }

  Future<List<Report>> _getData(BuildContext context, int index) async {
    try {
      int skip = index * 10;
      return await _bloc.findByStatus(skip, "Submitted");
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <Report>[];
  }

  Future<void> _onHandlerApprove(BuildContext context, int id) async {
    try {
      showDialog(context: context, child: LoadingDialog());
      await _bloc.approve(id);
      Navigator.pop(context);
      Modular.navigator.pop("success");
    } on PlatformException catch (e) {
      Navigator.pop(context);
      AlertMessage.showToast(context: context, message: e.message);
    }
  }
}
