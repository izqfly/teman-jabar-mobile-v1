import 'dart:async';

import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:repository/models/report/report.dart';

part 'package:internal/screens/public_report/detail/approve.dart';
part 'package:internal/screens/public_report/detail/progress.dart';
part 'package:internal/screens/public_report/detail/done.dart';

class DetailPublicReportScreen extends StatelessWidget {
  DetailPublicReportScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: title == "Approve"
          ? _Approve()
          : title == "Progress"
              ? _Progress()
              : _Done(),
    );
  }
}
