part of 'package:internal/screens/public_report/detail/detail_public_report_screen.dart';

class _Done extends StatelessWidget {
  final _bloc = Modular.get<ReportingStreetBloc>();
  @override
  Widget build(BuildContext context) {
    return PagewiseListView<Report>(
      pageSize: 10,
      addAutomaticKeepAlives: false,
      pageFuture: (index) async => await _getData(context, index),
      itemBuilder: (context, entry, index) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            title: Text("Nomor Pengaduan"),
            subtitle: Text(
              entry.reportNumber ?? "-",
              style: TextStyle(color: Colors.black45),
            ),
          ),
          Divider(
            thickness: 0.5,
            color: Colors.black45,
          )
        ],
      ),
      noItemsFoundBuilder: (context) {
        return Center(child: Text("Tidak ada data yang ditemukan"));
      },
      loadingBuilder: (context) {
        return Center(
            child: CircularProgressIndicator(
          valueColor:
              AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
        ));
      },
    );
  }

  Future<List<Report>> _getData(BuildContext context, int index) async {
    try {
      int skip = index * 10;
      return await _bloc.findByStatus(skip, "Done");
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <Report>[];
  }
}
