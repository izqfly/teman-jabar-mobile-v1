part of 'package:internal/screens/public_report/detail/detail_public_report_screen.dart';

class _Progress extends StatelessWidget {
  final _bloc = Modular.get<ReportingStreetBloc>();
  @override
  Widget build(BuildContext context) {
    return PagewiseListView<Report>(
      pageSize: 10,
      addAutomaticKeepAlives: false,
      pageFuture: (index) async => await _getData(context, index),
      itemBuilder: (context, entry, index) => GestureDetector(
        onTap: () => Modular.link.pushNamed(RouteName.DetailProgressReport, arguments: entry.id),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: Text("Nomor Pengaduan"),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    entry.reportNumber ?? "-",
                    style: TextStyle(color: Colors.black45),
                  ),
                  Text(
                    entry.description ?? "-",
                    style: TextStyle(color: Colors.black45),
                  )
                ],
              ),
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.black45,
                size: 18,
              ),
            ),
            Divider(
              thickness: 0.5,
              color: Colors.black45,
            )
          ],
        ),
      ),
      noItemsFoundBuilder: (context) {
        return Center(child: Text("Tidak ada data yang ditemukan"));
      },
      loadingBuilder: (context) {
        return Center(
            child: CircularProgressIndicator(
          valueColor:
              AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
        ));
      },
    );
  }

  Future<List<Report>> _getData(BuildContext context, int index) async {
    try {
      int skip = index * 10;
      return await _bloc.findByStatus(skip, "Progress");
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <Report>[];
  }
}
