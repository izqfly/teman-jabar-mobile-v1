import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:repository/models/report/report.dart';
import 'package:shimmer/shimmer.dart';
import 'package:cached_network_image/cached_network_image.dart';

class DetailProgressReportScreen extends StatelessWidget {
  DetailProgressReportScreen({Key key, this.id}) : super(key: key);
  final int id;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Progress"),
      ),
      body: FutureBuilder<List<Report>>(
        future: _getData(context),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return snapshot.data.isEmpty
                ? Center(
                    child: Text("Tidak ada data"),
                  )
                : Column(
                    children: [
                      Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: AlwaysScrollableScrollPhysics(),
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            final data = snapshot.data[index];
                            return Card(
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Container(
                                        height: 40,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(8.0),
                                            topRight: Radius.circular(8.0),
                                          ),
                                          color: Colors.grey[300],
                                        ),
                                      ),
                                      Text(
                                        data.date ?? "-",
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black54,
                                        ),
                                      )
                                    ],
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        _RowlItem._(
                                          title: "ID Petugas",
                                          value: data?.employeeNumber ?? "-",
                                        ),
                                        SizedBox(height: 10),
                                        _RowlItem._(
                                          title: "Petugas",
                                          value: data?.employeeName ?? "-",
                                        ),
                                        SizedBox(height: 10),
                                        _ColItem._(
                                          title: "Perkembangan",
                                          value: data?.progress ?? "-",
                                        ),
                                        SizedBox(height: 10),
                                        _RowlItem._(
                                          title: "Presentase",
                                          value: data?.getNicePercentage(),
                                        ),
                                        SizedBox(height: 10),
                                        _ColItem._(
                                          title: "Domentasi",
                                          child: CachedNetworkImage(
                                            imageUrl: data.documentationUrl,
                                            errorWidget: (_, __, ___) => Icon(
                                              Icons.broken_image,
                                              color: Colors.black54,
                                            ),
                                            fit: BoxFit.fitHeight,
                                            filterQuality: FilterQuality.high,
                                            height: 150,
                                            width: double.infinity,
                                            placeholder: (__, _) =>
                                                Shimmer.fromColors(
                                              child: Container(
                                                height: 150,
                                                width: double.infinity,
                                                color: Colors.white,
                                              ),
                                              baseColor: Colors.grey[300],
                                              highlightColor: Colors.grey[100],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      snapshot.data.isEmpty
                          ? Container()
                          : GestureDetector(
                              onTap: () => Modular.link.pushNamed(
                                  RouteName.FormProgressReport,
                                  arguments: id),
                              child: Container(
                                width:
                                    FlutterScreenUtil().widthDefault(context),
                                height: 50,
                                color: Colors.orange[900],
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Perbaharui Progress",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: FlutterScreenUtil()
                                                .fontSize(18)),
                                      ),
                                      SizedBox(width: 5),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                        size: 18,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                    ],
                  );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Future<List<Report>> _getData(BuildContext context) async {
    try {
      return await Modular.get<ReportingStreetBloc>().getByStatusOnProgress(id);
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <Report>[];
  }
}

class _RowlItem extends StatelessWidget {
  _RowlItem._({this.title, this.value});
  final String title;
  final String value;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: _screen.fontSize(14),
              fontWeight: FontWeight.bold,
              color: Colors.black45),
        ),
        Text(
          value ?? "-",
          style: TextStyle(
            fontSize: _screen.fontSize(14),
            color: Colors.black,
          ),
          textAlign: TextAlign.right,
        )
      ],
    );
  }
}

class _ColItem extends StatelessWidget {
  _ColItem._({this.title, this.value, this.child});
  final String title;
  final String value;
  final Widget child;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: _screen.fontSize(14),
              fontWeight: FontWeight.bold,
              color: Colors.black45),
        ),
        SizedBox(height: 5),
        child ??
            Text(
              value ?? "-",
              style: TextStyle(
                fontSize: _screen.fontSize(14),
                color: Colors.black,
              ),
              textAlign: TextAlign.right,
            )
      ],
    );
  }
}
