part of 'package:internal/screens/public_report/detail/progress/form_update_progress.dart';

class _DropdownEmployee extends StatelessWidget {
  final String _title = "Pegawai";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<DropdownResponse>>(
      initialData: <DropdownResponse>[],
      future: _getEmployee(context),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return CostumDropdownSearch<DropdownResponse>(
            compateFn: (item, selectedItem) => item?.id == selectedItem?.id,
            dropdownBuilder: (_, item, __) => Text(
              item?.name ?? "Pilih $_title",
            ),
            hintText: "Pilih $_title",
            itemAsString: (item) => item?.name,
            items: snapshot.data,
            onChanged: Modular.get<ReportingStreetBloc>().onChangedEmployee,
            popupItemBuilder: (_, item, selected) => ListTile(
              selected: selected,
              title: Row(
                children: <Widget>[
                  selected
                      ? Icon(
                          Icons.check,
                          color: Theme.of(context).primaryColor,
                        )
                      : Container(),
                  SizedBox(width: selected ? 7 : 0),
                  Expanded(child: Text(item?.name))
                ],
              ),
            ),
            selectedItem: DropdownResponse(),
            title: _title,
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(
                Theme.of(context).primaryColor),
          ),
        );
      },
    );
  }

  Future<List<DropdownResponse>> _getEmployee(BuildContext context) async {
    try {
      return await Modular.get<ReportingStreetBloc>().getEmployee();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <DropdownResponse>[];
  }
}
