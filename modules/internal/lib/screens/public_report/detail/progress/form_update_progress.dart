import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/form_input/costum_file_picker.dart';
import 'package:common/widgets/form_input/costum_form_field.dart';
import 'package:common/widgets/form_input/date_form_field_title.dart';
import 'package:common/widgets/loading_dialog.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:flutter/material.dart';
import 'package:repository/response/dropdown_response.dart';
import 'package:common/widgets/form_input/costum_dropdown_search.dart';

part 'package:internal/screens/public_report/detail/progress/dropdown_employee.dart';

class FormUpdateProgress extends StatelessWidget {
  FormUpdateProgress({Key key, this.id}) : super(key: key);
  final int id;
  final _bloc = Modular.get<ReportingStreetBloc>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Pembaharuan"),
        actions: [
          StreamBuilder(
            stream: _bloc.isValidUpdateProgressReport,
            builder: (context, snapshot) => IconButton(
              icon: Icon(
                Icons.check,
                color: snapshot.hasData && snapshot.data
                    ? Colors.white
                    : Colors.grey[300],
              ),
              onPressed: snapshot.hasData && snapshot.data
                  ? () async => _onHandlerSave(context)
                  : null,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              _DropdownEmployee(),
              CostumDateFormFiled(
                onChanged: _bloc.onChangedDate,
                title: "Tanggal",
                hintText: "Pilih Tanggal",
              ),
              CostumTextFormField(
                onChanged: _bloc.onChangedProgress,
                title: "Perkembangan",
                hintText: "Keterangan Perkembangan",
              ),
              CostumTextFormField(
                onChanged: _bloc.onChangedPercentage,
                title: "Presentase",
                isNumber: true,
                hintText: "Presentase Perkembangan",
              ),
              CostumFilePicker(
                title: "Dokumentasi",
                onChanged: _bloc.onChangedDocumentation,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onHandlerSave(BuildContext context) async {
    try {
      showDialog(context: context, child: LoadingDialog());
      await Modular.get<ReportingStreetBloc>().updateOnProgressReport(id);
      Navigator.pop(context);
      String route =
          "${RouteName.BaseModule}${RouteName.LandingPage}${RouteName.PublicReportModule}${RouteName.PublicReport}";
      Modular.to.pushNamedAndRemoveUntil(
        route,
        (Route<dynamic> route) => false,
      );
    } on PlatformException catch (e) {
      Navigator.pop(context);
      AlertMessage.showToast(context: context, message: e.message);
    }
  }
}
