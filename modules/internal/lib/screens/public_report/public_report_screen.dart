import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:flutter_modular/flutter_modular.dart';

class PublicReportScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Laporan Masyarakat"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _ProgressCard._(
              title: "Approve",
              description:
                  "Lihat laporan masyarakat yang menunggu persetujuan.",
            ),
            _ProgressCard._(
              title: "Progress",
              description: "Lihat laporan masyarakat yang sedang dalam proses.",
            ),
            _ProgressCard._(
              title: "Done",
              description: "Lihat laporan masyarakat yang sudah selesai.",
            ),
          ],
        ),
      ),
    );
  }
}

class _ProgressCard extends StatelessWidget {
  _ProgressCard._({this.description, this.title, this.isHideTopDivider = true});

  final String title;
  final String description;
  final bool isHideTopDivider;
  final _screen = FlutterScreenUtil();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        isHideTopDivider
            ? Container()
            : Divider(thickness: 0.5, color: Colors.black45),
        FlatButton(
          onPressed: () async {
            final result = await Modular.link.pushNamed(
              RouteName.DetailPublicReport,
              arguments: title,
            );

            if (result != null) {
              AlertMessage.showToast(
                context: context,
                message: "Data berhasil disimpan",
                textColor: Colors.white,
                backgroundColor: Colors.black45,
              );
            }
          },
          padding: const EdgeInsets.symmetric(horizontal: 10),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 3),
                    Text(
                      description,
                      style: TextStyle(
                        fontSize: _screen.fontSize(14),
                        color: Colors.black45,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(width: 20),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.black45,
                size: 16,
              ),
            ],
          ),
        ),
        Divider(thickness: 0.5, color: Colors.black45),
      ],
    );
  }
}
