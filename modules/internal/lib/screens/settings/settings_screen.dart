import 'package:auth_bloc/auth_bloc.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/user_account.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SettingsScreen extends StatelessWidget {
  final _bloc = Modular.get<AuthBloc>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pengaturan"),
        elevation: 0,
      ),
      body: UserAccount(
        isSetting: true,
        stream: _bloc.isValidChangePassword,
        passwordNew: _bloc.onChangedPasswordNew,
        passwordOld: _bloc.onChangedPassword,
        confirmPassword: _bloc.onChangedConfirmPassword,
        onTap: (startLoading, stopLoading) {
          _onHandlerChangePassword(context, startLoading, stopLoading);
        },
      ),
    );
  }

  Future<void> _onHandlerChangePassword(
    BuildContext context,
    Function startLoading,
    Function stopLoading,
  ) async {
    try {
      startLoading();
      await _bloc.changePassword();
      stopLoading();
      _bloc..onChangedConfirmPassword(null);
      _bloc.onChangedPassword(null);
      _bloc.onChangedPasswordNew(null);
      AlertMessage.showToast(
        context: context,
        message: "Kata sandi berhasil diubah",
        textColor: Colors.white,
        backgroundColor: Colors.black45.withOpacity(0.5),
      );
    } on PlatformException catch (e) {
      stopLoading();
      _bloc..onChangedConfirmPassword(null);
      _bloc.onChangedPassword(null);
      _bloc.onChangedPasswordNew(null);
      AlertMessage.showToast(context: context, message: e.message);
    }
  }
}
