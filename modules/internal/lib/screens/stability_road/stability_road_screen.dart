import 'package:common/utils/color_util.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:internal_bloc/stability_road/stability_road_bloc.dart';
import 'package:location_permissions/location_permissions.dart' as lp;
import 'package:repository/models/stability_road/stability_road.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/models/chart/chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:common/widgets/row_item.dart';

part 'package:internal/screens/stability_road/widgets/dialog_marker.dart';
part 'package:internal/screens/stability_road/widgets/pie_chart.dart';

class StabilityRoadScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StabilityRoadState();
  }
}

class _StabilityRoadState extends State<StabilityRoadScreen> {
  Widget _body;

  void initState() {
    super.initState();
    _body = Center(
        child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(
        ColorUtil().parseHexColor("#3355b4"),
      ),
    ));
    _checkLocationPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Kemantapan Jalan"),
      ),
      body: Stack(
        children: [
          _body,
          FutureBuilder<StabilityRoad>(
            future: _onHandlerGetRecapitulation(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return SlidingUpPanel(
                  collapsed: Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Icon(
                      Icons.arrow_circle_up,
                      size: 30,
                      color: Colors.black45,
                    ),
                  ),
                  header: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Rekapitulasi Data Kemantapan Jalan",
                          style: TextStyle(
                            fontSize: FlutterScreenUtil().fontSize(16),
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text("Dengan Total Luas: ${snapshot.data.roadArea} m2"),
                      ],
                    ),
                  ),
                  panel: snapshot.data?.roadConditions == null
                      ? Container()
                      : Center(
                          child: SizedBox(
                            width: double.infinity,
                            height: 250,
                            child: new charts.PieChart(
                              _chartData(snapshot.data.roadConditions.asList()),
                              animate: false,
                              defaultRenderer: new charts.ArcRendererConfig(
                                arcWidth: 100,
                                arcRendererDecorators: [
                                  new charts.ArcLabelDecorator()
                                ],
                              ),
                            ),
                          ),
                        ),
                );
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  void _checkLocationPermission() async {
    final statusFuture = await lp.LocationPermissions().checkPermissionStatus();

    if (statusFuture != lp.PermissionStatus.granted) {
      lp.PermissionStatus ps =
          await lp.LocationPermissions().requestPermissions(
        permissionLevel: lp.LocationPermissionLevel.locationWhenInUse,
      );
      if (ps != lp.PermissionStatus.granted) {
        return;
      }
      _setMap(ps);
    } else {
      _setMap(statusFuture);
    }
  }

  void _setMap(lp.PermissionStatus permissionStatus) {
    Future.delayed(const Duration(seconds: 2), () async {
      if (mounted) {
        final markers = await _setMarker();
        setState(() {
          _body = permissionStatus == lp.PermissionStatus.granted
              ? GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(
                      -7.090911,
                      107.668887,
                    ),
                    zoom: 8,
                  ),
                  markers: markers,
                )
              : Container();
        });
      }
    });
  }

  Future<Set<Marker>> _setMarker() async {
    final result = await _onHandlerFetchList();
    Set<Marker> markers = {};
    for (int i = 0; i < result.length; i++) {
      markers.add(Marker(
        markerId: MarkerId(i.toString()),
        position: LatLng(result[i].latitude, result[i].longitude),
        icon: await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5),
          '${BasePathAsset.icons}/markers/road_repair.png',
        ),
        infoWindow: InfoWindow(title: result[i].description),
        onTap: () => showDialog(
          context: context,
          barrierDismissible: true,
          child: _DialogMarker._(id: result[i].id),
        ),
      ));
    }

    return markers;
  }

  List<charts.Series<Chart, String>> _chartData(List<Chart> data) {
    return [
      new charts.Series<Chart, String>(
        id: "Kondisi Jalan",
        domainFn: (Chart ca, _) => ca.domain,
        measureFn: (Chart ca, _) => ca.measure,
        data: data,
        labelAccessorFn: (datum, index) => "${datum.domain}",
      )
    ];
  }

  Future<List<StabilityRoad>> _onHandlerFetchList() async {
    try {
      return await Modular.get<StabilityRoadBloc>().fetchList();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: null, message: e.message);
    }
    return <StabilityRoad>[];
  }

  Future<StabilityRoad> _onHandlerGetRecapitulation() async {
    try {
      return await Modular.get<StabilityRoadBloc>().getRecapitulation();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return new StabilityRoad();
  }
}
