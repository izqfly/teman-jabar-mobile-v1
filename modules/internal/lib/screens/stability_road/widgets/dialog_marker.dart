part of 'package:internal/screens/stability_road/stability_road_screen.dart';

class _DialogMarker extends StatelessWidget {
  _DialogMarker._({this.id});
  final int id;
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Container(
        height: _screen.height(360),
        width: _screen.widthDefault(context),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              FutureBuilder<StabilityRoad>(
                future: _onHandlerFindByID(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    final data = snapshot.data;
                    return Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RowItem(
                              title: "No. Ruas",
                              value: data.roadSectionNumber ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Sub Ruas",
                              value: data?.subRoadSection ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Bulan",
                              value: "${data?.month ?? ""}",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Tahun",
                              value: "${data.year ?? ""}",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Kota/Kabupaten",
                              value: data?.city ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Latitude Awal",
                              value: data?.originLat ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Longitude Awal",
                              value: data?.originLng ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Latitude Akhir",
                              value: data?.destLat ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Latitude Akhir",
                              value: data?.destLng ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "Keterangan",
                              value: data?.description ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "SPP/SUP",
                              value: data?.spp ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            RowItem(
                              title: "UPTD",
                              value: data?.uptd ?? "",
                            ),
                            Divider(thickness: 0.5, color: Colors.black45),
                            SizedBox(height: 20),
                            _PieChart(
                              data.roadConditions.asList(),
                              "${data?.roadArea}",
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  return Container();
                },
              ),
              SizedBox(height: 10),
              ButtonTheme(
                minWidth: double.infinity,
                height: 40.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: () => Navigator.pop(context, false),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(24.0)),
                  child: Text(
                    "TUTUP",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                      letterSpacing: 0.3,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<StabilityRoad> _onHandlerFindByID() async {
    try {
      return await Modular.get<StabilityRoadBloc>().findByID(id);
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: null, message: e.message);
    }
    return new StabilityRoad();
  }
}
