part of 'package:internal/screens/stability_road/stability_road_screen.dart';

class _PieChart extends StatelessWidget {
  final bool animate;
  final String roadArea;
  final List<Chart> data;

  _PieChart(this.data, this.roadArea, {this.animate});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Kondisi Jalan",
                style: TextStyle(
                  fontSize: FlutterScreenUtil().fontSize(17),
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text(
                "Dari Luas Jalan $roadArea m2",
                style: TextStyle(
                  fontSize: FlutterScreenUtil().fontSize(14),
                ),
              )
            ],
          ),
        ),
        data.length > 0
            ? SizedBox(
                width: double.infinity,
                height: 250,
                child: new charts.PieChart(
                  _chartData(),
                  animate: animate,
                  defaultRenderer: new charts.ArcRendererConfig(
                    arcWidth: 100,
                    arcRendererDecorators: [new charts.ArcLabelDecorator()],
                  ),
                ),
              )
            : Container()
      ],
    );
  }

  List<charts.Series<Chart, String>> _chartData() {
    return [
      new charts.Series<Chart, String>(
        id: "Kondisi Jalan",
        domainFn: (Chart ca, _) => ca.domain,
        measureFn: (Chart ca, _) => ca.measure,
        data: data,
        labelAccessorFn: (datum, index) => "${datum.domain} ${datum.measure}%",
      )
    ];
  }
}
