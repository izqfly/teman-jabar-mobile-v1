import 'dart:async';

import 'package:auth_bloc/auth_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:common/helper/fcm.dart';
import 'package:internal/modules/constants/route_name.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class WebViewScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _WebViewState();
  }
}

class _WebViewState extends State<WebViewScreen> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  String _url = "";

  void initState() {
    super.initState();
    Fcm().initializeNotification(context);
  }

  void dispose() {
    flutterWebViewPlugin.dispose();
    flutterWebViewPlugin.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    flutterWebViewPlugin.onUrlChanged.listen((String url) async {
      _url = url;
      print("URL NYA UDAH GANTI");
      print("UEREL : " + _url);
      if (url == "http://124.81.122.131/temanjabar/public/login"){
        await Modular.get<AuthBloc>().logout();
        Modular.navigator.pop();
        Modular.to.pushNamedAndRemoveUntil(
          "${RouteName.BaseModule}",
              (Route<dynamic> route) => false,
        );
      }
    });
    return Scaffold(
      body: Stack(
        children: [
          Container(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 25,
                  color: Color.fromARGB(255, 38, 53, 68),
                ),
                Expanded(
                  child: WebviewScaffold(
                    url: _getUrl(),
                    withJavascript: true,
                    withLocalStorage: true,
                    geolocationEnabled: true,
                    ignoreSSLErrors: true,
                    withZoom: false,
                    userAgent: _userAgent(),
                    scrollBar: false,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _nowUrl() {
    String _url;
    flutterWebViewPlugin.onUrlChanged.listen((String url) {
      _url = url;
    });
    print("UEREL : " + _url);
    return _url;
  }

  String _userAgent() {
    return 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';
  }

  String _getUrl() {
    final param = FlutterLocalStorage().user.encryptedId;
    print(param);
    //!URL
    // final url =
    //     "http://nextcloud.talikuat-bima-jabar.com/temanjabar/public/forced-login/$param";

    //!USING IP
    final url = "http://124.81.122.131/temanjabar/public/forced-login/$param";
    
    return url;
  }

  void _showAlertDialog(BuildContext context) async {
    flutterWebViewPlugin.hide();
    AlertDialog alert = AlertDialog(
      title: Text("Konfirmasi"),
      content: Text("Apakah anda akan logout akun?"),
      actions: [
        FlatButton(
          child: Text(
            "Logout",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () async {
            await Modular.get<AuthBloc>().logout();
            Modular.navigator.pop();
            Modular.to.pushNamedAndRemoveUntil(
              "${RouteName.BaseModule}",
              (Route<dynamic> route) => false,
            );
          },
        ),
        FlatButton(
          child: Text(
            "Batal",
            style: TextStyle(color: Colors.black),
          ),
          onPressed: () {
            Modular.navigator.pop();
            flutterWebViewPlugin.show();
          },
        ),
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
