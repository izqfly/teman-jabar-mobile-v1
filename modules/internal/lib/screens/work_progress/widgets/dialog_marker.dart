part of 'package:internal/screens/work_progress/work_progress_screen.dart';

class _DialogMarker extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Container(
        height: _screen.height(360),
        width: _screen.width(100),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ColumnItem(
                        title: "Nama Paket",
                        value:
                            "Paket Pekerjaan Peningkatan Jalan Ruas Jalan Cibadak - Cikidang - Pelabuhan Ratu",
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      RowItem(
                        title: "Status",
                        value: "On Progress",
                        child: _TextStatus._(
                          value: "On Progress",
                        ),
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      RowItem(
                        title: "Tanggal",
                        value: "10 Sep 2020",
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      RowItem(
                        title: "Jenis Pekerjaan",
                        value: "Hotmix",
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      ColumnItem(
                        title: "Ruas Jalan",
                        value: "Cibadak - Cikadang - Pelabuhan Ratu",
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      ColumnItem(
                        title: "Lokasi",
                        value: "Km. Jkt. 100+500 - Km. Jkt. 101+300",
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      RowItem(
                        title: "Rencana",
                        value: "37.3470%",
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      RowItem(
                        title: "Realisasi",
                        value: "60.3470%",
                      ),
                      Divider(thickness: 0.5, color: Colors.black45),
                      RowItem(
                        title: "Deviasi",
                        value: "20.3470%",
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10),
              ButtonTheme(
                minWidth: double.infinity,
                height: 40.0,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: () => Navigator.pop(context, false),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(24.0)),
                  child: Text(
                    "TUTUP",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                      letterSpacing: 0.3,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _TextStatus extends StatelessWidget {
  _TextStatus._({this.value});

  final String value;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(24),
        color: _getColorStatus(value),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Text(
          value,
          style: TextStyle(
            color: Colors.white,
            fontSize: FlutterScreenUtil().fontSize(12),
          ),
          textAlign: TextAlign.right,
        ),
      ),
    );
  }

  Color _getColorStatus(String status) {
    switch (status) {
      case "On Progress":
        return Colors.green[400];
      default:
        return Colors.blue[400];
    }
  }
}
