import 'package:common/utils/color_util.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:location_permissions/location_permissions.dart' as lp;
import 'package:common/widgets/row_item.dart';
import 'package:common/widgets/column_item.dart';

part 'package:internal/screens/work_progress/widgets/dialog_marker.dart';

class WorkProgressScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _WorkProgressnState();
  }
}

class _WorkProgressnState extends State<WorkProgressScreen> {
  Widget _body;

  void initState() {
    super.initState();
    _body = Center(
        child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(
        ColorUtil().parseHexColor("#3355b4"),
      ),
    ));
    _checkLocationPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Progress Pekerjaan"),
      ),
      body: _body,
    );
  }

  void _checkLocationPermission() async {
    final statusFuture = await lp.LocationPermissions().checkPermissionStatus();

    if (statusFuture != lp.PermissionStatus.granted) {
      lp.PermissionStatus ps =
          await lp.LocationPermissions().requestPermissions(
        permissionLevel: lp.LocationPermissionLevel.locationWhenInUse,
      );
      if (ps != lp.PermissionStatus.granted) {
        return;
      }
      _setMap(ps);
    } else {
      _setMap(statusFuture);
    }
  }

  void _setMap(lp.PermissionStatus permissionStatus) {
    Future.delayed(const Duration(seconds: 2), () async {
      if (mounted) {
        final markers = await _setMarker();
        setState(() {
          _body = permissionStatus == lp.PermissionStatus.granted
              ? GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(
                      -6.902481,
                      107.61881,
                    ),
                    zoom: 18,
                  ),
                  markers: markers,
                )
              : Image.asset('${BasePathAsset.icons}/markers/marker-office.png');
        });
      }
    });
  }

  Future<Set<Marker>> _setMarker() async {
    return <Marker>[
      Marker(
        markerId: MarkerId("finish"),
        position: LatLng(-6.903481, 107.61881),
        icon: await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5),
          '${BasePathAsset.icons}/markers/marker-office.png',
        ),
        infoWindow: InfoWindow(title: "Hotmix"),
        onTap: () => showDialog(
          context: context,
          barrierDismissible: true,
          child: _DialogMarker(),
        ),
      ),
      Marker(
        markerId: MarkerId("off progress"),
        position: LatLng(-6.902081, 107.61881),
        icon: await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5),
          '${BasePathAsset.icons}/markers/marker-office.png',
        ),
        infoWindow: InfoWindow(title: "Box Culvert"),
        onTap: () => showDialog(
          context: context,
          barrierDismissible: true,
          child: _DialogMarker(),
        ),
      ),
    ].toSet();
  }
}
