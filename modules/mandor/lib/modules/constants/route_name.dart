class RouteName {
  static const BaseModule = "/auth";
  static const LandingPage = "/mandor-landing-page";
  static const EditPage = "/edit";
}
