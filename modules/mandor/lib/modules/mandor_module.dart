import 'package:flutter_modular/flutter_modular.dart';
import 'package:mandor/screens/landing_page/mandor_landing_page.dart';
import 'package:mandor/screens/detail_task/detail_task.dart';
import 'package:public_bloc/input_task/bloc.dart';
import 'package:public_bloc/material/bloc.dart';

class MandorModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind<InputTaskBloc>((i) => InputTaskService()),
        Bind<MaterialBloc>((i) => MaterialService()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          "/",
          child: (_, args) => MandorLandingPage(),
          transition: _transitionType,
        ),
        ModularRouter(
          "/edit",
          child: (_, args) => DetailTask(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.rightToLeft;
}
