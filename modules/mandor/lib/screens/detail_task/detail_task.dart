import 'dart:io';
import 'dart:ui';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:mandor/screens/edit_material/edit_material.dart';
import 'package:mandor/screens/edit_task/edit_task.dart';
import 'package:public_bloc/input_task/bloc.dart';

class DetailTask extends StatefulWidget {
  DetailTask({this.data});

  final data;

  @override
  _DetailTaskState createState() => _DetailTaskState(data);
}

class _DetailTaskState extends State<DetailTask> {
  _DetailTaskState(this.data);

  final data;

  final _bloc = Modular.get<InputTaskBloc>();

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    FlutterStatusbarcolor.setNavigationBarColor(Colors.transparent);
    FlutterStatusbarcolor.setNavigationBarWhiteForeground(false);
    return Scaffold(
      bottomNavigationBar: Container(
        height: 70,
        color: Colors.white,
        child: _buildEditButton(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                height: 85,
                color: Colors.indigo,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 65, left: 24),
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 8),
                    child: Text(
                      data.id,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: FlutterScreenUtil().fontSize(16),
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(50),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 0), // changes position of shadow
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    _builtDetails("Tanggal :", data.tanggal),
                    _builtDetails("SUP :", data.sup),
                    _builtDetails("Ruas Jalan :", data.ruas),
                    _builtDetails("Jenis Pekerjaan :", data.jenis),
                    _builtDetails("Paket :", data.paket),
                    _builtDetails("Lokasi :", data.lokasi),
                    _builtDetails("Panjang Jalan :", data.panjang + " meter"),
                    _builtDetails("Peralatan :", data?.alat ?? ""),
                    _builtDetails("Jumlah Pekerja :", data.pekerja.toString()),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildEditButton (){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: FlatButton(
              child: Container(
                child: Center(
                  child: Text(
                    "Bahan Material",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: FlutterScreenUtil().fontSize(16),
                    ),
                  ),
                ),
                height: 50,
              ),
              color: Colors.amber[400],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              onPressed: () async {
                try {
                  final result = await InternetAddress.lookup('google.com');
                  if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                    print('connected');
                    final result = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => EditMaterial(data: data,)),
                    );
                    if (result == "Berhasil Menyimpan" || result == "Berhasil Memperbarui"){
                      Navigator.pop(context, "Berhasil Menyimpan");
                    }
                  }
                } on SocketException catch (_) {
                  showDialog(
                      context: context,
                      builder: (context) => new AlertDialog(
                        title: Text("Peringatan"),
                        content:
                        Text("Anda tidak memiliki koneksi internet"),
                        actions: [
                          FlatButton(
                            child: Text(
                              "OK",
                              style: TextStyle(color: Colors.blue),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      ));
                }
              },
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: FlatButton(
              child: Container(
                child: Center(
                  child: Text(
                    "Edit",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: FlutterScreenUtil().fontSize(16),
                    ),
                  ),
                ),
                height: 50,
              ),
              color: Colors.indigo,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              onPressed: () {
                _goEdit();
              },
            ),
          ),
          SizedBox(width: 10),
          FlatButton(
            child: Container(
              child: Center(
                child: Icon(Icons.delete_outline_rounded),
              ),
              height: 50,
            ),
            color: Colors.red,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => new AlertDialog(
                    title: Text("Konfirmasi"),
                    content: Text(
                        "Apakah anda ingin menghapus data?"),
                    actions: [
                      FlatButton(
                        child: Text(
                          "Hapus",
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () async {
                          Navigator.pop(context);
                          showDialog(context: context, child: LoadingDialog());
                          int success = await _bloc.delete(data.id);
                          if (success == 0) {
                            Navigator.pop(context);
                            Navigator.pop(context, "deleted");
                          }
                        },
                      ),
                      FlatButton(
                        child: Text(
                          "Batal",
                          style: TextStyle(color: Colors.black),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ));
            },
          )
        ],
      ),
    );
  }

  void _goEdit() async{
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EditTask(data: data)),
    );
    if(result == "updated"){
      Navigator.pop(context, "updated");
    }
  }

  Widget _builtDetails(String title, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                ),
              ),
              SizedBox(height: 10),
              Text(
                value,
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: FlutterScreenUtil().fontSize(16),
                ),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
        Divider(
          color: Colors.grey,
        )
      ],
    );
  }
}
