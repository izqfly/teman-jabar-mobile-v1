import 'dart:io';

import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:repository/sqflite/crud_sqflite.dart';
import 'package:repository/sqflite/model_task.dart';
import 'package:common/widgets/loading_dialog.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/services.dart';
import 'package:public_bloc/input_task/bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

class NotUploaded extends StatefulWidget {
  NotUploaded({Key key}) : super(key: key);

  @override
  _NotUploadedState createState() => _NotUploadedState();
}

class _NotUploadedState extends State<NotUploaded> {
  CrudSqfLite dbHelper = CrudSqfLite();
  Future<List<ModelTask>> _getUpdate;
  final _bloc = Modular.get<InputTaskBloc>();

  @override
  void initState() {
    super.initState();
    updateListView();
  }

  void updateListView() {
    setState(() {
      _getUpdate = dbHelper.getTaskList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: FutureBuilder<List<ModelTask>>(
              future: _getUpdate,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                        children: snapshot.data
                            .map((todo) => _buildItem(context, todo))
                            .toList()),
                  );
                } else if(snapshot.data == null){
                  return Center(
                      child: Text(
                          "Data Pekerjaan Kosong"));
                }
                return Container();
              }),
        ),
      ],
    );
  }

  Widget _buildItem(BuildContext context, data) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Container(
            color: Colors.grey[100],
            child: Material(
              color: Colors.transparent,
              child: IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      width: 10,
                      color: Colors.red,
                    ),
                    Expanded(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data.jenis_pekerjaan,
                            style: TextStyle(
                              fontSize: FlutterScreenUtil().fontSize(14),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            data.nama_paket,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: FlutterScreenUtil().fontSize(14),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            data.ruas_jalan,
                            style: TextStyle(
                              fontSize: FlutterScreenUtil().fontSize(14),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            data.lokasi ?? "",
                            style: TextStyle(
                                fontSize: FlutterScreenUtil().fontSize(14),
                                color: Colors.indigo),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            data.tanggal,
                            style: TextStyle(
                                fontSize: FlutterScreenUtil().fontSize(14),
                                color: Colors.indigo),
                          ),
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(18),
                                child: Container(
                                  height: 36,
                                  width: 36,
                                  color: Colors.red,
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () async {
                                        showDialog(
                                            context: context,
                                            builder: (context) => new AlertDialog(
                                              title: Text("Konfirmasi"),
                                              content: Text(
                                                  "Apakah anda ingin menghapus data?"),
                                              actions: [
                                                FlatButton(
                                                  child: Text(
                                                    "Hapus",
                                                    style: TextStyle(
                                                      color: Theme.of(context).primaryColor,
                                                      fontWeight: FontWeight.bold,
                                                    ),
                                                  ),
                                                  onPressed: () async {
                                                    Navigator.pop(context);
                                                    int result = await dbHelper.delete(data);
                                                    if (result > 0){
                                                      updateListView();
                                                    }
                                                  },
                                                ),
                                                FlatButton(
                                                  child: Text(
                                                    "Batal",
                                                    style: TextStyle(color: Colors.black),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            ));
                                      },
                                      child: Icon(Icons.delete_rounded, color: Colors.white, size: 14,),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 10),
                              ClipRRect(
                                borderRadius: BorderRadius.circular(18),
                                child: Container(
                                  height: 36,
                                  width: 36,
                                  color: Colors.indigoAccent,
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () async {
                                        try {
                                          final result = await InternetAddress.lookup('google.com');
                                          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                                            print('connected');
                                            _onHandleSave(context, data);
                                          }
                                        } on SocketException catch (_) {
                                          showDialog(
                                              context: context,
                                              builder: (context) => new AlertDialog(
                                                title: Text("Peringatan"),
                                                content: Text(
                                                    "Anda tidak memiliki koneksi internet"),
                                                actions: [
                                                  FlatButton(
                                                    child: Text(
                                                      "OK",
                                                      style: TextStyle(color: Colors.blue),
                                                    ),
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                ],
                                              ));
                                        }
                                      },
                                      child: Icon(Icons.cloud_upload_rounded, color: Colors.white, size: 14,),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  Future<void> _onHandleSave(BuildContext context, data) async {
    try {
      showDialog(context: context, child: LoadingDialog());
      int success = await _bloc.saveDraft(data);
      if (success == 0) {
        int result = await dbHelper.delete(data);
        if (result > 0){
          updateListView();
        }
        Navigator.pop(context);
      }
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message, duration: 3);
      Navigator.pop(context);
    }
  }

}
