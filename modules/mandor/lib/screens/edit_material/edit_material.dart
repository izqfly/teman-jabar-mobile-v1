//import 'dart:html';
import 'package:common/widgets/loading_dialog.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mandor/screens/edit_material/widget/dynamic_form.dart';
import 'package:mandor/screens/input_task/widget/custom_text_field.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public_bloc/material/bloc.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/form_input/costum_rounded_dropdown_search.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:repository/response/material_response.dart';

part 'package:mandor/screens/edit_material/widget/dropdown_bahan.dart';

part 'package:mandor/screens/edit_material/widget/dropdown_satuan.dart';

class EditMaterial extends StatefulWidget {
  EditMaterial({Key key, this.data}) : super(key: key);

  final data;

  @override
  _EditMaterialState createState() => _EditMaterialState(data);
}

class _EditMaterialState extends State<EditMaterial> {
  final _bloc = Modular.get<MaterialBloc>();
  bool isUpdated = false;
  bool isLoading = true;
  var data;
  String peralatan;

  _EditMaterialState(this.data);

  _getData() async {
    peralatan = data?.alat ?? "";
    var result = await _bloc.getData(data.id);
    if (result != null) {
      setState(() {
        isUpdated = true;
        isLoading = false;
        data = result;
      });
    } else {
      setState(() {
        isUpdated = false;
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _bloc.init();
    _getData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    return Scaffold(
      backgroundColor: Colors.white,
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    controller: ModalScrollController.of(context),
                    child: Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 50,
                            ),
                            Text(
                              "Edit Bahan Material",
                              style: TextStyle(
                                  fontSize: FlutterScreenUtil().fontSize(20),
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                CostumTextField(
                                  title: "No. Pekerjaan",
                                  readOnly: true,
                                  initialValue: data.id,
                                  onChanged: _bloc.onChangeId(data.id),
                                ),
                                CostumTextField(
                                  title: "Tanggal",
                                  initialValue: data.tanggal,
                                  readOnly: true,
                                ),
                                CostumTextField(
                                  title: "Jenis Pekerjaan",
                                  readOnly: true,
                                  initialValue: data.jenis,
                                  onChanged: _bloc.onChangJenis(data.jenis),
                                ),
                                CostumTextField(
                                  title: "Peralatan",
                                  hintText: "Peralatan yang digunakan",
                                  initialValue: peralatan,
                                  onChanged: _bloc.onChangeAlat,
                                ),
                                //DynamicForm(),
                                _builtThreeInput(
                                  "1",
                                  isUpdated ? data?.jumlah1 ?? null : "0",
                                  isUpdated
                                      ? data?.bahan1 ?? null
                                      : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan1 ?? null : 'Satuan',
                                  _bloc.onChangeBahan1,
                                  _bloc.onChangeJumlah1,
                                  _bloc.onChangeSatuan1,
                                ),
                                _builtThreeInput(
                                  '2',
                                  isUpdated ? data?.jumlah2 ?? null : '0',
                                  isUpdated ? data?.bahan2 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan2 ?? null : 'Satuan',
                                  _bloc.onChangeBahan2,
                                  _bloc.onChangeJumlah2,
                                  _bloc.onChangeSatuan2,
                                ),
                                _builtThreeInput(
                                  '3',
                                  isUpdated ? data?.jumlah3 ?? null : '0',
                                  isUpdated ? data?.bahan3 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan3 ?? null : 'Satuan',
                                  _bloc.onChangeBahan3,
                                  _bloc.onChangeJumlah3,
                                  _bloc.onChangeSatuan3,
                                ),
                                _builtThreeInput(
                                  '4',
                                  isUpdated ? data?.jumlah4 ?? null : '0',
                                  isUpdated ? data?.bahan4 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan4 ?? null : 'Satuan',
                                  _bloc.onChangeBahan4,
                                  _bloc.onChangeJumlah4,
                                  _bloc.onChangeSatuan4,
                                ),
                                _builtThreeInput(
                                  '5',
                                  isUpdated ? data?.jumlah5 ?? null : '0',
                                  isUpdated ? data?.bahan5 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan5 ?? null : 'Satuan',
                                  _bloc.onChangeBahan5,
                                  _bloc.onChangeJumlah5,
                                  _bloc.onChangeSatuan5,
                                ),
                                _builtThreeInput(
                                  '6',
                                  isUpdated ? data?.jumlah6 ?? null : '0',
                                  isUpdated ? data?.bahan6 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan6 ?? null : 'Satuan',
                                  _bloc.onChangeBahan6,
                                  _bloc.onChangeJumlah6,
                                  _bloc.onChangeSatuan6,
                                ),
                                _builtThreeInput(
                                  '7',
                                  isUpdated ? data?.jumlah7 ?? null : '0',
                                  isUpdated ? data?.bahan7 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan7 ?? null : 'Satuan',
                                  _bloc.onChangeBahan7,
                                  _bloc.onChangeJumlah7,
                                  _bloc.onChangeSatuan7,
                                ),
                                _builtThreeInput(
                                  '8',
                                  isUpdated ? data?.jumlah8 ?? null : '0',
                                  isUpdated ? data?.bahan8 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan8 ?? null : 'Satuan',
                                  _bloc.onChangeBahan8,
                                  _bloc.onChangeJumlah8,
                                  _bloc.onChangeSatuan8,
                                ),
                                _builtThreeInput(
                                  '9',
                                  isUpdated ? data?.jumlah9 ?? null : '0',
                                  isUpdated ? data?.bahan9 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan9 ?? null : 'Satuan',
                                  _bloc.onChangeBahan9,
                                  _bloc.onChangeJumlah9,
                                  _bloc.onChangeSatuan9,
                                ),
                                _builtThreeInput(
                                  '10',
                                  isUpdated ? data?.jumlah10 ?? null : '0',
                                  isUpdated ? data?.bahan10 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan10 ?? null : 'Satuan',
                                  _bloc.onChangeBahan10,
                                  _bloc.onChangeJumlah10,
                                  _bloc.onChangeSatuan10,
                                ),
                                _builtThreeInput(
                                  '11',
                                  isUpdated ? data?.jumlah11 ?? null : '0',
                                  isUpdated ? data?.bahan11 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan11 ?? null : 'Satuan',
                                  _bloc.onChangeBahan11,
                                  _bloc.onChangeJumlah11,
                                  _bloc.onChangeSatuan11,
                                ),
                                _builtThreeInput(
                                  '12',
                                  isUpdated ? data?.jumlah12 ?? null : '0',
                                  isUpdated ? data?.bahan12 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan12 ?? null : 'Satuan',
                                  _bloc.onChangeBahan12,
                                  _bloc.onChangeJumlah12,
                                  _bloc.onChangeSatuan12,
                                ),
                                _builtThreeInput(
                                  '13',
                                  isUpdated ? data?.jumlah13 ?? null : '0',
                                  isUpdated ? data?.bahan13 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan13 ?? null : 'Satuan',
                                  _bloc.onChangeBahan13,
                                  _bloc.onChangeJumlah13,
                                  _bloc.onChangeSatuan13,
                                ),
                                _builtThreeInput(
                                  '14',
                                  isUpdated ? data?.jumlah14 ?? null : '0',
                                  isUpdated ? data?.bahan14 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan14 ?? null : 'Satuan',
                                  _bloc.onChangeBahan14,
                                  _bloc.onChangeJumlah14,
                                  _bloc.onChangeSatuan14,
                                ),
                                _builtThreeInput(
                                  '15',
                                  isUpdated ? data?.jumlah15 ?? null : '0',
                                  isUpdated ? data?.bahan15 ?? null : 'Pilih Bahan Material',
                                  isUpdated ? data?.satuan15 ?? null : 'Satuan',
                                  _bloc.onChangeBahan15,
                                  _bloc.onChangeJumlah15,
                                  _bloc.onChangeSatuan15,
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                                StreamBuilder<bool>(
                                    stream: null,
                                    initialData: true,
                                    builder: (context, snapshot) {
                                      return FlatButton(
                                        child: Container(
                                          child: Center(
                                            child: Text(
                                              "Simpan",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: FlutterScreenUtil()
                                                    .fontSize(18),
                                              ),
                                            ),
                                          ),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 60,
                                        ),
                                        color: snapshot.data
                                            ? Colors.indigo
                                            : Colors.grey,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        onPressed: () {
                                          _onHandleSave(context);
                                        },
                                      );
                                    }),
                                SizedBox(
                                  height: 40,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 30,
                  color: Colors.white,
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Container(
                          height: 5,
                          width: 50,
                          color: Colors.black38,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Widget _builtThreeInput(String title, String jumlah, String nama, String satuan,
      Function onChangedBahan, Function onChangedJumlah, Function onChangedSatuan) {
    return Container(
      child: Column(
        children: [
          DropdownBahan(
            onChanged: onChangedBahan,
            title: title,
            initialValue: nama,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: CostumTextField(
                  title: "Jumlah",
                  isNumber: true,
                  initialValue: jumlah,
                  onChanged: onChangedJumlah,
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                  flex: 2,
                  child: DropdownSatuan(
                    onChanged: onChangedSatuan,
                    title: "",
                    initialValue: satuan,
                  )),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> _onHandleSave(BuildContext context) async {
    try {
      showDialog(context: context, child: LoadingDialog());
      if (isUpdated){
        int success = await _bloc.update(data.uptd);
        if (success == 0) {
          Navigator.pop(context);
          Navigator.pop(context, "Berhasil Memperbarui");
        }
      } else {
        int success = await _bloc.save(data.uptd);
        if (success == 0) {
          Navigator.pop(context);
          Navigator.pop(context, "Berhasil Menyimpan");
        }
      }
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message, duration: 3);
      Navigator.pop(context);
    }
  }
}
