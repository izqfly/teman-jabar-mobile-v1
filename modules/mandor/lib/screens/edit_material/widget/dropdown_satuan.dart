part of 'package:mandor/screens/edit_material/edit_material.dart';

class DropdownSatuan extends StatelessWidget {
  final String _title = "Satuan";
  final String title;
  final Function onChanged;
  final String initialValue;

  const DropdownSatuan({Key key, this.title, this.onChanged, this.initialValue}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<SatuanMaterial>>(
      initialData: <SatuanMaterial>[],
      future: _getReportLocation(context),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return CostumDropdownSearch<SatuanMaterial>(
            compateFn: (item, selectedItem) => item?.id == selectedItem?.id,
            dropdownBuilder: (_, item, __) => Text(item?.name ?? (initialValue != null ? initialValue : "Satuan")),
            hintText: "$_title",
            itemAsString: (item) => item?.name,
            items: snapshot.data,
            onChanged: (snapshot) => onChanged(snapshot),
            popupItemBuilder: (_, item, selected) => ListTile(
              selected: selected,
              title: Row(
                children: <Widget>[
                  selected
                      ? Icon(
                          Icons.check,
                          color: Theme.of(context).primaryColor,
                        )
                      : Container(),
                  SizedBox(width: selected ? 7 : 0),
                  Expanded(child: Text(item?.name))
                ],
              ),
            ),
            selectedItem: SatuanMaterial(),
            title: _title + " " + title,
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(
                Theme.of(context).primaryColor),
          ),
        );
      },
    );
  }

  Future<List<SatuanMaterial>> _getReportLocation(BuildContext context) async {
    try {
      return await Modular.get<MaterialBloc>().getSatuan();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <SatuanMaterial>[];
  }
}
