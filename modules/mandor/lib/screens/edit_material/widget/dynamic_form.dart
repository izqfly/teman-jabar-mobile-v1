import 'package:flutter/material.dart';
import 'package:mandor/screens/input_task/widget/custom_text_field.dart';

import '../edit_material.dart';

class DynamicForm extends StatefulWidget {
  @override
  _DynamicFormState createState() => _DynamicFormState();
}

class _DynamicFormState extends State<DynamicForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _nameController;
  static List<String> friendsList = [null];

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ..._getFriends(),
        ],
      ),
    );
  }


  /// get firends text-fields
  List<Widget> _getFriends(){
    List<Widget> friendsTextFields = [];
    for(int i=0; i<friendsList.length; i++){
      friendsTextFields.add(
          Row(
            children: [
              Expanded(child: FriendTextFields(i)),
              SizedBox(width: 16,),
              // we need add button at last friends row
              _addRemoveButton(i == friendsList.length-1, i),
            ],
          )
      );
    }
    return friendsTextFields;
  }

  /// add / remove button
  Widget _addRemoveButton(bool add, int index){
    return InkWell(
      onTap: (){
        if(add){
          // add new text-fields at the top of all friends textfields
          friendsList.insert(0, null);
        }
        else friendsList.removeAt(index);
        setState((){});
      },
      child: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: (add) ? Colors.green : Colors.red,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Icon((add) ? Icons.add : Icons.remove, color: Colors.white,),
      ),
    );
  }


}

class FriendTextFields extends StatefulWidget {
  final int index;
  FriendTextFields(this.index);
  @override
  _FriendTextFieldsState createState() => _FriendTextFieldsState(index);
}

class _FriendTextFieldsState extends State<FriendTextFields> {
  TextEditingController _nameController;
  final int index;

  _FriendTextFieldsState(this.index);

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _nameController.text = _DynamicFormState.friendsList[widget.index] ?? '';
    });

    return _builtThreeInput(index == null ? "1" : (index+1).toString(), "0", "Pilih Bahan Material", "Satuan", null, null, null);
  }

  Widget _builtThreeInput(String title, String jumlah, String nama, String satuan,
      Function onChangedBahan, Function onChangedJumlah, Function onChangedSatuan) {
    return Container(
      child: Column(
        children: [
          DropdownBahan(
            onChanged: onChangedBahan,
            title: title,
            initialValue: nama,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: CostumTextField(
                  title: "Jumlah",
                  isNumber: true,
                  initialValue: jumlah,
                  onChanged: onChangedJumlah,
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                  flex: 2,
                  child: DropdownSatuan(
                    onChanged: onChangedSatuan,
                    title: "",
                    initialValue: satuan,
                  )),
            ],
          ),
        ],
      ),
    );
  }

}
