import 'package:flutter/material.dart';
import 'package:common/widgets/loading_dialog.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:mandor/screens/input_task/widget/custom_text_field.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/services.dart';
import 'package:common/widgets/form_input/costume_camera_picker.dart';
import 'package:common/widgets/form_input/costum_video_picker.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public_bloc/input_task/bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class EditTask extends StatefulWidget {
  EditTask({this.data});

  final data;

  @override
  _EditTaskState createState() => _EditTaskState(data);
}

class _EditTaskState extends State<EditTask> {

  _EditTaskState(this.data);

  final data;

  final _bloc = Modular.get<InputTaskBloc>();


  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    return Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: SingleChildScrollView(
              controller: ModalScrollController.of(context),
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 50,
                      ),
                      Text(
                        "Edit Data Pekerjaan",
                        style: TextStyle(
                            fontSize: FlutterScreenUtil().fontSize(20),
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CostumTextField(
                            title: "Tanggal",
                            readOnly: true,
                            initialValue: data.tanggal,
                          ),
                          CostumTextField(
                            title: "Jenis Pekerjaan",
                            readOnly: true,
                            initialValue: data.jenis,
                          ),
                          CostumTextField(
                            title: "Nama Paket",
                            readOnly: true,
                            initialValue: data.paket,
                          ),
                          CostumTextField(
                            title: "SUP",
                            readOnly: true,
                            initialValue: data.sup,
                          ),
                          CostumTextField(
                            title: "Ruas Jalan",
                            readOnly: true,
                            initialValue: data.ruas,
                          ),
                          CostumTextField(
                            title: "Lokasi",
                            readOnly: true,
                            initialValue: data.lokasi,
                          ),
                          CostumTextField(
                            title: "Panjang Jalan (meter)",
                            initialValue: data.panjang,
                            onChanged: _bloc.onChangedPanjang,
                          ),
                          CostumTextField(
                            title: "Jumlah Pekerja",
                            initialValue: data.pekerja.toString(),
                            onChanged: _bloc.onChangedPekerja,
                          ),
                          CostumTextField(
                            title: "Alat yang Digunakan",
                            initialValue: data.alat,
                            onChanged: _bloc.onChangedAlat,
                          ),
                          CostumCameraPicker(
                            title: "Foto Dokumentasi (0%)",
                            onChanged: _bloc.onChangedFotoAwal,
                          ),
                          CostumCameraPicker(
                            title: "Foto Dokumentasi (50%)",
                            onChanged: _bloc.onChangedFotoSedang,
                          ),
                          CostumCameraPicker(
                            title: "Foto Dokumentasi (100%)",
                            onChanged: _bloc.onChangedFotoAkhir,
                          ),
                          CostumVideoPicker(
                            title: "Video Dokumentasi",
                            onChanged: _bloc.onChangedVideo,
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          StreamBuilder<bool>(
                              stream:
                              _bloc.isValidEdit,
                              initialData: true,
                              builder: (context, snapshot) {
                                return FlatButton(
                                  child: Container(
                                    child: Center(
                                      child: Text(
                                        "Simpan",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize:
                                          FlutterScreenUtil().fontSize(18),
                                        ),
                                      ),
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    height: 60,
                                  ),
                                  color: snapshot.data
                                      ? Colors.indigo
                                      : Colors.grey,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onPressed: snapshot.data
                                      ? () {
                                    _onHandleSave(context);
                                    // AlertMessage.showToast(
                                    //   context: context,
                                    //   message: "Data berhasil disimpan",
                                    //   backgroundColor: Colors.indigo,
                                    //   textColor: Colors.white,);
                                  }
                                      : () {
                                    print(snapshot.data);
                                  },
                                );
                              }),
                          SizedBox(
                            height: 40,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 30,
            color: Colors.white,
          ),
        ],
      ),
    );
  }

  Future<void> _onHandleSave(BuildContext context) async {
    try {
      showDialog(context: context, child: LoadingDialog());
      int success = await _bloc.edit(data.id, data.id_sup);
      if (success == 0) {
        AlertMessage.showToast(
            context: context,
            message: "Data berhasil disimpan",
            backgroundColor: Colors.indigo,
            textColor: Colors.white,);
        Navigator.pop(context);
        Navigator.pop(context, "updated");
      }
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message, duration: 3);
      Navigator.pop(context);
    }
  }
}