//import 'dart:html';

import 'package:common/widgets/loading_dialog.dart';
import 'package:geocoder/geocoder.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mandor/screens/input_task/widget/custom_text_field.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:location_permissions/location_permissions.dart' as lp;
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public_bloc/input_task/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:repository/response/mandor_response.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/form_input/costum_rounded_dropdown_search.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:common/widgets/form_input/costume_camera_picker.dart';
import 'package:common/widgets/form_input/costum_video_picker.dart';
import 'package:repository/common/flutter_local_storage.dart';
part 'package:mandor/screens/input_task/widget/street_location.dart';
part 'package:mandor/screens/input_task/widget/dropdown_ruas_jalan.dart';
part 'package:mandor/screens/input_task/widget/dropdown_sup.dart';
part 'package:mandor/screens/input_task/widget/dropdown_jenis_pekerjaan.dart';
part 'package:mandor/screens/input_task/widget/dropdown_nama_paket.dart';

class InputTask extends StatefulWidget {
  InputTask({Key key}) : super(key: key);

  @override
  _InputTaskState createState() => _InputTaskState();
}

class _InputTaskState extends State<InputTask> {
  final _bloc = Modular.get<InputTaskBloc>();
  String finalDate = '';
  final _storage = FlutterLocalStorage();

  @override
  void initState() {
    this.getCurrentDate();
  }

  getCurrentDate() {
    var date = new DateTime.now().toString();

    var dateParse = DateTime.parse(date);

    var day, month;

    if (dateParse.day < 10) {
      day = "0" + dateParse.day.toString();
    } else {
      day = dateParse.day.toString();
    }

    if (dateParse.month < 10) {
      month = "0" + dateParse.month.toString();
    } else {
      month = dateParse.month.toString();
    }

    var formattedDate = "${dateParse.year}-$month-$day";

    setState(() {
      finalDate = formattedDate.toString();
      _bloc.onChangedDate(finalDate);
    });
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              controller: ModalScrollController.of(context),
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 50,
                      ),
                      Text(
                        "Input Pekerjaan",
                        style: TextStyle(
                            fontSize: FlutterScreenUtil().fontSize(20),
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CostumTextField(
                            title: "Tanggal",
                            readOnly: true,
                            initialValue: finalDate,
                          ),
                          CostumTextField(
                            title: "Jenis Pekerjaan",
                            initialValue: "Pemeliharaan",
                            readOnly: true,
                          ),
                          _DropdownNamaPaket(),
                          CostumTextField(
                            title: "SUP",
                            readOnly: true,
                            initialValue: _storage.user.sup,
                          ),
                          _DropdownRuasJalan(),
                          _StreetLocation(),
                          CostumTextField(
                            title: "Panjang (meter)",
                            hintText: "000",
                            onChanged: _bloc.onChangedPanjang,
                          ),
                          CostumTextField(
                            title: "Jumlah Pekerja",
                            hintText: "00",
                            onChanged: _bloc.onChangedPekerja,
                          ),
                          // CostumTextField(
                          //   title: "Alat yang Digunakan",
                          //   hintText: "Sapu, Kuas",
                          //   onChanged: _bloc.onChangedAlat,
                          // ),
                          CostumCameraPicker(
                            title: "Foto Pegawai",
                            onChanged: _bloc.onChangedFotoPegawai,
                          ),
                          CostumCameraPicker(
                            title: "Foto Dokumentasi (0%)",
                            onChanged: _bloc.onChangedFotoAwal,
                          ),
                          CostumCameraPicker(
                            title: "Foto Dokumentasi (50%)",
                            onChanged: _bloc.onChangedFotoSedang,
                          ),
                          CostumCameraPicker(
                            title: "Foto Dokumentasi (100%)",
                            onChanged: _bloc.onChangedFotoAkhir,
                          ),
                          CostumVideoPicker(
                            title: "Video Dokumentasi",
                            onChanged: _bloc.onChangedVideo,
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          StreamBuilder<bool>(
                              stream: _bloc.isValidSubmit,
                              initialData: false,
                              builder: (context, snapshot) {
                                return FlatButton(
                                  child: Container(
                                    child: Center(
                                      child: Text(
                                        "Simpan",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize:
                                              FlutterScreenUtil().fontSize(18),
                                        ),
                                      ),
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    height: 60,
                                  ),
                                  color: snapshot.data != null
                                      ? Colors.indigo
                                      : Colors.grey,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onPressed: snapshot.data != null
                                      ? () {
                                          _onHandleSave(context);
                                        }
                                      : () {
                                          print(snapshot.data);
                                        },
                                );
                              }),
                          SizedBox(
                            height: 40,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 30,
            color: Colors.white,
          ),
        ],
      ),
    );
  }

  Future<void> _onHandleSave(BuildContext context) async {
    try {
      showDialog(context: context, child: LoadingDialog());
      int success = await _bloc.save();
      if (success == 0) {
        Navigator.pop(context);
        Navigator.pop(context, "Berhasil Menyimpan");
      } else {
        AlertMessage.showToast(
          context: context,
          message: "Gagal! data tersimpan di draft",
          backgroundColor: Colors.red,
          textColor: Colors.white,
        );
        Navigator.pop(context);
        Navigator.pop(context);
      }
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message, duration: 3);
      Navigator.pop(context);
    }
  }
}
