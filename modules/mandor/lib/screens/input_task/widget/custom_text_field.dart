import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CostumTextField extends StatelessWidget {
  CostumTextField({
    this.onChanged,
    @required this.title,
    this.hintText,
    this.errorText,
    this.isNumber = false,
    this.maxLength,
    this.readOnly = false,
    this.isDynamicWidget = false,
    this.initialValue = "",
  });

  final Function onChanged;
  final String hintText;
  final String title;
  final String errorText;
  final bool isNumber;
  final int maxLength;
  final bool readOnly;
  final bool isDynamicWidget;
  final String initialValue;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            title,
            style: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(15),
              color: Colors.black54,
            ),
          ),
        ),
        SizedBox(height: 4),
        TextFormField(
          key: isDynamicWidget ? UniqueKey() : ValueKey(title),
          readOnly: readOnly,
          autocorrect: false,
          initialValue: initialValue,
          onChanged: onChanged,
          enableSuggestions: false,
          textInputAction: TextInputAction.next,
          keyboardType: isNumber ? TextInputType.number : TextInputType.text,
          inputFormatters: isNumber
              ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
              : <TextInputFormatter>[],
          maxLines: null,
          maxLength: maxLength,
          decoration: InputDecoration(
            border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(15.0),
                borderSide: new BorderSide(),
            ),
            hintText: hintText ?? "",
            hintStyle: TextStyle(
              fontSize: FlutterScreenUtil().fontSize(14),
              color: Colors.black45,
            ),
            errorText: errorText,
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }
}
