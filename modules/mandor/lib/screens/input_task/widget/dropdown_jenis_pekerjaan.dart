part of 'package:mandor/screens/input_task/input_task.dart';

class _DropdownJenisPekerjaan extends StatelessWidget {
  final String _title = "Jenis Pekerjaan";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<JenisPekerjaan>>(
      initialData: <JenisPekerjaan>[],
      future: _getReportLocation(context),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return CostumDropdownSearch<JenisPekerjaan>(
            compateFn: (item, selectedItem) => item?.id == selectedItem?.id,
            dropdownBuilder: (_, item, __) => Text(
              item?.name ?? "Pilih $_title",
            ),
            hintText: "Pilih $_title",
            itemAsString: (item) => item?.name,
            items: snapshot.data,
            onChanged: Modular.get<InputTaskBloc>().onChangedPekerjaanType,
            popupItemBuilder: (_, item, selected) => ListTile(
              selected: selected,
              title: Row(
                children: <Widget>[
                  selected
                      ? Icon(
                          Icons.check,
                          color: Theme.of(context).primaryColor,
                        )
                      : Container(),
                  SizedBox(width: selected ? 7 : 0),
                  Expanded(child: Text(item?.name))
                ],
              ),
            ),
            selectedItem: JenisPekerjaan(),
            title: _title,
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(
                Theme.of(context).primaryColor),
          ),
        );
      },
    );
  }

  Future<List<JenisPekerjaan>> _getReportLocation(BuildContext context) async {
    try {
      return await Modular.get<InputTaskBloc>().getPekerjaanType();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <JenisPekerjaan>[];
  }
}
