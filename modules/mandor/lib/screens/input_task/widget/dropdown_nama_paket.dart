part of 'package:mandor/screens/input_task/input_task.dart';

class _DropdownNamaPaket extends StatelessWidget {
  final String _title = "Jenis Kegiatan";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<PaketPekerjaan>>(
      initialData: <PaketPekerjaan>[],
      future: _getReportLocation(context),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return CostumDropdownSearch<PaketPekerjaan>(
            compateFn: (item, selectedItem) => item?.id == selectedItem?.id,
            dropdownBuilder: (_, item, __) => Text(
              item?.name ?? "Pilih $_title",
            ),
            hintText: "Pilih $_title",
            itemAsString: (item) => item?.name,
            items: snapshot.data,
            onChanged: (snapshot) => Modular.get<InputTaskBloc>().onChangedPaketPekerjaan(snapshot),
            popupItemBuilder: (_, item, selected) => ListTile(
              selected: selected,
              title: Row(
                children: <Widget>[
                  selected
                      ? Icon(
                          Icons.check,
                          color: Theme.of(context).primaryColor,
                        )
                      : Container(),
                  SizedBox(width: selected ? 7 : 0),
                  Expanded(child: Text(item?.name))
                ],
              ),
            ),
            selectedItem: PaketPekerjaan(),
            title: _title,
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(
                Theme.of(context).primaryColor),
          ),
        );
      },
    );
  }

  Future<List<PaketPekerjaan>> _getReportLocation(BuildContext context) async {
    try {
      return await Modular.get<InputTaskBloc>().getpaketPekerjaan();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <PaketPekerjaan>[];
  }
}
