part of 'package:mandor/screens/input_task/input_task.dart';

class _DropdownSup extends StatelessWidget {
  final String _title = "SUP";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<DropdownResponse>>(
      initialData: <DropdownResponse>[],
      future: _getReportLocation(context),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return CostumDropdownSearch<DropdownResponse>(
            compateFn: (item, selectedItem) => item?.id == selectedItem?.id,
            dropdownBuilder: (_, item, __) => Text(
              item?.name ?? "Pilih $_title",
            ),
            hintText: "Pilih $_title",
            itemAsString: (item) => item?.name,
            items: snapshot.data,
            onChanged: (snapshot) => Modular.get<InputTaskBloc>().onChangedSup(snapshot),
            popupItemBuilder: (_, item, selected) => ListTile(
              selected: selected,
              title: Row(
                children: <Widget>[
                  selected
                      ? Icon(
                          Icons.check,
                          color: Theme.of(context).primaryColor,
                        )
                      : Container(),
                  SizedBox(width: selected ? 7 : 0),
                  Expanded(child: Text(item?.name))
                ],
              ),
            ),
            selectedItem: DropdownResponse(),
            title: _title,
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(
                Theme.of(context).primaryColor),
          ),
        );
      },
    );
  }

  Future<List<DropdownResponse>> _getReportLocation(BuildContext context) async {
    try {
      return await Modular.get<InputTaskBloc>().getSUP();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <DropdownResponse>[];
  }
}
