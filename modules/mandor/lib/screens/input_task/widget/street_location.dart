part of 'package:mandor/screens/input_task/input_task.dart';

class _StreetLocation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StreetLocationState();
  }
}

class _StreetLocationState extends State<_StreetLocation> {
  final _bloc = Modular.get<InputTaskBloc>();
  final _positionNotifier = ValueNotifier<Position>(new Position());
  final _addressNotifer = ValueNotifier<Address>(new Address());
  String location;

  void initState() {
    super.initState();
    _checkLocationPermission();
  }

  void _checkLocationPermission() async {
    final statusFuture = await lp.LocationPermissions().checkPermissionStatus();
    if (statusFuture != lp.PermissionStatus.granted) {
      lp.PermissionStatus ps =
          await lp.LocationPermissions().requestPermissions(
        permissionLevel: lp.LocationPermissionLevel.locationWhenInUse,
      );
      if (ps != lp.PermissionStatus.granted) {
        return;
      } else {
        _getLocation();
      }
    } else {
      _getLocation();
    }
  }

  void _getLocation() async {
    final res = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.best,
    );

    _positionNotifier.value = res;

    if (res != null) {
      _bloc.onChangedLatitude(res.latitude);
      _bloc.onChangedLongitude(res.longitude);
      final coordinates = new Coordinates(res.latitude, res.longitude);
      _findaddress(coordinates);
    }
  }

  void _findaddress(Coordinates coordinates) async {
    final result =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);

    if (result != null) {
      _bloc.onChangeLocation(result.first.addressLine);
      setState(() {
        location = result.first.addressLine;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Position>(
      valueListenable: _positionNotifier,
      builder: (_, value, widget) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CostumTextField(
            title: "Lokasi",
            initialValue: location ?? "Checking...",
            readOnly: true,
            isDynamicWidget: true,
          ),
          CostumTextField(
            title: "Latitude",
            initialValue: value?.latitude?.toString() ?? "Checking...",
            readOnly: true,
            isDynamicWidget: true,
          ),
          CostumTextField(
            title: "Longitude",
            initialValue: value?.longitude?.toString() ?? "Checking...",
            readOnly: true,
            isDynamicWidget: true,
          ),
        ],
      ),
    );
  }
}
