//import 'dart:html';

import 'package:background_fetch/background_fetch.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:common/helper/fcm.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:mandor/screens/draft/draft.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:flutter/material.dart';
import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/response/json_response.dart';
import 'package:mandor/screens/task/task.dart';
import 'package:mandor/screens/profile/profile.dart';
import 'package:mandor/screens/input_task/input_task.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class MandorLandingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MandorLandingPageState();
  }
}

class _MandorLandingPageState extends State<MandorLandingPage> {
  final _storage = FlutterLocalStorage();
  int _selectedFilter = 0;
  final _flutterRestClient = FlutterRestClient();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Future<void> initPlatformState() async {
    BackgroundFetch.configure(
        BackgroundFetchConfig(
            minimumFetchInterval: 1,
            enableHeadless: true,
            stopOnTerminate: false,
            startOnBoot: true,
            forceAlarmManager: true), (String taskId) async {
      print("[BackgroundFetch] taskId: $taskId");
      BackgroundFetch.finish(taskId);
    }, (String taskId) async {
      print("[BackgroundFetch] TIMEOUT taskId: $taskId");
      BackgroundFetch.finish(taskId);
    });
  }

  showNotification(String desc) async {
    var android = new AndroidNotificationDetails(
      'id',
      'channel ',
      'description',
      priority: Priority.high,
      importance: Importance.max,
      autoCancel: false,
      enableVibration: true,
      playSound: true,
    );
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await flutterLocalNotificationsPlugin.show(
        0, 'Background Service', desc, platform);
  }

  @override
  void initState() {
    super.initState();
    //initPlatformState();
    Fcm().initializeNotification(context);
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    FlutterStatusbarcolor.setNavigationBarColor(Colors.transparent);
    FlutterStatusbarcolor.setNavigationBarWhiteForeground(false);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            _selectedFilter == 2
                ? SizedBox(
                    height: 40,
                  )
                : Container(
                    height: 60,
                    //color: Colors.blue,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          "${BasePathAsset.icons}/common/temanjabar-logo.png",
                          height: AppBar().preferredSize.height / 1.5,
                        ),
                        SizedBox(width: 10),
                        Text(
                          "Teman Jabar",
                          style: TextStyle(
                              fontSize: FlutterScreenUtil().fontSize(18),
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.5),
                        )
                      ],
                    ),
                  ),
            Expanded(
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 24, right: 24),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _selectedFilter == 2
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Container(
                                    height: 200,
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.indigoAccent[100],
                                    child: Stack(
                                      children: [
                                        Positioned(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              180,
                                          bottom: -100,
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(120),
                                            child: Container(
                                              height: 240,
                                              width: 240,
                                              color: Colors.indigo[100],
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 15,
                                            right: 15,
                                            left: 24,
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Text(
                                                "Teman Jabar",
                                                style: TextStyle(
                                                    fontSize:
                                                        FlutterScreenUtil()
                                                            .fontSize(15),
                                                    color: Colors.white),
                                              ),
                                              SizedBox(width: 10),
                                              Image.asset(
                                                "${BasePathAsset.icons}/common/temanjabar-logo.png",
                                                height: AppBar()
                                                        .preferredSize
                                                        .height /
                                                    1.5,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(24),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                _storage?.user?.fullname,
                                                style: TextStyle(
                                                  fontSize: FlutterScreenUtil()
                                                      .fontSize(20),
                                                  fontWeight: FontWeight.bold,
                                                  letterSpacing: 0.5,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              Text(
                                                _storage?.user?.sup,
                                                style: TextStyle(
                                                  fontSize: FlutterScreenUtil()
                                                      .fontSize(14),
                                                  fontWeight: FontWeight.normal,
                                                  letterSpacing: 0.5,
                                                  color: Colors.white,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              : Column(
                                  children: [
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      child: Text(
                                        "Selamat Bekerja " +
                                            "\n" +
                                            _storage?.user?.fullname,
                                        style: TextStyle(
                                            fontSize: FlutterScreenUtil()
                                                .fontSize(28),
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1),
                                      ),
                                    ),
                                  ],
                                ),
                          SizedBox(
                            height: 20,
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: <Widget>[
                                buildFilter('Pekerjaan', 0),
                                buildFilter('Draft', 1),
                                buildFilter('Profile', 2),
                                SizedBox(width: 22),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Expanded(child: _body[_selectedFilter]),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildFilter(String title, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedFilter = index;
        });
      },
      child: Row(
        children: <Widget>[
          Container(
            height: 32,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: 14),
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                SizedBox(width: 14),
              ],
            ),
            decoration: BoxDecoration(
              color:
                  index == _selectedFilter ? Colors.indigo : Colors.indigo[100],
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          SizedBox(
            width: 10,
          )
        ],
      ),
    );
  }

  final _body = <Widget>[
    Task(),
    NotUploaded(),
    Profile(),
  ];
}
