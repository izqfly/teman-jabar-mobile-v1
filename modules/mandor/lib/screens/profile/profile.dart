import 'package:auth_bloc/auth_bloc.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:mandor/modules/constants/route_name.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/response/json_response.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final _storage = FlutterLocalStorage();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Nama :"),
            SizedBox(height: 8),
            Text(
              _storage?.user?.fullname ?? "-",
              style: TextStyle(
                fontSize: FlutterScreenUtil().fontSize(16),
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 15),
            Text("Email :"),
            SizedBox(height: 8),
            Text(
              _storage?.user?.email ?? "-",
              style: TextStyle(
                fontSize: FlutterScreenUtil().fontSize(16),
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 15),
            Text("Jabatan :"),
            SizedBox(height: 8),
            Text(
              _storage?.user?.role ?? "-",
              style: TextStyle(
                fontSize: FlutterScreenUtil().fontSize(16),
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 15),
            Text("SUP :"),
            SizedBox(height: 8),
            Text(
              _storage?.user?.sup ?? "-",
              style: TextStyle(
                fontSize: FlutterScreenUtil().fontSize(16),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Align(
            alignment: Alignment.bottomRight,
            child: FloatingActionButton(
              child: Icon(Icons.exit_to_app_rounded),
              onPressed: () async {
                await Modular.get<AuthBloc>().logout();
                Modular.navigator.pop();
                Modular.to.pushNamedAndRemoveUntil(
                  "${RouteName.BaseModule}",
                  (Route<dynamic> route) => false,
                );
              },
            ),
          ),
        )
      ],
    );
  }
}
