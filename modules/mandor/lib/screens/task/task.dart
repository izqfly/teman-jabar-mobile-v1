import 'dart:io';

import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:mandor/screens/detail_task/detail_task.dart';
import 'package:mandor/screens/edit_task/edit_task.dart';
import 'package:mandor/screens/input_task/input_task.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:public_bloc/input_task/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:repository/models/task/task.dart' as modelTask;
import 'package:mandor/modules/constants/route_name.dart';

class Task extends StatefulWidget {
  Task({Key key}) : super(key: key);

  @override
  _TaskState createState() => _TaskState();
}

class _TaskState extends State<Task> {
  final _bloc = Modular.get<InputTaskBloc>();

  Future<List<modelTask.Task>> _getUpdate;

  @override
  void initState() {
    super.initState();
    _getUpdate = _getList(context);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: FutureBuilder<List<modelTask.Task>>(
              future: _getUpdate,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<modelTask.Task> data = snapshot.data;
                  return listTask(data);
                } else if (snapshot.hasError) {
                  return Center(child: Text("Data Pekerjaan Kosong"));
                }
                return Center(child: CircularProgressIndicator());
              }),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Align(
            alignment: Alignment.bottomRight,
            child: FloatingActionButton(
              child: Icon(Icons.add_road_rounded),
              onPressed: () async {
                try {
                  final result = await InternetAddress.lookup('google.com');
                  if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                    print('connected');
                    _navigateInput(context);
                  }
                } on SocketException catch (_) {
                  showDialog(
                      context: context,
                      builder: (context) => new AlertDialog(
                            title: Text("Peringatan"),
                            content:
                                Text("Anda tidak memiliki koneksi internet"),
                            actions: [
                              FlatButton(
                                child: Text(
                                  "OK",
                                  style: TextStyle(color: Colors.blue),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                          ));
                }
              },
            ),
          ),
        )
      ],
    );
  }

  _navigateInput(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => InputTask()),
    );
    _onClose(result);
  }

  void _onClose(result) {
    if (result == "Berhasil Menyimpan") {
      AlertMessage.showToast(
        context: context,
        message: "Data berhasil disimpan",
        backgroundColor: Colors.indigo,
        textColor: Colors.white,
      );
      setState(() {
        _getUpdate = _getList(context);
      });
    } else if (result == "updated") {
      setState(() {
        _getUpdate = _getList(context);
      });
    } else if (result == "deleted") {
      AlertMessage.showToast(
        context: context,
        message: "Data berhasil dihapus",
        backgroundColor: Colors.indigo,
        textColor: Colors.white,
      );
      setState(() {
        _getUpdate = _getList(context);
      });
    }
  }

  Widget listTask(data) {
    if (data.length == 0) {
      return Center(child: Text("Data Pekerjaan Kosong"));
    }
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: data == null ? 0 : data.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildItem(context, data, index);
      },
    );
  }

  Future<List<modelTask.Task>> _getList(BuildContext context) async {
    try {
      var respon = await _bloc.getTaskList();
      print("======= UPDATED ===== ");
      return respon;
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.toString());
    }
    return <modelTask.Task>[];
  }

  Widget _buildItem(BuildContext context, data, int index) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Container(
            color: Colors.grey[100],
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () async {
                  final result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailTask(
                              data: data[index],
                            )),
                  );
                  _onClose(result);
                },
                child: IntrinsicHeight(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        width: 10,
                        color: Colors.indigo,
                      ),
                      Expanded(
                          child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data[index].jenis,
                              style: TextStyle(
                                fontSize: FlutterScreenUtil().fontSize(14),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              data[index].paket,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FlutterScreenUtil().fontSize(14),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              data[index].ruas,
                              style: TextStyle(
                                fontSize: FlutterScreenUtil().fontSize(14),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              data[index].lokasi,
                              style: TextStyle(
                                  fontSize: FlutterScreenUtil().fontSize(14),
                                  color: Colors.indigo),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              data[index].tanggal,
                              style: TextStyle(
                                  fontSize: FlutterScreenUtil().fontSize(14),
                                  color: Colors.indigo),
                            ),
                          ],
                        ),
                      )),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }
}
