import 'package:flutter_modular/flutter_modular.dart';
import 'package:public/modules/route_module.dart';
import 'package:public/screens/beranda/menu/reporting_street/reporting_street_screen.dart';

import 'package:public_bloc/reporting_street/bloc.dart';


import 'constants/route_name.dart';

class BerandaModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind<ReportingStreetBloc>((i) => ReportingStreetService()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          RouteName.RouteModule,
          module: RouteModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.ReportingStreet,
          child: (context, args) => ReportingStreetScreen(),
          transition: _transitionType,
        ),
    
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
