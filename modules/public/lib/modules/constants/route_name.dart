class RouteName {
  static const BaseModule = "/auth";
  static const LandingPage = "/public-landing-page";
  static const About = "/about";
  static const Settings = "/settings";
  static const Announcement = "/anouncement";

  static const Beranda = "/beranda";
  static const ReportingStreet = "/reporting-street";

  static const RouteModule = "/rute";
  static const EidRute = "/eid-rute";
  static const RouteDirections = "/rute-directions";
}
