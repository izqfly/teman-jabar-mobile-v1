import 'package:flutter_modular/flutter_modular.dart';
import 'package:public/modules/beranda_module.dart';
import 'package:public/modules/constants/route_name.dart';
import 'package:public/screens/landing_page/public_landing_page.dart';
import 'package:public_bloc/anouncement/bloc.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:auth_bloc/bloc.dart';
import 'package:common/widgets/about_screen.dart';
import 'package:public/screens/settings/settings_screen.dart';
import 'package:public/screens/announcement/notification_screen.dart';

class PublicModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind<AuthBloc>((i) => AuthService()),
        Bind<ReportingStreetBloc>((i) => ReportingStreetService()),
        Bind<AnouncementBloc>((i) => AnouncementService()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          "/",
          child: (_, args) => PublicLandingPage(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.Beranda,
          module: BerandaModule(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.About,
          child: (_, args) => AboutScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.Settings,
          child: (_, args) => SettingsScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.Announcement,
          child: (_, args) => NotificationScreen(),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
