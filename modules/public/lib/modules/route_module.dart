import 'package:flutter_modular/flutter_modular.dart';
import 'package:public/modules/constants/route_name.dart';
import 'package:public/screens/beranda/menu/route/eid_route.dart';
import 'package:public/screens/beranda/menu/route/route_directions.dart';
import 'package:public/screens/beranda/menu/route/route_screen.dart';
import 'package:public_bloc/route/bloc.dart';

class RouteModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind<RouteStreetBloc>((i) => RouteStreetService()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          "/",
          child: (_, args) => RouteScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.EidRute,
          child: (_, args) => EidRouteScreen(),
          transition: _transitionType,
        ),
        ModularRouter(
          RouteName.RouteDirections,
          child: (_, args) => RouteDirectionsScreen(
            routeCoordinates: args.data,
          ),
          transition: _transitionType,
        ),
      ];

  TransitionType get _transitionType => TransitionType.leftToRight;
}
