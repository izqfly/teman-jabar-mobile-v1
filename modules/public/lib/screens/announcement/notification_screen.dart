import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:public_bloc/anouncement/bloc.dart';
import 'package:repository/models/anouncement/anouncement.dart' as notifModel;
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class NotificationScreen extends StatefulWidget {
  NotificationScreen({Key key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final _bloc = Modular.get<AnouncementBloc>();

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    FlutterStatusbarcolor.setNavigationBarColor(Colors.transparent);
    FlutterStatusbarcolor.setNavigationBarWhiteForeground(false);
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            left: -40,
            top: -100,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(300),
              child: Container(
                height: 300,
                width: 300,
                color: Colors.indigoAccent[100],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              children: [
                SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Notifikasi",
                      style: TextStyle(
                          fontSize: FlutterScreenUtil().fontSize(22),
                          fontWeight: FontWeight.normal,
                          letterSpacing: 1,
                          color: Colors.white),
                    ),
                    Icon(
                      Icons.notifications_active_rounded,
                      color: Colors.black54,
                    )
                  ],
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 0,
                          blurRadius: 2,
                          offset: Offset(0, 0), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Container(
                      child: FutureBuilder<List<notifModel.Anouncement>>(
                          future: _getList(context),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              List<notifModel.Anouncement> data = snapshot.data;
                              return listTask(data);
                            } else if (snapshot.hasError) {
                              return Center(
                                  child: Text("Data Pekerjaan Kosong"));
                            }
                            return Center(child: CircularProgressIndicator());
                          }),
                    ),
                  ),
                ),
                SizedBox(height: 20)
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget listTask(data) {
    if (data.length == 0) {
      return Center(child: Text("Tidak Ada Notifikasi"));
    }
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: data == null ? 0 : data.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildList(context, data, index);
      },
    );
  }

  Widget _buildList(BuildContext context, data, int index) {
    return Column(
      children: [
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(data[index].title),
                        HtmlWidget(
                            data[index]?.content ?? ""
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: 15),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Divider(
            height: 1,
            color: Colors.black26,
          ),
        )
      ],
    );
  }

  Future<List<notifModel.Anouncement>> _getList(BuildContext context) async {
    try {
      var respon = await _bloc.getList();
      print("======= UPDATED ===== ");
      return respon;
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.toString());
    }
    return <notifModel.Anouncement>[];
  }
}
