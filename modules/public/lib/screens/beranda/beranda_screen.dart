part of 'package:public/screens/landing_page/public_landing_page.dart';

class _BerandaScreen extends StatelessWidget {
  _BerandaScreen._({this.landingPageKey});
  final _screen = FlutterScreenUtil();
  final GlobalKey<ScaffoldState> landingPageKey;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: _screen.widthDefault(context),
          height: 230,
          child: Stack(
            children: [
              _Carousel(),
              _CostumAppBar._(landingPageKey: landingPageKey),
              _BerandaTitle(),
            ],
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("LAYANAN",
                      style: TextStyle(
                        color: Colors.black45,
                        fontSize: _screen.fontSize(14),
                      )),
                  Text(
                    "UTAMA",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: _screen.fontSize(18),
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.5),
                  ),
                  SizedBox(height: 15),
                  _MainMenu()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
