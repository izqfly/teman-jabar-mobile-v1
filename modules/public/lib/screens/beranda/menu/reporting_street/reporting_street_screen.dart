import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/form_input/costum_dropdown_search.dart';
import 'package:common/widgets/form_input/costum_file_picker.dart';
import 'package:common/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/widgets/form_input/costum_form_field.dart';
import 'package:geolocator/geolocator.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:repository/response/dropdown_response.dart';
import 'package:location_permissions/location_permissions.dart' as lp;
import 'package:repository/common/flutter_local_storage.dart';
part 'package:public/screens/beranda/menu/reporting_street/widgets/header.dart';
part 'package:public/screens/beranda/menu/reporting_street/widgets/reporter.dart';
part 'package:public/screens/beranda/menu/reporting_street/widgets/detail_report.dart';
part 'package:public/screens/beranda/menu/reporting_street/widgets/widgets_detail_report/dropdown_report_type.dart';
part 'package:public/screens/beranda/menu/reporting_street/widgets/widgets_detail_report/dropdown_uptd.dart';
part 'package:public/screens/beranda/menu/reporting_street/widgets/widgets_detail_report/dropdown_report_location.dart';
part 'package:public/screens/beranda/menu/reporting_street/widgets/widgets_detail_report/street_location.dart';

class ReportingStreetScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ReportingStreetState();
  }
}

class _ReportingStreetState extends State<ReportingStreetScreen> {
  void initState() {
    super.initState();
    Modular.get<ReportingStreetBloc>().init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pengaduan Masyarakat"),
        actions: [
          StreamBuilder<bool>(
            stream: Modular.get<ReportingStreetBloc>().isValidSubmit,
            initialData: false,
            builder: (_, snapshot) {
              return IconButton(
                padding: const EdgeInsets.only(right: 13),
                icon: Icon(
                  Icons.check,
                  color: snapshot.data ? Colors.white : Colors.grey,
                ),
                onPressed: snapshot.data ? () => _onHandleSave(context) : null,
              );
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _Reporter(),
              SizedBox(height: 15),
              _DetailReport(),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onHandleSave(BuildContext context) async {
    try {
      showDialog(context: context, child: LoadingDialog());
      await Modular.get<ReportingStreetBloc>().save();
      Navigator.pop(context);
      Navigator.pop(context, "success");
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message, duration: 3);
      Navigator.pop(context);
    }
  }
}
