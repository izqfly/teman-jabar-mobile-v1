part of 'package:public/screens/beranda/menu/reporting_street/reporting_street_screen.dart';

class _DetailReport extends StatelessWidget {
  final _bloc = Modular.get<ReportingStreetBloc>();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _Header._(title: "Detail Laporan", iconData: Icons.book),
        Divider(thickness: 1, color: Colors.black45),
        _DropdownReportType(),
        _DropdownReportLocation(),
        _DropdownUtpd(),
        CostumFilePicker(
          title: "Foto keadaan Jalan",
          onChanged: _bloc.onChangedStreetPicture,
        ),
        _StreetLocation(),
        CostumTextFormField(
          onChanged: _bloc.onChangedDescription,
          title: "Penjelasan",
          hintText: "Isikan detail keadaan jalan",
        ),
      ],
    );
  }
}
