part of 'package:public/screens/beranda/menu/reporting_street/reporting_street_screen.dart';

class _Header extends StatelessWidget {
  _Header._({this.iconData, this.title});
  final String title;
  final IconData iconData;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Icon(iconData, color: Theme.of(context).primaryColor),
            SizedBox(width: 5),
            Text(
              title,
              style: TextStyle(
                fontSize: FlutterScreenUtil().fontSize(16),
                color: Theme.of(context).primaryColor,
                letterSpacing: 0.3,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
        SizedBox(height: 10),
      ],
    );
  }
}
