part of 'package:public/screens/beranda/menu/reporting_street/reporting_street_screen.dart';

class _Reporter extends StatelessWidget {
  final _bloc = Modular.get<ReportingStreetBloc>();
  final _storage = FlutterLocalStorage();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _Header._(title: "Pelapor", iconData: Icons.person),
        Divider(thickness: 1, color: Colors.black45),
        CostumTextFormField(
          onChanged: _bloc.onChangedName,
          title: "Nama",
          hintText: "Masukkan nama pelapor",
        ),
        StreamBuilder<String>(
          stream: _bloc.identityCard,
          builder: (_, snapshot) {
            return CostumTextFormField(
              onChanged: _bloc.onChangedIdentityCard,
              title: "No. KTP",
              hintText: "Masukkan nomor KTP",
              errorText: snapshot.error,
              maxLength: 16,
              isNumber: true,
            );
          },
        ),
        CostumTextFormField(
          onChanged: _bloc.onChangeAddress,
          title: "Alamat",
          hintText: "Isikan alamat",

        ),
        CostumTextFormField(
          onChanged: _bloc.onChangePhoneNumber,
          title: "No. Handphone",
          isNumber: true,
          hintText: "Masukkan nomor handphone",
          maxLength: 12,
        ),
        StreamBuilder<String>(
          stream: _bloc.email,
          builder: (_, snapshot) {
            return CostumTextFormField(
              onChanged: _bloc.onChangedEmail,
              title: "Email",
              hintText: "Masukkan alamat email",
              errorText: snapshot.error,
              readOnly: true,
              initialValue: _storage?.user?.email,
            );
          },
        ),
      ],
    );
  }
}
