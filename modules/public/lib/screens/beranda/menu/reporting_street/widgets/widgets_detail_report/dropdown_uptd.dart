part of 'package:public/screens/beranda/menu/reporting_street/reporting_street_screen.dart';

class _DropdownUtpd extends StatelessWidget {
  final String _title = "UPTD";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<DropdownResponse>>(
      initialData: <DropdownResponse>[],
      future: _getReportType(context),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return CostumDropdownSearch<DropdownResponse>(
            compateFn: (item, selectedItem) => item?.id == selectedItem?.id,
            dropdownBuilder: (_, item, __) => Text(
              item?.name ?? "",
            ),
            hintText: "Pilih $_title",
            itemAsString: (item) => item?.name,
            items: snapshot.data,
            onChanged: Modular.get<ReportingStreetBloc>().onChangedUptdID,
            popupItemBuilder: (_, item, selected) => ListTile(
              selected: selected,
              title: Row(
                children: <Widget>[
                  selected
                      ? Icon(
                          Icons.check,
                          color: Theme.of(context).primaryColor,
                        )
                      : Container(),
                  SizedBox(width: selected ? 7 : 0),
                  Expanded(child: Text(item?.name))
                ],
              ),
            ),
            selectedItem: DropdownResponse(),
            title: _title,
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(
                Theme.of(context).primaryColor),
          ),
        );
      },
    );
  }

  Future<List<DropdownResponse>> _getReportType(BuildContext context) async {
    try {
      return await Modular.get<ReportingStreetBloc>().getUPTD();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <DropdownResponse>[];
  }
}
