part of 'package:public/screens/beranda/menu/reporting_street/reporting_street_screen.dart';

class _StreetLocation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StreetLocationState();
  }
}

class _StreetLocationState extends State<_StreetLocation> {
  final _bloc = Modular.get<ReportingStreetBloc>();
  final _positionNotifier = ValueNotifier<Position>(new Position());

  void initState() {
    super.initState();
    _checkLocationPermission();
  }

  void _checkLocationPermission() async {
    final statusFuture = await lp.LocationPermissions().checkPermissionStatus();
    if (statusFuture != lp.PermissionStatus.granted) {
      lp.PermissionStatus ps =
          await lp.LocationPermissions().requestPermissions(
        permissionLevel: lp.LocationPermissionLevel.locationWhenInUse,
      );
      if (ps != lp.PermissionStatus.granted) {
        return;
      } else {
        _getLocation();
      }
    } else {
      _getLocation();
    }
  }

  void _getLocation() async {
    final res = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.best,
    );

    _positionNotifier.value = res;

    if (res != null) {
      _bloc.onChangedLatitude(res.latitude);
      _bloc.onChangedLongitude(res.longitude);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Position>(
      valueListenable: _positionNotifier,
      builder: (_, value, widget) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CostumTextFormField(
            title: "Latitude",
            initialValue: value?.latitude?.toString() ?? "Checking...",
            readOnly: true,
            isDynamicWidget: true,
          ),
          CostumTextFormField(
            title: "Longitude",
            initialValue: value?.longitude?.toString() ?? "Checking...",
            readOnly: true,
            isDynamicWidget: true,
          ),
        ],
      ),
    );
  }
}
