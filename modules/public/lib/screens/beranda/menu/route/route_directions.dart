import 'dart:async';

import 'package:common/utils/color_util.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:repository/models/route_coordinate/route_coordinates.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public/utils/map_style.dart';
import 'package:public_bloc/route/bloc.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:repository/models/route/route.dart' as models;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

part 'package:public/screens/beranda/menu/route/widgets/navigation.dart';

class RouteDirectionsScreen extends StatefulWidget {
  RouteDirectionsScreen({Key key, this.routeCoordinates}) : super(key: key);
  final RouteCoordinates routeCoordinates;

  @override
  State<StatefulWidget> createState() {
    return _RouteDirectionsState();
  }
}

class _RouteDirectionsState extends State<RouteDirectionsScreen> {
  final String marketIdOrigin = "startingPoint";
  final String markerIdDest = "destination";
  final double cameraZoom = 18;
  final double cameraTilt = 0;
  final double cameraBearing = 0;
  final _controller = Completer();
  final _screen = FlutterScreenUtil();
  models.Route routeRepair = new models.Route();

  GoogleMapController _mapController;
  Set<Marker> _markers = {};
  Map<PolylineId, Polyline> _polylines = {};
  List<LatLng> _polylineCoordinates = [];
  double _roadRepairDiffDistance = 0.0;

  final _bloc = Modular.get<RouteStreetBloc>();

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  void initState() {
    if (mounted) {
      super.initState();
      _drawingRoute();
      _trackingLocation();
    }

    var initializationSettingsAndroid =
        AndroidInitializationSettings('Teman Jabar');
    var initializationSettingsIOs = IOSInitializationSettings();
    var initSetttings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOs);
    flutterLocalNotificationsPlugin.initialize(initSetttings);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          GoogleMap(
            zoomControlsEnabled: false,
            myLocationEnabled: true,
            myLocationButtonEnabled: false,
            compassEnabled: false,
            zoomGesturesEnabled: true,
            markers: _markers,
            mapType: MapType.satellite,
            polylines: Set<Polyline>.of(_polylines.values),
            initialCameraPosition: CameraPosition(
              target: LatLng(
                widget.routeCoordinates.originLat,
                widget.routeCoordinates.originLong,
              ),
            ),
            onMapCreated: _onMapCreated,
          ),
          _roadRepairDiffDistance > 0 && _roadRepairDiffDistance <= 1
              ? Column(
                  children: [
                    Container(
                      width: _screen.widthDefault(context),
                      alignment: Alignment.topCenter,
                      height: 120,
                      color: Colors.yellow,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: AppBar().preferredSize.height,
                          left: 20,
                          right: 20,
                        ),
                        child: Row(
                          children: [
                            Icon(Icons.warning,
                                color: Colors.black45, size: 50),
                            SizedBox(width: 10),
                            Flexible(
                              child: Text(
                                "Jarak 100m Didepan terdapat perbaikan jalan",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: _screen.fontSize(20),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              : Container(),
          _Navigation._(onTap: () {
            _cancelNotification();
          }),
        ],
      ),
    );
  }

  void _trackingLocation() {
    Geolocator.getPositionStream().listen((event) async {
      await _getRouteDirections();
      final result = await _findRouteRepair(event);
      final icon = await _setIcon(
        "assets/icons/markers/starting_point_pin.png",
      );
      final iconRepair = await _setIcon(
        "assets/icons/markers/road_repair.png",
      );
      CameraPosition currentPosition = CameraPosition(
        zoom: cameraZoom,
        tilt: cameraTilt,
        bearing: cameraBearing,
        target: LatLng(event.latitude, event.longitude),
      );
      double distanceInMeters = 0;
      if (result.latitude != null && result.longitude != null) {
        routeRepair = result;
        distanceInMeters = Geolocator.distanceBetween(
          event.latitude,
          event.longitude,
          result.latitude,
          result.longitude,
        );
      }



      if (mounted) {
        setState(() {
          _roadRepairDiffDistance = distanceInMeters;
          _updateMarker(
              LatLng(event.latitude, event.longitude),
              LatLng(
                result.latitude ?? 0,
                result.longitude ?? 0,
              ),
              icon,
              iconRepair);
          _mapController.animateCamera(
            CameraUpdate.newCameraPosition(currentPosition),
          );
        });
        if(result.activity != null ){
          showNotification(result.activity);
        } else {
          _cancelNotification();
        }
      }
    });
  }

  showNotification(String desc) async {
    var android = new AndroidNotificationDetails(
        'id', 'channel ', 'description',
        priority: Priority.high, importance: Importance.max);
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await flutterLocalNotificationsPlugin.show(
        0, 'Hati-Hati', 'Jarak 200m didepan ada $desc jalan', platform);
  }

  Future<void> _cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancel(0);
  }


  void _updateMarker(LatLng latLng, LatLng latLngRepair, BitmapDescriptor icon,
      BitmapDescriptor iconRepair) {
    _markers.removeWhere((m) => m.markerId.value == marketIdOrigin);
    _markers.add(
      Marker(
        markerId: MarkerId(marketIdOrigin),
        position: latLng,
        icon: icon,
      ),
    );
    if (latLngRepair.latitude != 0) {
      _markers.add(
        Marker(
          markerId: MarkerId("repair"),
          position: latLngRepair,
          icon: iconRepair,
        ),
      );
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(MapStyle.style);
    if (!_controller.isCompleted) {
      _controller.complete(controller);
    }
    _mapController = controller;
  }

  void _drawingRoute() async {
    final routes = await _getRouteDirections();
    await _setMarker(routes);
    await _setPolyline();
  }

  Future<void> _setPolyline() async {
    final coordiantes = await _getCoordinates();

    setState(() {
      if (_polylines.isNotEmpty) _polylines.clear();
      if (_polylineCoordinates.isNotEmpty) _polylineCoordinates.clear();

      if (coordiantes.isNotEmpty) {
        coordiantes.forEach((PointLatLng point) {
          _polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });
      }

      PolylineId id = PolylineId('poly');
      Polyline polyline = Polyline(
        polylineId: id,
        color: Theme.of(context).primaryColor,
        points: _polylineCoordinates,
        width: 5,
      );
      _polylines[id] = polyline;
    });
  }

  Future<void> _setMarker(routes) async {
    final originIcon = await _setIcon(
      "assets/icons/markers/starting_point_pin.png",
    );
    final destIcon = await _setIcon(
      "assets/icons/markers/destination_pin.png",
    );
    setState(() {
      if (_markers.isNotEmpty) _markers.clear();

      _markers.add(
        Marker(
          markerId: MarkerId(marketIdOrigin),
          position: LatLng(
            widget.routeCoordinates.originLat,
            widget.routeCoordinates.originLong,
          ),
          infoWindow: InfoWindow(
              title: 'Lokasi Anda',
              snippet: routes["legs"][0]["start_address"]),
          icon: originIcon,
        ),
      );
      _markers.add(
        Marker(
          markerId: MarkerId(markerIdDest),
          position: LatLng(
            widget.routeCoordinates.destLat,
            widget.routeCoordinates.destLong,
          ),
          infoWindow: InfoWindow(
            title: 'Lokasi Tujuan',
            snippet: routes["legs"][0]["end_address"],
          ),
          icon: destIcon,
        ),
      );
    });
  }

  Future<List<PointLatLng>> _getCoordinates() async {
    try {
      return await _bloc.getRouteBetweenCoordinates(
        routeCoordinates: widget.routeCoordinates,
      );
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
      return [];
    }
  }

  Future<models.Route> _findRouteRepair(Position position) async {
    try {
      return await _bloc.findRouteRepair(position);
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
      return new models.Route();
    }
  }

  Future<dynamic> _getRouteDirections() async {
    try {
      return await _bloc.getRouteDirections(
        routeCoordinates: widget.routeCoordinates,
      );
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
  }

  Future<BitmapDescriptor> _setIcon(String name) async {
    return await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 2.5),
      name,
    );
  }
}
