import 'dart:async';

import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:public/screens/beranda/menu/route/widgets/input_place.dart';
import 'package:public/screens/beranda/menu/route/widgets/my_location.dart';
import 'package:public/screens/beranda/menu/route/widgets/start_navigation.dart';
import 'package:repository/models/route_coordinate/route_coordinates.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location_permissions/location_permissions.dart' as lp;
import 'package:repository/constants/default_location.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public/utils/map_style.dart';
import 'package:public_bloc/route/bloc.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

class RouteScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RouteScreenState();
  }
}

class _RouteScreenState extends State<RouteScreen> {
  final _controller = Completer();
  final _initCameraPosition = CameraPosition(
    target: LatLng(DefaultLocation.Latitude, DefaultLocation.Longitude),
  );

  GoogleMapController _mapController;
  Set<Marker> _markers = {};
  Map<PolylineId, Polyline> _polylines = {};
  List<LatLng> _polylineCoordinates = [];
  bool _showNavigation = false;

  final _bloc = Modular.get<RouteStreetBloc>();

  void initState() {
    super.initState();
    _checkLocationPermission();
    _bloc.init();
    _getCurrentLocation();
  }

  void _checkLocationPermission() async {
    final statusFuture = await lp.LocationPermissions().checkPermissionStatus();
    if (statusFuture != lp.PermissionStatus.granted) {
      lp.PermissionStatus ps =
          await lp.LocationPermissions().requestPermissions(
        permissionLevel: lp.LocationPermissionLevel.locationWhenInUse,
      );
      if (ps != lp.PermissionStatus.granted) {
        return;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          GoogleMap(
            zoomControlsEnabled: false,
            myLocationEnabled: true,
            myLocationButtonEnabled: false,
            compassEnabled: false,
            zoomGesturesEnabled: true,
            markers: _markers,
            polylines: Set<Polyline>.of(_polylines.values),
            initialCameraPosition: _initCameraPosition,
            onMapCreated: _onMapCreated,
            mapType: MapType.satellite,
          ),
          InputPlace(
            title: "Rute Jalan",
            myLocation: () => _getCurrentLocation(),
            drawingRoute: (routes) => _drawingRoute(routes),
          ),
          MyLocation(onTap: () async => _getCurrentLocation()),
          _showNavigation ? StartNavigation() : Container(),
        ],
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(MapStyle.style);
    if (!_controller.isCompleted) {
      _controller.complete(controller);
    }
    _mapController = controller;
  }

  void _drawingRoute(RouteCoordinates routeCoordinates) async {
    final routes = await _getRouteDirections();
    await _setMarker(routeCoordinates, routes);
    await _setPolyline();

    setState(() {
      _mapController.animateCamera(
        CameraUpdate.newLatLngBounds(
          LatLngBounds(
            northeast: LatLng(
              routes["bounds"]["northeast"]["lat"],
              routes["bounds"]["northeast"]["lng"],
            ),
            southwest: LatLng(
              routes["bounds"]["southwest"]["lat"],
              routes["bounds"]["southwest"]["lng"],
            ),
          ),
          100.0,
        ),
      );
      _showNavigation = true;
    });
  }

  Future<void> _setPolyline() async {
    final coordiantes = await _getCoordinates();

    setState(() {
      if (_polylines.isNotEmpty) _polylines.clear();
      if (_polylineCoordinates.isNotEmpty) _polylineCoordinates.clear();

      if (coordiantes.isNotEmpty) {
        coordiantes.forEach((PointLatLng point) {
          _polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });
      }

      PolylineId id = PolylineId('poly');
      Polyline polyline = Polyline(
        polylineId: id,
        color: Theme.of(context).primaryColor,
        points: _polylineCoordinates,
        width: 3,
      );
      _polylines[id] = polyline;
    });
  }

  Future<void> _setMarker(RouteCoordinates routeCoordinates, routes) async {
    final originIcon = await _setIcon(
      "assets/icons/markers/starting_point_pin.png",
    );
    final destIcon = await _setIcon(
      "assets/icons/markers/destination_pin.png",
    );
    setState(() {
      if (_markers.isNotEmpty) _markers.clear();

      _markers.add(
        Marker(
          markerId: MarkerId("startingPoint"),
          position: LatLng(
            routeCoordinates.originLat,
            routeCoordinates.originLong,
          ),
          infoWindow: InfoWindow(
              title: 'Lokasi Anda',
              snippet: routes["legs"][0]["start_address"]),
          icon: originIcon,
        ),
      );
      _markers.add(
        Marker(
          markerId: MarkerId("destination"),
          position: LatLng(
            routeCoordinates.destLat,
            routeCoordinates.destLong,
          ),
          infoWindow: InfoWindow(
            title: 'Lokasi Tujuan',
            snippet: routes["legs"][0]["end_address"],
          ),
          icon: destIcon,
        ),
      );
    });
  }

  Future<List<PointLatLng>> _getCoordinates() async {
    try {
      return await _bloc.getRouteBetweenCoordinates();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
      return [];
    }
  }

  Future<void> _getCurrentLocation() async {
    try {
      final result = await _bloc.getCurrentLocation();
      setState(() {
        final longLat = LatLng(result.latitude, result.longitude);
        _mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: longLat,
              zoom: 18,
            ),
          ),
        );
      });
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
  }

  Future<dynamic> _getRouteDirections() async {
    try {
      return await _bloc.getRouteDirections();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
  }

  Future<BitmapDescriptor> _setIcon(String name) async {
    return await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 2.5),
      name,
    );
  }
}
