import 'package:common/utils/color_util.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:repository/models/route_coordinate/route_coordinates.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public_bloc/route/bloc.dart';
import 'package:google_place/google_place.dart';

class InputPlace extends StatelessWidget {
  InputPlace({
    this.myLocation,
    this.drawingRoute,
    this.title,
  });
  final String title;
  final Function myLocation;
  final Function(RouteCoordinates) drawingRoute;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: ClipOval(
                child: Material(
                  color: ColorUtil().parseHexColor("#fdfdfd"),
                  child: InkWell(
                    child: SizedBox(
                      width: 45,
                      height: 45,
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.black45,
                      ),
                    ),
                    onTap: () => Navigator.pop(context),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                  width: FlutterScreenUtil().widthDefault(context) * 0.9,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          title,
                          style: TextStyle(fontSize: 20.0),
                        ),
                        SizedBox(height: 10),
                        StreamBuilder(
                          initialData: "",
                          stream: Modular.get<RouteStreetBloc>().startingPoint,
                          builder: (context, snapshot) => _TextField._(
                            label: 'Lokasi Anda',
                            hint: 'Pilih lokasi',
                            initialValue: snapshot.data,
                            prefixIcon: Icon(Icons.location_history),
                            controller: TextEditingController(
                              text: snapshot.data,
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        StreamBuilder(
                          initialData: "",
                          stream: Modular.get<RouteStreetBloc>().destination,
                          builder: (context, snapshot) => _TextField._(
                            label: 'Tujuan',
                            hint: 'Pilih lokasi tujuan',
                            initialValue: snapshot.data,
                            prefixIcon: Icon(Icons.location_on),
                            controller: TextEditingController(
                              text: snapshot.data.toString(),
                            ),
                            drawingRoute: drawingRoute,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _TextField extends StatelessWidget {
  _TextField._({
    this.hint,
    this.label,
    this.locationCallback,
    this.prefixIcon,
    this.initialValue,
    this.controller,
    this.drawingRoute,
  });
  final String label;
  final String hint;
  final String initialValue;
  final Icon prefixIcon;
  final TextEditingController controller;
  final Function(String) locationCallback;
  final Function(RouteCoordinates) drawingRoute;
  @override
  Widget build(BuildContext context) {
    return Container(
      key: UniqueKey(),
      width: FlutterScreenUtil().widthDefault(context) * 0.8,
      child: TypeAheadField<AutocompletePrediction>(
        textFieldConfiguration: TextFieldConfiguration(
          controller: controller,
          decoration: InputDecoration(
            prefixIcon: prefixIcon,
            labelText: label,
            filled: true,
            fillColor: Colors.white,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
              borderSide: BorderSide(
                color: Colors.grey[400],
                width: 2,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
              borderSide: BorderSide(
                color: Theme.of(context).primaryColor,
                width: 2,
              ),
            ),
            contentPadding: EdgeInsets.all(15),
            hintText: hint,
          ),
        ),
        suggestionsCallback: (pattern) async {
          if (pattern.isNotEmpty) {
            return _getSuggestion(pattern);
          }
          return [];
        },
        keepSuggestionsOnLoading: true,
        hideOnEmpty: true,
        noItemsFoundBuilder: (context) => Container(width: 0, height: 0),
        hideOnError: true,
        hideSuggestionsOnKeyboardHide: true,
        itemBuilder: (context, suggestion) {
          return ListTile(
            dense: true,
            leading: Icon(
              Icons.location_pin,
              color: Theme.of(context).primaryColor,
            ),
            title: Text(suggestion.description),
          );
        },
        onSuggestionSelected: (suggestion) async => await _onHandleSelected(
          context,
          suggestion.placeId,
        ),
      ),
    );
  }

  Future<List<AutocompletePrediction>> _getSuggestion(String pattern) async {
    try {
      return await Modular.get<RouteStreetBloc>().getAddressPrediction(pattern);
    } on PlatformException catch (_) {
      return [];
    }
  }

  Future<void> _onHandleSelected(BuildContext context, String placeId) async {
    try {
      final isStartingPoint = label.contains("Tujuan") ? false : true;
      final routes = await Modular.get<RouteStreetBloc>().getDetailPlace(
        placeId,
        isStartingPoint,
      );
      if (!isStartingPoint) {
        drawingRoute(routes);
      }
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
  }
}
