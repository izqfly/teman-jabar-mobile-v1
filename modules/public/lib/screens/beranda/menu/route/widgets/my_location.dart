import 'package:common/utils/color_util.dart';
import 'package:flutter/material.dart';

class MyLocation extends StatelessWidget {
  MyLocation({@required this.onTap});
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Align(
        alignment: Alignment.topRight,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: ClipOval(
            child: Material(
              color: ColorUtil().parseHexColor("#fdfdfd"),
              child: InkWell(
                child: SizedBox(
                  width: 45,
                  height: 45,
                  child: Icon(
                    Icons.my_location,
                    color: Colors.black45,
                  ),
                ),
                onTap: onTap,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
