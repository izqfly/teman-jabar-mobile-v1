part of 'package:public/screens/beranda/menu/route/route_directions.dart';

class _Navigation extends StatelessWidget {
  _Navigation._({
    this.onTap
  });

  final _screen = FlutterScreenUtil();
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: FlutterScreenUtil().widthDefault(context),
        height: 100,
        decoration: BoxDecoration(
          color: ColorUtil().parseHexColor("#fdfdfd"),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              StreamBuilder(
                stream: Modular.get<RouteStreetBloc>().directions,
                builder: (context, snapshot) => Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: RichText(
                    text: TextSpan(
                      text: snapshot.hasData
                          ? "${snapshot.data["legs"][0]["duration"]["text"]}"
                          : "Checking...",
                      style: TextStyle(
                          color: Colors.green[300],
                          fontSize: _screen.fontSize(18)),
                      children: <TextSpan>[
                        TextSpan(
                          text: snapshot.hasData
                              ? " (${snapshot.data["legs"][0]["distance"]["text"]})"
                              : "",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: _screen.fontSize(18)),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 7),
              Row(
                children: [
                  RaisedButton.icon(
                    shape: StadiumBorder(),
                    onPressed: () {
                      //onTap;
                      Navigator.pop(context);
                    },
                    color: Colors.red[600],
                    icon: Icon(
                      Icons.clear,
                      color: Colors.white,
                    ),
                    label: Text(
                      "Stop",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: FlutterScreenUtil().fontSize(20),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
