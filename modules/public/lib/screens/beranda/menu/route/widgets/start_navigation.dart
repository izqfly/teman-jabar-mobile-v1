import 'package:common/utils/color_util.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:public/modules/constants/route_name.dart';
import 'package:public_bloc/route/bloc.dart';
import 'package:repository/models/route_coordinate/route_coordinates.dart';

class StartNavigation extends StatelessWidget {
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: FlutterScreenUtil().widthDefault(context),
        height: 100,
        decoration: BoxDecoration(
          color: ColorUtil().parseHexColor("#fdfdfd"),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: StreamBuilder(
            stream: Modular.get<RouteStreetBloc>().directions,
            builder: (context, snapshot) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: RichText(
                    text: TextSpan(
                      text: snapshot.hasData
                          ? "${snapshot.data["legs"][0]["duration"]["text"]}"
                          : "Checking...",
                      style: TextStyle(
                          color: Colors.green[300],
                          fontSize: _screen.fontSize(18)),
                      children: <TextSpan>[
                        TextSpan(
                          text: snapshot.hasData
                              ? " (${snapshot.data["legs"][0]["distance"]["text"]})"
                              : "",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: _screen.fontSize(18)),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 7),
                RaisedButton.icon(
                  shape: StadiumBorder(),
                  onPressed: () {
                    final data = snapshot.data["legs"][0];
                    final origin = data["start_location"];
                    final dest = data["end_location"];

                    Modular.link.pushNamed(
                      RouteName.RouteDirections,
                      arguments: RouteCoordinates(
                        originLat: origin["lat"],
                        originLong: origin["lng"],
                        destLat: dest["lat"],
                        destLong: dest["lng"],
                      ),
                    );
                  },
                  color: Theme.of(context).primaryColor,
                  icon: Icon(
                    Icons.navigation_outlined,
                    color: Colors.white,
                  ),
                  label: Text(
                    "Mulai",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: FlutterScreenUtil().fontSize(20),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
