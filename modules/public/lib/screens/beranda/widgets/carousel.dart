part of 'package:public/screens/landing_page/public_landing_page.dart';

class _Carousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      unlimitedMode: true,
      enableAutoSlider: true,
      slideBuilder: (index) {
        return Container(
          alignment: Alignment.center,
          child: Image.asset(
            sprintf(
              "assets/slider/beranda/hero0%s.jpg",
              [index + 1],
            ),
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.fill,
          ),
        );
      },
      slideTransform: ParallaxTransform(),
      slideIndicator: CircularSlideIndicator(
        alignment: Alignment.bottomRight,
        currentIndicatorColor: Colors.white,
        indicatorRadius: 5,
        itemSpacing: 15,
        padding: EdgeInsets.only(bottom: 32, right: 20),
      ),
      itemCount: 3,
    );
  }
}
