part of 'package:public/screens/landing_page/public_landing_page.dart';

class _MainMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(
        3,
        (index) => _MenuCard._(index: index),
      ),
    );
  }
}

class _MenuCard extends StatelessWidget {
  _MenuCard._({this.index});
  final int index;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width / 3) - 20,
      child: Column(
        children: [
          FlatButton(
            padding: const EdgeInsets.all(0),
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: () async {
              if (index > 1) {
                final result = await Modular.to.pushNamed(
                  "${RouteName.BaseModule}${RouteName.LandingPage}${RouteName.Beranda}${_getRoute()}",
                );

                if (result != null && result != "") {
                  AlertMessage.showToast(
                    context: context,
                    message: "Data berhasil disimpan",
                    backgroundColor: Colors.black,
                    textColor: Colors.white,
                  );
                }
              } else {
                Modular.to.pushNamed(
                  "${RouteName.BaseModule}${RouteName.LandingPage}${RouteName.Beranda}${_getRoute()}",
                );
              }
            },
            child: Card(
              elevation: 3,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Container(
                height: 65,
                width: 65,
                padding: const EdgeInsets.all(10),
                child: Icon(
                  _getIcon(),
                  size: 45,
                  color: _getColor(),
                ),
              ),
            ),
          ),
          SizedBox(height: 5),
          Text(
            _getTitle(),
            style: TextStyle(
                color: Colors.black54,
                fontSize: FlutterScreenUtil().fontSize(14)),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  String _getRoute() {
    switch (index) {
      case 0:
        return RouteName.RouteModule;
      case 1:
        return RouteName.RouteModule + RouteName.EidRute;
      default:
        return RouteName.ReportingStreet;
    }
  }

  String _getTitle() {
    switch (index) {
      case 0:
        return "Rute";
      case 1:
        return "Jalur Lebaran";
      default:
        return "Pengaduan Jalan";
    }
  }

  IconData _getIcon() {
    switch (index) {
      case 0:
        return Icons.navigation;
      case 1:
        return Icons.map;
      default:
        return Icons.report;
    }
  }

  Color _getColor() {
    switch (index) {
      case 0:
        return Colors.blue[400];
      case 1:
        return Colors.green;
      default:
        return Colors.yellow[800];
    }
  }
}
