import 'package:auth_bloc/auth_bloc.dart';
import 'package:common/constants/base_path_asset.dart';
import 'package:common/helper/fcm.dart';
import 'package:common/utils/color_util.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:common/widgets/bottom_bar.dart';
import 'package:common/widgets/user_account.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_carousel_slider/carousel_slider_indicators.dart';
import 'package:flutter_carousel_slider/carousel_slider_transforms.dart';
import 'package:location/location.dart';
import 'package:public/modules/constants/route_name.dart';
import 'package:public_bloc/reporting_street/bloc.dart';
import 'package:sprintf/sprintf.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:repository/models/notification/notification.dart' as modelNotif;
import 'package:repository/models/report/report.dart';
import 'package:repository/common/flutter_local_storage.dart';

part 'package:public/screens/beranda/beranda_screen.dart';
part 'package:public/screens/beranda/widgets/carousel.dart';
part 'package:public/screens/beranda/widgets/costum_app_bar.dart';
part 'package:public/screens/beranda/widgets/beranda_title.dart';
part 'package:public/screens/beranda/widgets/main_menu.dart';

part 'package:public/screens/notification/notification_screen.dart';
part 'package:public/screens/my_report/my_report_screen.dart';

part 'package:public/screens/landing_page/widgets/app_drawer.dart';
part 'package:public/screens/landing_page/widgets/body.dart';
part 'package:public/screens/landing_page/widgets/bottom_bar.dart';

class PublicLandingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PublicLandingPageState();
  }
}

class _PublicLandingPageState extends State<PublicLandingPage> {
  final _indexNotifier = ValueNotifier<int>(0);
  final _landingPageKey = GlobalKey<ScaffoldState>();

  void initState() {
    super.initState();
    _requestLocPermission();
    Fcm().initializeNotification(context);
  }

  void _requestLocPermission() async {
    final location = new Location();

    bool _serviceEnabled;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: _indexNotifier,
      builder: (context, value, child) => Scaffold(
        key: _landingPageKey,
        drawer: _AppDrawer(),
        appBar: value == 0
            ? null
            : AppBar(
                elevation: 0,
                title: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      "${BasePathAsset.icons}/common/temanjabar-logo.png",
                      height: AppBar().preferredSize.height / 1.5,
                    ),
                    SizedBox(width: 10),
                    Text(
                      "Teman Jabar",
                      style: TextStyle(
                          fontSize: FlutterScreenUtil().fontSize(18),
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    )
                  ],
                ),
              ),
        body: _Body._(index: value, landingPageKey: _landingPageKey),
        bottomNavigationBar: _BottomBar._(
          currentIndex: value,
          indexNotifier: _indexNotifier,
        ),
      ),
    );
  }
}
