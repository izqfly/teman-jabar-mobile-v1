part of 'package:public/screens/landing_page/public_landing_page.dart';

class _AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      // key: landingPageKey,
      child: ListView(
        padding: EdgeInsets.zero,
        addAutomaticKeepAlives: false,
        children: <Widget>[
          _DrawerHeader(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List.generate(
              4,
              (index) => _DrawerItem._(
                index: index,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _DrawerHeader extends StatelessWidget {
  final _storage = FlutterLocalStorage();
  @override
  Widget build(BuildContext context) {
    return UserAccountsDrawerHeader(
      accountName: Text(_storage?.user?.fullname ?? ""),
      accountEmail: Text(_storage?.user?.email ?? "-"),
      currentAccountPicture: CircleAvatar(
        backgroundColor: Colors.white,
        child: Text(
          _storage?.user?.getFistCharacterFullname(),
          style: TextStyle(fontSize: 40.0),
        ),
      ),
    );
  }
}

class _DrawerItem extends StatelessWidget {
  _DrawerItem._({this.index});
  final int index;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          selected: index == 0,
          title: Text(_getTitle()),
          leading: Icon(_getIconData()),
          onTap: () {
            if (index == 3) {
              _showAlertDialog(context);
            } else if (index == 1 || index == 2) {
              final routeName =
                  index == 1 ? RouteName.About : RouteName.Settings;
              Modular.to.pushNamed(
                "${RouteName.BaseModule}${RouteName.LandingPage}$routeName",
              );
            }
          },
        ),
        index == 2
            ? Divider(color: Colors.black45, thickness: 0.5)
            : Container(),
      ],
    );
  }

  String _getTitle() {
    switch (index) {
      case 1:
        return "Tentang Kami";
      case 2:
        return "Settings";
      case 3:
        return "Keluar";
      default:
        return "Beranda";
    }
  }

  IconData _getIconData() {
    switch (index) {
      case 1:
        return Icons.help;
      case 2:
        return Icons.settings;
      case 3:
        return Icons.exit_to_app;
      default:
        return Icons.dashboard;
    }
  }

  void _showAlertDialog(BuildContext context) async {
    AlertDialog alert = AlertDialog(
      title: Text("Konfirmasi"),
      content: Text("Apakah anda ingin keluar dari Teman Jabar?"),
      actions: [
        FlatButton(
          child: Text(
            "Batal",
            style: TextStyle(color: Colors.black),
          ),
          onPressed: () => Modular.navigator.pop(),
        ),
        FlatButton(
          child: Text(
            "Keluar",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () async {
            await Modular.get<AuthBloc>().logout();
            Modular.navigator.pop();
            Modular.to.pushNamedAndRemoveUntil(
              "${RouteName.BaseModule}",
              (Route<dynamic> route) => false,
            );
          },
        ),
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
