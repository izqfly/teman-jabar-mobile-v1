part of 'package:public/screens/landing_page/public_landing_page.dart';

class _Body extends StatelessWidget {
  _Body._({Key key, this.index, this.landingPageKey}) : super(key: key);
  final int index;
  final GlobalKey<ScaffoldState> landingPageKey;
  @override
  Widget build(BuildContext context) {
    switch (index) {
      case 1:
        return _Notificationcreen();
      case 2:
        return _MyReportScreen();
      case 3:
        return UserAccount();
      default:
        return _BerandaScreen._(landingPageKey: landingPageKey);
    }
  }
}