part of 'package:public/screens/landing_page/public_landing_page.dart';

class _MyReportScreen extends StatelessWidget {
  final _bloc = Modular.get<ReportingStreetBloc>();
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 70,
          color: Theme.of(context).primaryColor,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Daftar Laporan",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: _screen.fontSize(16),
                  ),
                ),
                SizedBox(height: 7),
                Text(
                  "Proses Pengaduan Masyarakat",
                  style: TextStyle(
                    color: Colors.grey[200],
                    fontSize: _screen.fontSize(14),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: PagewiseListView(
            pageSize: 10,
            addAutomaticKeepAlives: false,
            pageFuture: (index) async => await _getAll(context, index),
            itemBuilder: (context, entry, index) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _CardReport._(model: entry),
                Divider(
                  thickness: 0.5,
                  color: Colors.black45,
                )
              ],
            ),
            noItemsFoundBuilder: (context) {
              return Center(child: Text("Tidak ada data yang ditemukan"));
            },
            loadingBuilder: (context) {
              return Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                    Theme.of(context).primaryColor),
              ));
            },
          ),
        )
      ],
    );
  }

  Future<List<Report>> _getAll(BuildContext context, int index) async {
    try {
      print("Index"+index.toString());
      return await _bloc.getAll(index*10);
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: e.message);
    }
    return <Report>[];
  }
}

class _CardReport extends StatelessWidget {
  _CardReport._({this.model});

  final Report model;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        //"tes 1",
        model?.name != null ? "" + model.name : "",
        style: TextStyle(
          fontSize: FlutterScreenUtil().fontSize(15),
        ),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            //"tes 2",
            model?.reportType != null ? model.reportType : "",
            style: TextStyle(
              color: Colors.black54,
              fontSize: FlutterScreenUtil().fontSize(14),
            ),
          ),
          SizedBox(height: 5),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              color: model?.status  != null ? _getColorStatus(model.status) : Colors.blue,
            ),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                //"tes 3",
                model?.status != null ? model?.status ?? "" : "",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: FlutterScreenUtil().fontSize(12),
                ),
              ),
            ),
          )
        ],
      ),
      trailing: Icon(Icons.arrow_forward_ios, size: 16),
    );
  }

  Color _getColorStatus(String status) {
    final color = ColorUtil();
    switch (status) {
      case "Submited":
        return color.parseHexColor("#007bff");
      case "Approved":
        return color.parseHexColor("#545b62");
      case "On Progress":
        return color.parseHexColor("#c69500");
      default:
        return color.parseHexColor("#1e7e34");
    }
  }
}
