part of 'package:public/screens/landing_page/public_landing_page.dart';

class _Notificationcreen extends StatelessWidget {
  final _bloc = Modular.get<ReportingStreetBloc>();
  final _screen = FlutterScreenUtil();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 70,
          color: Theme.of(context).primaryColor,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Pemberitahuan",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: _screen.fontSize(16),
                  ),
                ),
                SizedBox(height: 7),
                Text(
                  "Pemberitahuan Terbaru",
                  style: TextStyle(
                    color: Colors.grey[200],
                    fontSize: _screen.fontSize(14),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: PagewiseListView(
            pageSize: 10,
            addAutomaticKeepAlives: false,
            pageFuture: (index) async => await _getAll(context),
            itemBuilder: (context, entry, index) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _CardNotif._(model: entry),
                Divider(
                  thickness: 0.5,
                  color: Colors.black45,
                )
              ],
            ),
            noItemsFoundBuilder: (context) {
              return Center(child: Text("Tidak ada data yang ditemukan"));
            },
            loadingBuilder: (context) {
              return Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                    Theme.of(context).primaryColor),
              ));
            },
          ),
        )
      ],
    );
  }

  Future<List<modelNotif.Notification>> _getAll(BuildContext context) async {
    try {
      return await _bloc.getNotificationReport();
    } on PlatformException catch (e) {
      AlertMessage.showToast(context: context, message: "Tidak ada data ditemukan");
    }
    return <modelNotif.Notification>[];
  }
}

class _CardNotif extends StatelessWidget {
  _CardNotif._({this.model});

  final modelNotif.Notification model;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(
        "${BasePathAsset.icons}/common/help.png",
        // width: 50,
        // height: 50,
        fit: BoxFit.fitWidth,
      ),
      title: Text(
        model.title,
        style: TextStyle(
          fontSize: FlutterScreenUtil().fontSize(15),
        ),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            model.subtitle,
            style: TextStyle(
              color: Colors.black54,
              fontSize: FlutterScreenUtil().fontSize(14),
            ),
          ),
          SizedBox(height: 5),
          Text(
            model.datePublish,
            style: TextStyle(
              color: Colors.black45,
              fontSize: FlutterScreenUtil().fontSize(12),
            ),
          )
        ],
      ),
    );
  }
}
