import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:common/widgets/form_input/costum_form_field.dart';
import 'package:auth_bloc/auth_bloc.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/costum_argon_button.dart';
import 'package:common/widgets/alerts/alert_message.dart';
import 'package:repository/common/flutter_local_storage.dart';

class ChangeDetail extends StatelessWidget {
  final _bloc = Modular.get<AuthBloc>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Detail Profil"),
        elevation: 0,
      ),
      body: _ChangeDetail._(
        onTap: (startLoading, stopLoading) {
          _onHandlerChangePassword(context, startLoading, stopLoading);
        },
        stream: _bloc.isValidChangePassword,
        passwordNew: _bloc.onChangedPasswordNew,
        passwordOld: _bloc.onChangedPassword,
        confirmPassword: _bloc.onChangedConfirmPassword,
      ),
    );
  }

  Future<void> _onHandlerChangePassword(
    BuildContext context,
    Function startLoading,
    Function stopLoading,
  ) async {
    try {
      startLoading();
      await _bloc.changePassword();
      stopLoading();
      _bloc..onChangedConfirmPassword(null);
      _bloc.onChangedPassword(null);
      _bloc.onChangedPasswordNew(null);
      AlertMessage.showToast(
        context: context,
        message: "Kata sandi berhasil diubah",
        textColor: Colors.white,
        backgroundColor: Colors.black45.withOpacity(0.5),
      );
    } on PlatformException catch (e) {
      stopLoading();
      _bloc..onChangedConfirmPassword(null);
      _bloc.onChangedPassword(null);
      _bloc.onChangedPasswordNew(null);
      AlertMessage.showToast(context: context, message: e.message);
    }
  }
}

class _ChangeDetail extends StatelessWidget {
  _ChangeDetail._(
      {this.onTap,
      this.stream,
      this.confirmPassword,
      this.passwordNew,
      this.passwordOld});

  final _screen = FlutterScreenUtil();
  final Function(Function startLoading, Function stopLoading) onTap;
  final Stream stream;
  final Function passwordOld;
  final Function passwordNew;
  final Function confirmPassword;
  final _storage = FlutterLocalStorage();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            SizedBox(height: 20),
            CostumTextFormField(
              title: "Nama",
              readOnly: true,
              initialValue: _storage?.user?.fullname,
            ),
            CostumTextFormField(
              title: "Email",
              readOnly: true,
              initialValue: _storage?.user?.email,
            ),
            CostumTextFormField(
              onChanged: passwordNew,
              title: "Alamat",
              hintText: "Masukkan alamat",
              initialValue: _storage?.user?.address ?? "",
            ),
            CostumTextFormField(
              onChanged: confirmPassword,
              title: "No. Handphone",
              hintText: "Masukkan No. Handphone",
              initialValue: _storage?.user?.phoneNumber ?? "",
            ),
            SizedBox(height: 40),
            Align(
              alignment: Alignment.center,
              child: CostumArgonButton(
                onTap: onTap,
                stream: stream,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    "Simpan",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: FlutterScreenUtil().fontSize(18),
                        letterSpacing: 1,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
