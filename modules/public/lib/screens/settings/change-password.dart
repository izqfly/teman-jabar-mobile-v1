import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:common/widgets/form_input/costum_form_field.dart';
import 'package:auth_bloc/auth_bloc.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:common/utils/flutter_screen_util.dart';
import 'package:common/widgets/costum_argon_button.dart';
import 'package:common/widgets/alerts/alert_message.dart';

class ChangePassword extends StatelessWidget{
  final _bloc = Modular.get<AuthBloc>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ganti Kata Sandi"),
        elevation: 0,
      ),
      body: _ChangePassword._(
        onTap: (startLoading, stopLoading) {
          _onHandlerChangePassword(context, startLoading, stopLoading);
        },
        stream: _bloc.isValidChangePassword,
        passwordNew: _bloc.onChangedPasswordNew,
        passwordOld: _bloc.onChangedPassword,
        confirmPassword: _bloc.onChangedConfirmPassword,
      ),
    );
  }
  Future<void> _onHandlerChangePassword(
      BuildContext context,
      Function startLoading,
      Function stopLoading,
      ) async {
    try {
      startLoading();
      await _bloc.changePassword();
      stopLoading();
      _bloc..onChangedConfirmPassword(null);
      _bloc.onChangedPassword(null);
      _bloc.onChangedPasswordNew(null);
      AlertMessage.showToast(
        context: context,
        message: "Kata sandi berhasil diubah",
        textColor: Colors.white,
        backgroundColor: Colors.black45.withOpacity(0.5),
      );
    } on PlatformException catch (e) {
      stopLoading();
      _bloc..onChangedConfirmPassword(null);
      _bloc.onChangedPassword(null);
      _bloc.onChangedPasswordNew(null);
      AlertMessage.showToast(context: context, message: e.message);
    }
  }
}

class _ChangePassword extends StatelessWidget {
  _ChangePassword._(
      {this.onTap,
        this.stream,
        this.confirmPassword,
        this.passwordNew,
        this.passwordOld});

  final _screen = FlutterScreenUtil();
  final Function(Function startLoading, Function stopLoading) onTap;
  final Stream stream;
  final Function passwordOld;
  final Function passwordNew;
  final Function confirmPassword;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          SizedBox(height: 20),
          Text(
            "Ganti Kata Sandi",
            style: TextStyle(
              fontSize: _screen.fontSize(16),
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 15),
          CostumTextFormField(
            onChanged: passwordOld,
            title: "Kata Sandi Lama",
            hintText: "Masukkan kata sandi lama",
          ),
          CostumTextFormField(
            onChanged: passwordNew,
            title: "Kata Sandi Baru",
            hintText: "Masukkan kata sandi baru",
          ),
          CostumTextFormField(
            onChanged: confirmPassword,
            title: "Konfirmasi Kata Sandi Baru",
            hintText: "Masukkan kata sandi baru",
          ),
          SizedBox(height: 40),
          Align(
            alignment: Alignment.center,
            child: CostumArgonButton(
              onTap: onTap,
              stream: stream,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                  "Simpan",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: FlutterScreenUtil().fontSize(18),
                      letterSpacing: 1,
                      fontWeight: FontWeight.w700),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}