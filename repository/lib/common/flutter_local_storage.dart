import 'package:localstorage/localstorage.dart';
import 'package:repository/models/user/user.dart';

class FlutterLocalStorage {
  final _appStorage = new LocalStorage("app");
  final _user = "user";
  final _token = "token";

  static final FlutterLocalStorage _singleton =
      new FlutterLocalStorage._internal();

  factory FlutterLocalStorage() {
    return _singleton;
  }

  FlutterLocalStorage._internal();

  LocalStorage getUserStorage() {
    return _appStorage;
  }

  Future<void> saveAppStorage(dynamic param) async {
    _appStorage.setItem(_user, param["user"]);
    _appStorage.setItem(_token, param["token"]["access_token"]);
  }

  User get user => _appStorage.getItem(_user) != null
      ? User().fromJson(_appStorage.getItem(_user))
      : new User();


  String get token => _appStorage.getItem(_token);

  Future<void> clearAppStorage() async {
    await _appStorage.clear();
  }
}
