class ErrorCode {
  static const internalServerError = "internal_server_error";
  static const failedLoadData = "failed_load_data";
  static const failedSaveData = "failed_save_data";
  static const timeout = "timeoout";
  static const entityToLarge = "entity_to_large";
}
