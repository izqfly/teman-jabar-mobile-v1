class ErrorMessage {
  static const timeout =
      'Tidak dapat terhubung ke server. Waktu sambungan telah habis';
  static const serverOffline =
      "Tidak bisa terhubung ke server. Silahkan coba beberapa saat lagi.";
  static const unauthorized = "Sesi anda sudah habis. Silahkan login kembali.";
  static const maximumUpload =
      "Upload media berukuran lebih besar dari batas maksimum";
  static const internalServer =
      "Mohon maaf, sedang terjadi gangguan pada sistem. Silahkan coba lagi nanti.";
  static const apiNotFound = "API Method not found";
  static const failedLoadData = "Gagal menampilkan";
  static const failedLoadDetailData = "Gagal menampilkan detail";
  static const failedToSave = "Gagal menyimpan data";
  static const failedToUpload = "Gagal upload";
}
