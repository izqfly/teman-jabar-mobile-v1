import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:google_place/google_place.dart';
import 'package:repository/constants/env.dart';
import 'package:repository/constants/error_code.dart';
import 'package:repository/constants/error_message.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:repository/models/route_coordinate/route_coordinates.dart';
import 'package:http/http.dart' as http;

class GoogleApiService {
  final googlePlace = GooglePlace(Env.googleAMapApiKey);

  Future<DetailsResult> getDetailPlace(String placeId) async {
    var response = await googlePlace.details.get(placeId);
    if (response != null && response.result != null) {
      return response.result;
    } else {
      throw PlatformException(
        code: ErrorCode.failedLoadData,
        message: "${ErrorMessage.failedLoadDetailData} lokasi",
      );
    }
  }

  Future<List<AutocompletePrediction>> getAddressPrediction(
      String address) async {
    var response = await googlePlace.autocomplete.get(address, language: "id");
    if (response != null && response.predictions != null) {
      return response.predictions;
    } else {
      throw PlatformException(
        code: ErrorCode.failedLoadData,
        message: "${ErrorMessage.failedLoadData} lokasi",
      );
    }
  }

  Future<List<PointLatLng>> getRouteBetweenCoordinates(
    RouteCoordinates coordinates,
  ) async {
    final response = await PolylinePoints().getRouteBetweenCoordinates(
      Env.googleAMapApiKey,
      PointLatLng(coordinates.originLat, coordinates.originLong),
      PointLatLng(coordinates.destLat, coordinates.destLong),
    );
    if (response.points.isNotEmpty) {
      return response.points;
    } else {
      throw PlatformException(
        code: ErrorCode.failedLoadData,
        message: "Gagal mengambil data koordinat lokasi",
      );
    }
  }

  Future<dynamic> getRouteDirections(
    RouteCoordinates coordinates,
  ) async {
    String url =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${coordinates.originLat},${coordinates.originLong}&destination=${coordinates.destLat},${coordinates.destLong}&key=${Env.googleAMapApiKey}";
    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      Map values = jsonDecode(response.body);
      if (values.containsKey("error_message")) {
        throw PlatformException(
          code: ErrorCode.failedLoadData,
          message: "Gagal mengambil data koordinat lokasi",
        );
      } else {
        return values["routes"][0];
      }
    } else {
      throw PlatformException(
        code: ErrorCode.failedLoadData,
        message: "Gagal mengambil data koordinat lokasi",
      );
    }
  }
}
