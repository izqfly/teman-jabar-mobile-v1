import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:sprintf/sprintf.dart';

class DummyCreator {
  static final DummyCreator _singleton = new DummyCreator._internal();

  factory DummyCreator() {
    return _singleton;
  }

  DummyCreator._internal();

  Future<dynamic> create(String jsonFile) async {
    try {
      String content = await rootBundle
          .loadString(sprintf("assets/dummy/%s.json", [jsonFile]));
      Map<String, dynamic> jsonMap = json.decode(content);
      return jsonMap;
    } catch (e) {
      throw PlatformException(
        code: "ERROR_CREATE_DUMMY",
        message: e,
      );
    }
  }
}
