import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:repository/constants/error_message.dart';

class JsonWebToken {
  static final JsonWebToken _singleton = new JsonWebToken._internal();

  final _storage = FlutterLocalStorage();

  factory JsonWebToken() {
    return _singleton;
  }

  JsonWebToken._internal();

  Future<void> validateToken() async {
    if (_storage.token != null) {
      dynamic payload = parseJwt(_storage.token);
      int exp = payload["exp"];

      String date = DateTime.now().millisecondsSinceEpoch.toString();
      int now = int.parse(date.substring(0, date.length - 3));

      if (now > exp) {
        await _storage.clearAppStorage();
        throw PlatformException(
          code: "UNAUTHORIZED",
          message: ErrorMessage.unauthorized,
        );
      }
    }
  }

  Map<String, dynamic> parseJwt(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      throw PlatformException(
        code: "UNAUTHORIZED",
        message: ErrorMessage.unauthorized,
      );
    }

    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw PlatformException(
        code: "UNAUTHORIZED",
        message: ErrorMessage.unauthorized,
      );
    }
    return payloadMap;
  }

  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw PlatformException(
          code: "UNAUTHORIZED",
          message: ErrorMessage.unauthorized,
        );
    }

    return utf8.decode(base64Url.decode(output));
  }
}
