import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:repository/common/flutter_local_storage.dart';
import 'package:repository/constants/env.dart';
import 'package:repository/constants/error_code.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/response/network_respone.dart';
import 'package:sprintf/sprintf.dart';
import 'package:http/http.dart' as http;
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

class FlutterRestClient {
  final _baseUrl = Env.baseUrl;
  final _networkResponses = NetworkResponse();
  final _storage = FlutterLocalStorage();
  final _timeout = 30;

  Future<dynamic> post(dynamic endpoint, dynamic body) async {
    var result;
    try {
      final url = sprintf("%s%s", [_baseUrl, endpoint]);
      final response = await http
          .post(
            url,
            headers: await _dynamicHeader(),
            body: body?.toJson(),
          )
          .timeout(Duration(seconds: _timeout));

      result = await _networkResponses.notify(response);
    } on TimeoutException catch (_) {
      throw PlatformException(
        code: ErrorCode.timeout,
        message: ErrorMessage.timeout,
      );
    } on SocketException {
      throw PlatformException(
        code: ErrorCode.internalServerError,
        message: ErrorMessage.internalServer,
      );
    }

    return result;
  }

  Future<dynamic> put(dynamic endpoint, dynamic body) async {
    var result;
    try {
      final url = sprintf("%s/%s", [_baseUrl, endpoint]);
      final response = await http
          .put(
            url,
            headers: await _dynamicHeader(),
            body: body.toJson(),
          )
          .timeout(Duration(seconds: _timeout));
      result = await _networkResponses.notify(response);
    } on TimeoutException catch (_) {
      throw PlatformException(
        code: ErrorCode.timeout,
        message: ErrorMessage.timeout,
      );
    } on SocketException {
      throw PlatformException(
        code: ErrorCode.internalServerError,
        message: ErrorMessage.internalServer,
      );
    }
    return result;
  }

  Future<dynamic> get(dynamic endpoint) async {
    var result;
    try {
      final url = sprintf("%s%s", [_baseUrl, endpoint]);
      final response = await http
          .get(
            url,
            headers: await _dynamicHeader(),
          )
          .timeout(Duration(seconds: _timeout));
      //print(response.body);
      result = _networkResponses.notify(response);
    } on TimeoutException catch (_) {
      throw PlatformException(
        code: ErrorCode.timeout,
        message: ErrorMessage.timeout,
      );
    } on SocketException {
      throw PlatformException(
        code: ErrorCode.internalServerError,
        message: ErrorMessage.internalServer,
      );
    }
    return result;
  }

  Future<dynamic> multipartFile(
    List<Map<String, File>> files,
    dynamic endpoint,
    String method, {
    dynamic body,
  }) async {
    var result;
    try {
      final url = sprintf("%s%s", [_baseUrl, endpoint]);
      final request = http.MultipartRequest(method, Uri.parse(url));

      request.headers["Authorization"] = sprintf("Bearer %s", [_storage.token]);

      if (files.length > 0) {
        for (int i = 0; i < files.length; i++) {
          files[i].forEach((key, data) async {
            final mimes =
                lookupMimeType(data.path, headerBytes: [0xFF, 0xD8]).split('/');
            final file = await http.MultipartFile.fromPath(key, data.path,
                contentType: MediaType(mimes[0], mimes[1]));
            request.files.add(file);
          });
        }
      }

      if (body != null) {
        final jsonStr = body.toJson();
        var fields = json.decode(jsonStr);

        Map<String, String> field = new Map<String, String>();

        fields.forEach((key, data) {
          if (key != null) {
            Map<String, String> item = new Map<String, String>();
            if (data is List) {
              int index = 0;
              data.forEach((element) {
                item.addAll({key.toString() + "[$index]": element.toString()});
                index++;
              });
            } else {
              item = {key.toString(): data.toString()};
            }
            field.addAll(item);
          }
        });

        request.fields.addAll(field);
      }

      //print("REQUEST = "+request.fields.toString());

      final streamedResponse =
          await request.send().timeout(Duration(seconds: _timeout));
      final response = await http.Response.fromStream(streamedResponse);

      result = _networkResponses.notify(response);
    } on TimeoutException catch (_) {
      throw PlatformException(
        code: ErrorCode.timeout,
        message: ErrorMessage.timeout,
      );
    } on SocketException {
      throw PlatformException(
        code: ErrorCode.internalServerError,
        message: ErrorMessage.internalServer,
      );
    }

    return result;
  }

  Future<dynamic> multipartAndSqlite(
      List<Map<String, File>> files,
      dynamic endpoint,
      String method, {
        dynamic body,
      }) async {
    var result;
    try {
      final url = sprintf("%s%s", [_baseUrl, endpoint]);
      final request = http.MultipartRequest(method, Uri.parse(url));

      request.headers["Authorization"] = sprintf("Bearer %s", [_storage.token]);

      if (files.length > 0) {
        for (int i = 0; i < files.length; i++) {
          files[i].forEach((key, data) async {
            final mimes =
            lookupMimeType(data.path, headerBytes: [0xFF, 0xD8]).split('/');
            final file = await http.MultipartFile.fromPath(key, data.path,
                contentType: MediaType(mimes[0], mimes[1]));
            request.files.add(file);
          });
        }
      }

      if (body != null) {
        final jsonStr = body.toJson();
        var fields = json.decode(jsonStr);

        Map<String, String> field = new Map<String, String>();

        fields.forEach((key, data) {
          if (key != null) {
            Map<String, String> item = new Map<String, String>();
            if (data is List) {
              int index = 0;
              data.forEach((element) {
                item.addAll({key.toString() + "[$index]": element.toString()});
                index++;
              });
            } else {
              item = {key.toString(): data.toString()};
            }
            field.addAll(item);
          }
        });

        request.fields.addAll(field);
      }

      //print("REQUEST = "+request.fields.toString());

      final streamedResponse =
      await request.send().timeout(Duration(seconds: _timeout));
      final response = await http.Response.fromStream(streamedResponse);

      result = _networkResponses.notify(response);
    } on TimeoutException catch (_) {
      return result;
    } on SocketException {
      return result;
    }

    return result;
  }

  Future<dynamic> multipartBackground(
      List<Map<String, File>> files,
      String token,
      dynamic endpoint,
      String method, {
        dynamic body,
      }) async {
    var result;
    try {
      final url = sprintf("%s%s", [_baseUrl, endpoint]);
      final request = http.MultipartRequest(method, Uri.parse(url));

      request.headers["Authorization"] = sprintf("Bearer %s", [token]);

      if (files.length > 0) {
        for (int i = 0; i < files.length; i++) {
          files[i].forEach((key, data) async {
            final mimes =
            lookupMimeType(data.path, headerBytes: [0xFF, 0xD8]).split('/');
            final file = await http.MultipartFile.fromPath(key, data.path,
                contentType: MediaType(mimes[0], mimes[1]));
            request.files.add(file);
          });
        }
      }

      if (body != null) {
        final jsonStr = body.toJson();
        var fields = json.decode(jsonStr);

        Map<String, String> field = new Map<String, String>();

        fields.forEach((key, data) {
          if (key != null) {
            Map<String, String> item = new Map<String, String>();
            if (data is List) {
              int index = 0;
              data.forEach((element) {
                item.addAll({key.toString() + "[$index]": element.toString()});
                index++;
              });
            } else {
              item = {key.toString(): data.toString()};
            }
            field.addAll(item);
          }
        });

        request.fields.addAll(field);
      }

      //print("REQUEST = "+request.fields.toString());

      final streamedResponse =
      await request.send().timeout(Duration(seconds: _timeout));
      final response = await http.Response.fromStream(streamedResponse);

      result = _networkResponses.notify(response);
    } on TimeoutException catch (_) {
      return result;
    } on SocketException {
      return result;
    }

    return result;
  }

  dynamic _dynamicHeader() {
    String token = _storage.token;
    if (token != "" && token != null) {
      return {
        "Content-Type": "application/json",
        "Authorization": sprintf("Bearer %s", [token])
      };
    } else {
      return {"Content-Type": "application/json"};
    }
  }
}
