import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/http/services/anouncement_service.dart';
import 'package:repository/models/anouncement/anouncement.dart';

class AnouncementMiddlemanService {
  final _service = AnouncementService();

  Future<List<Anouncement>> getList(String role) async {
    final response = await _service.getList(role);
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => Anouncement().fromJson(it))
          .toList()
          .cast<Anouncement>();

      //print("DATA == " + response.getData());
      return result;
    } else {
      throw PlatformException(
        code: "getNotification",
        message: ErrorMessage.failedLoadData,
      );
    }
  }
}
