import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/http/services/auth_service.dart';
import 'package:repository/models/user/user.dart';
import 'package:repository/response/json_response.dart';

class AuthMiddlemanService {
  final _service = AuthService();

  Future<dynamic> login(User u) async {
    JsonResponse jsonResponse = await _service.login(u);
    if (jsonResponse != null && jsonResponse.isSuccess()) {
      return jsonResponse.getData();
    } else {
      throw PlatformException(
        code: "login",
        message: "Username atau Password salah",
      );
    }
  }

  Future<int> register(User u) async {
    JsonResponse jsonResponse = await _service.register(u);
    if (jsonResponse != null && jsonResponse.isSuccess()) {
      return 0;
    } else {
      throw PlatformException(
        code: "register",
        message:
            "Gagal melakukan pendaftaran akun baru. Email ${u.email} sudah terdaftar.",
      );
    }
  }

  Future<void> sendOTP(User u) async {
    JsonResponse jsonResponse = await _service.sendOTP(u);
    if (jsonResponse == null || jsonResponse.isFailed()) {
      throw PlatformException(
        code: "sendOTP",
        message: "Gagal mengirim kode otentifikasi",
      );
    }
  }
  //TO DO
  Future<void> storeTokenFcm(User u) async {
    JsonResponse jsonResponse = await _service.storeTokenFcm(u);
    if (jsonResponse == null || jsonResponse.isFailed()) {
      throw PlatformException(
        code: "storeTokenFcm",
        message: "Gagal menyimpan token Fcm"//, 
        +" : "+ u.fcmToken,
      );
    }
  }

  Future<void> verifOTP(User u) async {
    JsonResponse jsonResponse = await _service.verifOTP(u);
    if (jsonResponse == null || jsonResponse.isFailed()) {
      throw PlatformException(
        code: "verifOTP",
        message: "Kode otentifikasi tidak valid",
      );
    }
  }

  Future<void> changePassword(User u) async {
    JsonResponse jsonResponse = await _service.changePassword(u);
    if (jsonResponse == null || jsonResponse.isFailed()) {
      throw PlatformException(
        code: "changePassword",
        message: "Gagal mengubah kata sandi",
      );
    }
  }

  Future<void> resetPassword(User u) async {
    JsonResponse jsonResponse = await _service.resetPassword(u);
    if (jsonResponse == null || jsonResponse.isFailed()) {
      throw PlatformException(
        code: "resetPassword",
        message: ErrorMessage.internalServer,
      );
    }
  }
}
