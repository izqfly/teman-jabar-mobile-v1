import 'package:flutter/services.dart';
import 'package:repository/constants/error_code.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/http/services/contract_project_service.dart';
import 'package:repository/models/contract_project/contract_project.dart';

class ContractProjectMiddlemanService {
  final _service = ContractProjectService();

  Future<ContractProject> getSummary() async {
    final response = await _service.getSummary();
    if (response != null && response.isSuccess()) {
      return ContractProject().fromJson(response.getData());
    } else {
      throw PlatformException(
        code: ErrorCode.failedLoadData,
        message: "${ErrorMessage.failedLoadData} summary proyek kontrak",
      );
    }
  }

  Future<List<ContractProject>> fetchList(int skip, String query) async {
    final response = await _service.fetchList(skip, query);
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => ContractProject().fromJson(it))
          .toList()
          .cast<ContractProject>();
      return result;
    } else {
      throw PlatformException(
        code: "fetchList",
        message: "${ErrorMessage.failedLoadData} proyek kontrak",
      );
    }
  }

  Future<List<ContractProject>> geDetail() async {
    final response = await _service.geDetail();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => ContractProject().fromJson(it))
          .toList()
          .cast<ContractProject>();
      return result;
    } else {
      throw PlatformException(
        code: "geDetail",
        message: "${ErrorMessage.failedLoadData} detail proyek kontrak",
      );
    }
  }
}
