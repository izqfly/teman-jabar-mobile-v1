import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/http/services/damage_report_service.dart';
import 'package:repository/models/damage_road/damage_road.dart';

class DamageReportMiddlemanService {
  final _service = DamageReportService();

  Future<List<DamageRoad>> fetchList() async {
    final response = await _service.fetchList();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => DamageRoad().fromJson(it))
          .toList()
          .cast<DamageRoad>();
      return result;
    } else {
      throw PlatformException(
        code: "fetchList",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<DamageRoad> findByID(int id) async {
    final response = await _service.findByID(id);
    if (response != null && response.isSuccess()) {
      if (response.getData() != null) {
        return DamageRoad().fromJson(response.getData());
      } else {
        return new DamageRoad();
      }
    } else {
      throw PlatformException(
        code: "findByID",
        message:
            ErrorMessage.failedLoadData + " detail laporan kerusakan jalan",
      );
    }
  }
}
