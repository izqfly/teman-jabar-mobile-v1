import 'package:repository/http/services/input_task_service.dart';
import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/models/task/task.dart';
import 'package:repository/response/mandor_response.dart';

class InputTaskMiddlemanService {
  final _service = InputTaskService();

  Future<List<MandorResponse>> getRuasJalan() async {
    final response = await _service.getRuasJalan();
    if (response != null && response.isSuccess()) {
      final result = (response.getData()["ruas_jalan"] as List)
          .map((it) => MandorResponse().fromJson(it))
          .toList()
          .cast<MandorResponse>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportType",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<DropdownResponse>> getSUP() async {
    final response = await _service.getSUP();
    if (response != null && response.isSuccess()) {
      final result = (response.getData()["sup"] as List)
          .map((it) => DropdownResponse().fromJson(it))
          .toList()
          .cast<DropdownResponse>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportType",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<JenisPekerjaan>> getPekerjaanType() async {
    final response = await _service.getPekerjaanType();
    if (response != null && response.isSuccess()) {
      final result = (response.getData()["jenis_pekerjaan"] as List)
          .map((it) => JenisPekerjaan().fromJson(it))
          .toList()
          .cast<JenisPekerjaan>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportType",
        message: ErrorMessage.failedLoadData,
      );
    }
  }
  Future<List<PaketPekerjaan>> getPackage() async {
    final response = await _service.getPackage();
    if (response != null && response.isSuccess()) {
      final result = (response.getData()["nama_kegiatan_pekerjaan"] as List)
          .map((it) => PaketPekerjaan().fromJson(it))
          .toList()
          .cast<PaketPekerjaan>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportType",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<int> save(dynamic files, Task task) async {
    final response = await _service.save(files, task);
    print(response.getData());
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      return 1;
    }
  }

  Future<int> delete(String id) async {
    final response = await _service.delete(id);
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      return 1;
    }
  }

  Future<int> edit(dynamic files, Task task, String id_pek) async {
    final response = await _service.edit(files, task, id_pek);
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      throw PlatformException(
        code: "save",
        message: ErrorMessage.failedToSave,
      );
    }
  }

  Future<List<Task>> getTaskList() async {
    final response = await _service.getTaskList();
    if (response != null && response.isSuccess()) {
      final result = (response.getData()["pekerjaan"] as List)
          .map((it) => Task().fromJson(it))
          .toList()
          .cast<Task>();
      return result;
    } else {
      throw PlatformException(
        code: "getNotificationReport",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

}