import 'package:repository/http/services/material_service.dart';
import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/models/material/material.dart';
import 'package:repository/response/json_response.dart';
import 'package:repository/response/material_response.dart';

class MaterialMiddlemanService {
  final _service = MaterialService();

  Future<List<BahanMaterial>> getBahanMaterial() async {
    final response = await _service.getBahanMaterial();
    if (response != null && response.isSuccess()) {
      final result = (response.getData()["bahan_material"] as List)
          .map((it) => BahanMaterial().fromJson(it))
          .toList()
          .cast<BahanMaterial>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportType",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<SatuanMaterial>> getSatuan() async {
    final response = await _service.getSatuan();
    if (response != null && response.isSuccess()) {
      final result = (response.getData()["satuan_material"] as List)
          .map((it) => SatuanMaterial().fromJson(it))
          .toList()
          .cast<SatuanMaterial>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportType",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<Material> getData(String id) async {
    JsonResponse jsonResponse = await _service.getData(id);
    if (jsonResponse != null && jsonResponse.isSuccess()) {
      final result = Material().fromJson(jsonResponse.getData()["bahan_material"]);
      print("RESULT ==== "+result.toString());
      return result;
    } else {
      throw PlatformException(
        code: "Material",
        message: "",
      );
    }
  }
  
  Future<int> save(Material material) async {
    final response = await _service.save(material);
    print(response.getData());
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      throw PlatformException(
        code: "save",
        message: ErrorMessage.failedToSave,
      );
    }
  }

  Future<int> edit(Material material, String id) async {
    final response = await _service.edit(material, id);
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      throw PlatformException(
        code: "edit",
        message: ErrorMessage.failedToSave,
      );
    }
  }
}
