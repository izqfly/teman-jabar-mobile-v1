import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/http/services/reporting_street_service.dart';
import 'package:repository/models/notification/notification.dart';
import 'package:repository/models/report/report.dart';
import 'package:repository/response/dropdown_response.dart';

class ReportingStreetMiddlemanService {
  final _service = ReportingStreetService();

  Future<List<Report>> getAll(int skip) async {
    final response = await _service.getAll(skip);
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => Report().fromJson(it))
          .toList()
          .cast<Report>();
      return result;
    } else {
      throw PlatformException(
        code: "getAll",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<Notification>> getNotificationReport() async {
    final response = await _service.getNotificationReport();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => Notification().fromJson(it))
          .toList()
          .cast<Notification>();
      return result;
    } else {
      throw PlatformException(
        code: "getNotificationReport",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<DropdownResponse>> getReportType() async {
    final response = await _service.getReportType();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => DropdownResponse().fromJson(it))
          .toList()
          .cast<DropdownResponse>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportType",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<DropdownResponse>> getReportLocation() async {
    final response = await _service.getReportLocation();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => DropdownResponse().fromJson(it))
          .toList()
          .cast<DropdownResponse>();
      return result;
    } else {
      throw PlatformException(
        code: "getReportLocation",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<DropdownResponse>> getUPTD() async {
    final response = await _service.getUPTD();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => DropdownResponse().fromJson(it))
          .toList()
          .cast<DropdownResponse>();
      return result;
    } else {
      throw PlatformException(
        code: "getUPTD",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<Report>> findByStatus(int skip, String status) async {
    final response = await _service.findByStatus(skip, status);
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => Report().fromJson(it))
          .toList()
          .cast<Report>();
      return result;
    } else {
      throw PlatformException(
        code: "findByStatus",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<DropdownResponse>> getEmployee() async {
    final response = await _service.getEmployee();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => DropdownResponse().fromJson(it))
          .toList()
          .cast<DropdownResponse>();
      return result;
    } else {
      throw PlatformException(
        code: "getEmployee",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<List<Report>> getByStatusOnProgress(int id) async {
    final response = await _service.getByStatusOnProgress(id);
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => Report().fromJson(it))
          .toList()
          .cast<Report>();
      return result;
    } else {
      throw PlatformException(
        code: "getByStatusOnProgress",
        message: ErrorMessage.failedToSave,
      );
    }
  }

  Future<int> save(dynamic files, Report report) async {
    final response = await _service.save(files, report);
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      throw PlatformException(
        code: "save",
        message: ErrorMessage.failedToSave,
      );
    }
  }

  Future<int> updateOnProgressReport(dynamic files, Report report) async {
    final response = await _service.updateOnProgressReport(files, report);
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      throw PlatformException(
        code: "updateOnProgressReport",
        message: ErrorMessage.failedToSave,
      );
    }
  }

  Future<int> approve(Report report) async {
    final response = await _service.approve(report);
    if (response != null && response.isSuccess()) {
      return 0;
    } else {
      throw PlatformException(
        code: "approve",
        message: ErrorMessage.failedToSave,
      );
    }
  }
}
