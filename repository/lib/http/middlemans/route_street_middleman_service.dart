import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/http/services/route_street_service.dart';
import 'package:repository/models/route/route.dart';

class RouteStreetMiddlemanService {
  final _service = RouteStreetService();

  Future<Route> findRoadRepair(Route route) async {
    final response = await _service.findRoadRepair(route);
    if (response != null && response.isSuccess()) {
      if (response.getData() != null) {
        return Route().fromJson(response.getData());
      } else {
        return new Route();
      }
    } else {
      throw PlatformException(
        code: "findRoadRepair",
        message: ErrorMessage.failedLoadData + " perbaikan jalan",
      );
    }
  }
}
