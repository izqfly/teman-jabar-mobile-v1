import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';
import 'package:repository/http/services/stability_road_service.dart';
import 'package:repository/models/stability_road/stability_road.dart';

class StabilityRoadMiddlemanService {
  final _service = StabilityRoadService();

  Future<List<StabilityRoad>> fetchList() async {
    final response = await _service.fetchList();
    if (response != null && response.isSuccess()) {
      final result = (response.getData() as List)
          .map((it) => StabilityRoad().fromJson(it))
          .toList()
          .cast<StabilityRoad>();
      return result;
    } else {
      throw PlatformException(
        code: "fetchList",
        message: ErrorMessage.failedLoadData,
      );
    }
  }

  Future<StabilityRoad> findByID(int id) async {
    final response = await _service.findByID(id);
    if (response != null && response.isSuccess()) {
      if (response.getData() != null) {
        return StabilityRoad().fromJson(response.getData());
      } else {
        return new StabilityRoad();
      }
    } else {
      throw PlatformException(
        code: "findByID",
        message: ErrorMessage.failedLoadData + " detail kemantapan jalan",
      );
    }
  }

  Future<StabilityRoad> getRecapitulation() async {
    final response = await _service.getRecapitulation();
    if (response != null && response.isSuccess()) {
      if (response.getData() != null) {
        return StabilityRoad().fromJson(response.getData());
      } else {
        return new StabilityRoad();
      }
    } else {
      throw PlatformException(
        code: "getRecapitulation",
        message: ErrorMessage.failedLoadData + " rekapitulasi kemantapan jalan",
      );
    }
  }
}
