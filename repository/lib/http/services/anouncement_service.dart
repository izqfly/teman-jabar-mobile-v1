import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/response/json_response.dart';

class AnouncementService {
  final _flutterRestClient = FlutterRestClient();


  Future<JsonNotif> getList(String role) async {
    print("ROLE ==== $role");
    try {
      final response = await _flutterRestClient.get("pengumuman/$role");
      JsonNotif getResponse = JsonNotif(response);
      //print("RESPONSES : "+getResponse.toString());
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
}
