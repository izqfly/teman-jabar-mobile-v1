import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/response/json_response.dart';
import 'package:sprintf/sprintf.dart';

class AuthService {
  final _endpoint = "auth";
  final _flutterRestClient = FlutterRestClient();

  Future<JsonResponse> login(dynamic body) async {
    try {
      final response = await _flutterRestClient.post(
        sprintf("%s/login", [this._endpoint]),
        body,
      );
      JsonResponse getResponse = JsonResponse(response);
      print(JsonResponse(response).getData());
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> register(dynamic body) async {
    try {
      final response = await _flutterRestClient.post(
        sprintf("%s/register", [this._endpoint]),
        body,
      );
      JsonResponse getResponse = JsonResponse(response);
      print(JsonResponse(response).isSuccess());
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> sendOTP(dynamic body) async {
    try {
      final response = await _flutterRestClient.post(
        sprintf("%s/resendOTPMail", [this._endpoint]),
        body,
      );
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> verifOTP(dynamic body) async {
    try {
      final response = await _flutterRestClient.post(
        sprintf("%s/verifyOTP", [this._endpoint]),
        body,
      );
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> storeTokenFcm(dynamic body) async {
    try {
      final response = await _flutterRestClient.post(
        "save-token",
        body,
      );
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> changePassword(dynamic body) async {
    try {
      final response = await _flutterRestClient.post(
        sprintf("%s/change-password", [this._endpoint]),
        body,
      );
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> resetPassword(dynamic body) async {
    try {
      final response = await _flutterRestClient.post(
        sprintf("%s/reset-password", [this._endpoint]),
        body,
      );
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
}
