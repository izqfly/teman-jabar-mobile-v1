import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/response/json_response.dart';

class ContractProjectService {
  final endpoint = "proyek-kontrak";
  final _flutterRestClient = FlutterRestClient();

  Future<JsonResponse> getSummary() async {
    try {
      final response = await _flutterRestClient.get("$endpoint/count");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> fetchList(int skip, String query) async {
    try {
      final response =
          await _flutterRestClient.get("$endpoint?skip=$skip&take=10&q=$query");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> geDetail() async {
    try {
      final response = await _flutterRestClient.get(
        "$endpoint/status/CRITICAL CONTRACT",
      );
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
}
