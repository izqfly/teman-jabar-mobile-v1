import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/response/json_response.dart';

class DamageReportService {
  final _flutterRestClient = FlutterRestClient();

  Future<JsonResponse> fetchList() async {
    try {
      final response = await _flutterRestClient.get("laporan-masyarakat");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> findByID(int id) async {
    try {
      final response = await _flutterRestClient.get("laporan-masyarakat/$id");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
}
