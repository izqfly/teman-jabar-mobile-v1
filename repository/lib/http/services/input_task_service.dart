import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/models/task/task.dart';
import 'package:repository/response/json_response.dart';

class InputTaskService{
  final _flutterRestClient = FlutterRestClient();

  Future<JsonResponse> getRuasJalan() async {
    try {
      final response = await _flutterRestClient.get("pekerjaan/get-ruas-jalan");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getSUP() async {
    try {
      final response = await _flutterRestClient.get("pekerjaan/get-sup");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
  Future<JsonResponse> getPekerjaanType() async {
    try {
      final response = await _flutterRestClient.get("pekerjaan/get-jenis-pekerjaan");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
  Future<JsonResponse> getPackage() async {
    try {
      final response = await _flutterRestClient.get("pekerjaan/get-nama-kegiatan-pekerjaan");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonSqlite> save(dynamic file, Task task) async {
    try {
      final response = await _flutterRestClient.multipartAndSqlite(
        file,
        "pekerjaan",
        "POST",
        body: task,
      );
      JsonSqlite jsonResponse = JsonSqlite(response);
      return jsonResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> edit(dynamic file, Task task, String id_pek) async {
    try {
      final response = await _flutterRestClient.multipartFile(
        file,
        "pekerjaan/$id_pek?_method=PUT",
        "POST",
        body: task,
      );
      JsonResponse jsonResponse = JsonResponse(response);
      return jsonResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> delete(String id) async {
    try {
      final response = await _flutterRestClient.post("pekerjaan/$id?_method=DELETE", null);
      JsonResponse jsonResponse = JsonResponse(response);
      print("RESPONSE ==== "+jsonResponse.getData().toString());
      return jsonResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getTaskList() async {
    try {
      final response = await _flutterRestClient
          .get("pekerjaan");
      JsonResponse getResponse = JsonResponse(response);
      //print("RESPONSES : "+getResponse.toString());
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
}