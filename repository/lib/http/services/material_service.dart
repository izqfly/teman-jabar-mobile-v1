import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/models/material/material.dart';
import 'package:repository/models/task/task.dart';
import 'package:repository/response/json_response.dart';

class MaterialService {
  final _flutterRestClient = FlutterRestClient();

  Future<JsonResponse> getBahanMaterial() async {
    try {
      final response = await _flutterRestClient
          .get("pekerjaan/material_pekerjaan/bahan_material");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getSatuan() async {
    try {
      final response = await _flutterRestClient
          .get("pekerjaan/material_pekerjaan/satuan_material");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getData(String id) async {
    try {
      final response =
          await _flutterRestClient.get("pekerjaan/material_pekerjaan/$id");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> save(Material material) async {
    try {
      final response = await _flutterRestClient.post("pekerjaan/material_pekerjaan", material);
      JsonResponse jsonResponse = JsonResponse(response);
      print("RESPONSE ==== "+jsonResponse.getData().toString());
      return jsonResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> edit(Material material, String id) async {
    try {
      final response = await _flutterRestClient.post("pekerjaan/material_pekerjaan/$id?_method=PUT", material);
      JsonResponse jsonResponse = JsonResponse(response);
      print("RESPONSE ==== "+jsonResponse.getData().toString());
      return jsonResponse;
    } catch (_) {
      rethrow;
    }
  }
}
