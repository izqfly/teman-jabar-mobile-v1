import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/models/report/report.dart';
import 'package:repository/response/json_response.dart';
import 'package:repository/common/flutter_local_storage.dart';

class ReportingStreetService {
  final _flutterRestClient = FlutterRestClient();
  final _storage = FlutterLocalStorage();

  Future<JsonResponse> getAll(int skip) async {
    try {
      String email = _storage?.user?.email;
      //print("EMAIL : " + email);
      final response = await _flutterRestClient
          .get("laporan-masyarakat?email=$email&skip=$skip&take=10");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> findByStatus(int skip, String status) async {
    try {
      final response = await _flutterRestClient
          .get("laporan-masyarakat/status/$status?skip=$skip&take=10");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getByStatusOnProgress(int id) async {
    try {
      final response =
          await _flutterRestClient.get("laporan-masyarakat/progress/$id");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> approve(Report report) async {
    try {
      final response =
          await _flutterRestClient.post("laporan-masyarakat/approve", report);
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> updateOnProgressReport(
      dynamic files, Report report) async {
    try {
      final response = await _flutterRestClient.multipartFile(
        files,
        "laporan-masyarakat/progress",
        "POST",
        body: report,
      );
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getNotificationReport() async {
    try {
      String id = _storage?.user?.userId.toString();
      //print("ID : " + id.toString());
      final response = await _flutterRestClient
          .get("laporan-masyarakat/getNotifikasiByUserId/$id");
      JsonResponse getResponse = JsonResponse(response);
      //print("RESPONSES : "+getResponse.toString());
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getEmployee() async {
    try {
      final response = await _flutterRestClient.get("utils/petugas");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getReportType() async {
    try {
      final response = await _flutterRestClient.get("utils/jenis-laporan");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getReportLocation() async {
    try {
      final response = await _flutterRestClient.get("utils/lokasi");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> getUPTD() async {
    try {
      final response = await _flutterRestClient.get("utils/uptd");
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }

  Future<JsonResponse> save(dynamic files, Report report) async {
    try {
      final response = await _flutterRestClient.multipartFile(
        files,
        "laporan-masyarakat",
        "POST",
        body: report,
      );
      JsonResponse jsonResponse = JsonResponse(response);
      return jsonResponse;
    } catch (_) {
      rethrow;
    }
  }
}
