import 'package:repository/http/client/flutter_rest_client.dart';
import 'package:repository/models/route/route.dart';
import 'package:repository/response/json_response.dart';

class RouteStreetService {
  final _flutterRestClient = FlutterRestClient();

  Future<JsonResponse> findRoadRepair(Route route) async {
    try {
      final response = await _flutterRestClient.post("perbaikan-jalan", route);
      JsonResponse getResponse = JsonResponse(response);
      return getResponse;
    } catch (_) {
      rethrow;
    }
  }
}
