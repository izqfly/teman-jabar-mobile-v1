library anouncement;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'anouncement.g.dart';

abstract class Anouncement implements Built<Anouncement, AnouncementBuilder> {

  @nullable
  @BuiltValueField(wireName: 'title')
  String get title;

  @nullable
  @BuiltValueField(wireName: 'content')
  String get content;

  @nullable
  @BuiltValueField(wireName: 'image')
  String get image;

  Anouncement._();

  factory Anouncement([void Function(AnouncementBuilder) updates]) =
      _$Anouncement;

  static Serializer<Anouncement> get serializer => _$anouncementSerializer;

  Anouncement fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(Anouncement.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }

  String toJson() {
    return jsonEncode(serializers.serializeWith(Anouncement.serializer, this));
  }
}
