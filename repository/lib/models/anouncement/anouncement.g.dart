// GENERATED CODE - DO NOT MODIFY BY HAND

part of anouncement;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Anouncement> _$anouncementSerializer = new _$AnouncementSerializer();

class _$AnouncementSerializer implements StructuredSerializer<Anouncement> {
  @override
  final Iterable<Type> types = const [Anouncement, _$Anouncement];
  @override
  final String wireName = 'Anouncement';

  @override
  Iterable<Object> serialize(Serializers serializers, Anouncement object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.content != null) {
      result
        ..add('content')
        ..add(serializers.serialize(object.content,
            specifiedType: const FullType(String)));
    }
    if (object.image != null) {
      result
        ..add('image')
        ..add(serializers.serialize(object.image,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Anouncement deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AnouncementBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'content':
          result.content = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'image':
          result.image = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Anouncement extends Anouncement {
  @override
  final String title;
  @override
  final String content;
  @override
  final String image;

  factory _$Anouncement([void Function(AnouncementBuilder) updates]) =>
      (new AnouncementBuilder()..update(updates)).build();

  _$Anouncement._({this.title, this.content, this.image}) : super._();

  @override
  Anouncement rebuild(void Function(AnouncementBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AnouncementBuilder toBuilder() => new AnouncementBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Anouncement &&
        title == other.title &&
        content == other.content &&
        image == other.image;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, title.hashCode), content.hashCode), image.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Anouncement')
          ..add('title', title)
          ..add('content', content)
          ..add('image', image))
        .toString();
  }
}

class AnouncementBuilder implements Builder<Anouncement, AnouncementBuilder> {
  _$Anouncement _$v;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _content;
  String get content => _$this._content;
  set content(String content) => _$this._content = content;

  String _image;
  String get image => _$this._image;
  set image(String image) => _$this._image = image;

  AnouncementBuilder();

  AnouncementBuilder get _$this {
    if (_$v != null) {
      _title = _$v.title;
      _content = _$v.content;
      _image = _$v.image;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Anouncement other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Anouncement;
  }

  @override
  void update(void Function(AnouncementBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Anouncement build() {
    final _$result = _$v ??
        new _$Anouncement._(title: title, content: content, image: image);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
