import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'chart.g.dart';

abstract class Chart implements Built<Chart, ChartBuilder> {
  @nullable
  String get domain;

  @nullable
  double get measure;

  Chart._();

  factory Chart([void Function(ChartBuilder) updates]) = _$Chart;

  static Serializer<Chart> get serializer => _$chartSerializer;

  Chart fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(Chart.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }
}
