// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chart.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Chart> _$chartSerializer = new _$ChartSerializer();

class _$ChartSerializer implements StructuredSerializer<Chart> {
  @override
  final Iterable<Type> types = const [Chart, _$Chart];
  @override
  final String wireName = 'Chart';

  @override
  Iterable<Object> serialize(Serializers serializers, Chart object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.domain != null) {
      result
        ..add('domain')
        ..add(serializers.serialize(object.domain,
            specifiedType: const FullType(String)));
    }
    if (object.measure != null) {
      result
        ..add('measure')
        ..add(serializers.serialize(object.measure,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  Chart deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ChartBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'domain':
          result.domain = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'measure':
          result.measure = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$Chart extends Chart {
  @override
  final String domain;
  @override
  final double measure;

  factory _$Chart([void Function(ChartBuilder) updates]) =>
      (new ChartBuilder()..update(updates)).build();

  _$Chart._({this.domain, this.measure}) : super._();

  @override
  Chart rebuild(void Function(ChartBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChartBuilder toBuilder() => new ChartBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Chart && domain == other.domain && measure == other.measure;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, domain.hashCode), measure.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Chart')
          ..add('domain', domain)
          ..add('measure', measure))
        .toString();
  }
}

class ChartBuilder implements Builder<Chart, ChartBuilder> {
  _$Chart _$v;

  String _domain;
  String get domain => _$this._domain;
  set domain(String domain) => _$this._domain = domain;

  double _measure;
  double get measure => _$this._measure;
  set measure(double measure) => _$this._measure = measure;

  ChartBuilder();

  ChartBuilder get _$this {
    if (_$v != null) {
      _domain = _$v.domain;
      _measure = _$v.measure;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Chart other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Chart;
  }

  @override
  void update(void Function(ChartBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Chart build() {
    final _$result = _$v ?? new _$Chart._(domain: domain, measure: measure);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
