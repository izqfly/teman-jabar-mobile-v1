import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'contract_project.g.dart';

abstract class ContractProject
    implements Built<ContractProject, ContractProjectBuilder> {
  @nullable
  double get onProgress;

  @nullable
  double get criticalContract;

  @nullable
  double get offProgress;

  @nullable
  double get finish;

  @nullable
  @BuiltValueField(wireName: 'nama_paket')
  String get packageName;

  @nullable
  @BuiltValueField(wireName: 'date_from')
  String get startDate;

  @nullable
  @BuiltValueField(wireName: 'date_to')
  String get endDate;

  @nullable
  @BuiltValueField(wireName: 'rencana')
  double get planning;

  @nullable
  @BuiltValueField(wireName: 'realisasi')
  double get realization;

  @nullable
  @BuiltValueField(wireName: 'deviasi')
  double get deviation;

  @nullable
  String get date;

  @nullable
  @BuiltValueField(wireName: 'jenis_pekerjaan')
  String get typeWork;

  @nullable
  @BuiltValueField(wireName: 'ruas_jalan')
  String get roads;

  @nullable
  String get status;

  ContractProject._();

  factory ContractProject([void Function(ContractProjectBuilder) updates]) =
      _$ContractProject;

  static Serializer<ContractProject> get serializer =>
      _$contractProjectSerializer;

  ContractProject fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(ContractProject.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }

  String getRealization() {
    if (realization != null) {
      return realization.toStringAsFixed(4);
    }
    return "0";
  }

  String getPlanning() {
    if (planning != null) {
      return planning.toStringAsFixed(4);
    }
    return "0";
  }

  String getDeviation() {
    if (deviation != null) {
      return deviation.toStringAsFixed(4);
    }
    return "0";
  }
}
