// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_project.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ContractProject> _$contractProjectSerializer =
    new _$ContractProjectSerializer();

class _$ContractProjectSerializer
    implements StructuredSerializer<ContractProject> {
  @override
  final Iterable<Type> types = const [ContractProject, _$ContractProject];
  @override
  final String wireName = 'ContractProject';

  @override
  Iterable<Object> serialize(Serializers serializers, ContractProject object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.onProgress != null) {
      result
        ..add('onProgress')
        ..add(serializers.serialize(object.onProgress,
            specifiedType: const FullType(double)));
    }
    if (object.criticalContract != null) {
      result
        ..add('criticalContract')
        ..add(serializers.serialize(object.criticalContract,
            specifiedType: const FullType(double)));
    }
    if (object.offProgress != null) {
      result
        ..add('offProgress')
        ..add(serializers.serialize(object.offProgress,
            specifiedType: const FullType(double)));
    }
    if (object.finish != null) {
      result
        ..add('finish')
        ..add(serializers.serialize(object.finish,
            specifiedType: const FullType(double)));
    }
    if (object.packageName != null) {
      result
        ..add('nama_paket')
        ..add(serializers.serialize(object.packageName,
            specifiedType: const FullType(String)));
    }
    if (object.startDate != null) {
      result
        ..add('date_from')
        ..add(serializers.serialize(object.startDate,
            specifiedType: const FullType(String)));
    }
    if (object.endDate != null) {
      result
        ..add('date_to')
        ..add(serializers.serialize(object.endDate,
            specifiedType: const FullType(String)));
    }
    if (object.planning != null) {
      result
        ..add('rencana')
        ..add(serializers.serialize(object.planning,
            specifiedType: const FullType(double)));
    }
    if (object.realization != null) {
      result
        ..add('realisasi')
        ..add(serializers.serialize(object.realization,
            specifiedType: const FullType(double)));
    }
    if (object.deviation != null) {
      result
        ..add('deviasi')
        ..add(serializers.serialize(object.deviation,
            specifiedType: const FullType(double)));
    }
    if (object.date != null) {
      result
        ..add('date')
        ..add(serializers.serialize(object.date,
            specifiedType: const FullType(String)));
    }
    if (object.typeWork != null) {
      result
        ..add('jenis_pekerjaan')
        ..add(serializers.serialize(object.typeWork,
            specifiedType: const FullType(String)));
    }
    if (object.roads != null) {
      result
        ..add('ruas_jalan')
        ..add(serializers.serialize(object.roads,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ContractProject deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ContractProjectBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'onProgress':
          result.onProgress = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'criticalContract':
          result.criticalContract = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'offProgress':
          result.offProgress = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'finish':
          result.finish = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'nama_paket':
          result.packageName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date_from':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date_to':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rencana':
          result.planning = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'realisasi':
          result.realization = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'deviasi':
          result.deviation = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jenis_pekerjaan':
          result.typeWork = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ruas_jalan':
          result.roads = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ContractProject extends ContractProject {
  @override
  final double onProgress;
  @override
  final double criticalContract;
  @override
  final double offProgress;
  @override
  final double finish;
  @override
  final String packageName;
  @override
  final String startDate;
  @override
  final String endDate;
  @override
  final double planning;
  @override
  final double realization;
  @override
  final double deviation;
  @override
  final String date;
  @override
  final String typeWork;
  @override
  final String roads;
  @override
  final String status;

  factory _$ContractProject([void Function(ContractProjectBuilder) updates]) =>
      (new ContractProjectBuilder()..update(updates)).build();

  _$ContractProject._(
      {this.onProgress,
      this.criticalContract,
      this.offProgress,
      this.finish,
      this.packageName,
      this.startDate,
      this.endDate,
      this.planning,
      this.realization,
      this.deviation,
      this.date,
      this.typeWork,
      this.roads,
      this.status})
      : super._();

  @override
  ContractProject rebuild(void Function(ContractProjectBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContractProjectBuilder toBuilder() =>
      new ContractProjectBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContractProject &&
        onProgress == other.onProgress &&
        criticalContract == other.criticalContract &&
        offProgress == other.offProgress &&
        finish == other.finish &&
        packageName == other.packageName &&
        startDate == other.startDate &&
        endDate == other.endDate &&
        planning == other.planning &&
        realization == other.realization &&
        deviation == other.deviation &&
        date == other.date &&
        typeWork == other.typeWork &&
        roads == other.roads &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            0,
                                                            onProgress
                                                                .hashCode),
                                                        criticalContract
                                                            .hashCode),
                                                    offProgress.hashCode),
                                                finish.hashCode),
                                            packageName.hashCode),
                                        startDate.hashCode),
                                    endDate.hashCode),
                                planning.hashCode),
                            realization.hashCode),
                        deviation.hashCode),
                    date.hashCode),
                typeWork.hashCode),
            roads.hashCode),
        status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContractProject')
          ..add('onProgress', onProgress)
          ..add('criticalContract', criticalContract)
          ..add('offProgress', offProgress)
          ..add('finish', finish)
          ..add('packageName', packageName)
          ..add('startDate', startDate)
          ..add('endDate', endDate)
          ..add('planning', planning)
          ..add('realization', realization)
          ..add('deviation', deviation)
          ..add('date', date)
          ..add('typeWork', typeWork)
          ..add('roads', roads)
          ..add('status', status))
        .toString();
  }
}

class ContractProjectBuilder
    implements Builder<ContractProject, ContractProjectBuilder> {
  _$ContractProject _$v;

  double _onProgress;
  double get onProgress => _$this._onProgress;
  set onProgress(double onProgress) => _$this._onProgress = onProgress;

  double _criticalContract;
  double get criticalContract => _$this._criticalContract;
  set criticalContract(double criticalContract) =>
      _$this._criticalContract = criticalContract;

  double _offProgress;
  double get offProgress => _$this._offProgress;
  set offProgress(double offProgress) => _$this._offProgress = offProgress;

  double _finish;
  double get finish => _$this._finish;
  set finish(double finish) => _$this._finish = finish;

  String _packageName;
  String get packageName => _$this._packageName;
  set packageName(String packageName) => _$this._packageName = packageName;

  String _startDate;
  String get startDate => _$this._startDate;
  set startDate(String startDate) => _$this._startDate = startDate;

  String _endDate;
  String get endDate => _$this._endDate;
  set endDate(String endDate) => _$this._endDate = endDate;

  double _planning;
  double get planning => _$this._planning;
  set planning(double planning) => _$this._planning = planning;

  double _realization;
  double get realization => _$this._realization;
  set realization(double realization) => _$this._realization = realization;

  double _deviation;
  double get deviation => _$this._deviation;
  set deviation(double deviation) => _$this._deviation = deviation;

  String _date;
  String get date => _$this._date;
  set date(String date) => _$this._date = date;

  String _typeWork;
  String get typeWork => _$this._typeWork;
  set typeWork(String typeWork) => _$this._typeWork = typeWork;

  String _roads;
  String get roads => _$this._roads;
  set roads(String roads) => _$this._roads = roads;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  ContractProjectBuilder();

  ContractProjectBuilder get _$this {
    if (_$v != null) {
      _onProgress = _$v.onProgress;
      _criticalContract = _$v.criticalContract;
      _offProgress = _$v.offProgress;
      _finish = _$v.finish;
      _packageName = _$v.packageName;
      _startDate = _$v.startDate;
      _endDate = _$v.endDate;
      _planning = _$v.planning;
      _realization = _$v.realization;
      _deviation = _$v.deviation;
      _date = _$v.date;
      _typeWork = _$v.typeWork;
      _roads = _$v.roads;
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContractProject other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ContractProject;
  }

  @override
  void update(void Function(ContractProjectBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContractProject build() {
    final _$result = _$v ??
        new _$ContractProject._(
            onProgress: onProgress,
            criticalContract: criticalContract,
            offProgress: offProgress,
            finish: finish,
            packageName: packageName,
            startDate: startDate,
            endDate: endDate,
            planning: planning,
            realization: realization,
            deviation: deviation,
            date: date,
            typeWork: typeWork,
            roads: roads,
            status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
