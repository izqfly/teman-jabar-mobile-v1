import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'damage_road.g.dart';

abstract class DamageRoad implements Built<DamageRoad, DamageRoadBuilder> {
  @nullable
  int get id;

  @nullable
  double get latitude;

  @nullable
  double get longitude;

  @nullable
  @BuiltValueField(wireName: 'nomorPengaduan')
  String get reportNumber;

  @nullable
  @BuiltValueField(wireName: 'namaPelapor')
  String get reporterName;

  @nullable
  @BuiltValueField(wireName: 'nikPelapor')
  String get reporterIdentity;

  @nullable
  @BuiltValueField(wireName: 'nomorHpPelapor')
  String get reporterPhoneNumber;

  @nullable
  @BuiltValueField(wireName: 'emailPelapor')
  String get reporterEmail;

  @nullable
  String get uptd;

  @nullable
  @BuiltValueField(wireName: 'keterangan')
  String get description;

  @nullable
  @BuiltValueField(wireName: 'kategori_laporan')
  String get reportCategory;

  @nullable
  @BuiltValueField(wireName: 'foto_kondisi')
  String get photoConditionUrl;

  DamageRoad._();

  factory DamageRoad([void Function(DamageRoadBuilder) updates]) = _$DamageRoad;

  static Serializer<DamageRoad> get serializer => _$damageRoadSerializer;

  DamageRoad fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(DamageRoad.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }
}
