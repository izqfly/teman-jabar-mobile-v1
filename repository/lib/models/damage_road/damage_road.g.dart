// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'damage_road.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DamageRoad> _$damageRoadSerializer = new _$DamageRoadSerializer();

class _$DamageRoadSerializer implements StructuredSerializer<DamageRoad> {
  @override
  final Iterable<Type> types = const [DamageRoad, _$DamageRoad];
  @override
  final String wireName = 'DamageRoad';

  @override
  Iterable<Object> serialize(Serializers serializers, DamageRoad object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.latitude != null) {
      result
        ..add('latitude')
        ..add(serializers.serialize(object.latitude,
            specifiedType: const FullType(double)));
    }
    if (object.longitude != null) {
      result
        ..add('longitude')
        ..add(serializers.serialize(object.longitude,
            specifiedType: const FullType(double)));
    }
    if (object.reportNumber != null) {
      result
        ..add('nomorPengaduan')
        ..add(serializers.serialize(object.reportNumber,
            specifiedType: const FullType(String)));
    }
    if (object.reporterName != null) {
      result
        ..add('namaPelapor')
        ..add(serializers.serialize(object.reporterName,
            specifiedType: const FullType(String)));
    }
    if (object.reporterIdentity != null) {
      result
        ..add('nikPelapor')
        ..add(serializers.serialize(object.reporterIdentity,
            specifiedType: const FullType(String)));
    }
    if (object.reporterPhoneNumber != null) {
      result
        ..add('nomorHpPelapor')
        ..add(serializers.serialize(object.reporterPhoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.reporterEmail != null) {
      result
        ..add('emailPelapor')
        ..add(serializers.serialize(object.reporterEmail,
            specifiedType: const FullType(String)));
    }
    if (object.uptd != null) {
      result
        ..add('uptd')
        ..add(serializers.serialize(object.uptd,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('keterangan')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.reportCategory != null) {
      result
        ..add('kategori_laporan')
        ..add(serializers.serialize(object.reportCategory,
            specifiedType: const FullType(String)));
    }
    if (object.photoConditionUrl != null) {
      result
        ..add('foto_kondisi')
        ..add(serializers.serialize(object.photoConditionUrl,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  DamageRoad deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DamageRoadBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'nomorPengaduan':
          result.reportNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'namaPelapor':
          result.reporterName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nikPelapor':
          result.reporterIdentity = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nomorHpPelapor':
          result.reporterPhoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'emailPelapor':
          result.reporterEmail = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'uptd':
          result.uptd = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'keterangan':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'kategori_laporan':
          result.reportCategory = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'foto_kondisi':
          result.photoConditionUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$DamageRoad extends DamageRoad {
  @override
  final int id;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final String reportNumber;
  @override
  final String reporterName;
  @override
  final String reporterIdentity;
  @override
  final String reporterPhoneNumber;
  @override
  final String reporterEmail;
  @override
  final String uptd;
  @override
  final String description;
  @override
  final String reportCategory;
  @override
  final String photoConditionUrl;

  factory _$DamageRoad([void Function(DamageRoadBuilder) updates]) =>
      (new DamageRoadBuilder()..update(updates)).build();

  _$DamageRoad._(
      {this.id,
      this.latitude,
      this.longitude,
      this.reportNumber,
      this.reporterName,
      this.reporterIdentity,
      this.reporterPhoneNumber,
      this.reporterEmail,
      this.uptd,
      this.description,
      this.reportCategory,
      this.photoConditionUrl})
      : super._();

  @override
  DamageRoad rebuild(void Function(DamageRoadBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DamageRoadBuilder toBuilder() => new DamageRoadBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DamageRoad &&
        id == other.id &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        reportNumber == other.reportNumber &&
        reporterName == other.reporterName &&
        reporterIdentity == other.reporterIdentity &&
        reporterPhoneNumber == other.reporterPhoneNumber &&
        reporterEmail == other.reporterEmail &&
        uptd == other.uptd &&
        description == other.description &&
        reportCategory == other.reportCategory &&
        photoConditionUrl == other.photoConditionUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, id.hashCode),
                                                latitude.hashCode),
                                            longitude.hashCode),
                                        reportNumber.hashCode),
                                    reporterName.hashCode),
                                reporterIdentity.hashCode),
                            reporterPhoneNumber.hashCode),
                        reporterEmail.hashCode),
                    uptd.hashCode),
                description.hashCode),
            reportCategory.hashCode),
        photoConditionUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DamageRoad')
          ..add('id', id)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('reportNumber', reportNumber)
          ..add('reporterName', reporterName)
          ..add('reporterIdentity', reporterIdentity)
          ..add('reporterPhoneNumber', reporterPhoneNumber)
          ..add('reporterEmail', reporterEmail)
          ..add('uptd', uptd)
          ..add('description', description)
          ..add('reportCategory', reportCategory)
          ..add('photoConditionUrl', photoConditionUrl))
        .toString();
  }
}

class DamageRoadBuilder implements Builder<DamageRoad, DamageRoadBuilder> {
  _$DamageRoad _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  double _latitude;
  double get latitude => _$this._latitude;
  set latitude(double latitude) => _$this._latitude = latitude;

  double _longitude;
  double get longitude => _$this._longitude;
  set longitude(double longitude) => _$this._longitude = longitude;

  String _reportNumber;
  String get reportNumber => _$this._reportNumber;
  set reportNumber(String reportNumber) => _$this._reportNumber = reportNumber;

  String _reporterName;
  String get reporterName => _$this._reporterName;
  set reporterName(String reporterName) => _$this._reporterName = reporterName;

  String _reporterIdentity;
  String get reporterIdentity => _$this._reporterIdentity;
  set reporterIdentity(String reporterIdentity) =>
      _$this._reporterIdentity = reporterIdentity;

  String _reporterPhoneNumber;
  String get reporterPhoneNumber => _$this._reporterPhoneNumber;
  set reporterPhoneNumber(String reporterPhoneNumber) =>
      _$this._reporterPhoneNumber = reporterPhoneNumber;

  String _reporterEmail;
  String get reporterEmail => _$this._reporterEmail;
  set reporterEmail(String reporterEmail) =>
      _$this._reporterEmail = reporterEmail;

  String _uptd;
  String get uptd => _$this._uptd;
  set uptd(String uptd) => _$this._uptd = uptd;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _reportCategory;
  String get reportCategory => _$this._reportCategory;
  set reportCategory(String reportCategory) =>
      _$this._reportCategory = reportCategory;

  String _photoConditionUrl;
  String get photoConditionUrl => _$this._photoConditionUrl;
  set photoConditionUrl(String photoConditionUrl) =>
      _$this._photoConditionUrl = photoConditionUrl;

  DamageRoadBuilder();

  DamageRoadBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _latitude = _$v.latitude;
      _longitude = _$v.longitude;
      _reportNumber = _$v.reportNumber;
      _reporterName = _$v.reporterName;
      _reporterIdentity = _$v.reporterIdentity;
      _reporterPhoneNumber = _$v.reporterPhoneNumber;
      _reporterEmail = _$v.reporterEmail;
      _uptd = _$v.uptd;
      _description = _$v.description;
      _reportCategory = _$v.reportCategory;
      _photoConditionUrl = _$v.photoConditionUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DamageRoad other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DamageRoad;
  }

  @override
  void update(void Function(DamageRoadBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DamageRoad build() {
    final _$result = _$v ??
        new _$DamageRoad._(
            id: id,
            latitude: latitude,
            longitude: longitude,
            reportNumber: reportNumber,
            reporterName: reporterName,
            reporterIdentity: reporterIdentity,
            reporterPhoneNumber: reporterPhoneNumber,
            reporterEmail: reporterEmail,
            uptd: uptd,
            description: description,
            reportCategory: reportCategory,
            photoConditionUrl: photoConditionUrl);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
