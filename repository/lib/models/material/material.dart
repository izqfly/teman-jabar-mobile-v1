library material;
import 'dart:convert';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'material.g.dart';

abstract class Material implements Built<Material, MaterialBuilder> {

  @nullable
  @BuiltValueField(wireName: 'id_pek')
  String get id;

  @nullable
  @BuiltValueField(wireName: 'jenis_pekerjaan')
  String get jenis;

  @nullable
  @BuiltValueField(wireName: 'nama_mandor')
  String get name;

  @nullable
  @BuiltValueField(wireName: 'tanggal')
  String get tanggal;

  @nullable
  @BuiltValueField(wireName: 'uptd_id')
  int get uptd;

  @nullable
  @BuiltValueField(wireName: 'peralatan')
  String get alat;

  @nullable
  @BuiltValueField(wireName: 'rule')
  String get rule;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan1')
  String get bahan1;

  @nullable
  @BuiltValueField(wireName: 'jum_bahan1')
  String get jumlah1;

  @nullable
  @BuiltValueField(wireName: 'satuan1')
  String get satuan1;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan2')
  String get bahan2;

  @nullable
  @BuiltValueField(wireName: 'jum_bahan2')
  String get jumlah2;

  @nullable
  @BuiltValueField(wireName: 'satuan2')
  String get satuan2;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan3')
  String get bahan3;

  @nullable
  @BuiltValueField(wireName: 'jum_bahan3')
  String get jumlah3;

  @nullable
  @BuiltValueField(wireName: 'satuan3')
  String get satuan3;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan4')
  String get bahan4;

  @nullable
  @BuiltValueField(wireName: 'jum_bahan4')
  String get jumlah4;

  @nullable
  @BuiltValueField(wireName: 'satuan4')
  String get satuan4;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan5')
  String get bahan5;

  @nullable
  @BuiltValueField(wireName: 'jum_bahan5')
  String get jumlah5;

  @nullable
  @BuiltValueField(wireName: 'satuan5')
  String get satuan5;
  @nullable
  @BuiltValueField(wireName: 'nama_bahan6')
  String get bahan6;

  @nullable
  @BuiltValueField(wireName: 'jum_bahan6')
  String get jumlah6;

  @nullable
  @BuiltValueField(wireName: 'satuan6')
  String get satuan6;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan7')
  String get bahan7;

  @nullable
  @BuiltValueField(wireName: 'jum_bahan7')
  String get jumlah7;

  @nullable
  @BuiltValueField(wireName: 'satuan7')
  String get satuan7;
  
  @nullable
  @BuiltValueField(wireName: 'nama_bahan8')
  String get bahan8;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan8')
  String get jumlah8;
  
  @nullable
  @BuiltValueField(wireName: 'satuan8')
  String get satuan8;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan9')
  String get bahan9;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan9')
  String get jumlah9;
  
  @nullable
  @BuiltValueField(wireName: 'satuan9')
  String get satuan9;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan10')
  String get bahan10;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan10')
  String get jumlah10;
  
  @nullable
  @BuiltValueField(wireName: 'satuan10')
  String get satuan10;
  
  @nullable
  @BuiltValueField(wireName: 'nama_bahan11')
  String get bahan11;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan11')
  String get jumlah11;
  
  @nullable
  @BuiltValueField(wireName: 'satuan11')
  String get satuan11;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan12')
  String get bahan12;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan12')
  String get jumlah12;
  
  @nullable
  @BuiltValueField(wireName: 'satuan12')
  String get satuan12;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan13')
  String get bahan13;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan13')
  String get jumlah13;
  
  @nullable
  @BuiltValueField(wireName: 'satuan13')
  String get satuan13;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan14')
  String get bahan14;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan14')
  String get jumlah14;
  
  @nullable
  @BuiltValueField(wireName: 'satuan14')
  String get satuan14;

  @nullable
  @BuiltValueField(wireName: 'nama_bahan15')
  String get bahan15;
  
  @nullable
  @BuiltValueField(wireName: 'jum_bahan15')
  String get jumlah15;
  
  @nullable
  @BuiltValueField(wireName: 'satuan15')
  String get satuan15;

  Material._();

  factory Material([void Function(MaterialBuilder updates)]) = _$Material;

  static Serializer<Material> get serializer => _$materialSerializer;

  String toJson() {
    return json.encode(serializers.serializeWith(Material.serializer, this));
  }

  Material fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(Material.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }
}