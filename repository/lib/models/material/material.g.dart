// GENERATED CODE - DO NOT MODIFY BY HAND

part of material;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Material> _$materialSerializer = new _$MaterialSerializer();

class _$MaterialSerializer implements StructuredSerializer<Material> {
  @override
  final Iterable<Type> types = const [Material, _$Material];
  @override
  final String wireName = 'Material';

  @override
  Iterable<Object> serialize(Serializers serializers, Material object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id_pek')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.jenis != null) {
      result
        ..add('jenis_pekerjaan')
        ..add(serializers.serialize(object.jenis,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('nama_mandor')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.tanggal != null) {
      result
        ..add('tanggal')
        ..add(serializers.serialize(object.tanggal,
            specifiedType: const FullType(String)));
    }
    if (object.uptd != null) {
      result
        ..add('uptd_id')
        ..add(serializers.serialize(object.uptd,
            specifiedType: const FullType(int)));
    }
    if (object.alat != null) {
      result
        ..add('peralatan')
        ..add(serializers.serialize(object.alat,
            specifiedType: const FullType(String)));
    }
    if (object.rule != null) {
      result
        ..add('rule')
        ..add(serializers.serialize(object.rule,
            specifiedType: const FullType(String)));
    }
    if (object.bahan1 != null) {
      result
        ..add('nama_bahan1')
        ..add(serializers.serialize(object.bahan1,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah1 != null) {
      result
        ..add('jum_bahan1')
        ..add(serializers.serialize(object.jumlah1,
            specifiedType: const FullType(String)));
    }
    if (object.satuan1 != null) {
      result
        ..add('satuan1')
        ..add(serializers.serialize(object.satuan1,
            specifiedType: const FullType(String)));
    }
    if (object.bahan2 != null) {
      result
        ..add('nama_bahan2')
        ..add(serializers.serialize(object.bahan2,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah2 != null) {
      result
        ..add('jum_bahan2')
        ..add(serializers.serialize(object.jumlah2,
            specifiedType: const FullType(String)));
    }
    if (object.satuan2 != null) {
      result
        ..add('satuan2')
        ..add(serializers.serialize(object.satuan2,
            specifiedType: const FullType(String)));
    }
    if (object.bahan3 != null) {
      result
        ..add('nama_bahan3')
        ..add(serializers.serialize(object.bahan3,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah3 != null) {
      result
        ..add('jum_bahan3')
        ..add(serializers.serialize(object.jumlah3,
            specifiedType: const FullType(String)));
    }
    if (object.satuan3 != null) {
      result
        ..add('satuan3')
        ..add(serializers.serialize(object.satuan3,
            specifiedType: const FullType(String)));
    }
    if (object.bahan4 != null) {
      result
        ..add('nama_bahan4')
        ..add(serializers.serialize(object.bahan4,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah4 != null) {
      result
        ..add('jum_bahan4')
        ..add(serializers.serialize(object.jumlah4,
            specifiedType: const FullType(String)));
    }
    if (object.satuan4 != null) {
      result
        ..add('satuan4')
        ..add(serializers.serialize(object.satuan4,
            specifiedType: const FullType(String)));
    }
    if (object.bahan5 != null) {
      result
        ..add('nama_bahan5')
        ..add(serializers.serialize(object.bahan5,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah5 != null) {
      result
        ..add('jum_bahan5')
        ..add(serializers.serialize(object.jumlah5,
            specifiedType: const FullType(String)));
    }
    if (object.satuan5 != null) {
      result
        ..add('satuan5')
        ..add(serializers.serialize(object.satuan5,
            specifiedType: const FullType(String)));
    }
    if (object.bahan6 != null) {
      result
        ..add('nama_bahan6')
        ..add(serializers.serialize(object.bahan6,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah6 != null) {
      result
        ..add('jum_bahan6')
        ..add(serializers.serialize(object.jumlah6,
            specifiedType: const FullType(String)));
    }
    if (object.satuan6 != null) {
      result
        ..add('satuan6')
        ..add(serializers.serialize(object.satuan6,
            specifiedType: const FullType(String)));
    }
    if (object.bahan7 != null) {
      result
        ..add('nama_bahan7')
        ..add(serializers.serialize(object.bahan7,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah7 != null) {
      result
        ..add('jum_bahan7')
        ..add(serializers.serialize(object.jumlah7,
            specifiedType: const FullType(String)));
    }
    if (object.satuan7 != null) {
      result
        ..add('satuan7')
        ..add(serializers.serialize(object.satuan7,
            specifiedType: const FullType(String)));
    }
    if (object.bahan8 != null) {
      result
        ..add('nama_bahan8')
        ..add(serializers.serialize(object.bahan8,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah8 != null) {
      result
        ..add('jum_bahan8')
        ..add(serializers.serialize(object.jumlah8,
            specifiedType: const FullType(String)));
    }
    if (object.satuan8 != null) {
      result
        ..add('satuan8')
        ..add(serializers.serialize(object.satuan8,
            specifiedType: const FullType(String)));
    }
    if (object.bahan9 != null) {
      result
        ..add('nama_bahan9')
        ..add(serializers.serialize(object.bahan9,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah9 != null) {
      result
        ..add('jum_bahan9')
        ..add(serializers.serialize(object.jumlah9,
            specifiedType: const FullType(String)));
    }
    if (object.satuan9 != null) {
      result
        ..add('satuan9')
        ..add(serializers.serialize(object.satuan9,
            specifiedType: const FullType(String)));
    }
    if (object.bahan10 != null) {
      result
        ..add('nama_bahan10')
        ..add(serializers.serialize(object.bahan10,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah10 != null) {
      result
        ..add('jum_bahan10')
        ..add(serializers.serialize(object.jumlah10,
            specifiedType: const FullType(String)));
    }
    if (object.satuan10 != null) {
      result
        ..add('satuan10')
        ..add(serializers.serialize(object.satuan10,
            specifiedType: const FullType(String)));
    }
    if (object.bahan11 != null) {
      result
        ..add('nama_bahan11')
        ..add(serializers.serialize(object.bahan11,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah11 != null) {
      result
        ..add('jum_bahan11')
        ..add(serializers.serialize(object.jumlah11,
            specifiedType: const FullType(String)));
    }
    if (object.satuan11 != null) {
      result
        ..add('satuan11')
        ..add(serializers.serialize(object.satuan11,
            specifiedType: const FullType(String)));
    }
    if (object.bahan12 != null) {
      result
        ..add('nama_bahan12')
        ..add(serializers.serialize(object.bahan12,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah12 != null) {
      result
        ..add('jum_bahan12')
        ..add(serializers.serialize(object.jumlah12,
            specifiedType: const FullType(String)));
    }
    if (object.satuan12 != null) {
      result
        ..add('satuan12')
        ..add(serializers.serialize(object.satuan12,
            specifiedType: const FullType(String)));
    }
    if (object.bahan13 != null) {
      result
        ..add('nama_bahan13')
        ..add(serializers.serialize(object.bahan13,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah13 != null) {
      result
        ..add('jum_bahan13')
        ..add(serializers.serialize(object.jumlah13,
            specifiedType: const FullType(String)));
    }
    if (object.satuan13 != null) {
      result
        ..add('satuan13')
        ..add(serializers.serialize(object.satuan13,
            specifiedType: const FullType(String)));
    }
    if (object.bahan14 != null) {
      result
        ..add('nama_bahan14')
        ..add(serializers.serialize(object.bahan14,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah14 != null) {
      result
        ..add('jum_bahan14')
        ..add(serializers.serialize(object.jumlah14,
            specifiedType: const FullType(String)));
    }
    if (object.satuan14 != null) {
      result
        ..add('satuan14')
        ..add(serializers.serialize(object.satuan14,
            specifiedType: const FullType(String)));
    }
    if (object.bahan15 != null) {
      result
        ..add('nama_bahan15')
        ..add(serializers.serialize(object.bahan15,
            specifiedType: const FullType(String)));
    }
    if (object.jumlah15 != null) {
      result
        ..add('jum_bahan15')
        ..add(serializers.serialize(object.jumlah15,
            specifiedType: const FullType(String)));
    }
    if (object.satuan15 != null) {
      result
        ..add('satuan15')
        ..add(serializers.serialize(object.satuan15,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Material deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MaterialBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_pek':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jenis_pekerjaan':
          result.jenis = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_mandor':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tanggal':
          result.tanggal = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'uptd_id':
          result.uptd = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'peralatan':
          result.alat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rule':
          result.rule = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan1':
          result.bahan1 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan1':
          result.jumlah1 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan1':
          result.satuan1 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan2':
          result.bahan2 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan2':
          result.jumlah2 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan2':
          result.satuan2 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan3':
          result.bahan3 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan3':
          result.jumlah3 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan3':
          result.satuan3 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan4':
          result.bahan4 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan4':
          result.jumlah4 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan4':
          result.satuan4 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan5':
          result.bahan5 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan5':
          result.jumlah5 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan5':
          result.satuan5 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan6':
          result.bahan6 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan6':
          result.jumlah6 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan6':
          result.satuan6 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan7':
          result.bahan7 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan7':
          result.jumlah7 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan7':
          result.satuan7 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan8':
          result.bahan8 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan8':
          result.jumlah8 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan8':
          result.satuan8 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan9':
          result.bahan9 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan9':
          result.jumlah9 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan9':
          result.satuan9 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan10':
          result.bahan10 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan10':
          result.jumlah10 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan10':
          result.satuan10 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan11':
          result.bahan11 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan11':
          result.jumlah11 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan11':
          result.satuan11 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan12':
          result.bahan12 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan12':
          result.jumlah12 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan12':
          result.satuan12 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan13':
          result.bahan13 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan13':
          result.jumlah13 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan13':
          result.satuan13 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan14':
          result.bahan14 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan14':
          result.jumlah14 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan14':
          result.satuan14 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_bahan15':
          result.bahan15 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jum_bahan15':
          result.jumlah15 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'satuan15':
          result.satuan15 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Material extends Material {
  @override
  final String id;
  @override
  final String jenis;
  @override
  final String name;
  @override
  final String tanggal;
  @override
  final int uptd;
  @override
  final String alat;
  @override
  final String rule;
  @override
  final String bahan1;
  @override
  final String jumlah1;
  @override
  final String satuan1;
  @override
  final String bahan2;
  @override
  final String jumlah2;
  @override
  final String satuan2;
  @override
  final String bahan3;
  @override
  final String jumlah3;
  @override
  final String satuan3;
  @override
  final String bahan4;
  @override
  final String jumlah4;
  @override
  final String satuan4;
  @override
  final String bahan5;
  @override
  final String jumlah5;
  @override
  final String satuan5;
  @override
  final String bahan6;
  @override
  final String jumlah6;
  @override
  final String satuan6;
  @override
  final String bahan7;
  @override
  final String jumlah7;
  @override
  final String satuan7;
  @override
  final String bahan8;
  @override
  final String jumlah8;
  @override
  final String satuan8;
  @override
  final String bahan9;
  @override
  final String jumlah9;
  @override
  final String satuan9;
  @override
  final String bahan10;
  @override
  final String jumlah10;
  @override
  final String satuan10;
  @override
  final String bahan11;
  @override
  final String jumlah11;
  @override
  final String satuan11;
  @override
  final String bahan12;
  @override
  final String jumlah12;
  @override
  final String satuan12;
  @override
  final String bahan13;
  @override
  final String jumlah13;
  @override
  final String satuan13;
  @override
  final String bahan14;
  @override
  final String jumlah14;
  @override
  final String satuan14;
  @override
  final String bahan15;
  @override
  final String jumlah15;
  @override
  final String satuan15;

  factory _$Material([void Function(MaterialBuilder) updates]) =>
      (new MaterialBuilder()..update(updates)).build();

  _$Material._(
      {this.id,
      this.jenis,
      this.name,
      this.tanggal,
      this.uptd,
      this.alat,
      this.rule,
      this.bahan1,
      this.jumlah1,
      this.satuan1,
      this.bahan2,
      this.jumlah2,
      this.satuan2,
      this.bahan3,
      this.jumlah3,
      this.satuan3,
      this.bahan4,
      this.jumlah4,
      this.satuan4,
      this.bahan5,
      this.jumlah5,
      this.satuan5,
      this.bahan6,
      this.jumlah6,
      this.satuan6,
      this.bahan7,
      this.jumlah7,
      this.satuan7,
      this.bahan8,
      this.jumlah8,
      this.satuan8,
      this.bahan9,
      this.jumlah9,
      this.satuan9,
      this.bahan10,
      this.jumlah10,
      this.satuan10,
      this.bahan11,
      this.jumlah11,
      this.satuan11,
      this.bahan12,
      this.jumlah12,
      this.satuan12,
      this.bahan13,
      this.jumlah13,
      this.satuan13,
      this.bahan14,
      this.jumlah14,
      this.satuan14,
      this.bahan15,
      this.jumlah15,
      this.satuan15})
      : super._();

  @override
  Material rebuild(void Function(MaterialBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MaterialBuilder toBuilder() => new MaterialBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Material &&
        id == other.id &&
        jenis == other.jenis &&
        name == other.name &&
        tanggal == other.tanggal &&
        uptd == other.uptd &&
        alat == other.alat &&
        rule == other.rule &&
        bahan1 == other.bahan1 &&
        jumlah1 == other.jumlah1 &&
        satuan1 == other.satuan1 &&
        bahan2 == other.bahan2 &&
        jumlah2 == other.jumlah2 &&
        satuan2 == other.satuan2 &&
        bahan3 == other.bahan3 &&
        jumlah3 == other.jumlah3 &&
        satuan3 == other.satuan3 &&
        bahan4 == other.bahan4 &&
        jumlah4 == other.jumlah4 &&
        satuan4 == other.satuan4 &&
        bahan5 == other.bahan5 &&
        jumlah5 == other.jumlah5 &&
        satuan5 == other.satuan5 &&
        bahan6 == other.bahan6 &&
        jumlah6 == other.jumlah6 &&
        satuan6 == other.satuan6 &&
        bahan7 == other.bahan7 &&
        jumlah7 == other.jumlah7 &&
        satuan7 == other.satuan7 &&
        bahan8 == other.bahan8 &&
        jumlah8 == other.jumlah8 &&
        satuan8 == other.satuan8 &&
        bahan9 == other.bahan9 &&
        jumlah9 == other.jumlah9 &&
        satuan9 == other.satuan9 &&
        bahan10 == other.bahan10 &&
        jumlah10 == other.jumlah10 &&
        satuan10 == other.satuan10 &&
        bahan11 == other.bahan11 &&
        jumlah11 == other.jumlah11 &&
        satuan11 == other.satuan11 &&
        bahan12 == other.bahan12 &&
        jumlah12 == other.jumlah12 &&
        satuan12 == other.satuan12 &&
        bahan13 == other.bahan13 &&
        jumlah13 == other.jumlah13 &&
        satuan13 == other.satuan13 &&
        bahan14 == other.bahan14 &&
        jumlah14 == other.jumlah14 &&
        satuan14 == other.satuan14 &&
        bahan15 == other.bahan15 &&
        jumlah15 == other.jumlah15 &&
        satuan15 == other.satuan15;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        $jc(
                                                                            $jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc(0, id.hashCode), jenis.hashCode), name.hashCode), tanggal.hashCode), uptd.hashCode), alat.hashCode), rule.hashCode), bahan1.hashCode), jumlah1.hashCode), satuan1.hashCode), bahan2.hashCode), jumlah2.hashCode), satuan2.hashCode), bahan3.hashCode), jumlah3.hashCode), satuan3.hashCode), bahan4.hashCode), jumlah4.hashCode), satuan4.hashCode), bahan5.hashCode), jumlah5.hashCode), satuan5.hashCode), bahan6.hashCode), jumlah6.hashCode), satuan6.hashCode), bahan7.hashCode), jumlah7.hashCode), satuan7.hashCode), bahan8.hashCode), jumlah8.hashCode), satuan8.hashCode), bahan9.hashCode), jumlah9.hashCode),
                                                                                satuan9.hashCode),
                                                                            bahan10.hashCode),
                                                                        jumlah10.hashCode),
                                                                    satuan10.hashCode),
                                                                bahan11.hashCode),
                                                            jumlah11.hashCode),
                                                        satuan11.hashCode),
                                                    bahan12.hashCode),
                                                jumlah12.hashCode),
                                            satuan12.hashCode),
                                        bahan13.hashCode),
                                    jumlah13.hashCode),
                                satuan13.hashCode),
                            bahan14.hashCode),
                        jumlah14.hashCode),
                    satuan14.hashCode),
                bahan15.hashCode),
            jumlah15.hashCode),
        satuan15.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Material')
          ..add('id', id)
          ..add('jenis', jenis)
          ..add('name', name)
          ..add('tanggal', tanggal)
          ..add('uptd', uptd)
          ..add('alat', alat)
          ..add('rule', rule)
          ..add('bahan1', bahan1)
          ..add('jumlah1', jumlah1)
          ..add('satuan1', satuan1)
          ..add('bahan2', bahan2)
          ..add('jumlah2', jumlah2)
          ..add('satuan2', satuan2)
          ..add('bahan3', bahan3)
          ..add('jumlah3', jumlah3)
          ..add('satuan3', satuan3)
          ..add('bahan4', bahan4)
          ..add('jumlah4', jumlah4)
          ..add('satuan4', satuan4)
          ..add('bahan5', bahan5)
          ..add('jumlah5', jumlah5)
          ..add('satuan5', satuan5)
          ..add('bahan6', bahan6)
          ..add('jumlah6', jumlah6)
          ..add('satuan6', satuan6)
          ..add('bahan7', bahan7)
          ..add('jumlah7', jumlah7)
          ..add('satuan7', satuan7)
          ..add('bahan8', bahan8)
          ..add('jumlah8', jumlah8)
          ..add('satuan8', satuan8)
          ..add('bahan9', bahan9)
          ..add('jumlah9', jumlah9)
          ..add('satuan9', satuan9)
          ..add('bahan10', bahan10)
          ..add('jumlah10', jumlah10)
          ..add('satuan10', satuan10)
          ..add('bahan11', bahan11)
          ..add('jumlah11', jumlah11)
          ..add('satuan11', satuan11)
          ..add('bahan12', bahan12)
          ..add('jumlah12', jumlah12)
          ..add('satuan12', satuan12)
          ..add('bahan13', bahan13)
          ..add('jumlah13', jumlah13)
          ..add('satuan13', satuan13)
          ..add('bahan14', bahan14)
          ..add('jumlah14', jumlah14)
          ..add('satuan14', satuan14)
          ..add('bahan15', bahan15)
          ..add('jumlah15', jumlah15)
          ..add('satuan15', satuan15))
        .toString();
  }
}

class MaterialBuilder implements Builder<Material, MaterialBuilder> {
  _$Material _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _jenis;
  String get jenis => _$this._jenis;
  set jenis(String jenis) => _$this._jenis = jenis;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _tanggal;
  String get tanggal => _$this._tanggal;
  set tanggal(String tanggal) => _$this._tanggal = tanggal;

  int _uptd;
  int get uptd => _$this._uptd;
  set uptd(int uptd) => _$this._uptd = uptd;

  String _alat;
  String get alat => _$this._alat;
  set alat(String alat) => _$this._alat = alat;

  String _rule;
  String get rule => _$this._rule;
  set rule(String rule) => _$this._rule = rule;

  String _bahan1;
  String get bahan1 => _$this._bahan1;
  set bahan1(String bahan1) => _$this._bahan1 = bahan1;

  String _jumlah1;
  String get jumlah1 => _$this._jumlah1;
  set jumlah1(String jumlah1) => _$this._jumlah1 = jumlah1;

  String _satuan1;
  String get satuan1 => _$this._satuan1;
  set satuan1(String satuan1) => _$this._satuan1 = satuan1;

  String _bahan2;
  String get bahan2 => _$this._bahan2;
  set bahan2(String bahan2) => _$this._bahan2 = bahan2;

  String _jumlah2;
  String get jumlah2 => _$this._jumlah2;
  set jumlah2(String jumlah2) => _$this._jumlah2 = jumlah2;

  String _satuan2;
  String get satuan2 => _$this._satuan2;
  set satuan2(String satuan2) => _$this._satuan2 = satuan2;

  String _bahan3;
  String get bahan3 => _$this._bahan3;
  set bahan3(String bahan3) => _$this._bahan3 = bahan3;

  String _jumlah3;
  String get jumlah3 => _$this._jumlah3;
  set jumlah3(String jumlah3) => _$this._jumlah3 = jumlah3;

  String _satuan3;
  String get satuan3 => _$this._satuan3;
  set satuan3(String satuan3) => _$this._satuan3 = satuan3;

  String _bahan4;
  String get bahan4 => _$this._bahan4;
  set bahan4(String bahan4) => _$this._bahan4 = bahan4;

  String _jumlah4;
  String get jumlah4 => _$this._jumlah4;
  set jumlah4(String jumlah4) => _$this._jumlah4 = jumlah4;

  String _satuan4;
  String get satuan4 => _$this._satuan4;
  set satuan4(String satuan4) => _$this._satuan4 = satuan4;

  String _bahan5;
  String get bahan5 => _$this._bahan5;
  set bahan5(String bahan5) => _$this._bahan5 = bahan5;

  String _jumlah5;
  String get jumlah5 => _$this._jumlah5;
  set jumlah5(String jumlah5) => _$this._jumlah5 = jumlah5;

  String _satuan5;
  String get satuan5 => _$this._satuan5;
  set satuan5(String satuan5) => _$this._satuan5 = satuan5;

  String _bahan6;
  String get bahan6 => _$this._bahan6;
  set bahan6(String bahan6) => _$this._bahan6 = bahan6;

  String _jumlah6;
  String get jumlah6 => _$this._jumlah6;
  set jumlah6(String jumlah6) => _$this._jumlah6 = jumlah6;

  String _satuan6;
  String get satuan6 => _$this._satuan6;
  set satuan6(String satuan6) => _$this._satuan6 = satuan6;

  String _bahan7;
  String get bahan7 => _$this._bahan7;
  set bahan7(String bahan7) => _$this._bahan7 = bahan7;

  String _jumlah7;
  String get jumlah7 => _$this._jumlah7;
  set jumlah7(String jumlah7) => _$this._jumlah7 = jumlah7;

  String _satuan7;
  String get satuan7 => _$this._satuan7;
  set satuan7(String satuan7) => _$this._satuan7 = satuan7;

  String _bahan8;
  String get bahan8 => _$this._bahan8;
  set bahan8(String bahan8) => _$this._bahan8 = bahan8;

  String _jumlah8;
  String get jumlah8 => _$this._jumlah8;
  set jumlah8(String jumlah8) => _$this._jumlah8 = jumlah8;

  String _satuan8;
  String get satuan8 => _$this._satuan8;
  set satuan8(String satuan8) => _$this._satuan8 = satuan8;

  String _bahan9;
  String get bahan9 => _$this._bahan9;
  set bahan9(String bahan9) => _$this._bahan9 = bahan9;

  String _jumlah9;
  String get jumlah9 => _$this._jumlah9;
  set jumlah9(String jumlah9) => _$this._jumlah9 = jumlah9;

  String _satuan9;
  String get satuan9 => _$this._satuan9;
  set satuan9(String satuan9) => _$this._satuan9 = satuan9;

  String _bahan10;
  String get bahan10 => _$this._bahan10;
  set bahan10(String bahan10) => _$this._bahan10 = bahan10;

  String _jumlah10;
  String get jumlah10 => _$this._jumlah10;
  set jumlah10(String jumlah10) => _$this._jumlah10 = jumlah10;

  String _satuan10;
  String get satuan10 => _$this._satuan10;
  set satuan10(String satuan10) => _$this._satuan10 = satuan10;

  String _bahan11;
  String get bahan11 => _$this._bahan11;
  set bahan11(String bahan11) => _$this._bahan11 = bahan11;

  String _jumlah11;
  String get jumlah11 => _$this._jumlah11;
  set jumlah11(String jumlah11) => _$this._jumlah11 = jumlah11;

  String _satuan11;
  String get satuan11 => _$this._satuan11;
  set satuan11(String satuan11) => _$this._satuan11 = satuan11;

  String _bahan12;
  String get bahan12 => _$this._bahan12;
  set bahan12(String bahan12) => _$this._bahan12 = bahan12;

  String _jumlah12;
  String get jumlah12 => _$this._jumlah12;
  set jumlah12(String jumlah12) => _$this._jumlah12 = jumlah12;

  String _satuan12;
  String get satuan12 => _$this._satuan12;
  set satuan12(String satuan12) => _$this._satuan12 = satuan12;

  String _bahan13;
  String get bahan13 => _$this._bahan13;
  set bahan13(String bahan13) => _$this._bahan13 = bahan13;

  String _jumlah13;
  String get jumlah13 => _$this._jumlah13;
  set jumlah13(String jumlah13) => _$this._jumlah13 = jumlah13;

  String _satuan13;
  String get satuan13 => _$this._satuan13;
  set satuan13(String satuan13) => _$this._satuan13 = satuan13;

  String _bahan14;
  String get bahan14 => _$this._bahan14;
  set bahan14(String bahan14) => _$this._bahan14 = bahan14;

  String _jumlah14;
  String get jumlah14 => _$this._jumlah14;
  set jumlah14(String jumlah14) => _$this._jumlah14 = jumlah14;

  String _satuan14;
  String get satuan14 => _$this._satuan14;
  set satuan14(String satuan14) => _$this._satuan14 = satuan14;

  String _bahan15;
  String get bahan15 => _$this._bahan15;
  set bahan15(String bahan15) => _$this._bahan15 = bahan15;

  String _jumlah15;
  String get jumlah15 => _$this._jumlah15;
  set jumlah15(String jumlah15) => _$this._jumlah15 = jumlah15;

  String _satuan15;
  String get satuan15 => _$this._satuan15;
  set satuan15(String satuan15) => _$this._satuan15 = satuan15;

  MaterialBuilder();

  MaterialBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _jenis = _$v.jenis;
      _name = _$v.name;
      _tanggal = _$v.tanggal;
      _uptd = _$v.uptd;
      _alat = _$v.alat;
      _rule = _$v.rule;
      _bahan1 = _$v.bahan1;
      _jumlah1 = _$v.jumlah1;
      _satuan1 = _$v.satuan1;
      _bahan2 = _$v.bahan2;
      _jumlah2 = _$v.jumlah2;
      _satuan2 = _$v.satuan2;
      _bahan3 = _$v.bahan3;
      _jumlah3 = _$v.jumlah3;
      _satuan3 = _$v.satuan3;
      _bahan4 = _$v.bahan4;
      _jumlah4 = _$v.jumlah4;
      _satuan4 = _$v.satuan4;
      _bahan5 = _$v.bahan5;
      _jumlah5 = _$v.jumlah5;
      _satuan5 = _$v.satuan5;
      _bahan6 = _$v.bahan6;
      _jumlah6 = _$v.jumlah6;
      _satuan6 = _$v.satuan6;
      _bahan7 = _$v.bahan7;
      _jumlah7 = _$v.jumlah7;
      _satuan7 = _$v.satuan7;
      _bahan8 = _$v.bahan8;
      _jumlah8 = _$v.jumlah8;
      _satuan8 = _$v.satuan8;
      _bahan9 = _$v.bahan9;
      _jumlah9 = _$v.jumlah9;
      _satuan9 = _$v.satuan9;
      _bahan10 = _$v.bahan10;
      _jumlah10 = _$v.jumlah10;
      _satuan10 = _$v.satuan10;
      _bahan11 = _$v.bahan11;
      _jumlah11 = _$v.jumlah11;
      _satuan11 = _$v.satuan11;
      _bahan12 = _$v.bahan12;
      _jumlah12 = _$v.jumlah12;
      _satuan12 = _$v.satuan12;
      _bahan13 = _$v.bahan13;
      _jumlah13 = _$v.jumlah13;
      _satuan13 = _$v.satuan13;
      _bahan14 = _$v.bahan14;
      _jumlah14 = _$v.jumlah14;
      _satuan14 = _$v.satuan14;
      _bahan15 = _$v.bahan15;
      _jumlah15 = _$v.jumlah15;
      _satuan15 = _$v.satuan15;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Material other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Material;
  }

  @override
  void update(void Function(MaterialBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Material build() {
    final _$result = _$v ??
        new _$Material._(
            id: id,
            jenis: jenis,
            name: name,
            tanggal: tanggal,
            uptd: uptd,
            alat: alat,
            rule: rule,
            bahan1: bahan1,
            jumlah1: jumlah1,
            satuan1: satuan1,
            bahan2: bahan2,
            jumlah2: jumlah2,
            satuan2: satuan2,
            bahan3: bahan3,
            jumlah3: jumlah3,
            satuan3: satuan3,
            bahan4: bahan4,
            jumlah4: jumlah4,
            satuan4: satuan4,
            bahan5: bahan5,
            jumlah5: jumlah5,
            satuan5: satuan5,
            bahan6: bahan6,
            jumlah6: jumlah6,
            satuan6: satuan6,
            bahan7: bahan7,
            jumlah7: jumlah7,
            satuan7: satuan7,
            bahan8: bahan8,
            jumlah8: jumlah8,
            satuan8: satuan8,
            bahan9: bahan9,
            jumlah9: jumlah9,
            satuan9: satuan9,
            bahan10: bahan10,
            jumlah10: jumlah10,
            satuan10: satuan10,
            bahan11: bahan11,
            jumlah11: jumlah11,
            satuan11: satuan11,
            bahan12: bahan12,
            jumlah12: jumlah12,
            satuan12: satuan12,
            bahan13: bahan13,
            jumlah13: jumlah13,
            satuan13: satuan13,
            bahan14: bahan14,
            jumlah14: jumlah14,
            satuan14: satuan14,
            bahan15: bahan15,
            jumlah15: jumlah15,
            satuan15: satuan15);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
