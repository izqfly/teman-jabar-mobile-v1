import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'notification.g.dart';

abstract class Notification
    implements Built<Notification, NotificationBuilder> {
  @nullable
  int get id;

  @nullable
  String get title;

  @nullable
  String get subtitle;

  @nullable
  String get datePublish;

  Notification._();

  factory Notification([void Function(NotificationBuilder) updates]) =
      _$Notification;

  static Serializer<Notification> get serializer => _$notificationSerializer;

  Notification fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(Notification.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }
}
