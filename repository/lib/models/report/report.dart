import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'report.g.dart';

abstract class Report implements Built<Report, ReportBuilder> {
  @nullable
  int get id;

  @nullable
  @BuiltValueField(wireName: 'nama')
  String get name;

  @nullable
  @BuiltValueField(wireName: 'nomorPengaduan')
  String get reportNumber;

  @nullable
  @BuiltValueField(wireName: 'nik')
  String get identityCard;

  @nullable
  String get email;

  @nullable
  @BuiltValueField(wireName: 'telp')
  String get phoneNumber;

  @nullable
  @BuiltValueField(wireName: 'alamat')
  String get address;

  @nullable
  @BuiltValueField(wireName: 'lokasi')
  String get location;

  @nullable
  @BuiltValueField(wireName: 'jenis')
  String get reportType;

  @nullable
  String get progressDate;

  @nullable
  String get status;

  @nullable
  @BuiltValueField(wireName: 'deskripsi')
  String get description;

  @nullable
  @BuiltValueField(wireName: 'long')
  String get longitude;

  @nullable
  @BuiltValueField(wireName: 'lat')
  String get latitude;

  @nullable
  @BuiltValueField(wireName: 'uptd_id')
  int get uptdId;

  @nullable
  @BuiltValueField(wireName: 'pegawai_id')
  int get employeeId;

  @nullable
  @BuiltValueField(wireName: 'tanggal')
  String get date;

  @nullable
  @BuiltValueField(wireName: 'perkembangan')
  String get progress;

  @nullable
  @BuiltValueField(wireName: 'persentase')
  double get percentage;

  @nullable
  @BuiltValueField(wireName: 'laporan_id')
  int get reportId;

  @nullable
  @BuiltValueField(wireName: 'gambar')
  String get photoUrl;

  @nullable
  @BuiltValueField(wireName: 'petugas')
  String get employeeName;

  @nullable
  @BuiltValueField(wireName: 'no_petugas')
  String get employeeNumber;

  @nullable
  @BuiltValueField(wireName: 'dokumentasi')
  String get documentationUrl;

  @nullable
  @BuiltValueField(wireName: 'user_id')
  String get userId;

  Report._();

  factory Report([void Function(ReportBuilder) updates]) = _$Report;

  static Serializer<Report> get serializer => _$reportSerializer;

  Report fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(Report.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }

  String toJson() {
    return jsonEncode(serializers.serializeWith(Report.serializer, this));
  }

  String getNicePercentage() {
    if (percentage != null) {
      return percentage.toStringAsFixed(2);
    }
    return "0";
  }
}
