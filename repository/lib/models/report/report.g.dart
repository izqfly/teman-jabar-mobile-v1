// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Report> _$reportSerializer = new _$ReportSerializer();

class _$ReportSerializer implements StructuredSerializer<Report> {
  @override
  final Iterable<Type> types = const [Report, _$Report];
  @override
  final String wireName = 'Report';

  @override
  Iterable<Object> serialize(Serializers serializers, Report object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.name != null) {
      result
        ..add('nama')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.reportNumber != null) {
      result
        ..add('nomorPengaduan')
        ..add(serializers.serialize(object.reportNumber,
            specifiedType: const FullType(String)));
    }
    if (object.identityCard != null) {
      result
        ..add('nik')
        ..add(serializers.serialize(object.identityCard,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.phoneNumber != null) {
      result
        ..add('telp')
        ..add(serializers.serialize(object.phoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.address != null) {
      result
        ..add('alamat')
        ..add(serializers.serialize(object.address,
            specifiedType: const FullType(String)));
    }
    if (object.location != null) {
      result
        ..add('lokasi')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(String)));
    }
    if (object.reportType != null) {
      result
        ..add('jenis')
        ..add(serializers.serialize(object.reportType,
            specifiedType: const FullType(String)));
    }
    if (object.progressDate != null) {
      result
        ..add('progressDate')
        ..add(serializers.serialize(object.progressDate,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('deskripsi')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.longitude != null) {
      result
        ..add('long')
        ..add(serializers.serialize(object.longitude,
            specifiedType: const FullType(String)));
    }
    if (object.latitude != null) {
      result
        ..add('lat')
        ..add(serializers.serialize(object.latitude,
            specifiedType: const FullType(String)));
    }
    if (object.uptdId != null) {
      result
        ..add('uptd_id')
        ..add(serializers.serialize(object.uptdId,
            specifiedType: const FullType(int)));
    }
    if (object.employeeId != null) {
      result
        ..add('pegawai_id')
        ..add(serializers.serialize(object.employeeId,
            specifiedType: const FullType(int)));
    }
    if (object.date != null) {
      result
        ..add('tanggal')
        ..add(serializers.serialize(object.date,
            specifiedType: const FullType(String)));
    }
    if (object.progress != null) {
      result
        ..add('perkembangan')
        ..add(serializers.serialize(object.progress,
            specifiedType: const FullType(String)));
    }
    if (object.percentage != null) {
      result
        ..add('persentase')
        ..add(serializers.serialize(object.percentage,
            specifiedType: const FullType(double)));
    }
    if (object.reportId != null) {
      result
        ..add('laporan_id')
        ..add(serializers.serialize(object.reportId,
            specifiedType: const FullType(int)));
    }
    if (object.photoUrl != null) {
      result
        ..add('gambar')
        ..add(serializers.serialize(object.photoUrl,
            specifiedType: const FullType(String)));
    }
    if (object.employeeName != null) {
      result
        ..add('petugas')
        ..add(serializers.serialize(object.employeeName,
            specifiedType: const FullType(String)));
    }
    if (object.employeeNumber != null) {
      result
        ..add('no_petugas')
        ..add(serializers.serialize(object.employeeNumber,
            specifiedType: const FullType(String)));
    }
    if (object.documentationUrl != null) {
      result
        ..add('dokumentasi')
        ..add(serializers.serialize(object.documentationUrl,
            specifiedType: const FullType(String)));
    }
    if (object.userId != null) {
      result
        ..add('user_id')
        ..add(serializers.serialize(object.userId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Report deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ReportBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'nama':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nomorPengaduan':
          result.reportNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nik':
          result.identityCard = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'telp':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'alamat':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lokasi':
          result.location = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jenis':
          result.reportType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'progressDate':
          result.progressDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'deskripsi':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'long':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lat':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'uptd_id':
          result.uptdId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'pegawai_id':
          result.employeeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'tanggal':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'perkembangan':
          result.progress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'persentase':
          result.percentage = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'laporan_id':
          result.reportId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'gambar':
          result.photoUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'petugas':
          result.employeeName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'no_petugas':
          result.employeeNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dokumentasi':
          result.documentationUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user_id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Report extends Report {
  @override
  final int id;
  @override
  final String name;
  @override
  final String reportNumber;
  @override
  final String identityCard;
  @override
  final String email;
  @override
  final String phoneNumber;
  @override
  final String address;
  @override
  final String location;
  @override
  final String reportType;
  @override
  final String progressDate;
  @override
  final String status;
  @override
  final String description;
  @override
  final String longitude;
  @override
  final String latitude;
  @override
  final int uptdId;
  @override
  final int employeeId;
  @override
  final String date;
  @override
  final String progress;
  @override
  final double percentage;
  @override
  final int reportId;
  @override
  final String photoUrl;
  @override
  final String employeeName;
  @override
  final String employeeNumber;
  @override
  final String documentationUrl;
  @override
  final String userId;

  factory _$Report([void Function(ReportBuilder) updates]) =>
      (new ReportBuilder()..update(updates)).build();

  _$Report._(
      {this.id,
      this.name,
      this.reportNumber,
      this.identityCard,
      this.email,
      this.phoneNumber,
      this.address,
      this.location,
      this.reportType,
      this.progressDate,
      this.status,
      this.description,
      this.longitude,
      this.latitude,
      this.uptdId,
      this.employeeId,
      this.date,
      this.progress,
      this.percentage,
      this.reportId,
      this.photoUrl,
      this.employeeName,
      this.employeeNumber,
      this.documentationUrl,
      this.userId})
      : super._();

  @override
  Report rebuild(void Function(ReportBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ReportBuilder toBuilder() => new ReportBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Report &&
        id == other.id &&
        name == other.name &&
        reportNumber == other.reportNumber &&
        identityCard == other.identityCard &&
        email == other.email &&
        phoneNumber == other.phoneNumber &&
        address == other.address &&
        location == other.location &&
        reportType == other.reportType &&
        progressDate == other.progressDate &&
        status == other.status &&
        description == other.description &&
        longitude == other.longitude &&
        latitude == other.latitude &&
        uptdId == other.uptdId &&
        employeeId == other.employeeId &&
        date == other.date &&
        progress == other.progress &&
        percentage == other.percentage &&
        reportId == other.reportId &&
        photoUrl == other.photoUrl &&
        employeeName == other.employeeName &&
        employeeNumber == other.employeeNumber &&
        documentationUrl == other.documentationUrl &&
        userId == other.userId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        $jc(
                                                                            $jc($jc($jc($jc($jc($jc($jc(0, id.hashCode), name.hashCode), reportNumber.hashCode), identityCard.hashCode), email.hashCode), phoneNumber.hashCode),
                                                                                address.hashCode),
                                                                            location.hashCode),
                                                                        reportType.hashCode),
                                                                    progressDate.hashCode),
                                                                status.hashCode),
                                                            description.hashCode),
                                                        longitude.hashCode),
                                                    latitude.hashCode),
                                                uptdId.hashCode),
                                            employeeId.hashCode),
                                        date.hashCode),
                                    progress.hashCode),
                                percentage.hashCode),
                            reportId.hashCode),
                        photoUrl.hashCode),
                    employeeName.hashCode),
                employeeNumber.hashCode),
            documentationUrl.hashCode),
        userId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Report')
          ..add('id', id)
          ..add('name', name)
          ..add('reportNumber', reportNumber)
          ..add('identityCard', identityCard)
          ..add('email', email)
          ..add('phoneNumber', phoneNumber)
          ..add('address', address)
          ..add('location', location)
          ..add('reportType', reportType)
          ..add('progressDate', progressDate)
          ..add('status', status)
          ..add('description', description)
          ..add('longitude', longitude)
          ..add('latitude', latitude)
          ..add('uptdId', uptdId)
          ..add('employeeId', employeeId)
          ..add('date', date)
          ..add('progress', progress)
          ..add('percentage', percentage)
          ..add('reportId', reportId)
          ..add('photoUrl', photoUrl)
          ..add('employeeName', employeeName)
          ..add('employeeNumber', employeeNumber)
          ..add('documentationUrl', documentationUrl)
          ..add('userId', userId))
        .toString();
  }
}

class ReportBuilder implements Builder<Report, ReportBuilder> {
  _$Report _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _reportNumber;
  String get reportNumber => _$this._reportNumber;
  set reportNumber(String reportNumber) => _$this._reportNumber = reportNumber;

  String _identityCard;
  String get identityCard => _$this._identityCard;
  set identityCard(String identityCard) => _$this._identityCard = identityCard;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _phoneNumber;
  String get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String phoneNumber) => _$this._phoneNumber = phoneNumber;

  String _address;
  String get address => _$this._address;
  set address(String address) => _$this._address = address;

  String _location;
  String get location => _$this._location;
  set location(String location) => _$this._location = location;

  String _reportType;
  String get reportType => _$this._reportType;
  set reportType(String reportType) => _$this._reportType = reportType;

  String _progressDate;
  String get progressDate => _$this._progressDate;
  set progressDate(String progressDate) => _$this._progressDate = progressDate;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _longitude;
  String get longitude => _$this._longitude;
  set longitude(String longitude) => _$this._longitude = longitude;

  String _latitude;
  String get latitude => _$this._latitude;
  set latitude(String latitude) => _$this._latitude = latitude;

  int _uptdId;
  int get uptdId => _$this._uptdId;
  set uptdId(int uptdId) => _$this._uptdId = uptdId;

  int _employeeId;
  int get employeeId => _$this._employeeId;
  set employeeId(int employeeId) => _$this._employeeId = employeeId;

  String _date;
  String get date => _$this._date;
  set date(String date) => _$this._date = date;

  String _progress;
  String get progress => _$this._progress;
  set progress(String progress) => _$this._progress = progress;

  double _percentage;
  double get percentage => _$this._percentage;
  set percentage(double percentage) => _$this._percentage = percentage;

  int _reportId;
  int get reportId => _$this._reportId;
  set reportId(int reportId) => _$this._reportId = reportId;

  String _photoUrl;
  String get photoUrl => _$this._photoUrl;
  set photoUrl(String photoUrl) => _$this._photoUrl = photoUrl;

  String _employeeName;
  String get employeeName => _$this._employeeName;
  set employeeName(String employeeName) => _$this._employeeName = employeeName;

  String _employeeNumber;
  String get employeeNumber => _$this._employeeNumber;
  set employeeNumber(String employeeNumber) =>
      _$this._employeeNumber = employeeNumber;

  String _documentationUrl;
  String get documentationUrl => _$this._documentationUrl;
  set documentationUrl(String documentationUrl) =>
      _$this._documentationUrl = documentationUrl;

  String _userId;
  String get userId => _$this._userId;
  set userId(String userId) => _$this._userId = userId;

  ReportBuilder();

  ReportBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _reportNumber = _$v.reportNumber;
      _identityCard = _$v.identityCard;
      _email = _$v.email;
      _phoneNumber = _$v.phoneNumber;
      _address = _$v.address;
      _location = _$v.location;
      _reportType = _$v.reportType;
      _progressDate = _$v.progressDate;
      _status = _$v.status;
      _description = _$v.description;
      _longitude = _$v.longitude;
      _latitude = _$v.latitude;
      _uptdId = _$v.uptdId;
      _employeeId = _$v.employeeId;
      _date = _$v.date;
      _progress = _$v.progress;
      _percentage = _$v.percentage;
      _reportId = _$v.reportId;
      _photoUrl = _$v.photoUrl;
      _employeeName = _$v.employeeName;
      _employeeNumber = _$v.employeeNumber;
      _documentationUrl = _$v.documentationUrl;
      _userId = _$v.userId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Report other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Report;
  }

  @override
  void update(void Function(ReportBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Report build() {
    final _$result = _$v ??
        new _$Report._(
            id: id,
            name: name,
            reportNumber: reportNumber,
            identityCard: identityCard,
            email: email,
            phoneNumber: phoneNumber,
            address: address,
            location: location,
            reportType: reportType,
            progressDate: progressDate,
            status: status,
            description: description,
            longitude: longitude,
            latitude: latitude,
            uptdId: uptdId,
            employeeId: employeeId,
            date: date,
            progress: progress,
            percentage: percentage,
            reportId: reportId,
            photoUrl: photoUrl,
            employeeName: employeeName,
            employeeNumber: employeeNumber,
            documentationUrl: documentationUrl,
            userId: userId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
