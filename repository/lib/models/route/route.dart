import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'route.g.dart';

abstract class Route implements Built<Route, RouteBuilder> {
  @nullable
  @BuiltValueField(wireName: 'RUAS_JALAN')
  String get streetName;

  @nullable
  @BuiltValueField(wireName: 'KEGIATAN')
  String get activity;

  @nullable
  @BuiltValueField(wireName: 'LAT')
  double get latitude;

  @nullable
  @BuiltValueField(wireName: 'LNG')
  double get longitude;

  @nullable
  String get lat;

  @nullable
  String get long;

  @nullable
  @BuiltValueField(wireName: 'DISTANCE')
  double get ditance;

  Route._();

  factory Route([void Function(RouteBuilder) updates]) = _$Route;

  static Serializer<Route> get serializer => _$routeSerializer;

  String toJson() {
    return jsonEncode(serializers.serializeWith(Route.serializer, this));
  }

  Route fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(Route.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }
}
