// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'route.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Route> _$routeSerializer = new _$RouteSerializer();

class _$RouteSerializer implements StructuredSerializer<Route> {
  @override
  final Iterable<Type> types = const [Route, _$Route];
  @override
  final String wireName = 'Route';

  @override
  Iterable<Object> serialize(Serializers serializers, Route object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.streetName != null) {
      result
        ..add('RUAS_JALAN')
        ..add(serializers.serialize(object.streetName,
            specifiedType: const FullType(String)));
    }
    if (object.activity != null) {
      result
        ..add('KEGIATAN')
        ..add(serializers.serialize(object.activity,
            specifiedType: const FullType(String)));
    }
    if (object.latitude != null) {
      result
        ..add('LAT')
        ..add(serializers.serialize(object.latitude,
            specifiedType: const FullType(double)));
    }
    if (object.longitude != null) {
      result
        ..add('LNG')
        ..add(serializers.serialize(object.longitude,
            specifiedType: const FullType(double)));
    }
    if (object.lat != null) {
      result
        ..add('lat')
        ..add(serializers.serialize(object.lat,
            specifiedType: const FullType(String)));
    }
    if (object.long != null) {
      result
        ..add('long')
        ..add(serializers.serialize(object.long,
            specifiedType: const FullType(String)));
    }
    if (object.ditance != null) {
      result
        ..add('DISTANCE')
        ..add(serializers.serialize(object.ditance,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  Route deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RouteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'RUAS_JALAN':
          result.streetName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'KEGIATAN':
          result.activity = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'LAT':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'LNG':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'lat':
          result.lat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'long':
          result.long = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'DISTANCE':
          result.ditance = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$Route extends Route {
  @override
  final String streetName;
  @override
  final String activity;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final String lat;
  @override
  final String long;
  @override
  final double ditance;

  factory _$Route([void Function(RouteBuilder) updates]) =>
      (new RouteBuilder()..update(updates)).build();

  _$Route._(
      {this.streetName,
      this.activity,
      this.latitude,
      this.longitude,
      this.lat,
      this.long,
      this.ditance})
      : super._();

  @override
  Route rebuild(void Function(RouteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RouteBuilder toBuilder() => new RouteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Route &&
        streetName == other.streetName &&
        activity == other.activity &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        lat == other.lat &&
        long == other.long &&
        ditance == other.ditance;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, streetName.hashCode), activity.hashCode),
                        latitude.hashCode),
                    longitude.hashCode),
                lat.hashCode),
            long.hashCode),
        ditance.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Route')
          ..add('streetName', streetName)
          ..add('activity', activity)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('lat', lat)
          ..add('long', long)
          ..add('ditance', ditance))
        .toString();
  }
}

class RouteBuilder implements Builder<Route, RouteBuilder> {
  _$Route _$v;

  String _streetName;
  String get streetName => _$this._streetName;
  set streetName(String streetName) => _$this._streetName = streetName;

  String _activity;
  String get activity => _$this._activity;
  set activity(String activity) => _$this._activity = activity;

  double _latitude;
  double get latitude => _$this._latitude;
  set latitude(double latitude) => _$this._latitude = latitude;

  double _longitude;
  double get longitude => _$this._longitude;
  set longitude(double longitude) => _$this._longitude = longitude;

  String _lat;
  String get lat => _$this._lat;
  set lat(String lat) => _$this._lat = lat;

  String _long;
  String get long => _$this._long;
  set long(String long) => _$this._long = long;

  double _ditance;
  double get ditance => _$this._ditance;
  set ditance(double ditance) => _$this._ditance = ditance;

  RouteBuilder();

  RouteBuilder get _$this {
    if (_$v != null) {
      _streetName = _$v.streetName;
      _activity = _$v.activity;
      _latitude = _$v.latitude;
      _longitude = _$v.longitude;
      _lat = _$v.lat;
      _long = _$v.long;
      _ditance = _$v.ditance;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Route other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Route;
  }

  @override
  void update(void Function(RouteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Route build() {
    final _$result = _$v ??
        new _$Route._(
            streetName: streetName,
            activity: activity,
            latitude: latitude,
            longitude: longitude,
            lat: lat,
            long: long,
            ditance: ditance);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
