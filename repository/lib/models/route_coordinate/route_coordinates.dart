class RouteCoordinates {
  final double originLat;
  final double originLong;
  final double destLat;
  final double destLong;

  RouteCoordinates({
    this.originLat,
    this.originLong,
    this.destLat,
    this.destLong,
  });
}
