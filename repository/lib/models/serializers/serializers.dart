library serializers;

import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:repository/models/chart/chart.dart';
import 'package:repository/models/contract_project/contract_project.dart';
import 'package:repository/models/damage_road/damage_road.dart';
import 'package:repository/models/material/material.dart';
import 'package:repository/models/notification/notification.dart';
import 'package:repository/models/report/report.dart';
import 'package:repository/models/route/route.dart';
import 'package:repository/models/stability_road/stability_road.dart';
import 'package:repository/models/user/user.dart';
import 'package:repository/models/task/task.dart';
import 'package:repository/models/anouncement/anouncement.dart';
import 'package:built_collection/built_collection.dart';

part 'serializers.g.dart';

@SerializersFor([
  User,
  Notification,
  Report,
  ContractProject,
  Route,
  StabilityRoad,
  DamageRoad,
  Chart,
  Task,
  Material,
  Anouncement,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
