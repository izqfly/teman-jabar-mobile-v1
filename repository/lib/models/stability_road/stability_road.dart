import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/chart/chart.dart';
import 'package:repository/models/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';

part 'stability_road.g.dart';

abstract class StabilityRoad
    implements Built<StabilityRoad, StabilityRoadBuilder> {
  @nullable
  int get id;

  @nullable
  @BuiltValueField(wireName: 'nomor_ruas')
  String get roadSectionNumber;

  @nullable
  @BuiltValueField(wireName: 'sub_ruas')
  String get subRoadSection;

  @nullable
  String get suffix;

  @nullable
  @BuiltValueField(wireName: 'bulan')
  int get month;

  @nullable
  int get year;

  @nullable
  @BuiltValueField(wireName: 'kota')
  String get city;

  @nullable
  @BuiltValueField(wireName: 'lat_awal')
  String get originLat;

  @nullable
  @BuiltValueField(wireName: 'lng_awal')
  String get originLng;

  @nullable
  @BuiltValueField(wireName: 'luas_jalan')
  double get roadArea;

  @nullable
  double get latitude;

  @nullable
  double get longitude;

  @nullable
  @BuiltValueField(wireName: 'lat_akhir')
  String get destLat;

  @nullable
  @BuiltValueField(wireName: 'lng_akhir')
  String get destLng;

  @nullable
  @BuiltValueField(wireName: 'keterangan')
  String get description;

  @nullable
  String get spp;

  @nullable
  String get uptd;

  @BuiltValueField(wireName: 'kondisi_jalan')
  @nullable
  BuiltList<Chart> get roadConditions;

  StabilityRoad._();

  factory StabilityRoad([void Function(StabilityRoadBuilder) updates]) =
      _$StabilityRoad;

  static Serializer<StabilityRoad> get serializer => _$stabilityRoadSerializer;

  StabilityRoad fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(StabilityRoad.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }
}
