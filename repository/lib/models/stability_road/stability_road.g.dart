// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stability_road.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<StabilityRoad> _$stabilityRoadSerializer =
    new _$StabilityRoadSerializer();

class _$StabilityRoadSerializer implements StructuredSerializer<StabilityRoad> {
  @override
  final Iterable<Type> types = const [StabilityRoad, _$StabilityRoad];
  @override
  final String wireName = 'StabilityRoad';

  @override
  Iterable<Object> serialize(Serializers serializers, StabilityRoad object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.roadSectionNumber != null) {
      result
        ..add('nomor_ruas')
        ..add(serializers.serialize(object.roadSectionNumber,
            specifiedType: const FullType(String)));
    }
    if (object.subRoadSection != null) {
      result
        ..add('sub_ruas')
        ..add(serializers.serialize(object.subRoadSection,
            specifiedType: const FullType(String)));
    }
    if (object.suffix != null) {
      result
        ..add('suffix')
        ..add(serializers.serialize(object.suffix,
            specifiedType: const FullType(String)));
    }
    if (object.month != null) {
      result
        ..add('bulan')
        ..add(serializers.serialize(object.month,
            specifiedType: const FullType(int)));
    }
    if (object.year != null) {
      result
        ..add('year')
        ..add(serializers.serialize(object.year,
            specifiedType: const FullType(int)));
    }
    if (object.city != null) {
      result
        ..add('kota')
        ..add(serializers.serialize(object.city,
            specifiedType: const FullType(String)));
    }
    if (object.originLat != null) {
      result
        ..add('lat_awal')
        ..add(serializers.serialize(object.originLat,
            specifiedType: const FullType(String)));
    }
    if (object.originLng != null) {
      result
        ..add('lng_awal')
        ..add(serializers.serialize(object.originLng,
            specifiedType: const FullType(String)));
    }
    if (object.roadArea != null) {
      result
        ..add('luas_jalan')
        ..add(serializers.serialize(object.roadArea,
            specifiedType: const FullType(double)));
    }
    if (object.latitude != null) {
      result
        ..add('latitude')
        ..add(serializers.serialize(object.latitude,
            specifiedType: const FullType(double)));
    }
    if (object.longitude != null) {
      result
        ..add('longitude')
        ..add(serializers.serialize(object.longitude,
            specifiedType: const FullType(double)));
    }
    if (object.destLat != null) {
      result
        ..add('lat_akhir')
        ..add(serializers.serialize(object.destLat,
            specifiedType: const FullType(String)));
    }
    if (object.destLng != null) {
      result
        ..add('lng_akhir')
        ..add(serializers.serialize(object.destLng,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('keterangan')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.spp != null) {
      result
        ..add('spp')
        ..add(serializers.serialize(object.spp,
            specifiedType: const FullType(String)));
    }
    if (object.uptd != null) {
      result
        ..add('uptd')
        ..add(serializers.serialize(object.uptd,
            specifiedType: const FullType(String)));
    }
    if (object.roadConditions != null) {
      result
        ..add('kondisi_jalan')
        ..add(serializers.serialize(object.roadConditions,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Chart)])));
    }
    return result;
  }

  @override
  StabilityRoad deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new StabilityRoadBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'nomor_ruas':
          result.roadSectionNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sub_ruas':
          result.subRoadSection = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'suffix':
          result.suffix = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'bulan':
          result.month = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'year':
          result.year = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'kota':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lat_awal':
          result.originLat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lng_awal':
          result.originLng = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'luas_jalan':
          result.roadArea = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'lat_akhir':
          result.destLat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lng_akhir':
          result.destLng = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'keterangan':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'spp':
          result.spp = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'uptd':
          result.uptd = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'kondisi_jalan':
          result.roadConditions.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Chart)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$StabilityRoad extends StabilityRoad {
  @override
  final int id;
  @override
  final String roadSectionNumber;
  @override
  final String subRoadSection;
  @override
  final String suffix;
  @override
  final int month;
  @override
  final int year;
  @override
  final String city;
  @override
  final String originLat;
  @override
  final String originLng;
  @override
  final double roadArea;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final String destLat;
  @override
  final String destLng;
  @override
  final String description;
  @override
  final String spp;
  @override
  final String uptd;
  @override
  final BuiltList<Chart> roadConditions;

  factory _$StabilityRoad([void Function(StabilityRoadBuilder) updates]) =>
      (new StabilityRoadBuilder()..update(updates)).build();

  _$StabilityRoad._(
      {this.id,
      this.roadSectionNumber,
      this.subRoadSection,
      this.suffix,
      this.month,
      this.year,
      this.city,
      this.originLat,
      this.originLng,
      this.roadArea,
      this.latitude,
      this.longitude,
      this.destLat,
      this.destLng,
      this.description,
      this.spp,
      this.uptd,
      this.roadConditions})
      : super._();

  @override
  StabilityRoad rebuild(void Function(StabilityRoadBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StabilityRoadBuilder toBuilder() => new StabilityRoadBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is StabilityRoad &&
        id == other.id &&
        roadSectionNumber == other.roadSectionNumber &&
        subRoadSection == other.subRoadSection &&
        suffix == other.suffix &&
        month == other.month &&
        year == other.year &&
        city == other.city &&
        originLat == other.originLat &&
        originLng == other.originLng &&
        roadArea == other.roadArea &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        destLat == other.destLat &&
        destLng == other.destLng &&
        description == other.description &&
        spp == other.spp &&
        uptd == other.uptd &&
        roadConditions == other.roadConditions;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        $jc(
                                                                            0,
                                                                            id
                                                                                .hashCode),
                                                                        roadSectionNumber
                                                                            .hashCode),
                                                                    subRoadSection
                                                                        .hashCode),
                                                                suffix
                                                                    .hashCode),
                                                            month.hashCode),
                                                        year.hashCode),
                                                    city.hashCode),
                                                originLat.hashCode),
                                            originLng.hashCode),
                                        roadArea.hashCode),
                                    latitude.hashCode),
                                longitude.hashCode),
                            destLat.hashCode),
                        destLng.hashCode),
                    description.hashCode),
                spp.hashCode),
            uptd.hashCode),
        roadConditions.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('StabilityRoad')
          ..add('id', id)
          ..add('roadSectionNumber', roadSectionNumber)
          ..add('subRoadSection', subRoadSection)
          ..add('suffix', suffix)
          ..add('month', month)
          ..add('year', year)
          ..add('city', city)
          ..add('originLat', originLat)
          ..add('originLng', originLng)
          ..add('roadArea', roadArea)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('destLat', destLat)
          ..add('destLng', destLng)
          ..add('description', description)
          ..add('spp', spp)
          ..add('uptd', uptd)
          ..add('roadConditions', roadConditions))
        .toString();
  }
}

class StabilityRoadBuilder
    implements Builder<StabilityRoad, StabilityRoadBuilder> {
  _$StabilityRoad _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _roadSectionNumber;
  String get roadSectionNumber => _$this._roadSectionNumber;
  set roadSectionNumber(String roadSectionNumber) =>
      _$this._roadSectionNumber = roadSectionNumber;

  String _subRoadSection;
  String get subRoadSection => _$this._subRoadSection;
  set subRoadSection(String subRoadSection) =>
      _$this._subRoadSection = subRoadSection;

  String _suffix;
  String get suffix => _$this._suffix;
  set suffix(String suffix) => _$this._suffix = suffix;

  int _month;
  int get month => _$this._month;
  set month(int month) => _$this._month = month;

  int _year;
  int get year => _$this._year;
  set year(int year) => _$this._year = year;

  String _city;
  String get city => _$this._city;
  set city(String city) => _$this._city = city;

  String _originLat;
  String get originLat => _$this._originLat;
  set originLat(String originLat) => _$this._originLat = originLat;

  String _originLng;
  String get originLng => _$this._originLng;
  set originLng(String originLng) => _$this._originLng = originLng;

  double _roadArea;
  double get roadArea => _$this._roadArea;
  set roadArea(double roadArea) => _$this._roadArea = roadArea;

  double _latitude;
  double get latitude => _$this._latitude;
  set latitude(double latitude) => _$this._latitude = latitude;

  double _longitude;
  double get longitude => _$this._longitude;
  set longitude(double longitude) => _$this._longitude = longitude;

  String _destLat;
  String get destLat => _$this._destLat;
  set destLat(String destLat) => _$this._destLat = destLat;

  String _destLng;
  String get destLng => _$this._destLng;
  set destLng(String destLng) => _$this._destLng = destLng;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _spp;
  String get spp => _$this._spp;
  set spp(String spp) => _$this._spp = spp;

  String _uptd;
  String get uptd => _$this._uptd;
  set uptd(String uptd) => _$this._uptd = uptd;

  ListBuilder<Chart> _roadConditions;
  ListBuilder<Chart> get roadConditions =>
      _$this._roadConditions ??= new ListBuilder<Chart>();
  set roadConditions(ListBuilder<Chart> roadConditions) =>
      _$this._roadConditions = roadConditions;

  StabilityRoadBuilder();

  StabilityRoadBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _roadSectionNumber = _$v.roadSectionNumber;
      _subRoadSection = _$v.subRoadSection;
      _suffix = _$v.suffix;
      _month = _$v.month;
      _year = _$v.year;
      _city = _$v.city;
      _originLat = _$v.originLat;
      _originLng = _$v.originLng;
      _roadArea = _$v.roadArea;
      _latitude = _$v.latitude;
      _longitude = _$v.longitude;
      _destLat = _$v.destLat;
      _destLng = _$v.destLng;
      _description = _$v.description;
      _spp = _$v.spp;
      _uptd = _$v.uptd;
      _roadConditions = _$v.roadConditions?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StabilityRoad other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$StabilityRoad;
  }

  @override
  void update(void Function(StabilityRoadBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$StabilityRoad build() {
    _$StabilityRoad _$result;
    try {
      _$result = _$v ??
          new _$StabilityRoad._(
              id: id,
              roadSectionNumber: roadSectionNumber,
              subRoadSection: subRoadSection,
              suffix: suffix,
              month: month,
              year: year,
              city: city,
              originLat: originLat,
              originLng: originLng,
              roadArea: roadArea,
              latitude: latitude,
              longitude: longitude,
              destLat: destLat,
              destLng: destLng,
              description: description,
              spp: spp,
              uptd: uptd,
              roadConditions: _roadConditions?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'roadConditions';
        _roadConditions?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'StabilityRoad', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
