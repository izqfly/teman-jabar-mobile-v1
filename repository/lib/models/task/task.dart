import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'task.g.dart';

abstract class Task implements Built<Task, TaskBuilder> {
  
  @nullable
  @BuiltValueField(wireName: 'id_pek')
  String get id;

  @nullable
  @BuiltValueField(wireName: 'tanggal')
  String get tanggal;

  @nullable
  @BuiltValueField(wireName: 'idSup')
  int get idSup;

  @nullable
  @BuiltValueField(wireName: 'namaPaket')
  String get namaPaket;

  @nullable
  String get email;

  @nullable
  @BuiltValueField(wireName: 'idRuasJalan')
  String get idRuasJalan;

  @nullable
  @BuiltValueField(wireName: 'idJenisPekerjaan')
  int get idJenisPekerjaan;

  @nullable
  @BuiltValueField(wireName: 'lokasi')
  String get lokasi;

  @nullable
  @BuiltValueField(wireName: 'lat')
  double get lat;

  @nullable
  @BuiltValueField(wireName: 'long')
  double get long;

  @nullable
  @BuiltValueField(wireName: 'panjang')
  String get panjang;

  @nullable
  @BuiltValueField(wireName: 'jumlah_pekerja')
  int get pekerja;

  @nullable
  @BuiltValueField(wireName: 'peralatan')
  String get alat;

  @nullable
  @BuiltValueField(wireName: 'video')
  String get videoUrl;

  @nullable
  @BuiltValueField(wireName: 'fotoAwal')
  String get fotoAwal;

  @nullable
  @BuiltValueField(wireName: 'fotoAkhir')
  String get fotoAkhir;

  @nullable
  @BuiltValueField(wireName: 'fotoSedang')
  String get fotoSedang;

  @nullable
  @BuiltValueField(wireName: 'ruas_jalan')
  String get ruas;

  @nullable
  @BuiltValueField(wireName: 'jenis_pekerjaan')
  String get jenis;

  @nullable
  @BuiltValueField(wireName: 'paket')
  String get paket;

  @nullable
  @BuiltValueField(wireName: 'sup')
  String get sup;

  @nullable
  @BuiltValueField(wireName: 'sup_id')
  int get id_sup;

  @nullable
  @BuiltValueField(wireName: 'uptd_id')
  int get uptd;

  Task._();

  factory Task([void Function(TaskBuilder) updates]) = _$Task;

  static Serializer<Task> get serializer => _$taskSerializer;

  Task fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(Task.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }

  String toJson() {
    return jsonEncode(serializers.serializeWith(Task.serializer, this));
  }
  
}
