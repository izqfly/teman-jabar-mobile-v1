// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Task> _$taskSerializer = new _$TaskSerializer();

class _$TaskSerializer implements StructuredSerializer<Task> {
  @override
  final Iterable<Type> types = const [Task, _$Task];
  @override
  final String wireName = 'Task';

  @override
  Iterable<Object> serialize(Serializers serializers, Task object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id_pek')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.tanggal != null) {
      result
        ..add('tanggal')
        ..add(serializers.serialize(object.tanggal,
            specifiedType: const FullType(String)));
    }
    if (object.idSup != null) {
      result
        ..add('idSup')
        ..add(serializers.serialize(object.idSup,
            specifiedType: const FullType(int)));
    }
    if (object.namaPaket != null) {
      result
        ..add('namaPaket')
        ..add(serializers.serialize(object.namaPaket,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.idRuasJalan != null) {
      result
        ..add('idRuasJalan')
        ..add(serializers.serialize(object.idRuasJalan,
            specifiedType: const FullType(String)));
    }
    if (object.idJenisPekerjaan != null) {
      result
        ..add('idJenisPekerjaan')
        ..add(serializers.serialize(object.idJenisPekerjaan,
            specifiedType: const FullType(int)));
    }
    if (object.lokasi != null) {
      result
        ..add('lokasi')
        ..add(serializers.serialize(object.lokasi,
            specifiedType: const FullType(String)));
    }
    if (object.lat != null) {
      result
        ..add('lat')
        ..add(serializers.serialize(object.lat,
            specifiedType: const FullType(double)));
    }
    if (object.long != null) {
      result
        ..add('long')
        ..add(serializers.serialize(object.long,
            specifiedType: const FullType(double)));
    }
    if (object.panjang != null) {
      result
        ..add('panjang')
        ..add(serializers.serialize(object.panjang,
            specifiedType: const FullType(String)));
    }
    if (object.pekerja != null) {
      result
        ..add('jumlah_pekerja')
        ..add(serializers.serialize(object.pekerja,
            specifiedType: const FullType(int)));
    }
    if (object.alat != null) {
      result
        ..add('peralatan')
        ..add(serializers.serialize(object.alat,
            specifiedType: const FullType(String)));
    }
    if (object.videoUrl != null) {
      result
        ..add('video')
        ..add(serializers.serialize(object.videoUrl,
            specifiedType: const FullType(String)));
    }
    if (object.fotoAwal != null) {
      result
        ..add('fotoAwal')
        ..add(serializers.serialize(object.fotoAwal,
            specifiedType: const FullType(String)));
    }
    if (object.fotoAkhir != null) {
      result
        ..add('fotoAkhir')
        ..add(serializers.serialize(object.fotoAkhir,
            specifiedType: const FullType(String)));
    }
    if (object.fotoSedang != null) {
      result
        ..add('fotoSedang')
        ..add(serializers.serialize(object.fotoSedang,
            specifiedType: const FullType(String)));
    }
    if (object.ruas != null) {
      result
        ..add('ruas_jalan')
        ..add(serializers.serialize(object.ruas,
            specifiedType: const FullType(String)));
    }
    if (object.jenis != null) {
      result
        ..add('jenis_pekerjaan')
        ..add(serializers.serialize(object.jenis,
            specifiedType: const FullType(String)));
    }
    if (object.paket != null) {
      result
        ..add('paket')
        ..add(serializers.serialize(object.paket,
            specifiedType: const FullType(String)));
    }
    if (object.sup != null) {
      result
        ..add('sup')
        ..add(serializers.serialize(object.sup,
            specifiedType: const FullType(String)));
    }
    if (object.id_sup != null) {
      result
        ..add('sup_id')
        ..add(serializers.serialize(object.id_sup,
            specifiedType: const FullType(int)));
    }
    if (object.uptd != null) {
      result
        ..add('uptd_id')
        ..add(serializers.serialize(object.uptd,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  Task deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_pek':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tanggal':
          result.tanggal = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'idSup':
          result.idSup = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'namaPaket':
          result.namaPaket = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'idRuasJalan':
          result.idRuasJalan = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'idJenisPekerjaan':
          result.idJenisPekerjaan = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'lokasi':
          result.lokasi = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lat':
          result.lat = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'long':
          result.long = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'panjang':
          result.panjang = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jumlah_pekerja':
          result.pekerja = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'peralatan':
          result.alat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'video':
          result.videoUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'fotoAwal':
          result.fotoAwal = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'fotoAkhir':
          result.fotoAkhir = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'fotoSedang':
          result.fotoSedang = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ruas_jalan':
          result.ruas = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jenis_pekerjaan':
          result.jenis = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'paket':
          result.paket = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sup':
          result.sup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sup_id':
          result.id_sup = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'uptd_id':
          result.uptd = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$Task extends Task {
  @override
  final String id;
  @override
  final String tanggal;
  @override
  final int idSup;
  @override
  final String namaPaket;
  @override
  final String email;
  @override
  final String idRuasJalan;
  @override
  final int idJenisPekerjaan;
  @override
  final String lokasi;
  @override
  final double lat;
  @override
  final double long;
  @override
  final String panjang;
  @override
  final int pekerja;
  @override
  final String alat;
  @override
  final String videoUrl;
  @override
  final String fotoAwal;
  @override
  final String fotoAkhir;
  @override
  final String fotoSedang;
  @override
  final String ruas;
  @override
  final String jenis;
  @override
  final String paket;
  @override
  final String sup;
  @override
  final int id_sup;
  @override
  final int uptd;

  factory _$Task([void Function(TaskBuilder) updates]) =>
      (new TaskBuilder()..update(updates)).build();

  _$Task._(
      {this.id,
      this.tanggal,
      this.idSup,
      this.namaPaket,
      this.email,
      this.idRuasJalan,
      this.idJenisPekerjaan,
      this.lokasi,
      this.lat,
      this.long,
      this.panjang,
      this.pekerja,
      this.alat,
      this.videoUrl,
      this.fotoAwal,
      this.fotoAkhir,
      this.fotoSedang,
      this.ruas,
      this.jenis,
      this.paket,
      this.sup,
      this.id_sup,
      this.uptd})
      : super._();

  @override
  Task rebuild(void Function(TaskBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskBuilder toBuilder() => new TaskBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Task &&
        id == other.id &&
        tanggal == other.tanggal &&
        idSup == other.idSup &&
        namaPaket == other.namaPaket &&
        email == other.email &&
        idRuasJalan == other.idRuasJalan &&
        idJenisPekerjaan == other.idJenisPekerjaan &&
        lokasi == other.lokasi &&
        lat == other.lat &&
        long == other.long &&
        panjang == other.panjang &&
        pekerja == other.pekerja &&
        alat == other.alat &&
        videoUrl == other.videoUrl &&
        fotoAwal == other.fotoAwal &&
        fotoAkhir == other.fotoAkhir &&
        fotoSedang == other.fotoSedang &&
        ruas == other.ruas &&
        jenis == other.jenis &&
        paket == other.paket &&
        sup == other.sup &&
        id_sup == other.id_sup &&
        uptd == other.uptd;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        $jc(
                                                                            $jc($jc($jc($jc($jc(0, id.hashCode), tanggal.hashCode), idSup.hashCode), namaPaket.hashCode),
                                                                                email.hashCode),
                                                                            idRuasJalan.hashCode),
                                                                        idJenisPekerjaan.hashCode),
                                                                    lokasi.hashCode),
                                                                lat.hashCode),
                                                            long.hashCode),
                                                        panjang.hashCode),
                                                    pekerja.hashCode),
                                                alat.hashCode),
                                            videoUrl.hashCode),
                                        fotoAwal.hashCode),
                                    fotoAkhir.hashCode),
                                fotoSedang.hashCode),
                            ruas.hashCode),
                        jenis.hashCode),
                    paket.hashCode),
                sup.hashCode),
            id_sup.hashCode),
        uptd.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Task')
          ..add('id', id)
          ..add('tanggal', tanggal)
          ..add('idSup', idSup)
          ..add('namaPaket', namaPaket)
          ..add('email', email)
          ..add('idRuasJalan', idRuasJalan)
          ..add('idJenisPekerjaan', idJenisPekerjaan)
          ..add('lokasi', lokasi)
          ..add('lat', lat)
          ..add('long', long)
          ..add('panjang', panjang)
          ..add('pekerja', pekerja)
          ..add('alat', alat)
          ..add('videoUrl', videoUrl)
          ..add('fotoAwal', fotoAwal)
          ..add('fotoAkhir', fotoAkhir)
          ..add('fotoSedang', fotoSedang)
          ..add('ruas', ruas)
          ..add('jenis', jenis)
          ..add('paket', paket)
          ..add('sup', sup)
          ..add('id_sup', id_sup)
          ..add('uptd', uptd))
        .toString();
  }
}

class TaskBuilder implements Builder<Task, TaskBuilder> {
  _$Task _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _tanggal;
  String get tanggal => _$this._tanggal;
  set tanggal(String tanggal) => _$this._tanggal = tanggal;

  int _idSup;
  int get idSup => _$this._idSup;
  set idSup(int idSup) => _$this._idSup = idSup;

  String _namaPaket;
  String get namaPaket => _$this._namaPaket;
  set namaPaket(String namaPaket) => _$this._namaPaket = namaPaket;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _idRuasJalan;
  String get idRuasJalan => _$this._idRuasJalan;
  set idRuasJalan(String idRuasJalan) => _$this._idRuasJalan = idRuasJalan;

  int _idJenisPekerjaan;
  int get idJenisPekerjaan => _$this._idJenisPekerjaan;
  set idJenisPekerjaan(int idJenisPekerjaan) =>
      _$this._idJenisPekerjaan = idJenisPekerjaan;

  String _lokasi;
  String get lokasi => _$this._lokasi;
  set lokasi(String lokasi) => _$this._lokasi = lokasi;

  double _lat;
  double get lat => _$this._lat;
  set lat(double lat) => _$this._lat = lat;

  double _long;
  double get long => _$this._long;
  set long(double long) => _$this._long = long;

  String _panjang;
  String get panjang => _$this._panjang;
  set panjang(String panjang) => _$this._panjang = panjang;

  int _pekerja;
  int get pekerja => _$this._pekerja;
  set pekerja(int pekerja) => _$this._pekerja = pekerja;

  String _alat;
  String get alat => _$this._alat;
  set alat(String alat) => _$this._alat = alat;

  String _videoUrl;
  String get videoUrl => _$this._videoUrl;
  set videoUrl(String videoUrl) => _$this._videoUrl = videoUrl;

  String _fotoAwal;
  String get fotoAwal => _$this._fotoAwal;
  set fotoAwal(String fotoAwal) => _$this._fotoAwal = fotoAwal;

  String _fotoAkhir;
  String get fotoAkhir => _$this._fotoAkhir;
  set fotoAkhir(String fotoAkhir) => _$this._fotoAkhir = fotoAkhir;

  String _fotoSedang;
  String get fotoSedang => _$this._fotoSedang;
  set fotoSedang(String fotoSedang) => _$this._fotoSedang = fotoSedang;

  String _ruas;
  String get ruas => _$this._ruas;
  set ruas(String ruas) => _$this._ruas = ruas;

  String _jenis;
  String get jenis => _$this._jenis;
  set jenis(String jenis) => _$this._jenis = jenis;

  String _paket;
  String get paket => _$this._paket;
  set paket(String paket) => _$this._paket = paket;

  String _sup;
  String get sup => _$this._sup;
  set sup(String sup) => _$this._sup = sup;

  int _id_sup;
  int get id_sup => _$this._id_sup;
  set id_sup(int id_sup) => _$this._id_sup = id_sup;

  int _uptd;
  int get uptd => _$this._uptd;
  set uptd(int uptd) => _$this._uptd = uptd;

  TaskBuilder();

  TaskBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _tanggal = _$v.tanggal;
      _idSup = _$v.idSup;
      _namaPaket = _$v.namaPaket;
      _email = _$v.email;
      _idRuasJalan = _$v.idRuasJalan;
      _idJenisPekerjaan = _$v.idJenisPekerjaan;
      _lokasi = _$v.lokasi;
      _lat = _$v.lat;
      _long = _$v.long;
      _panjang = _$v.panjang;
      _pekerja = _$v.pekerja;
      _alat = _$v.alat;
      _videoUrl = _$v.videoUrl;
      _fotoAwal = _$v.fotoAwal;
      _fotoAkhir = _$v.fotoAkhir;
      _fotoSedang = _$v.fotoSedang;
      _ruas = _$v.ruas;
      _jenis = _$v.jenis;
      _paket = _$v.paket;
      _sup = _$v.sup;
      _id_sup = _$v.id_sup;
      _uptd = _$v.uptd;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Task other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Task;
  }

  @override
  void update(void Function(TaskBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Task build() {
    final _$result = _$v ??
        new _$Task._(
            id: id,
            tanggal: tanggal,
            idSup: idSup,
            namaPaket: namaPaket,
            email: email,
            idRuasJalan: idRuasJalan,
            idJenisPekerjaan: idJenisPekerjaan,
            lokasi: lokasi,
            lat: lat,
            long: long,
            panjang: panjang,
            pekerja: pekerja,
            alat: alat,
            videoUrl: videoUrl,
            fotoAwal: fotoAwal,
            fotoAkhir: fotoAkhir,
            fotoSedang: fotoSedang,
            ruas: ruas,
            jenis: jenis,
            paket: paket,
            sup: sup,
            id_sup: id_sup,
            uptd: uptd);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
