import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:repository/models/serializers/serializers.dart';

part 'user.g.dart';

abstract class User implements Built<User, UserBuilder> {
  @nullable
  @BuiltValueField(wireName: 'name')
  String get fullname;

  @nullable
  String get city;

  @nullable
  String get address;

  @nullable
  String get phoneNumber;

  @nullable
  String get email;

  @nullable
  String get password;

  @nullable
  String get passwordOld;

  @nullable
  String get passwordNew;

  @nullable
  String get role;

  @nullable
  @BuiltValueField(wireName: 'kode_otp')
  String get otp;

  @nullable
  @BuiltValueField(wireName: 'token')
  String get fcmToken;

  @nullable
  @BuiltValueField(wireName: 'id')
  int get id;

  @nullable
  @BuiltValueField(wireName: 'user_id')
  int get userId;

  @nullable
  @BuiltValueField(wireName: 'encrypted_id')
  String get encryptedId;

  @nullable
  @BuiltValueField(wireName: 'sup')
  String get sup;

  @nullable
  @BuiltValueField(wireName: 'sup_id')
  int get supid;

  User._();

  factory User([void Function(UserBuilder) updates]) = _$User;

  static Serializer<User> get serializer => _$userSerializer;

  String toJson() {
    return jsonEncode(serializers.serializeWith(User.serializer, this));
  }

  User fromJson(dynamic json) {
    try {
      return serializers.deserializeWith(User.serializer, json);
    } catch (e) {
      print(e);
      return null;
    }
  }

  String getNiceRole() {
    if (role == "internal"){
      return "/auth/internal-landing-page";
    } else if (role == "mandor"){
      return "/auth/mandor-landing-page";
    }else {
      return "/auth/public-landing-page";
    }
  }

  String getFistCharacterFullname() {
    if (fullname != null && fullname.length > 1) {
      return fullname.substring(0, 1).toUpperCase();
    }
    return "";
  }
}
