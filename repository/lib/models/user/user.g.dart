// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<User> _$userSerializer = new _$UserSerializer();

class _$UserSerializer implements StructuredSerializer<User> {
  @override
  final Iterable<Type> types = const [User, _$User];
  @override
  final String wireName = 'User';

  @override
  Iterable<Object> serialize(Serializers serializers, User object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.fullname != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.fullname,
            specifiedType: const FullType(String)));
    }
    if (object.city != null) {
      result
        ..add('city')
        ..add(serializers.serialize(object.city,
            specifiedType: const FullType(String)));
    }
    if (object.address != null) {
      result
        ..add('address')
        ..add(serializers.serialize(object.address,
            specifiedType: const FullType(String)));
    }
    if (object.phoneNumber != null) {
      result
        ..add('phoneNumber')
        ..add(serializers.serialize(object.phoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.password != null) {
      result
        ..add('password')
        ..add(serializers.serialize(object.password,
            specifiedType: const FullType(String)));
    }
    if (object.passwordOld != null) {
      result
        ..add('passwordOld')
        ..add(serializers.serialize(object.passwordOld,
            specifiedType: const FullType(String)));
    }
    if (object.passwordNew != null) {
      result
        ..add('passwordNew')
        ..add(serializers.serialize(object.passwordNew,
            specifiedType: const FullType(String)));
    }
    if (object.role != null) {
      result
        ..add('role')
        ..add(serializers.serialize(object.role,
            specifiedType: const FullType(String)));
    }
    if (object.otp != null) {
      result
        ..add('kode_otp')
        ..add(serializers.serialize(object.otp,
            specifiedType: const FullType(String)));
    }
    if (object.fcmToken != null) {
      result
        ..add('token')
        ..add(serializers.serialize(object.fcmToken,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.userId != null) {
      result
        ..add('user_id')
        ..add(serializers.serialize(object.userId,
            specifiedType: const FullType(int)));
    }
    if (object.encryptedId != null) {
      result
        ..add('encrypted_id')
        ..add(serializers.serialize(object.encryptedId,
            specifiedType: const FullType(String)));
    }
    if (object.sup != null) {
      result
        ..add('sup')
        ..add(serializers.serialize(object.sup,
            specifiedType: const FullType(String)));
    }
    if (object.supid != null) {
      result
        ..add('sup_id')
        ..add(serializers.serialize(object.supid,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  User deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.fullname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'address':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phoneNumber':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'password':
          result.password = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'passwordOld':
          result.passwordOld = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'passwordNew':
          result.passwordNew = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'role':
          result.role = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'kode_otp':
          result.otp = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'token':
          result.fcmToken = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'user_id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'encrypted_id':
          result.encryptedId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sup':
          result.sup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sup_id':
          result.supid = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$User extends User {
  @override
  final String fullname;
  @override
  final String city;
  @override
  final String address;
  @override
  final String phoneNumber;
  @override
  final String email;
  @override
  final String password;
  @override
  final String passwordOld;
  @override
  final String passwordNew;
  @override
  final String role;
  @override
  final String otp;
  @override
  final String fcmToken;
  @override
  final int id;
  @override
  final int userId;
  @override
  final String encryptedId;
  @override
  final String sup;
  @override
  final int supid;

  factory _$User([void Function(UserBuilder) updates]) =>
      (new UserBuilder()..update(updates)).build();

  _$User._(
      {this.fullname,
      this.city,
      this.address,
      this.phoneNumber,
      this.email,
      this.password,
      this.passwordOld,
      this.passwordNew,
      this.role,
      this.otp,
      this.fcmToken,
      this.id,
      this.userId,
      this.encryptedId,
      this.sup,
      this.supid})
      : super._();

  @override
  User rebuild(void Function(UserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserBuilder toBuilder() => new UserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
        fullname == other.fullname &&
        city == other.city &&
        address == other.address &&
        phoneNumber == other.phoneNumber &&
        email == other.email &&
        password == other.password &&
        passwordOld == other.passwordOld &&
        passwordNew == other.passwordNew &&
        role == other.role &&
        otp == other.otp &&
        fcmToken == other.fcmToken &&
        id == other.id &&
        userId == other.userId &&
        encryptedId == other.encryptedId &&
        sup == other.sup &&
        supid == other.supid;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    0,
                                                                    fullname
                                                                        .hashCode),
                                                                city.hashCode),
                                                            address.hashCode),
                                                        phoneNumber.hashCode),
                                                    email.hashCode),
                                                password.hashCode),
                                            passwordOld.hashCode),
                                        passwordNew.hashCode),
                                    role.hashCode),
                                otp.hashCode),
                            fcmToken.hashCode),
                        id.hashCode),
                    userId.hashCode),
                encryptedId.hashCode),
            sup.hashCode),
        supid.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('User')
          ..add('fullname', fullname)
          ..add('city', city)
          ..add('address', address)
          ..add('phoneNumber', phoneNumber)
          ..add('email', email)
          ..add('password', password)
          ..add('passwordOld', passwordOld)
          ..add('passwordNew', passwordNew)
          ..add('role', role)
          ..add('otp', otp)
          ..add('fcmToken', fcmToken)
          ..add('id', id)
          ..add('userId', userId)
          ..add('encryptedId', encryptedId)
          ..add('sup', sup)
          ..add('supid', supid))
        .toString();
  }
}

class UserBuilder implements Builder<User, UserBuilder> {
  _$User _$v;

  String _fullname;
  String get fullname => _$this._fullname;
  set fullname(String fullname) => _$this._fullname = fullname;

  String _city;
  String get city => _$this._city;
  set city(String city) => _$this._city = city;

  String _address;
  String get address => _$this._address;
  set address(String address) => _$this._address = address;

  String _phoneNumber;
  String get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String phoneNumber) => _$this._phoneNumber = phoneNumber;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  String _passwordOld;
  String get passwordOld => _$this._passwordOld;
  set passwordOld(String passwordOld) => _$this._passwordOld = passwordOld;

  String _passwordNew;
  String get passwordNew => _$this._passwordNew;
  set passwordNew(String passwordNew) => _$this._passwordNew = passwordNew;

  String _role;
  String get role => _$this._role;
  set role(String role) => _$this._role = role;

  String _otp;
  String get otp => _$this._otp;
  set otp(String otp) => _$this._otp = otp;

  String _fcmToken;
  String get fcmToken => _$this._fcmToken;
  set fcmToken(String fcmToken) => _$this._fcmToken = fcmToken;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  int _userId;
  int get userId => _$this._userId;
  set userId(int userId) => _$this._userId = userId;

  String _encryptedId;
  String get encryptedId => _$this._encryptedId;
  set encryptedId(String encryptedId) => _$this._encryptedId = encryptedId;

  String _sup;
  String get sup => _$this._sup;
  set sup(String sup) => _$this._sup = sup;

  int _supid;
  int get supid => _$this._supid;
  set supid(int supid) => _$this._supid = supid;

  UserBuilder();

  UserBuilder get _$this {
    if (_$v != null) {
      _fullname = _$v.fullname;
      _city = _$v.city;
      _address = _$v.address;
      _phoneNumber = _$v.phoneNumber;
      _email = _$v.email;
      _password = _$v.password;
      _passwordOld = _$v.passwordOld;
      _passwordNew = _$v.passwordNew;
      _role = _$v.role;
      _otp = _$v.otp;
      _fcmToken = _$v.fcmToken;
      _id = _$v.id;
      _userId = _$v.userId;
      _encryptedId = _$v.encryptedId;
      _sup = _$v.sup;
      _supid = _$v.supid;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(User other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$User;
  }

  @override
  void update(void Function(UserBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$User build() {
    final _$result = _$v ??
        new _$User._(
            fullname: fullname,
            city: city,
            address: address,
            phoneNumber: phoneNumber,
            email: email,
            password: password,
            passwordOld: passwordOld,
            passwordNew: passwordNew,
            role: role,
            otp: otp,
            fcmToken: fcmToken,
            id: id,
            userId: userId,
            encryptedId: encryptedId,
            sup: sup,
            supid: supid);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
