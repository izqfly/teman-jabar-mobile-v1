class DropdownResponse {
  dynamic id;
  String name;
  String child;
  bool isChecked;

  DropdownResponse({this.id, this.name, this.isChecked, this.child});

  DropdownResponse fromJson(Map<String, dynamic> json) {
    DropdownResponse model = new DropdownResponse();
    model.id = json["id"];
    model.name = json["name"];
    model.child = json["child"] != null ? json["child"] : null;
    return model;
  }

  @override
  String toString() {
    return "ID : " + id.toString() + "\n " + "NAME : " + name + "\n ";
  }
}
