class JsonResponse {
  dynamic _data;
  String _message;
  String _status;
  int _pageSize;

  JsonResponse(dynamic json) {
    _status = json["status"] != null ? json["status"] : "error";
    _message = json["message"] != null ? json["message"] : null;
    _data = json["data"] != null ? json["data"] : null;
  }

  JsonSqlite(dynamic json) {
    if (json != null && json != ""){
      _status = json["status"] != null ? json["status"] : "error";
      _message = json["message"] != null ? json["message"] : null;
      _data = json["data"] != null ? json["data"] : null;
    } else {
      _status = "error";
    }

  }

  bool isSuccess() =>
      _status == "Success" || _status == "success" ? true : false;
  bool isFailed() => _status != "success" ? true : false;
  bool isWarning() => _status == "warning" ? true : false;

  dynamic getData() => _data;
  String getMessage() => _message;
  int getSize() => _pageSize;
}

class JsonSqlite {
  dynamic _data;
  String _message;
  String _status;
  int _pageSize;

  JsonSqlite(dynamic json) {
    if (json != null && json != ""){
      _status = json["status"] != null ? json["status"] : "error";
      _message = json["message"] != null ? json["message"] : null;
      _data = json["data"] != null ? json["data"] : null;
    } else {
      _status = "error";
    }

  }

  bool isSuccess() =>
      _status == "Success" || _status == "success" ? true : false;
  bool isFailed() => _status != "success" ? true : false;
  bool isWarning() => _status == "warning" ? true : false;

  dynamic getData() => _data;
  String getMessage() => _message;
  int getSize() => _pageSize;
}

class JsonNotif {
  dynamic _data;
  String _message;
  int _status;
  int _pageSize;

  JsonNotif(dynamic json) {
    if (json != null && json != ""){
      _status = json["response"]["status"] != null ? json["response"]["status"] : 1;
      _message = json["response"]["message"] != null ? json["response"]["message"] : null;
      _data = json["data"] != null ? json["data"] : null;
    } else {
      _status = 1;
    }

  }

  bool isSuccess() =>
      _status == 200 ? true : false;
  bool isFailed() => _status != 200 ? true : false;
  bool isWarning() => _status == 10 ? true : false;

  dynamic getData() => _data;
  String getMessage() => _message;
  int getSize() => _pageSize;
}
