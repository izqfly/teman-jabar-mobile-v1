class MandorResponse {
  dynamic id;
  String name;
  String child;
  bool isChecked;

  MandorResponse({this.id, this.name, this.isChecked, this.child});

  MandorResponse fromJson(Map<String, dynamic> json) {
    MandorResponse model = new MandorResponse();
    model.id = json["id_ruas_jalan"];
    model.name = json["nama_ruas_jalan"];
    model.child = json["child"] != null ? json["child"] : null;
    return model;
  }

  @override
  String toString() {
    return "ID : " + id.toString() + "\n " + "NAME : " + name + "\n ";
  }
}

class DropdownResponse {
  dynamic id;
  String name;
  String child;
  bool isChecked;

  DropdownResponse({this.id, this.name, this.isChecked, this.child});

  DropdownResponse fromJson(Map<String, dynamic> json) {
    DropdownResponse model = new DropdownResponse();
    model.id = json["id"];
    model.name = json["name"];
    return model;
  }

  @override
  String toString() {
    return "ID : " + id.toString() + "\n " + "NAME : " + name + "\n ";
  }
}

class JenisPekerjaan {

  dynamic id;
  String name;
  String child;
  bool isChecked;

  JenisPekerjaan({this.id, this.name, this.isChecked, this.child});

  JenisPekerjaan fromJson(Map<String, dynamic> json) {
    JenisPekerjaan model = new JenisPekerjaan();
    model.id = json["no"];
    model.name = json["nama_item"];
    return model;
  }

}

class PaketPekerjaan {
  
  dynamic id;
  String name;
  String child;
  bool isChecked;

  PaketPekerjaan({this.id, this.name, this.isChecked, this.child});

  PaketPekerjaan fromJson(Map<String, dynamic> json) {
    PaketPekerjaan model = new PaketPekerjaan();
    model.id = json["id"];
    model.name = json["name"];
    return model;
  }

  @override
  String toString() {
    return "ID : " + id.toString() + "\n " + "NAME : " + name + "\n ";
  }
}

