class BahanMaterial {

  dynamic id;
  String name;
  String child;
  bool isChecked;

  BahanMaterial({this.id, this.name, this.isChecked, this.child});

  BahanMaterial fromJson(Map<String, dynamic> json) {
    BahanMaterial model = new BahanMaterial();
    model.id = json["no"];
    model.name = json["nama_item"];
    return model;
  }

}

class SatuanMaterial {

  dynamic id;
  String name;
  String child;
  bool isChecked;

  SatuanMaterial({this.id, this.name, this.isChecked, this.child});

  SatuanMaterial fromJson(Map<String, dynamic> json) {
    SatuanMaterial model = new SatuanMaterial();
    model.id = json["no"];
    model.name = json["satuan"];
    return model;
  }

}