import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:flutter/services.dart';
import 'package:repository/constants/error_message.dart';

class NetworkResponse {
  dynamic notify(Response response) async {
    if (response.statusCode == 401) {
      throw PlatformException(
        code: HttpStatus.unauthorized.toString(),
        message: ErrorMessage.unauthorized,
      );
    } else if (response.statusCode == 413) {
      throw PlatformException(
        code: HttpStatus.requestEntityTooLarge.toString(),
        message: ErrorMessage.maximumUpload,
      );
    } else if (response.statusCode == 404) {
      throw PlatformException(
        code: HttpStatus.notFound.toString(),
        message: ErrorMessage.apiNotFound,
      );
    } else if (response.statusCode != 200) {
      throw PlatformException(
        code: HttpStatus.internalServerError.toString(),
        message: ErrorMessage.internalServer,
      );
    }

    return jsonDecode(response.body);
  }
}
