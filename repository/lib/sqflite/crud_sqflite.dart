import 'package:sqflite/sqlite_api.dart';
import 'sqf_lite.dart';
import 'model_task.dart';

class CrudSqfLite {
  SqfLite dbHelper = new SqfLite();
  Future<int> insert(ModelTask modelTask) async {
    Database db = await dbHelper.initDb();
    int count = await db.insert('draft', modelTask.toMap());
    return count;
  }
  Future<int> update(ModelTask todo) async {
    Database db = await dbHelper.initDb();
    int count = await db.update('draft', todo.toMap(),
        where: 'id=?', whereArgs: [todo.id]);
    return count;
  }
  Future<int> delete(ModelTask todo) async {
    Database db = await dbHelper.initDb();
    int count =
    await db.delete('draft', where: 'id=?', whereArgs: [todo.id]);
    return count;
  }
  Future<List<ModelTask>> getTaskList() async {
    Database db = await dbHelper.initDb();
    List<Map<String, dynamic>> mapList =
    await db.query('draft', orderBy: 'id DESC');
    int count = mapList.length;
    List<ModelTask> taskList = List<ModelTask>();
    for (int i = 0; i < count; i++) {
      taskList.add(ModelTask.fromMap(mapList[i]));
    }
    return taskList;
  }


}
