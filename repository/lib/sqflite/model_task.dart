class ModelTask {
  int id;
  String tanggal;
  int id_pekerjaan;
  String jenis_pekerjaan;
  String nama_paket;
  int id_sup;
  String sup;
  String id_ruas;
  String ruas_jalan;
  String lokasi;
  double lat;
  double long;
  String panjang;
  int jumlah_pekerja;
  //String alat;
  String foto_pegawai;
  String foto_awal;
  String foto_sedang;
  String foto_akhir;
  String video;


  ModelTask(
      this.tanggal,
      this.id_pekerjaan,
      this.jenis_pekerjaan,
      this.nama_paket,
      this.id_sup,
      this.sup,
      this.id_ruas,
      this.ruas_jalan,
      this.lokasi,
      this.lat,
      this.long,
      this.panjang,
      this.jumlah_pekerja,
      //this.alat,
      this.foto_pegawai,
      this.foto_awal,
      this.foto_sedang,
      this.foto_akhir,
      this.video);

  ModelTask.fromMap(Map<String, dynamic> map) {
    this.id = map["id"];
    this.tanggal  = map["tanggal"];
    this.id_pekerjaan = map["id_jenis"];
    this.jenis_pekerjaan = map["jenis_pekerjaan"];
    this.nama_paket = map["nama_paket"];
    this.id_sup = int.parse(map["id_sup"]);
    this.sup = map["sup"];
    this.id_ruas = map["id_ruas"];
    this.ruas_jalan = map["ruas_jalan"];
    this.lokasi = map["lokasi"];
    this.lat = map["lat"];
    this.long = map["long"];
    this.panjang = map["panjang"];
    this.jumlah_pekerja = map["jumlah_pekerja"];
    //this.alat = map["alat"];
    this.foto_pegawai = map["foto_pegawai"];
    this.foto_awal = map["foto_awal"];
    this.foto_sedang = map["foto_sedang"];
    this.foto_akhir = map["foto_akhir"];
    this.video = map["video"];
  }

  // int get _id => id;
  // String get _tanggal => tanggal;
  // int get id_jenis => id_pekerjaan;
  // String get _paket => nama_paket;
  // int get _sup => sup;
  // String get _ruas => ruas_jalan;
  // String get _lokasi => lokasi;
  // double get _lat => lat;
  // double get _long => long;
  // String get _panjang => panjang;
  // int get _jumlah => jumlah_pekerja;
  // String get _alat => alat;
  // String get _awal => foto_awal;
  // String get _sedang => foto_sedang;
  // String get _akhir => foto_akhir;
  // String get _video => video;

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = null;
    map['tanggal'] = tanggal;
    map['id_jenis'] = id_pekerjaan;
    map['jenis_pekerjaan'] = jenis_pekerjaan;
    map['nama_paket'] = nama_paket;
    map['id_sup'] = id_sup;
    map['sup'] = sup;
    map['id_ruas'] = id_ruas;
    map['ruas_jalan'] = ruas_jalan;
    map['lokasi'] = lokasi;
    map['lat'] = lat;
    map['long'] = long;
    map['panjang'] = panjang;
    map['jumlah_pekerja'] = jumlah_pekerja;
    //map['alat'] = alat;
    map['foto_pegawai'] = foto_pegawai;
    map['foto_awal'] = foto_awal;
    map['foto_sedang'] = foto_sedang;
    map['foto_akhir'] = foto_akhir;
    map['video'] = video;

    return map;
  }

}
