import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SqfLite{
  Future<Database> initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'draft.db';
    var todoDatabase = openDatabase(path, version: 1, onCreate: _createDb);
    return todoDatabase;
  }

  void _createDb(Database db, int version) async {
    await db.execute('''
      CREATE TABLE draft (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        tanggal TEXT,
        id_jenis INT,
        jenis_pekerjaan TEXT,
        nama_paket TEXT,
        id_sup TEXT,
        sup TEXT,
        id_ruas TEXT,
        ruas_jalan TEXT,
        lokasi TEXT,
        lat DOUBLE,
        long DOUBLE,
        panjang TEXT,
        jumlah_pekerja INTEGER,
        foto_pegawai TEXT,
        foto_awal TEXT,
        foto_sedang TEXT,
        foto_akhir TEXT,
        video TEXT
      )
    ''');
  }
}